import React, { Suspense, useEffect } from "react";
import { I18nextProvider } from "react-i18next";
import i18n from "../src/components/storiesI18n";

export const parameters = {
	actions: { argTypesRegex: "^on[A-Z].*" },
	options: {
		storySort: {
			order: [
				"Volcano booking widget",
				"Checkout widget",
				"Order result widget",
				"Intermediary signup widget",
			],
		},
	},
};

export const globalTypes = {
	locale: {
		name: "Locale",
		description: "Internationalization locale",
		toolbar: {
			icon: "globe",
			items: [
				{ value: "es", title: "Español" },
				{ value: "en", title: "English" },
				{ value: "it", title: "italiano" },
				{ value: "nl", title: "holandes" },
				{ value: "pl", title: "polaco" },
				{ value: "de", title: "Deutsch" },
				{ value: "fr", title: "Français" },
			],
			showName: true,
		},
	},
};

// Wrap your stories in the I18nextProvider component
/* Snipped for brevity */
const withI18next = (Story, context) => {
	const { locale } = context.globals;

	// When the locale global changes
	// Set the new locale in i18n
	useEffect(() => {
		i18n.changeLanguage(locale);
	}, [locale]);

	return (
		<Suspense fallback={<div>loading translations...</div>}>
			<I18nextProvider i18n={i18n}>
				<Story />
			</I18nextProvider>
		</Suspense>
	);
};

// export decorators for storybook to wrap your stories in
export const decorators = [withI18next];
