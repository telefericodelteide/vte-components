export default {
	protocol: process.env.API_PROTOCOL,
	host: process.env.API_URL,
	port: process.env.API_PORT,
	strictSSL: true,
	locale: "es",
	timeout: 100000,
	site_key: "volcano",
};
