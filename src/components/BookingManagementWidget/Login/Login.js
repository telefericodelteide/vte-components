import './login.css';
import React, { useState, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import _isEmpty from 'lodash/isEmpty';
import Button from '../Button/Button';
import { useAuth } from '../context/auth.context';
import { Field, Form, Formik } from 'formik';

const Login = () => {
	const authContext = useAuth();
	const [error, setError] = useState({ type: undefined, msg: '' });

	const { t } = useTranslation('bm');

	const initialValues = {
		bookingLocator: '',
		customerEmail: ''
	}

	const handleFormSubmit = (values, actions) => {
		if (values.bookingLocator.trim() === '') {
			setError({ type: 'locator', msg: t('manage_booking.login.submit_locator_error') })
		} else if (values.customerEmail.trim() === '') {
			setError({ type: 'email', msg: t('manage_booking.login.submit_email_empty_error') })
		} else if (
			!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.\w+$/i.test(
				values.customerEmail
			)
		) {
			setError({ type: 'email', msg: t('manage_booking.login.submit_email_format_error') })
		} else {
			setError({ type: undefined, msg: '' })
			authContext.authenticate(values.bookingLocator.trim(), values.customerEmail.trim())
		}
	};

	useEffect(() => {
		if (!_isEmpty(authContext.error)) {
			setError({ type: 'login', msg: t('manage_booking.login.login_request_error') })
		}
	}, [authContext, t]);

	return (
		<div id='login' className='login'>
			<Formik initialValues={initialValues} onSubmit={handleFormSubmit}>
				<Form className='form-wrapper uk-form-stacked'>
					<p>{t('manage_booking.login.body')}</p>
					<div className='form-field-wrapper'>
						<label htmlFor='bookingLocator' className='form-field-label uk-form-label'>{t('manage_booking.login.locator_title')}</label>
						<div className='uk-form-controls'>
							<Field
								name='bookingLocator'
								className='uk-input'
								placeholder='NKN6-48CAF'
							/>
							{(error.type === 'locator') ?
								<div className='form-field-error uk-text-danger'>
									{error.msg}
								</div> : ''
							}
						</div>
					</div>
					<div className='form-field-wrapper'>
						<label htmlFor='customerEmail' className='form-field-label uk-form-label'>{t('manage_booking.login.email_title')}</label>
						<div className='uk-form-controls'>
							<Field
								name='customerEmail'
								className='uk-input'
							/>
							{(error.type === 'email') ?
								<div className='form-field-error uk-text-danger'>
									{error.msg}
								</div> : ''
							}
						</div>
					</div>
					<Button text={t('manage_booking.login.button')}></Button>
				</Form>
			</Formik >
			<div className='uk-text-danger'>{(error.type === 'login') ? error.msg : authContext.error}</div>
		</div>
	);
};

export default Login;
