import React from 'react'
import { useTranslation } from 'react-i18next'
import _get from 'lodash/get'
import Rate from './Rate'

const BookingDetails = ({ booking }) => {

    const { t } = useTranslation('bm')

    return (
        <div className="booking-details">
            <h2 className="uk-text-large">{t('manage_booking.booking_details.title')}</h2>
            <p>
                <span className="strong">{t('manage_booking.booking_details.locator')}:</span>{' '}
            </p>
            <p>
                {_get(booking, 'locator')}
            </p>
            <p>
                <span className="strong">{t('manage_booking.booking_details.experience')}:</span>{' '}
            </p>
            <p>
                {_get(booking, 'product.experience.name')}
            </p>
            <p>
                <span className="strong">{t('manage_booking.booking_details.product')}:</span>{' '}
            </p>
            <p>
                {_get(booking, 'product.name')}
            </p>
            <p>
                <span className="strong">{t('manage_booking.booking_details.date')}:</span>{' '}
            </p>
            <p>
                {_get(booking, 'booking_date')}
            </p>
            <p>
                <span className="strong">{t('manage_booking.booking_details.rates')}</span>
            </p>
            <div className='ratesbm'>{
                _get(booking, 'product_rates', []).map((rate) => (
                    <Rate
                        key={rate.id}
                        name={rate.rate_name}
                        unitPrice={rate.pvp}
                        quantity={rate.qty}
                        total={rate.total_amount_pvp}
                        currency={_get(booking.order, 'currency')}
                    />
                ))
            }
            </div>
        </div>
    )
}

export default BookingDetails
