import React, { useEffect, useState, useCallback } from 'react'
import './booking-management.css'
import { useTranslation } from 'react-i18next'
import _get from 'lodash/get'
import _isEmpty from 'lodash/isEmpty'
import BookingDetails from './BookingDetails'
import BookingActions from './BookingActions'
import { useBooking } from '../context/booking.context'
import constants from '../constants'
import utils from '../../lib/utils'
import CancelRequestForm from './CancelRequestForm'
import DateChange from './DateChange'
import Invoices from './Invoices/Invoices'
import InvoiceRequestForm from './Invoices/InvoiceRequestForm'
import Loading from '../../Generic/Loading';
import { parseISO } from 'date-fns'

// View steps
const STEPS = {
    STEP_INITIAL: 0,
    STEP_CANCEL_REQUEST_CLICKED: 1,
    STEP_CANCEL_REQUEST_CONFIRMED: 2,
    STEP_DATE_CHANGE_REQUEST_CLICKED: 3,
    STEP_INVOICES_CLICKED: 4,
    STEP_INVOICE_REQUEST_CLICKED: 5
}
//

const allowSomeMainAction = (availableActions) => {
    if (_isEmpty(availableActions)) {
        return false
    }

    return (
        availableActions.change_date ||
        availableActions.cancel ||
        availableActions.cancel_refund
    )
}

const BookingManagement = ({ onLogout, availabilityFetcher, loading }) => {
    const bookingContext = useBooking()
    const { t } = useTranslation('bm')

    const [errorMsg, setErrorMsg] = useState('')
    const [successMsg, setSuccessMsg] = useState('')
    const [actions, setActions] = useState({})
    const [currentStep, setCurrentStep] = useState(STEPS.STEP_INITIAL)

    const handleActionClick = useCallback(
        async (action) => {
            switch (action.type) {
                case constants.BOOKING_ACTION_CANCEL_REQUEST:
                    if (action.action === "confirm") {
                        bookingContext
                            .cancellationRequest()
                            .then((result) => {
                                if (result.success) {
                                    if (_get(bookingContext.booking, 'state') === 'cancelled') {
                                        setSuccessMsg(t('manage_booking.booking_actions.cancel_request.cancelled'))
                                    } else {
                                        setSuccessMsg(t('manage_booking.booking_actions.cancel_request.refund_requested'))
                                    }

                                    setCurrentStep(STEPS.STEP_CANCEL_REQUEST_CONFIRMED)
                                }
                            })
                    } else if (action.action === "cancel") {
                        setCurrentStep(STEPS.STEP_INITIAL)
                    } else {
                        setCurrentStep(STEPS.STEP_CANCEL_REQUEST_CLICKED)
                        setSuccessMsg('')
                    }
                    break
                case constants.BOOKING_ACTION_CANCEL_REFUND:
                    bookingContext
                        .cancelRefund()
                        .then((result) => {
                            if (result.success) {
                                setSuccessMsg(t('manage_booking.booking_actions.cancel_request.refund_cancelled'))
                                setCurrentStep(STEPS.STEP_INITIAL)
                            }
                        })
                    break
                case constants.BOOKING_ACTION_CHANGE_DATE:
                    if (currentStep === STEPS.STEP_DATE_CHANGE_REQUEST_CLICKED) {
                        const date =
                            action.selection.date +
                            'T' +
                            (action.selection.session === 'day_wide'
                                ? '00:00:00'
                                : action.selection.session)

                        bookingContext
                            .dateChangeRequest(parseISO(date))
                            .then((result) => {
                                if (result.success) {
                                    setSuccessMsg(t('manage_booking.booking_actions.date_change.success'))
                                    setCurrentStep(STEPS.STEP_INITIAL)
                                } else {
                                    setSuccessMsg(t('manage_booking.booking_actions.date_change.defeat'))
                                }
                            })
                    } else {
                        setCurrentStep(STEPS.STEP_DATE_CHANGE_REQUEST_CLICKED)
                        setSuccessMsg('')
                    }
                    break
                case constants.BOOKING_ACTION_VOUCHER:
                    bookingContext
                        .getVoucherPdf()
                        .then((result) => {
                            if (result.success) {
                                utils.downloadFile(result.voucher.blob, result.voucher.fileName)
                            } else {
                                setSuccessMsg(t('manage_booking.booking_actions.voucher_download.defeat'))
                            }
                        })
                    break
                case constants.BOOKING_ACTION_INVOICES:
                    if (action.action === "download") {
                        bookingContext
                            .getInvoicePdf(action.invoiceId, action.number)
                            .then((result) => {
                                if (result.success) {
                                    utils.downloadFile(result.invoice.blob, result.invoice.fileName)
                                } else {
                                    setSuccessMsg(
                                        t('manage_booking.booking_actions.voucher_download.defeat')
                                    )
                                }
                            })
                        if (currentStep === STEPS.STEP_INVOICE_REQUEST_CLICKED) {
                            setCurrentStep(STEPS.STEP_INITIAL)
                        }
                    } else {
                        setCurrentStep(STEPS.STEP_INVOICES_CLICKED)
                        setSuccessMsg('')
                    }
                    break
                case constants.BOOKING_ACTION_INVOICE_REQUEST:
                    if (action.action === "submit") {
                        return bookingContext
                            .requestInvoice(_get(action.data, 'customer'))
                            .then((result) => {
                                if (result.success) {
                                    setSuccessMsg(t('manage_booking.booking_actions.invoices.request_form.success'))
                                    handleActionClick({
                                        type: constants.BOOKING_ACTION_INVOICES,
                                        action: 'download',
                                        invoiceId: _get(result.invoice, 'id'),
                                        number: _get(result.invoice, 'title')
                                    })
                                } else {
                                    //action.data
                                    //setCurrentStep(STEPS.STEP_INVOICE_REQUEST_CLICKED)
                                    setCurrentStep(STEPS.STEP_INITIAL)
                                }

                                return result.success
                            })
                    } else if (action.action === "cancel") {
                        setCurrentStep(STEPS.STEP_INITIAL)
                    } else {
                        setCurrentStep(STEPS.STEP_INVOICE_REQUEST_CLICKED)
                        setSuccessMsg('')
                    }
                    break
                case constants.BOOKING_ACTION_LOGOUT:
                    onLogout()
                    bookingContext.clearContext()
                    break
                default:
                    alert('onActionClick: ' + action.type)
                    break
            }
        },
        [bookingContext, currentStep, t, onLogout]
    )

    const processActions = useCallback(
        (availableActions, hasRefund, invoices) => {
            const actions = {}

            if (!_isEmpty(availableActions)) {
                if (allowSomeMainAction(availableActions) || hasRefund) {
                    actions[constants.BOOKING_ACTION_CHANGE_DATE] = {
                        type: constants.BOOKING_ACTION_CHANGE_DATE,
                        visible: (currentStep !== STEPS.STEP_DATE_CHANGE_REQUEST_CLICKED),
                        active: availableActions.change_date,
                        text: t('manage_booking.booking_actions.date_change.button'),
                    }

                    if (availableActions.cancel_refund) {
                        actions[constants.BOOKING_ACTION_CANCEL_REFUND] = {
                            type: constants.BOOKING_ACTION_CANCEL_REFUND,
                            visible: true,
                            active: true,
                            hideOnClick: true,
                            text: t('manage_booking.booking_actions.cancel_request.refund_cancel_button'),
                        }
                    } else {
                        actions[constants.BOOKING_ACTION_CANCEL_REQUEST] = {
                            type: constants.BOOKING_ACTION_CANCEL_REQUEST,
                            visible: (allowSomeMainAction(availableActions) || hasRefund) && (currentStep !== STEPS.STEP_CANCEL_REQUEST_CLICKED),
                            active: availableActions.cancel,
                            text: availableActions.cancel
                                ? t('manage_booking.booking_actions.cancel_request.button')
                                : t('manage_booking.booking_actions.cancel_request.in_process_button'),
                        }
                    }

                    actions[constants.BOOKING_ACTION_VOUCHER] = {
                        type: constants.BOOKING_ACTION_VOUCHER,
                        visible: true,
                        active: true,
                        text: t('manage_booking.booking_actions.voucher_download.button')
                    }
                }

                if (_isEmpty(invoices) && availableActions.request_invoice) {
                    actions[constants.BOOKING_ACTION_INVOICE_REQUEST] = {
                        type: constants.BOOKING_ACTION_INVOICE_REQUEST,
                        visible: (currentStep !== STEPS.STEP_INVOICE_REQUEST_CLICKED),
                        active: true,
                        text: t('manage_booking.booking_actions.invoices.request_button')
                    }
                }
            }

            if (!_isEmpty(invoices)) {
                actions[constants.BOOKING_ACTION_INVOICES] = {
                    type: constants.BOOKING_ACTION_INVOICES,
                    visible: (currentStep !== STEPS.STEP_INVOICES_CLICKED),
                    active: true,
                    text: t('manage_booking.booking_actions.invoices.button')
                }
            }

            actions[constants.BOOKING_ACTION_LOGOUT] = {
                type: constants.BOOKING_ACTION_LOGOUT,
                visible: true,
                active: true,
                text: t('manage_booking.logout.button')
            }

            return actions
        },
        [t, currentStep]
    )

    const renderView = useCallback(
        (booking) => {
            const loadedBooking = booking && booking !== undefined
            switch (currentStep) {
                case STEPS.STEP_CANCEL_REQUEST_CLICKED:
                    const cancellationRequestAction = _get(booking._links, constants.BOOKING_ACTION_CANCEL_REQUEST)
                    return (
                        <CancelRequestForm
                            confirmationConditionsMessage={_get(cancellationRequestAction, 'data.confirmation_conditions_message')}
                            onConfirm={() =>
                                handleActionClick({
                                    type: constants.BOOKING_ACTION_CANCEL_REQUEST,
                                    action: "confirm"
                                })
                            }
                            onCancelClick={
                                () => handleActionClick({
                                    type: constants.BOOKING_ACTION_CANCEL_REQUEST,
                                    action: "cancel"
                                })
                            }
                        ></CancelRequestForm>
                    )
                case STEPS.STEP_DATE_CHANGE_REQUEST_CLICKED:
                    return (
                        <DateChange
                            bookingId={_get(booking, 'id')}
                            quantity={
                                loadedBooking ? booking.getTotalQuantity() : 0
                            }
                            productId={_get(booking, 'product.id')}
                            availabilityFetcher={availabilityFetcher}
                            onConfirm={(selection) =>
                                handleActionClick({
                                    type: constants.BOOKING_ACTION_CHANGE_DATE,
                                    selection: selection,
                                })
                            }
                        ></DateChange>
                    )
                case STEPS.STEP_INVOICES_CLICKED:
                    return (
                        <Invoices invoices={_get(bookingContext.booking, 'invoices')}
                            onDownloadClick={handleActionClick}
                            downloadParams={{
                                type: constants.BOOKING_ACTION_INVOICES,
                                action: "download"
                            }}></Invoices >
                    )
                case STEPS.STEP_INVOICE_REQUEST_CLICKED:
                    return (
                        <InvoiceRequestForm
                            onSubmitForm={handleActionClick}
                            submitParams={{
                                type: constants.BOOKING_ACTION_INVOICE_REQUEST,
                                action: "submit"
                            }}
                            onCancelClick={
                                () => handleActionClick({
                                    type: constants.BOOKING_ACTION_INVOICE_REQUEST,
                                    action: "cancel"
                                })
                            }
                        ></InvoiceRequestForm>
                    )
                default:
                    return
            }
        },
        [currentStep, handleActionClick, bookingContext.booking]
    )

    const processRenderMessages =
        (successMsg, errorMsg) => {
            if (successMsg) {
                return <div className='success uk-text-success'>{successMsg}</div>
            }
            if (errorMsg) {
                return <div className='error uk-text-danger'>{errorMsg}</div>
            }
        }

    useEffect(() => {
        if (bookingContext.booking && bookingContext.availableActions) {

            const hasRefund = (_get(bookingContext.booking, 'state') === 'refund_requested')
            setActions(processActions(bookingContext.availableActions, hasRefund, _get(bookingContext.booking, 'invoices')))

            if ((currentStep !== STEPS.STEP_CANCEL_REQUEST_CONFIRMED) && !(allowSomeMainAction(bookingContext.availableActions) || hasRefund)) {
                setErrorMsg(t('manage_booking.booking_exceptional_states.' + _get(bookingContext.booking, 'product.contact_msg')))
            } else {
                setErrorMsg('')
            }
        }
    }, [bookingContext.booking, bookingContext.availableActions, currentStep, processActions])

    const isLoading = loading || bookingContext.loading

    return (
        <div id='booking-management' className='booking-management uk-container-center'>
            {isLoading && <Loading />}
            <div className='uk-grid'>
                <div className={[STEPS.STEP_INITIAL, STEPS.STEP_CANCEL_REQUEST_CONFIRMED].includes(currentStep) ? 'uk-width-1-1' : 'uk-width-1-1 uk-width-1-3@m uk-first-column'}>
                    <BookingDetails booking={bookingContext.booking}></BookingDetails>
                    {processRenderMessages(successMsg, _isEmpty(bookingContext.error) ? errorMsg : bookingContext.error)}
                    <BookingActions actions={actions} onActionClick={handleActionClick}></BookingActions>
                </div>
                {renderView(bookingContext.booking)}
            </div>
        </div>
    )
}
export default BookingManagement
