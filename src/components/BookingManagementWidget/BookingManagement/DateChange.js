import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import AvailabilityCalendar from '../../AvailabilityCalendar/AvailabilityCalendar'
import SessionList from '../../SessionList/SessionList'
import Button from '../Button/Button'

const DateChange = ({
    bookingId,
    productId,
    quantity,
    availabilityFetcher,
    onConfirm,
}) => {
    const { t } = useTranslation('bm')

    const BASE_SELECTION = {
        productId: productId,
        date: null,
        session: null,
    }

    const initSelection = (selection) => {
        let result = {
            ...BASE_SELECTION,
            ...selection,
        }

        if (result.productId) {
            result.productId = parseInt(result.productId)
        }

        return result
    }

    const [selection, setSelection] = useState(initSelection())
    const [sessionList, setSessionList] = useState([])

    const onSessionSelectionHandler = (session) => {
        setSelection({
            ...BASE_SELECTION,
            date: selection.date,
            session: session,
        })
    }

    const onDateSelectionHandler = (date) => {
        if (
            date.sessions.length === 1 &&
            date.sessions[0].session === 'day_wide'
        ) {
            setSelection({
                ...BASE_SELECTION,
                date: date.date,
                session: 'day_wide',
            })
        } else {
            setSelection({
                ...BASE_SELECTION,
                date: date.date,
            })
        }
        setSessionList(date.sessions)
    }

    const availabilityFetcherWrapper = (date) => {
        if (!date) {
            date = new Date()
        }
        return availabilityFetcher(bookingId, productId, quantity, date)
    }

    const onFormSubmit = (event) => {
        if (selection.date && selection.session) {
            onConfirm(selection)
        }

        event.preventDefault()
    }

    return (
        <div id="date-change" className="date-change uk-width-1-1 uk-width-2-3@m">
            <h2 className="uk-text-large">{t('manage_booking.booking_actions.date_change.title')}</h2>
            <p>{t('manage_booking.booking_actions.date_change.body')}</p>
            <AvailabilityCalendar
                onSelection={onDateSelectionHandler}
                availabilityFetcher={availabilityFetcherWrapper}
            ></AvailabilityCalendar>
            {sessionList && sessionList.length > 0 &&
                <div>
                    <p>{t('manage_booking.booking_actions.date_change.body_sessions')}</p>
                    <SessionList
                        sessions={sessionList}
                        selected={selection.session}
                        onSelection={onSessionSelectionHandler}
                        hiddenAvailable={true}
                    />
                </div>}
            <form onSubmit={(e) => onFormSubmit(e)}>
                {selection && selection.session && (
                    <Button
                        text={t(
                            'manage_booking.booking_actions.date_change.confirm_button'
                        )}
                    />
                )}
            </form>
        </div>
    )
}

export default DateChange
