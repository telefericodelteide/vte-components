import React from "react";
import Button from "../Button/Button";

const BookingActions = ({ actions, onActionClick }) => {
    return (
        <div className="booking-actions">
            {Object.keys(actions).map((key) => (
                actions[key].visible && (
                    <Button key={actions[key].type} disabled={!actions[key].active} hideOnClick={actions[key].hideOnClick} text={actions[key].text} onClick={() => onActionClick(actions[key])} />
                )
            ))}
        </div>
    )
}

export default BookingActions;