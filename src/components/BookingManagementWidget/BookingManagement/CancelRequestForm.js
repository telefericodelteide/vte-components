import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import Button from "../Button/Button";
import { Form, Formik, Field } from 'formik';
import _isEmpty from "lodash/isEmpty";

const CancelRequestForm = ({ confirmationConditionsMessage, onConfirm, onCancelClick }) => {
    const { t } = useTranslation("bm");
    const [error, setError] = useState("");

    const initialValues = {
        termsAccepted: false
    }

    const handleFormSubmit = (values, actions) => {
        if (values.termsAccepted) {
            onConfirm();
        }
        else {
            setError(t("manage_booking.booking_actions.cancel_request.confirm_terms_required"));
        }
    };

    return (
        <div id="cancel-request-form" className="cancel-request-form uk-width-1-1 uk-width-2-3@m">
            <h2 className="uk-text-large">{t("manage_booking.booking_actions.cancel_request.title")}</h2>
            <p>
                {confirmationConditionsMessage}
            </p>
            <Formik initialValues={initialValues} onSubmit={handleFormSubmit}>
                <Form className="form-wrapper uk-form-stacked">
                    <div className="form-field-wrapper">
                        <Field
                            type="checkbox"
                            id="termsAccepted"
                            name="termsAccepted"
                            className="uk-checkbox"
                        />
                        <label htmlFor="termsAccepted" className="form-field-condition-label">
                            {t(
                                "manage_booking.booking_actions.cancel_request.confirm_terms"
                            )}
                        </label>
                        {!_isEmpty(error) ?
                            <div className="form-field-error uk-text-danger">
                                {error}
                            </div> : ""
                        }
                    </div>
                    <div className="booking-actions">
                        <Button
                            text={t(
                                "manage_booking.booking_actions.cancel_request.confirm_button"
                            )}
                        ></Button>
                        <Button
                            text={t('manage_booking.booking_actions.cancel_request.cancel_button')}
                            onClick={() => onCancelClick()}
                        ></Button>
                    </div>
                </Form>
            </Formik>
        </div>
    );
};

export default CancelRequestForm;
