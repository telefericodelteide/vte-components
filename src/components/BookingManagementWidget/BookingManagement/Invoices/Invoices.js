import React, { useCallback } from 'react'
import { useTranslation } from 'react-i18next'
import InvoicesListItem from './InvoicesListItem'
import _get from 'lodash/get'

const Invoices = ({ invoices, onDownloadClick, downloadParams }) => {
    const { t } = useTranslation('bm')

    const handleDownloadClick = useCallback((index) => {
        onDownloadClick({
            ...downloadParams,
            invoiceId: _get(invoices[index], 'id'),
            number: _get(invoices[index], 'title')
        })
    }, [invoices, onDownloadClick, downloadParams])

    return (
        <div className='invoices uk-width-1-1 uk-width-2-3@m'>
            <h2 className='uk-text-large'>{t('manage_booking.booking_actions.invoices.title')}</h2>
            <table className='uk-table uk-table-striped'>
                <thead>
                    <tr>
                        <th>{t('manage_booking.booking_actions.invoices.table_header_number')}</th>
                        <th>{t('manage_booking.booking_actions.invoices.table_header_date')}</th>
                        <th>{t('manage_booking.booking_actions.invoices.table_header_amount')}</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {
                        invoices.map((invoice, index) => (
                            <InvoicesListItem
                                key={_get(invoice, 'id')}
                                number={_get(invoice, 'title')}
                                type={_get(invoice, 'type')}
                                date={_get(invoice, 'created')}
                                amount={_get(invoice, 'amount.total_amount')}
                                onDownloadClick={() => handleDownloadClick(index)}
                            />
                        ))
                    }
                </tbody>
            </table>
        </div>
    )
}

export default Invoices
