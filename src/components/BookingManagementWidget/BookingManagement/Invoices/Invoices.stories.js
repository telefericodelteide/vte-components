import React from "react";
import Invoices from "./Invoices";
import { invoices } from "../../stories-mockups";
import i18nResources from "../../config/i18n";
import i18n from "i18next";
import { initReactI18next, I18nextProvider } from "react-i18next";
import Backend from "i18next-http-backend";

export default {
    component: Invoices,
    title: "Booking Management Widget/Volcano booking invoices",
};

console.log(invoices)

const Template = (args) => {
    if (!i18n.isInitialized) {
        i18n.use(Backend)
            .use(initReactI18next)
            .init({
                interpolation: { escapeValue: false },
                lng: "es",
                resources: i18nResources,
                ns: ["bm"],
                defaultNS: "bm",
            });
    } else {
        i18n.addResourceBundle(
            i18n.language,
            "bm",
            i18nResources[i18n.language]["bm"]
        );
    }
    return (
        <I18nextProvider i18n={i18n}>
            <Invoices {...args} />
        </I18nextProvider>
    );
};

export const Default = Template.bind({});
Default.args = {
    invoices: invoices,
    locale: "es",
};
