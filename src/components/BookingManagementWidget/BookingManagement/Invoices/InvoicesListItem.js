import React from 'react';
import { useTranslation } from 'react-i18next';
import Button from '../../Button/Button';

const InvoicesListItem = ({ number, type, date, amount, onDownloadClick }) => {
    const { t, i18n } = useTranslation('bm');

    const priceFormatter = new Intl.NumberFormat(i18n.language, {
        style: "currency",
        currency: "eur",
    });

    return (
        <tr>
            <td>{number}</td>
            <td>{date}</td>
            <td>{priceFormatter.format(amount)}</td >
            <td>
                <Button
                    className="fa fa-download"
                    title={t('manage_booking.booking_actions.invoices.download_button')}
                    onClick={onDownloadClick}
                ></Button>
            </td>
        </tr>
    );
};

export default InvoicesListItem;
