import React from 'react';
import { useTranslation } from 'react-i18next';
import countries from '../../../assets/countries.json';
import streetTypes from '../../../assets/streetTypes.json';
import FormWidget from '../../../Generic/Form/FormWidget';
import _get from 'lodash/get';
import _set from 'lodash/set';
import isArray from 'lodash/isArray';

const InvoiceRequestForm = ({ onSubmitForm, submitParams, onCancelClick, initVals }) => {
    const { t, i18n } = useTranslation('bm')

    let initialValues = initVals ? initVals : {
        customer: {
            type: { value: 'person' },
            vat_number_type: { value: 'nif' },
            country: {
                id: { value: 'ES' }
            }
        }
    };

    let data = {};

    const handleFormSubmit = async (values) => {
        return new Promise((resolve, reject) => {
            return onSubmitForm({
                ...submitParams,
                data: values
            }).then(() => {
                resolve(values)
            })
        })
    };

    const form = {
        fields: [
            {
                title: null,
                fields: [
                    {
                        id: 'customer.type',
                        title: 'manage_booking.booking_actions.invoices.request_form.customer_type',
                        required: true,
                        type: 'select',
                        options: [
                            'person',
                            'enterprise'
                        ],
                    },
                    {
                        id: 'customer.name',
                        title: 'manage_booking.booking_actions.invoices.request_form.customer_name',
                        required: true,
                        type: 'text'
                    },
                    {
                        id: 'customer.vat_number_type',
                        title: 'manage_booking.booking_actions.invoices.request_form.vat_number_type',
                        required: true,
                        type: 'select',
                        options: [
                            'nif',
                            'passport',
                            'document_country',
                            'document_residence',
                            'document_other'
                        ],
                    },
                    {
                        id: 'customer.vat_number',
                        title: 'manage_booking.booking_actions.invoices.request_form.vat_number',
                        required: true,
                        type: 'text'
                    },
                    {
                        id: "customer.road_type.id",
                        title: 'manage_booking.booking_actions.invoices.request_form.road_type_id',
                        required: true,
                        type: "select",
                        options: streetTypes,
                    },
                    {
                        id: 'customer.address',
                        title: 'manage_booking.booking_actions.invoices.request_form.address',
                        required: true,
                        type: 'text'
                    },
                    {
                        id: 'customer.locality',
                        title: 'manage_booking.booking_actions.invoices.request_form.locality',
                        required: true,
                        type: 'text'
                    },
                    {
                        id: 'customer.postal_code',
                        title: 'manage_booking.booking_actions.invoices.request_form.postal_code',
                        required: true,
                        type: 'text'
                    },
                    {
                        id: 'customer.state',
                        title: 'manage_booking.booking_actions.invoices.request_form.state',
                        required: true,
                        type: 'text'
                    },
                    {
                        id: "customer.country.id",
                        title: 'manage_booking.booking_actions.invoices.request_form.country_id',
                        required: true,
                        type: "select",
                        options: countries,
                    }
                ],
            }
        ],
        conditions: []
    }

    form.fields.forEach((fieldset, i) =>
        fieldset.fields.forEach((field, j) => {
            const defaultValue = _get(initialValues, field.id);
            if (defaultValue) {
                form.fields[i].fields[j].type = _get(
                    defaultValue,
                    'hidden',
                    false
                )
                    ? 'hidden'
                    : form.fields[i].fields[j].type;
                _set(initialValues, field.id, defaultValue.value, '');
            } else {
                _set(initialValues, field.id, '');
            }
        })
    );

    const processForm = (form) => {
        form.fields = form.fields.map((item) => {
            item.title = t(item.title);
            item.fields = item.fields.map((field) => {
                const key = field.title;
                field.title = t(key);
                if (field.options && isArray(field.options)) {
                    var toptions = {};
                    field.options.forEach((option) => {
                        toptions[option] = t(key + '_options.' + option);
                    });
                    field.options = toptions;
                }
                return field;
            });
            return item;
        });
        return form;
    };

    if (i18n.isInitialized) {
        data = processForm(form)
    }

    return (
        <div className='invoice-request-form uk-width-1-1 uk-width-2-3@m'>
            <h2 className="uk-text-large">{t('manage_booking.booking_actions.invoices.request_form.title')}</h2>
            <div className="uk-text-small uk-margin-bottom">
                {t('manage_booking.booking_actions.invoices.request_form.body')}
            </div>
            {(
                <FormWidget
                    locale={i18n.language}
                    initialValues={initialValues}
                    form={data}
                    confirmButton={{
                        title: t('manage_booking.booking_actions.invoices.request_form.submit_button'),
                        handler: handleFormSubmit,
                    }}
                    cancelButton={{
                        title: t('manage_booking.booking_actions.cancel_request.cancel_button'),
                        handler: onCancelClick,
                    }}
                />)}
        </div>
    );
};

export default InvoiceRequestForm;
