import React, { useState } from 'react'
import DateChange from './DateChange'
import i18nResources from '../config/i18n'
import i18n from 'i18next'
import { initReactI18next, I18nextProvider } from 'react-i18next'
import Backend from 'i18next-http-backend'
import VolcanoApi from '@volcanoteide/volcanoteide-api-client'
import apiConfig from '../../../stories/configApi'
import Loading from '../../Generic/Loading'

export default {
    component: DateChange,
    title: 'Booking Management Widget/Volcano booking date change',
}

const Template = (args) => {
    const client = new VolcanoApi({
        ...apiConfig,
    })
    const [loading, setLoading] = useState(false)

    const availabilityFetcher = (productId, quantity, date) => {
        setLoading(true)
        const params = { min_qty: quantity }
        return client.experience
            .getProductAvailability(productId, date, true, params)
            .then((result) => {
                setLoading(false)
                return result
            })
    }
    if (!i18n.isInitialized) {
        i18n.use(Backend)
            .use(initReactI18next)
            .init({
                interpolation: { escapeValue: false },
                lng: 'es',
                resources: i18nResources,
                ns: ['bm'],
                defaultNS: 'bm',
            })
    } else {
        i18n.addResourceBundle(
            i18n.language,
            'bm',
            i18nResources[i18n.language]['bm']
        )
    }
    return (
        <I18nextProvider i18n={i18n}>
            {loading && <Loading />}
            <DateChange
                availabilityFetcher={availabilityFetcher}
                productId={46}
                quantity={3}
                onConfirm={(result) => console.log(result)}
                {...args}
            />
        </I18nextProvider>
    )
}

export const Default = Template.bind({})
Default.args = {}
