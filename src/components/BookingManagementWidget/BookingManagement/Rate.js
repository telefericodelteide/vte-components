import _replace from "lodash/replace";
import React from "react";
import { useTranslation } from "react-i18next";

const Rate = ({ name, unitPrice, quantity, total, currency }) => {
	const { t, i18n } = useTranslation("bm");

	const priceFormatter = new Intl.NumberFormat(i18n.language, {
		style: "currency",
		currency: currency || "eur",
	});

	const priceFormatted = (currency, price) => {
		let formattedPrice = priceFormatter.format(price);
		if (currency && currency === 'PLN') {
			formattedPrice = formattedPrice.replace('PLN', 'zł')
		}
		return formattedPrice;
	}

	return (
		<div className="ratebm">
			<ul className="uk-list">
				<li>
					<strong> {_replace(t("manage_booking.booking_details.rate"), ",", ", ")}: </strong>
					{" "}{name}
				</li>
				<li>

					<strong>{t("manage_booking.booking_details.price")}:</strong>
					{" "}
					{priceFormatted(currency, unitPrice)}
					{" - "}
					<strong>{t("manage_booking.booking_details.quantity")}:</strong>
					{" "}{quantity}

				</li>
				<li>
					<strong> {t("manage_booking.booking_details.total")}: </strong>
					{" "}{priceFormatted(currency, total)}
				</li>
			</ul>
		</div>
	);
};

export default Rate;
