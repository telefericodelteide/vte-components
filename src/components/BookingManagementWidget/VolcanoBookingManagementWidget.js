import React, { useState } from "react";
import VolcanoApi from "@volcanoteide/volcanoteide-api-client";
import BookingManagementWidget from "./BookingManagementWidget";
import i18nResources from "./config/i18n";
import i18n from "i18next";
import { initReactI18next, I18nextProvider } from "react-i18next";
import Backend from "i18next-http-backend";
import AuthProvider from "./context/auth.context";
import BookingProvider from "./context/booking.context";

const VolcanoBookingManagementWidget = ({ locale, apiConfig }) => {

	// init api client
	const client = new VolcanoApi({
		...apiConfig,
		locale: locale,
	});

	const [isLoading, setLoading] = useState(false)

	if (!i18n.isInitialized) {
		i18n.use(Backend)
			.use(initReactI18next)
			.init({
				interpolation: { escapeValue: false },
				lng: locale,
				resources: i18nResources,
				ns: ["bm"],
				defaultNS: "bm",
			});
	} else {
		if (i18n.language !== locale) {
			i18n.changeLanguage(locale);
		}
		i18n.addResourceBundle(locale, "bm", i18nResources[locale]["bm"]);
	}

	const availabilityFetcher = (bookingId, productId, quantity, date) => {
		setLoading(true)
		const params = { 
			booking_id: bookingId,
			min_qty: quantity
		}
		return client.experience
			.getProductAvailability(productId, date, true, params)
			.then((result) => {
				setLoading(false)
				return result
			}).catch(function (error) {
				setLoading(false)
				return []
			})
	}

	return (
		<I18nextProvider i18n={i18n}>
			<AuthProvider apiClient={client}>
				<BookingProvider apiClient={client} >
					<BookingManagementWidget availabilityFetcher={availabilityFetcher} loading={isLoading} />
				</BookingProvider>
			</AuthProvider>
		</I18nextProvider>
	);
};

export default VolcanoBookingManagementWidget;
