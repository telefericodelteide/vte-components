import React from 'react';
import Login from './Login/Login';
import './booking-management-widget.css';
import 'uikit/dist/css/uikit.min.css';
import { useAuth } from './context/auth.context';
import { useTranslation } from 'react-i18next';
import BookingManagement from './BookingManagement/BookingManagement';
import UIkit from 'uikit';

const BookingManagementWidget = ({ availabilityFetcher, loading }) => {
	const authContext = useAuth()
	const { t } = useTranslation('bm')

	if (typeof window !== `undefined`) {
		UIkit.container = ".uk-scope";
	}

	const handleLogout = () => {
		authContext.logout()
	}

	return (
		<div id='booking-management-widget' className={`booking-management-widget uk-scope ${authContext.isAuthenticated ? "logged" : "not-logged"}`}>
			<div className='title'>
				<i className='fa fa-ticket-alt fa-rotate-45'></i>
				<h1>{t('manage_booking.title')}</h1>
			</div>
			{authContext.isAuthenticated ? <BookingManagement onLogout={handleLogout} availabilityFetcher={availabilityFetcher} loading={loading}></BookingManagement> : <Login></Login>}
		</div>
	);
};

export default BookingManagementWidget;