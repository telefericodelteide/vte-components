import React from "react";
import apiConfig from "../../stories/configApi";
import VolcanoBookingManagementWidget from "./VolcanoBookingManagementWidget";

export default {
	component: VolcanoBookingManagementWidget,
	title: "Booking Management Widget/Volcano booking management widget",
};

const Template = (args) => <VolcanoBookingManagementWidget {...args} />;
export const Default = Template.bind({});
Default.args = {
	locale: 'es',
	apiConfig: apiConfig
};
