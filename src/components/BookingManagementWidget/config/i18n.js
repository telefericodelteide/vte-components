import es from "./locales/es.json";
import en from "./locales/en.json";
import de from "./locales/de.json";
import fr from "./locales/fr.json";
import nl from "./locales/nl.json";
import pl from "./locales/pl.json";
import it from "./locales/it.json";
import ru from "./locales/ru.json";
import calendarI18nResources from "../../AvailabilityCalendar/config/i18n";

const i18nResources = {
	es: {
		bm: es,
		calendar: calendarI18nResources.es.calendar,
	},
	en: {
		bm: en,
		calendar: calendarI18nResources.en.calendar,
	},
	it: {
		bm: it,
		calendar: calendarI18nResources.it.calendar,
	},
	nl: {
		bm: nl,
		calendar: calendarI18nResources.nl.calendar,
	},
	pl: {
		bm: pl,
		calendar: calendarI18nResources.pl.calendar,
	},
	de: {
		bm: de,
		calendar: calendarI18nResources.de.calendar,
	},
	fr: {
		bm: fr,
		calendar: calendarI18nResources.fr.calendar,
	},
	ru: {
		bm: ru,
		calendar: calendarI18nResources.ru.calendar,
	}
};

export default i18nResources;
