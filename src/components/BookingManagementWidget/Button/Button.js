import React, { useState } from "react";

const Button = ({ text, title, disabled, className, style, hideOnClick, onClick }) => {

    const type = onClick ? "" : "submit"
    const [visible, setVisible] = useState(true)

    const onButtonClick = (event) => {
        if (hideOnClick) {
            setVisible(false)
        }

        if (onClick !== undefined) {
            onClick();
            event.preventDefault();
        }
    }

    return (
        visible && (
            <button
                disabled={disabled}
                type={type}
                className={"uk-button uk-align-center volcano-button " + className}
                style={style}
                title={title}
                onClick={(e) => onButtonClick(e)}
            >
                {text}
            </button >
        )
    );
}

export default Button;