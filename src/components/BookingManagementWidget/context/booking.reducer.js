export const INITIALISE = 'INITIALISE'
export const LOAD_EXTENDED_DATA = 'LOAD_EXTENDED_DATA'
export const CANCEL_REQUEST = 'CANCEL_REQUEST'
export const DATE_CHANGE = 'DATE_CHANGE'
export const ERROR = 'ERROR'

export const reducer = (state, action) => {
    switch (action.type) {
        case INITIALISE:
        case CANCEL_REQUEST:
        case DATE_CHANGE:
            return {
                ...state,
                booking: action.booking,
                availableActions: null,
                error: null,
            }
        case LOAD_EXTENDED_DATA:
            return {
                ...state,
                booking: action.booking,
                availableActions: action.availableActions,
                error: null,
            }
        case ERROR:
            return {
                ...state,
                error: action.error,
            }
        default:
            return state
    }
}
