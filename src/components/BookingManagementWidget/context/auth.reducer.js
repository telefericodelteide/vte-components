import _isEmpty from "lodash/isEmpty"

export const INITIALISE = 'INITIALISE'
export const LOGOUT = 'LOGOUT'
export const ERROR = 'ERROR'

export const reducer = (state, action) => {
    switch (action.type) {
        case INITIALISE:
            return {
                ...state,
                isAuthenticated: !_isEmpty(action.token),
                error: null,
                token: action.token,
            }
        case LOGOUT:
            return {
                ...state,
                isAuthenticated: false,
                error: null,
                token: null,
            };
        case ERROR:
            return {
                ...state,
                isAuthenticated: false,
                error: action.error,
                token: null,
            }
        // todo: token refresh    
        default:
            return state
    }
}