import React, {
    createContext,
    useState,
    useMemo,
    useEffect,
    useReducer,
    useCallback,
    useContext,
} from 'react'
import { reducer } from './booking.reducer'
import _get from 'lodash/get'
import _set from 'lodash/set'
import { useAuth } from './auth.context'

const stub = () => {
    throw new Error(
        'You forgot to wrap your component in <BookingProvider></BookingProvider>.'
    )
}

const getBookingStateMessage = (booking) => {
    if (_get(booking, 'state') === 'cancelled') {
        return 'cancelled'
    }

    if (_get(booking, 'validation_date')) {
        return 'validated'
    }

    if (_get(booking, 'order.billing_type') === 1 && _get(booking, 'order.collaborator')) {
        return 'collaborator'
    }

    return 'default'

}

const initialContext = {
    booking: null,
    availableActions: null,
    error: null,
    loading: false,
    clearContext: stub,
    getBooking: stub,
    cancellationRequest: stub,
    cancelRefund: stub,
    dateChangeRequest: stub,
    getVoucherPdf: stub,
    getInvoicePdf: stub,
    requestInvoice: stub
}

const Context = createContext(initialContext)

const BookingProvider = ({ apiClient, children }) => {
    const authContext = useAuth()

    const [state, dispatch] = useReducer(reducer, initialContext)
    const [ready, setReady] = useState(false)
    const [loading, setLoading] = useState(false)

    const clearContext = useCallback(() => {
        dispatch({
            type: 'INITIALISE',
            booking: null
        })
    })

    const loadExtendedData = useCallback(async (booking) => {
        return Promise.all([booking.getRelatedEntities()])
            .then(([extBooking]) => {
                return Promise.all([booking.getAvailableActions()])
                    .then(([actions]) => {
                        dispatch({
                            type: 'LOAD_EXTENDED_DATA',
                            booking: extBooking,
                            availableActions: actions
                        })
                        setLoading(false)
                        return true
                    })
                    .catch(function (error) {
                        dispatch({
                            type: 'ERROR',
                            error: error.message,
                        })
                        setLoading(false)

                        return false
                    })
            })
            .catch(function (error) {
                dispatch({
                    type: 'ERROR',
                    error: error.message,
                })

                return false
            })
    }, [])

    const getBookingLocator = useCallback(() => {
        return apiClient.booking
            .getBookings({ locator: localStorage.bookingLocator })
            .then((collection) => {
                if (collection.getCount() === 1) {
                    let booking = collection.getItems()[0]
                    localStorage.setItem('bookingId', _get(booking, 'id'))

                } else {
                    setLoading(false)
                    dispatch({
                        type: 'ERROR',
                        error: 'Booking not found',
                    })
                }
            })
            .catch(function (error) {
                setLoading(false)
                dispatch({
                    type: 'ERROR',
                    error: error.message,
                })
            })
    }, [apiClient.booking])


    const getBooking = useCallback(async () => {
        setLoading(true)
        if (!localStorage.bookingId) {
            await getBookingLocator()
        }

        apiClient.booking
            .getBooking(localStorage.bookingId)
            .then((booking) => {
                booking = _set(
                    booking,
                    'product.contact_msg',
                    getBookingStateMessage(booking)
                )

                loadExtendedData(booking)
            })
            .catch((error) => {
                setLoading(false)
                dispatch({
                    type: 'ERROR',
                    error: error.message,
                })
            })
    }, [apiClient.booking, loadExtendedData])

    const cancellationRequest = useCallback(async () => {
        setLoading(true)
        return apiClient.booking
            .cancelBooking(_get(state.booking, 'id'))
            .then(async (booking) => {
                dispatch({
                    type: 'CANCEL_REQUEST',
                    booking: booking
                })

                return Promise.all([loadExtendedData(booking)])
                    .then((success) => {
                        setLoading(false)
                        return { success: success }
                    })
                    .catch(function (error) {
                        dispatch({
                            type: 'ERROR',
                            error: error.message,
                        })
                        setLoading(false)

                        return { success: false, error: error.message }
                    })
            })
            .catch(function (error) {
                setLoading(false)
                dispatch({
                    type: 'ERROR',
                    error: error.message,
                })

                return { success: false, error: error.message }
            })
    }, [apiClient.booking, state.booking, loadExtendedData])

    const dateChangeRequest = useCallback(async (date) => {
        setLoading(true)
        return apiClient.booking
            .changeBookingDate(_get(state.booking, 'id'), date)
            .then(async (booking) => {
                dispatch({
                    type: 'DATE_CHANGE',
                    booking: booking
                })

                return Promise.all([loadExtendedData(booking)])
                    .then((success) => {
                        setLoading(false)
                        return { success: success }
                    })
                    .catch(function (error) {
                        dispatch({
                            type: 'ERROR',
                            error: error.message,
                        })
                        setLoading(false)

                        return { success: false, error: error.message }
                    })
            })
            .catch((error) => {
                dispatch({
                    type: 'ERROR',
                    error: error.message,
                })
                return { success: false, error: error.message }
            })
    },
        [apiClient.booking, state.booking, loadExtendedData]
    )

    const getVoucherPdf = useCallback(async () => {
        setLoading(true)
        return apiClient.booking
            .getBookingPdf(
                _get(state.booking, 'id'),
                _get(state.booking, 'order.customer.sid'))
            .then((result) => {
                setLoading(false)
                return {
                    success: true,
                    voucher: {
                        fileName: _get(state.booking, 'locator') + ".pdf",
                        blob: result
                    }
                }
            })
            .catch((error) => {
                setLoading(false)
                dispatch({
                    type: 'ERROR',
                    error: error.message,
                })
                return { success: false, error: error.message }
            })
    },
        [apiClient.booking, state.booking]
    )

    const getInvoicePdf = useCallback(async (invoiceId, number) => {
        setLoading(true)
        return apiClient.invoice
            .getInvoicePdf(invoiceId, { order_id: _get(state.booking, 'order.id') })
            .then((result) => {
                setLoading(false)
                return {
                    success: true,
                    invoice: {
                        fileName: number + '_' + _get(state.booking, 'locator') + ".pdf",
                        blob: result
                    }
                }
            })
            .catch((error) => {
                setLoading(false)
                dispatch({
                    type: 'ERROR',
                    error: error.message,
                })
                return { success: false, error: error.message }
            })
    },
        [apiClient.invoice, state.booking]
    )

    const cancelRefund = useCallback(async () => {
        setLoading(true)

        return state.booking.getPendingRefund()
            .then((refund) => {
                return apiClient.refund
                    .cancelRefund(_get(refund, 'id'), { order_id: _get(state.booking, 'order.id') })
                    .then(() => {
                        getBooking()
                        setLoading(false)
                        return { success: true }
                    })
                    .catch(function (error) {
                        setLoading(false)
                        dispatch({
                            type: 'ERROR',
                            error: error.message,
                        })

                        return { success: false, error: error.message }
                    })
            })
            .catch(function (error) {
                setLoading(false)
                dispatch({
                    type: 'ERROR',
                    error: error.message,
                })
                return { success: false, error: error.message }
            })

    }, [apiClient.refund, state.booking])

    const requestInvoice = useCallback(async (data) => {
        setLoading(true)
        return apiClient.invoice
            .createInvoice(_get(state.booking, 'order.id'), data)
            .then((invoice) => {
                getBooking()
                setLoading(false)

                return { success: true, invoice: invoice }
            })
            .catch(function (error) {
                setLoading(false)
                dispatch({
                    type: 'ERROR',
                    error: error.message,
                })

                return { success: false, error: error.message }
            })
    }, [apiClient.booking, state.booking])

    useEffect(() => {
        if (authContext.isAuthenticated) {

            setReady(false)
            getBooking()
        }

        setReady(true)
    }, [authContext.isAuthenticated])

    const contextValue = useMemo(() => {
        return {
            ...state,
            loading: loading,
            clearContext,
            getBooking,
            cancellationRequest,
            dateChangeRequest,
            getVoucherPdf,
            cancelRefund,
            getInvoicePdf,
            requestInvoice
        }
    }, [
        state,
        loading,
        clearContext,
        getBooking,
        cancellationRequest,
        dateChangeRequest,
        getVoucherPdf,
        cancelRefund,
        getInvoicePdf,
        requestInvoice
    ])

    return (
        ready && (
            <Context.Provider value={contextValue}>{children}</Context.Provider>
        )
    )
}

export default BookingProvider
export const useBooking = () => useContext(Context)
