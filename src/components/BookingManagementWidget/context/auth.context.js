import _get from 'lodash/get';
import React, { createContext, useState, useMemo, useEffect, useReducer, useCallback, useContext } from 'react';
import { reducer } from './auth.reducer';

const stub = () => {
    throw new Error('You forgot to wrap your component in <AuthProvider></AuthProvider>.')
}

const initialContext = {
    isAuthenticated: false,
    error: null,
    token: null,
    authenticate: stub,
    logout: stub,
}

const Context = createContext(initialContext);

const AuthProvider = ({ apiClient, children }) => {
    const [state, dispatch] = useReducer(reducer, initialContext)
    const [ready, setReady] = useState(false)

    const authenticate = useCallback((bookingLocator, customerEmail) => {
        return apiClient.authenticateCustomer(bookingLocator, customerEmail)
            .then((result) => {
                localStorage.setItem("token", result.token);
                localStorage.setItem("bookingLocator", bookingLocator);

                dispatch({
                    type: 'INITIALISE',
                    token: localStorage.token
                });
            })
            .catch(function (error) {
                localStorage.removeItem("token");
                localStorage.removeItem("bookingLocator");
                localStorage.removeItem("bookingId");

                dispatch({
                    type: 'ERROR',
                    error: error.message,
                });
            });
    }, [apiClient]);

    const logout = useCallback(() => {
        localStorage.removeItem("token");
        localStorage.removeItem("bookingLocator");
        localStorage.removeItem("bookingId");

        apiClient.logout()

        dispatch({ type: 'LOGOUT' });
    }, []);

    useEffect(() => {
        if (localStorage.token) {
            apiClient.setToken(localStorage.token)

            if (_get(apiClient.getPayload(), 'exp') * 1000 < Date.now()) {
                logout()
            } else {
                dispatch({
                    type: 'INITIALISE',
                    token: localStorage.token
                })
            }
        }
        setReady(true)
    }, [apiClient]);

    const contextValue = useMemo(() => {
        return {
            ...state,
            authenticate,
            logout,
        }
    }, [
        state,
        authenticate,
        logout,
    ])

    return (
        ready && <Context.Provider value={contextValue}>{children}</Context.Provider>
    );
};

export default AuthProvider;
export const useAuth = () => useContext(Context);