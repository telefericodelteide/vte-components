import React, { Suspense } from "react";
import FormWidget from "../Generic/Form/FormWidget";
import VolcanoApi from "@volcanoteide/volcanoteide-api-client";
import _get from "lodash/get";
import _set from "lodash/set";
import isArray from "lodash/isArray";
import countries from "../assets/countries.json";
import streetTypes from "../assets/streetTypes.json";

import i18nResources from "./config/i18n";
import { initReactI18next } from "react-i18next";
import Backend from "i18next-http-backend";
import i18n from "i18next";

const DEFAULT_LOCALE = "es";

export default function VolcanoIntermediarySignupWidget({
	apiConfig,
	locale,
	conditions,
	successConfig,
	defaultValues,
}) {
	locale = locale || DEFAULT_LOCALE;

	i18n.use(Backend)
		.use(initReactI18next)
		.init({
			interpolation: { escapeValue: false },
			lng: locale,
			resources: i18nResources,
			ns: ["main"],
			defaultNS: "main",
		});

	let data = {};

	// init api client
	const client = new VolcanoApi({
		...apiConfig,
		locale: locale,
	});

	const form = {
		fields: [
			{
				title: "commercial_collaboration_type_fieldset",
				fields: [
					{
						id: "commercial.collaboration_type.id",
						title: "commercial_collaboration_type",
						required: true,
						type: "select",
						options: [
							"collaborator_partner",
							"collaborator_retail",
							"collaborator_credit",
							"affiliate",
							"api",
						],
					},
					{
						id: "commercial.agency_type.id",
						title: "commercial_agency_type",
						required: true,
						type: "select",
						options: [
							"travel_agency",
							"tourist_intermediary",
							"lodgin",
						],
					},
					{
						id: "commercial.business_type.id",
						title: "commercial_establishment_type",
						required: true,
						type: "select",
						options: [
							"establishment_travel_agency",
							"establishment_apartment",
							"establishment_hotel",
							"establishment_cart_hire",
							"establishment_tourist_office",
							"establishment_active_tourism",
							"establishment_lodgin",
						],
					},
					{
						id: "commercial.registry_number",
						title: "commercial_registry_number",
						required: true,
						type: "text",
					},
				],
			},
			{
				title: "commercial_fieldset",
				fields: [
					{
						id: "commercial.name",
						title: "commercial_name",
						required: true,
						type: "text",
					},
					{
						id: "commercial.contact_details.road_type_id",
						title: "address.street_type",
						required: true,
						type: "select",
						options: streetTypes,
					},
					{
						id: "commercial.contact_details.address",
						title: "address.address",
						required: true,
						type: "text",
					},
					{
						id: "commercial.contact_details.postal_code",
						title: "address.postal_code",
						required: true,
						type: "text",
					},
					{
						id: "commercial.contact_details.locality",
						title: "address.city",
						required: true,
						type: "text",
					},
					{
						id: "commercial.contact_details.state",
						title: "address.province",
						required: true,
						type: "text",
					},
					{
						id: "commercial.contact_details.country_id",
						title: "address.country",
						required: true,
						type: "select",
						options: countries,
					},
				],
			},
			{
				title: "commercial_contact_fieldset",
				fields: [
					{
						id: "commercial.booking_contact.name",
						title: "person.name",
						required: true,
						type: "text",
					},
					{
						id: "commercial.booking_contact.phone",
						title: "person.phone",
						required: true,
						type: "text",
					},
					{
						id: "commercial.booking_contact.email",
						title: "person.email",
						required: true,
						type: "text",
					},
				],
			},
			{
				title: "fiscal_data_fieldset",
				fields: [
					{
						id: "billing.name",
						title: "fiscal_company_name",
						required: true,
						type: "text",
					},
					{
						id: "billing.vat_number",
						title: "fiscal_vat_number",
						required: true,
						type: "text",
					},
					{
						id: "billing.contact_details.road_type_id",
						title: "address.street_type",
						required: true,
						type: "select",
						options: streetTypes,
					},
					{
						id: "billing.contact_details.address",
						title: "address.address",
						required: true,
						type: "text",
					},
					{
						id: "billing.contact_details.postal_code",
						title: "address.postal_code",
						required: true,
						type: "text",
					},
					{
						id: "billing.contact_details.locality",
						title: "address.city",
						required: true,
						type: "text",
					},
					{
						id: "billing.contact_details.state",
						title: "address.province",
						required: true,
						type: "text",
					},
					{
						id: "billing.contact_details.country_id",
						title: "address.country",
						required: true,
						type: "select",
						options: countries,
					},
					{
						id: "billing.contact_details.email",
						title: "person.email",
						required: true,
						type: "email",
					},
					{
						id: "billing.contact_details.phone",
						title: "person.phone",
						required: true,
						type: "text",
					},
				],
			},
		],
		conditions: conditions,
	};

	var initialValues = defaultValues || {};
	form.fields.forEach((fieldset, i) =>
		fieldset.fields.forEach((field, j) => {
			const defaultValue = _get(initialValues, field.id);
			if (defaultValue) {
				form.fields[i].fields[j].type = _get(
					defaultValue,
					"hidden",
					false
				)
					? "hidden"
					: form.fields[i].fields[j].type;
				_set(initialValues, field.id, defaultValue.value, "");
			} else {
				_set(initialValues, field.id, "");
			}
		})
	);

	const onConfirmHandler = (values) =>
		new Promise((resolve, reject) => {
			values.language = locale;
			client.intermediary.signup(values).then((result) => {
				resolve(values);
			});
		});

	const processForm = (form, i18n) => {
		form.fields = form.fields.map((item) => {
			item.title = i18n.t(item.title);
			item.fields = item.fields.map((field) => {
				const key = field.title;
				field.title = i18n.t(key);
				if (field.options && isArray(field.options)) {
					var toptions = {};
					field.options.forEach((option) => {
						toptions[option] = i18n.t(key + "_options." + option);
					});
					field.options = toptions;
				}
				return field;
			});
			return item;
		});
		return form;
	};

	if (i18n.isInitialized) {
		data = processForm(form, i18n);
	}

	return (
		<div
			id="intermediary-signup-widget"
			className="intermediary-signup-widget uk-scope"
		>
			<FormWidget
				locale={locale}
				initialValues={initialValues}
				form={data}
				successConfig={successConfig}
				confirmButton={{
					title: "continue",
					handler: onConfirmHandler,
				}}
			/>
		</div>
	);
}
