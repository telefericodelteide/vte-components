import React from "react";
import VolcanoIntermediarySignupWidget from "./VolcanoIntermediarySignupWidget";
import apiConfig from "../../stories/configApi";

export default {
  component: VolcanoIntermediarySignupWidget,
  title: "Intermediary signup widget",
};

const Template = (args) => <VolcanoIntermediarySignupWidget {...args} />;

const testConfirmMessage = {
  title: "¡Gracias por registrarte!",
  body:
    "\
    <p>Ya falta poco para completar tu alta. Recibirás un correo en el que te vamos a solicitar que nos remitas:</p>\
    <ul>\
      <li>Comprobante de alta como empresa de actividad turística en el Gobierno de Canarias.</li>\
      <li>Acreditación de tener el seguro en vigor (pago de la prima). Esto se te requerirá anualmente.</li>\
    </ul>\
    <p>Una vez recibidos y verificados por nuestro equipo te enviaremos, al correo que has proporcionado en el formulario de registro, el usuario y contraseña con los que podrás acceder a la extranet para empresas colaboradoras del Camino del Barranco de Masca.</p>\
    <p>¡Recuerda comprobar tu carpeta de spam!</p>\
    "
};

const testFormConditions = [
  {
    id: "terms",
    title: "Acepto los término y condiciones",
    required: true,
    type: "checkbox",
    modal: {
      title: "Términos y condiciones",
      content: "Contenido para los términos y condiciones"
    }
  },
  {
    id: "data_consent",
    title: "Expreso mi consentimiento al tratamiento de mis datos personales, por TELEFERICO DEL PICO DE TEIDE, S.A. con el fin de poder colaborar en la provisión de visitas, excursiones y actividades al Teide, a través de la plataforma que se ha creado para ello y a la que tengo acceso privado.",
    required: true,
    type: "checkbox"
  }
];

export const Default = Template.bind({});
Default.args = {
  apiConfig: apiConfig,  
  locale: "es",
  conditions: testFormConditions,
  successConfig: testConfirmMessage,
};

export const DefaultValues = Template.bind({});
DefaultValues.args = {
  ...Default.args,
  defaultValues: {
    commercial: {
      collaboration_type: {
        id: {
          value: "collaborator_partner",
          hidden: true
        }
      },
      agency_type: {
        id: {
          value: "tourist_intermediary"
        }
      },
      business_type: {
        id: {
          value: "establishment_active_tourism",
          hidden: true
        }
      },
      contact_details: {
        country_id: {
          value: "ES"
        }
      }
    },
    billing: {
      contact_details: {
        country_id: {
          value: "ES"
        }
      }
    }
  }
};

export const Localized = Template.bind({});
Localized.args = {
  ...Default.args,
  locale: "en",
};
