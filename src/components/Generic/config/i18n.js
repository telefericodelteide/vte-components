import es from "./locales/es.json";
import en from "./locales/en.json";
import it from "./locales/it.json";
import nl from "./locales/nl.json";
import pl from "./locales/pl.json";
import de from "./locales/de.json";
import fr from "./locales/fr.json";
import ru from "./locales/ru.json";

const i18nResources = {
	es: {
		main: es,
	},
	en: {
		main: en,
	},
	it: {
		main: it,
	},
	nl: {
		main: nl,
	},
	pl: {
		main: pl,
	},
	de: {
		main: de,
	},
	fr: {
		main: fr,
	},
	ru: {
		main: ru,
	},
};

export default i18nResources;
