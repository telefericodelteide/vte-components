import React from 'react';
import { isObject } from 'lodash';
import Button from '../BookingManagementWidget/Button/Button';
import "./generic.css";

function Alert(props) { 
  const createMarkup = content => { return { __html: content } };

  if (isObject(props.content)) {
    const {content: data} = props;
    const {content, button, url} = data;

    const defaultStyles = {
      wrapper: {
        backgroundColor: "#ededed"
      },
      content: {
        color: "#d84b55"
      },
      icon: {
        borderRadius: "0.35rem",
        backgroundColor: "#d84b55",
        padding: "4px 10px"
      },
      buttonText: {
        fontWeight: "bold",
        marginBottom: "20px"
      }
    };

    const onClick = () => {
      window.location.replace(url)
    }
    
    return (
      <div className={"volcano-alert volcano-alert-wrapper " + (props.className ? props.className : "")} style={defaultStyles.wrapper}>
        <Button
          text={button}
          onClick={onClick}
          style={defaultStyles.buttonText}
        />
        {props.icon && <i className={"volcano-alert-icon " + props.icon} style={defaultStyles.icon}></i>}
        <span className="volcano-alert-content" dangerouslySetInnerHTML={createMarkup(content)} style={defaultStyles.content}/>
      </div>
    )
  }

  return (
    <div className={"volcano-alert volcano-alert-wrapper " + (props.className ? props.className : "")}>
      {props.icon && <i className={"volcano-alert-icon " + props.icon}></i>}
      <span className="volcano-alert-content" dangerouslySetInnerHTML={createMarkup(props.content)} />
    </div>
  )
}

export default Alert;