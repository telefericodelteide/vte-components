import React from "react";
import { useTranslation } from "react-i18next";
import { ErrorMessage, Field } from "formik";
import { validate } from "./FormValidator";

function FormField(props) {
  const { t } = useTranslation("main");

  const getField = (props) => {
    switch (props.type) {
      case "select":
        return (
          <Field as="select" name={props.id} className="uk-select">
            {Object.entries(props.options).map(([key, value]) => (
              <option key={key} value={key} label={value} />
            ))}
          </Field>
        );
      case "email":
        return (
          <Field
            type="email"
            name={props.id}
            className="uk-input"
          //validate={validate.bind(this, props, t, props.values)}
          />
        );
      case "hidden":
        return <Field type="hidden" name={props.id} />;
      default:
        return (
          <Field
            name={props.id}
            className="uk-input"
            validate={validate.bind(this, props, t, props.values)}
          />
        );
    }
  };

  if (props.type === "hidden") {
    return getField(props);
  } else {
    return (
      <div className="form-field-wrapper uk-width-1-1 uk-width-1-2@m">
        <label htmlFor={props.id} className="form-field-label uk-form-label">
          {props.title}
        </label>
        <div className="uk-form-controls">
          {getField(props)}
          <ErrorMessage name={props.id}>
            {(msg) => (
              <div className="form-field-error uk-text-danger">{msg}</div>
            )}
          </ErrorMessage>
        </div>
      </div>
    );
  }
}

export default function FormFieldset({ title, fields, values }) {
  return (
    <div className="form-fieldset uk-grid-small" uk-grid="uk-grid">
      {title && (<div className="form-fieldset-title uk-width-1-1 uk-text-large">
        {title}
        <hr />
      </div>)}
      <div className="uk-width-1-1">
        <div className="uk-grid-small" uk-grid="uk-grid">
          {fields.map((field) => (
            <FormField key={field.id} values={values} {...field} />
          ))}
        </div>
      </div>
    </div>
  );
}
