import React, { useState } from "react";
import { Field, ErrorMessage } from "formik";
import Modal from "../Modal";
import _get from "lodash/get";
import _isObject from "lodash/isObject";
import { validate } from "./FormValidator";
import { useTranslation } from "react-i18next";
import uikit from "uikit";

export default function FormConditionField(props) {
  const { t } = useTranslation("main");
  const [checkClicked, setCheckClicked] = useState(false);  

  if (props.id && props.config) {
    const { type, required, title, info, modal } = props.config;

    const modalId = "modal-" + props.id.replace(/\./g, "-");

    const onCheckChange = event => {    
      if (modal) {
        if (!checkClicked) {
          uikit.modal("#" + modalId, {container: "#form-widget"}).show();
          setCheckClicked(true);
        }         
      }
    }

    return (
      <div className="uk-margin-small">
        <label
          htmlFor={props.id}
          className="form-field-condition-label"
          onClick={onCheckChange}     
        >
          {_isObject(modal) && (
            <Modal 
              id={modalId}
              header={modal.title}
            >
              {modal.content}
            </Modal>          
          )}
          <Field
            type="checkbox"
            id={props.id}
            name={props.id}
            className="uk-checkbox"
            validate={validate.bind(this, props.config, t, props.values)}
          />
          {props.noLabel ? null : (
            <span dangerouslySetInnerHTML={{ __html: " " + title }} />
          )}
          {required ? (
            <i className="required" aria-required={required}>
              {" "}
              *
            </i>
          ) : null}
        </label>

        <ErrorMessage name={props.id}>
          {(msg) => <div className="form-field-error uk-text-danger">{msg}</div>}
        </ErrorMessage>
      </div>
    );
  }

  return null;
}
