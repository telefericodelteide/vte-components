import React, { useState } from "react";
import UIkit from "uikit";
import "uikit/dist/css/uikit.min.css";
import { Form, Formik } from "formik";
import FormFieldset from "./FormFieldset";
import FormConditionField from "./FormConditionField";
import i18n from "i18next";
import { I18nextProvider, initReactI18next } from "react-i18next";
import i18nResources from "../config/i18n";
import Loading from "../Loading";
import Backend from 'i18next-http-backend';
import "../generic.css";

function SuccessMessage({ title, body }) {
	return (
		<div className="form-success">
			<h2>{title}</h2>
			<div dangerouslySetInnerHTML={{ __html: body }} />
		</div>
	);
}

export default function FormWidget({
	locale,
	initialValues,
	form,
	successConfig,
	confirmButton,
	cancelButton,
}) {

	initialValues = initialValues || {};

	if (!i18n.isInitialized) {
		i18n.use(Backend)
			.use(initReactI18next)
			.init({
				interpolation: { escapeValue: false },
				lng: locale,
				resources: i18nResources,
				ns: ["main"],
				defaultNS: "main",
			});
	} else {
		i18n.addResourceBundle(locale, "main", i18nResources[locale]["main"]);
	}

	if (typeof window !== `undefined`) {
		UIkit.container = ".uk-scope";
	}

	const [step, setStep] = useState(0);

	const handleFormSubmit = (values, actions) => {
		confirmButton.handler(values).then((result) => {
			actions.setSubmitting(false);
			setStep(1);
		});
	};

	return (
		<I18nextProvider i18n={i18n}>
			<div id="form-widget" className="volcano-form">
				{step === 0 && (
					<Formik
						initialValues={initialValues}
						onSubmit={handleFormSubmit}
					>
						{(formikProps) => {
							const {
								values,
								dirty,
								isValid,
								isSubmitting,
							} = formikProps;

							return (
								<Form className="form-wrapper uk-form-stacked">
									{isSubmitting && <Loading />}
									<div className="form-fieldsets-wrapper">
										{form.fields.map((fieldset, index) => (
											<FormFieldset
												key={index}
												{...fieldset}
												values={values}
											/>
										))}
									</div>
									<div className="form-conditions-wrapper uk-margin-top">
										{form.conditions.map((condition) => (
											<FormConditionField
												key={condition.id}
												id={condition.id}
												config={condition}
												values={values}
											/>
										))}
									</div>
									<div className="uk-margin">
										<button
											id="btnSubmit"
											type="submit"
											className="uk-button uk-align-center volcano-button"
											disabled={!dirty || !isValid}
										>
											{confirmButton.title}
										</button>
										{
											cancelButton &&
											(
												<button
													id="btnSubmit"
													className="uk-button uk-align-center volcano-button"
													onClick={() => cancelButton.handler()}
												>
													{cancelButton.title}
												</button>
											)
										}
									</div>
								</Form>
							);
						}}
					</Formik>
				)}
				{step === 1 && <SuccessMessage {...successConfig} />}
			</div>
		</I18nextProvider>
	);
}
