import _get from "lodash/get";
import _isEmpty from "lodash/isEmpty";

function validate(config, t, values, value) {
  const { type, required, equalTo } = config;
  
  if (required) {
    let hasError = false;
    switch (type) {
      case "checkbox":
        if (!value) {
          hasError = true;
        }
        break;
      default:
        if (!value || _isEmpty(value)) {
          hasError = true; 
        }
        break;
    }
      if(hasError) {
      return t("validation.required");
    }
  }

  if (type === "email") {
    if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.\w+$/i.test(value)) {
      return t("validation.email");
    }

    if (equalTo) {
      let valueOtherField = _get(values, equalTo);
      if (valueOtherField !== value) {
        return t("validation.equalTo");
      }
    }
  }

  if (type === "phone") {
    if (value === "invalid_phone_number") {
      return t("validation.phone");
    }
  }

  return null;
}

export { validate };
