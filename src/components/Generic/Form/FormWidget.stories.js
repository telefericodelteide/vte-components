import React from "react";
import FormWidget from "./FormWidget";
import _set from "lodash/set";

export default {
  component: FormWidget,
  title: "Form widget",
};

const Template = (args) => <FormWidget {...args} />;

const testForm = {
  fields: [
    {
      title: "Modalidad de colaboración",
      fields: [
        {
          id: "commercial.colaboration_type.id",
          title: "Tipo de colaboración",
          required: true,
          type: "select",
          options: {
            collaborator_wholesaler: "Colaborador",
            collaborator_retail: "Retail",
            collaborator_credit: "Crédito",
            affiliate: "Afiliado",
            api: "API",
          },
        },
        {
          id: "commercial.agency_type.id",
          title: "Tipo de agencia",
          required: true,
          type: "select",
          options: {
            travel_agency: "Agencia de viajes",
            tourist_intermediary: "Intermediador turístico",
          },
        },
        {
          id: "commercial.business_type.id",
          title: "Tipo de establecimiento",
          required: true,
          type: "select",
          options: {
            travel_agency: "Agencia de viajes",
            apartment: "Apartamentos",
            hotel: "Hotel",
            rent_a_car: "Alquiler de vehículos",
            tour_office: "Oficina de turismo",
          },
        },
        {
          id: "commercial.registration_number",
          title: "Nº de registro",
          required: true,
          type: "text",
        },
      ],
    },
    {
      title: "Datsos comerciales",
      fields: [
        {
          id: "commercial.name",
          title: "Nombre comercial",
          required: true,
          type: "text",
        },
        {
          id: "commercial.contact_details.road_type_id",
          title: "Tipo de vía",
          required: true,
          type: "select",
          options: [],
        },
        {
          id: "commercial.contact_details.address",
          title: "Dirección",
          required: true,
          type: "text",
        },
        {
          id: "commercial.contact_details.postal_code",
          title: "Código postal",
          required: true,
          type: "text",
        },
        {
          id: "commercial.contact_details.locality",
          title: "Localidad",
          required: true,
          type: "text",
        },
        {
          id: "commercial.contact_details.state",
          title: "Provincia",
          required: true,
          type: "text",
        },
        {
          id: "commercial.contact_details.country_id",
          title: "País",
          required: true,
          type: "select",
          options: [],
        },
      ],
    },
    {
      title: "Persona de contacto - Excursiones",
      fields: [
        {
          id: "commercial.booking_contact.name",
          title: "Nombre",
          required: true,
          type: "text",
        },
        {
          id: "commercial.booking_contact.phone",
          title: "Teléfono",
          required: true,
          type: "text",
        },
        {
          id: "commercial.booking_contact.email",
          title: "Correo electrónico",
          required: true,
          type: "text",
        },    
      ]
    },
    {
      title: "Datos fiscales",
      fields: [
        {
          id: "billing.name",
          title: "Razón social",
          required: true,
          type: "text",
        },
        {
          id: "billing.vat_number",
          title: "CIF",
          required: true,
          type: "text",
        },
        {
          id: "billing.contact_details.road_type_id",
          title: "Tipo de vía",
          required: true,
          type: "select",
          options: [],
        },
        {
          id: "billing.contact_details.address",
          title: "Dirección",
          required: true,
          type: "text",
        },
        {
          id: "billing.contact_details.postal_code",
          title: "Código postal",
          required: true,
          type: "text",
        },
        {
          id: "billing.contact_details.locality",
          title: "Localidad",
          required: true,
          type: "text",
        },
        {
          id: "billing.contact_details.state",
          title: "Provincia",
          required: true,
          type: "text",
        },
        {
          id: "billing.contact_details.country_id",
          title: "País",
          required: true,
          type: "select",
          options: [],
        },
        {
          id: "billing.contact_details.email",
          title: "Correo electrónico",
          required: true,
          type: "email",
        },
        {
          id: "billing.contact_details.phone",
          title: "Teléfono",
          required: true,
          type: "text",
        },
      ],
    },
  ],
  conditions: [
    {
      id: "terms",
      title: "Acepto los término y condiciones",
      required: true,
      type: "checkbox",
      modal: {
        title: "Términos y condiciones",
        content: "Contenido para los términos y condiciones"
      }
    },
    {
      id: "data_consent",
      title: "Expreso mi consentimiento al tratamiento de mis datos personales, por TELEFERICO DEL PICO DE TEIDE, S.A. con el fin de poder colaborar en la provisión de visitas, excursiones y actividades al Teide, a través de la plataforma que se ha creado para ello y a la que tengo acceso privado.",
      required: true,
      type: "checkbox"
    }
  ],
};

const testConfirmMessage = {
  title: "Solicitud de alta realizada",
  body:
    'Se ha efectuado la solicitud de alta. Para completar el proceso es necesario que envía al correo electrónico "reservas@caminobarrancodemasca.com" la siguiente información:',
};

var testInitialValues = {};
testForm.fields.forEach((fieldset) =>
  fieldset.fields.forEach((field) => _set(testInitialValues, field.id, ""))
);

export const Default = Template.bind({});
Default.args = {
  locale: "es",
  initialValues: testInitialValues,
  form: testForm,
  successConfig: testConfirmMessage,
  confirmButton: {
    title: "Aceptar",
    handler: (values) =>
      new Promise((resolve, reject) => {
        console.log(values);
        resolve(values);
      }),
  },
};

export const Localized = Template.bind({});
Localized.args = {
  ...Default.args,
  locale: "en",
};
