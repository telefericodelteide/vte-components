import React from 'react';
import "./generic.css";

function Loading(props) { 
  return (
    <div className="volcano-loading-wrapper">
      <div className="lds-ripple"><div></div><div></div></div>
    </div>
  )
}

export default Loading;