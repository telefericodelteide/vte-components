import React from 'react';
import "./flag-list.css";

function FlagList({flags, variant = false}) { 

  flags = flags || [];
  
  return (
    <div className="flag-list">
      {flags.map((flag,index) => variant ? 
        <span key={index} className="flag-iso">{flag}</span> : 
        <i key={index} className={"flag flag-" + flag} />
      )}
    </div> 
  )
}

export default FlagList;