import React, { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import UIkit from "uikit";
import "./generic.css";

function Modal(props) {
	const [checked, setChecked] = useState(false);
	const className = props.className ? props.className : "";

	const { t } = useTranslation("main");

	const onAcceptHandler = () => {
		UIkit.modal("#" + props.id).hide();
		if (props.onConfirm) {
			setTimeout(() => props.onConfirm(), 500);
		}
	};

	useEffect(() => {
		if (!props.check) {
			setChecked(true);
		}
	}, []);

	return (
		<div
			id={props.id}
			className={"volcano-modal " + className}
			uk-modal="esc-close:false; bg-close:false"
		>
			<div className="uk-modal-dialog">
				<div className="uk-modal-header">
					<h2 className="uk-modal-title">{props.header}</h2>
				</div>
				<div
					className="uk-modal-body"
					uk-overflow-auto="uk-overflow-auto"
				>
					{props.children}
					{props.check && (
						<label
							htmlFor="volcano-modal-check"
							className="volcano-modal-check"
						>
							<input
								id="volcano-modal-check"
								className="uk-checkbox"
								type="checkbox"
								onChange={() => {
									setChecked(!checked);
								}}
							/>
							{props.check[0]}
						</label>
					)}
				</div>
				<div className="uk-modal-footer uk-text-right">
					<button
						className="uk-button volcano-button"
						disabled={!checked}
						type="button"
						onClick={onAcceptHandler}
					>
						{t("accept")}
					</button>
				</div>
			</div>
		</div>
	);
}

export default Modal;
