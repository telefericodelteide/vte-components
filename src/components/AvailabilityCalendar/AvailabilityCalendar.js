import React, { useState, useEffect } from "react";
import DatePicker, { registerLocale } from "react-datepicker";

import { useTranslation } from "react-i18next";
import de from "date-fns/locale/de";
import en from "date-fns/locale/en-GB";
import fr from "date-fns/locale/fr";
import es from "date-fns/locale/es";
import it from "date-fns/locale/it";
import nl from "date-fns/locale/nl";
import pl from "date-fns/locale/pl";
import ru from "date-fns/locale/ru";

import {
	parseISO,
	startOfToday,
	startOfMonth,
	isEqual,
	format,
} from "date-fns";


import "react-datepicker/dist/react-datepicker.css";
import "./availability-calendar.css";
import {isFunction} from "formik";

registerLocale("de", de);
registerLocale("en", en);
registerLocale("fr", fr);
registerLocale("es", es);
registerLocale("it", it);
registerLocale("nl", nl);
registerLocale("pl", pl);
registerLocale("ru", ru);

const fetchData = async (setter, callback, args) => {
	try {
		const result = await callback(...args);
		setter(result);
	} catch (error) { }
};

export default function AvailabilityCalendar({
	availabilityFetcher,
	onSelection,
	availabilityCallBack
}) {
	const [availability, setAvailability] = useState([]);
	const [availableDays, setAvailableDays] = useState([]);

	const { t, i18n } = useTranslation("calendar");

	useEffect(() => {
		fetchData(setAvailability, availabilityFetcher, []);
	}, []);

	useEffect(() => {
		setAvailableDays(getAvailableDays(availability));
		if (isFunction(availabilityCallBack)
			&& availability.length > 0
		) {
			availabilityCallBack(getAvailableDays(availability));
		}
	}, [availability]);

	const onMonthChangeHandler = (date) => {
		fetchData(setAvailability, availabilityFetcher, [startOfMonth(date)]);
	};

	const onDateSelectionHandler = (date) => {
		const dateStr = format(date, "yyyy-MM-dd");
		onSelection(
			availability.find(
				(dayAvailability) => dayAvailability.date === dateStr
			)
		);
	};

	// process available dates
	const getAvailableDays = (availability) => {
		return availability
			.filter((day) => {
				return (
					day.sessions.length > 0 &&
					day.sessions.some((session) => {
						return session.available > 0 || session.available == -1;
					})
				);
			})
			.map((day) => {
				return parseISO(day.date);
			});
	};

	const isDayAvailable = (date) => {
		const today = startOfToday();
		if (date < today) {
			return false;
		}

		// check if date is available
		return availableDays.some((day) => {
			return isEqual(day, date);
		});
	};

	// const renderDayContents = (day, date) => {
	// 	// TODO: buscar el day availability, si hay coincidencia mostrar comprobar si hay sessiones (sessions.session) en el caso de "day_wide" se carga available si es <> -1
	// 	return (
	// 		<span>
	// 			{date.getDate()}
	// 		</span>)
	// };


	return (
		<div className="availability-calendar" uk-grid="uk-grid">
			<div className="uk-width-auto@m uk-width-1-1@s">
				<DatePicker
					inline
					disabledKeyboardNavigation
					locale={i18n.language}
					// renderDayContents={renderDayContents}
					//selected={selected}
					filterDate={isDayAvailable}
					dayClassName={(date) =>
						isDayAvailable(date) ? "available" : undefined
					}
					minDate={new Date()}
					onMonthChange={onMonthChangeHandler}
					onChange={onDateSelectionHandler}
				/>
			</div>
			<div className="calendar-legend uk-width-expand@m uk-width-1-1@s">
				<ul>
					<li>
						<span className="legend-color available-day"></span>
						{t("available")}
					</li>
					<li>
						<span className="legend-color selected-day"></span>
						{t("selectedDay")}
					</li>
					<li>
						<span className="legend-color not-available-day"></span>
						{t("notAvailable")}
					</li>
					<li>
						<span className="legend-color special-rate-day"></span>
						{t("specialRate")}
					</li>
					<li>
						<span className="legend-color available-places-day">
							<i
								className="step-icon fa fa-user"
								aria-hidden="true"
							></i>
						</span>
						{t("availablePlaces")}
					</li>
				</ul>
			</div>
		</div>
	);
}
