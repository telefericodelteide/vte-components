import es from "./locales/es.json";
import en from "./locales/en.json";
import it from "./locales/it.json";
import nl from "./locales/nl.json";
import pl from "./locales/pl.json";
import de from "./locales/de.json";
import fr from "./locales/fr.json";
import ru from "./locales/ru.json";

const i18nResources = {
	es: {
		calendar: es,
	},
	en: {
		calendar: en,
	},
	it: {
		calendar: it,
	},
	nl: {
		calendar: nl,
	},
	pl: {
		calendar: pl,
	},
	de: {
		calendar: de,
	},
	fr: {
		calendar: fr,
	},
	ru: {
		calendar: ru,
	},
};

export default i18nResources;
