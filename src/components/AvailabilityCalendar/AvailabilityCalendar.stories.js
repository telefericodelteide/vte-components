import React from "react";
import AvailabilityCalendar from "./AvailabilityCalendar";
import { availabilityFetcher } from "./stories-mockups";
import i18nResources from "./config/i18n";
import i18n from "i18next";
import { initReactI18next, I18nextProvider } from "react-i18next";
import Backend from "i18next-http-backend";

export default {
	component: AvailabilityCalendar,
	title: "Booking widget/Availability calendar",
};

const Template = (args) => {
	if (!i18n.isInitialized) {
		i18n.use(Backend)
			.use(initReactI18next)
			.init({
				interpolation: { escapeValue: false },
				lng: "es",
				resources: i18nResources,
				ns: ["calendar"],
				defaultNS: "calendar",
			});
	} else {
		i18n.addResourceBundle(
			i18n.language,
			"calendar",
			i18nResources[i18n.language]["calendar"]
		);
	}
	return (
		<I18nextProvider i18n={i18n}>
			<AvailabilityCalendar {...args} />
		</I18nextProvider>
	);
};

export const Default = Template.bind({});

Default.args = {
	onSelection: (date) => {
		console.log(date);
	},
	availabilityFetcher: () => availabilityFetcher(),
};
