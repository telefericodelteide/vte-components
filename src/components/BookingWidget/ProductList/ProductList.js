import React, { Fragment, useState } from "react";
import "./product-list.css";
import "@fortawesome/fontawesome-free/css/all.min.css";
import FlagList from "../../Generic/FlagList/FlagList";
import Calendar from "../../assets/calendar-icon.svg";
import { useTranslation } from "react-i18next";
import _isEmpty from "lodash"

function WeekDays({ weekdays, locale }) {
	const { t } = useTranslation();

	const processWeekDays = (processDays) => {
		const availableWeekDays = {
			1: "monday",
			2: "tuesday",
			4: "wednesday",
			8: "thursday",
			16: "friday",
			32: "saturday",
			64: "sunday",
		};

		return Object.keys(availableWeekDays)
			.filter((key) => processDays & key)
			.map((key) => availableWeekDays[key]);
	};

	const days = processWeekDays(weekdays);

	let textReturn;

	if (days.length === 0) {
		textReturn = <div className="day">{t("product_list.noDays")}</div>;
	} else if (days.length === 7) {
		textReturn = <div className="day">{t("product_list.allDays")}</div>;
	} else {
		textReturn = days.map((day, index) => (
			<div key={index} className="day">
				{t("product_list.weekdays." + day)}
			</div>
		));
	}

	return (
		<div className="weekdays">
			<img className="uk-visible@m" src={Calendar} alt="calendar" />
			<div className="days-container" uk-flex="true">
				{textReturn}
			</div>
		</div>
	);
}

function ProductContent({ htmlString }) {
	const updatedHtmlString = htmlString.replace(/<[a-zA-Z]+(\s+[a-zA-Z]+\s*=\s*("([^"]*)"|'([^']*)'))*\s*\/>/g, '');
	return (
		<div
			dangerouslySetInnerHTML={{
				__html:
					updatedHtmlString,
			}}
		/>
	);
}

function Product({ product, locale, selected, onSelection }) {
	const [viewMore, setViewMore] = useState(false);

	const { t } = useTranslation();

	const id = "product-" + product.id;
	const numberFormat = new Intl.NumberFormat(locale, {
		style: "currency",
		currency: product.lowest_rate.currency,
	});

	return (
		<div className="product">
			<label>
				<input
					type="radio"
					className="product-radio"
					name="product"
					value={product.id}
					defaultChecked={selected}
					onClick={() => {
						onSelection(product.id);
					}}
				/>
				<span className="product-name">{product.name}</span>
			</label>

			<div className="more-info">
				<span className="product-price">
					{numberFormat.format(product.lowest_rate.value)}
				</span>
				<a
					className="more-info-link"
					uk-toggle={
						"animation: uk-animation-fade; target: #more-info-" + id
					}
					href={"#more-info-" + id}
					onClick={() => setViewMore(!viewMore)}
				>
					<i className="fa fa-plus-circle"></i>
					<span>
						{viewMore
							? t("product_list.viewLess")
							: t("product_list.viewMore")}
					</span>
				</a>
			</div>
			<div className="product-flags">
				<FlagList flags={product.languages} variant={true}/>
			</div>
			<div id={"more-info-" + id} className="more-info-container" hidden>
				<div className="more-info-body">
					{Object.keys(product.description.summary_block).map(
						(key) => (
							<Fragment key={key}>
								<h4>
									{
										product.description.summary_block[key]
											.title
									}
								</h4>
								<ProductContent htmlString={product.description.summary_block[key].content} />
							</Fragment>
						)
					)}
				</div>
			</div>
			<div className="product-available-weekdays">
				<WeekDays
					weekdays={parseInt(product.week_days, 2)}
					locale={locale}
				/>
			</div>
		</div>
	);
}

function ProductListFilter ({ productList, onClick }) {

	const [languages, setLanguages] = useState(initializeLanguages(productList));
	const [disabledLanguages] = useState(initializeDisabledLanguages(languages, productList));
	const [pickups] = useState(initializePickups(productList));
	const [selectedLanguages, setSelectedLanguages] = useState([]);
	const [selectedPickups, setSelectedPickups] = useState([]);
	const {t} = useTranslation('main');

	if (!_isEmpty(languages) && disabledLanguages.length === languages.length) {
		setLanguages([]);
	}

	function initializeLanguages( productList ) {
		let totalLanguages = [];
	
		if (productList.length > 2) {
			productList.map((product) => {
				product.languages.forEach((language) => {
					if (!totalLanguages.includes(language)) {
						totalLanguages.push(language);
					}
				});
			});
			if (totalLanguages.length <= 2) {
				totalLanguages = [];
			}
		}
	
		return totalLanguages;
	}

	function initializeDisabledLanguages( languages, productList ) {
		let totalLanguages = [];

		languages.map((language) => {
			totalLanguages[language] = 0;
		});

		if (productList.length > 2) {
			productList.map((product) => {
				product.languages.forEach((language) => {
					totalLanguages[language] = totalLanguages[language] + 1;
				});
			});
		}

		let disabledLanguages = [];

		languages.map((language) => {
			if (totalLanguages[language] === productList.length) {
				disabledLanguages.push(language);
			}
		});

		return disabledLanguages;
	}

	function initializePickups( productList ) {
		let totalPickups = [];
		
		if (productList.length > 2) {
			productList.map((product) => {
				product.tags.forEach((tag) => {
					if ((tag.group && tag.group.key === 'pickups') && !totalPickups.includes(tag.name)) {
						totalPickups.push(tag.name);
					}
				})
			});
			if (totalPickups.length < 2) {
				totalPickups = [];
			}
		}

		return totalPickups;
	}

	const onFilterSelected = (e, type, filter) => {
		let sl = selectedLanguages;
		let sp = selectedPickups;
		if (type === 'language') {
			if (selectedLanguages.includes(filter)) {
				sl = selectedLanguages.filter((language) => language !== filter);
			} else {
				sl.push(filter);
			}
			setSelectedLanguages(sl);
		} else if (type === 'pickup') {
			if (selectedPickups.includes(filter)) {
				sp = selectedPickups.filter((pickup) => pickup !== filter);
			} else {
				sp.push(filter);
			}
			setSelectedPickups(sp);
		}
		onClick({
			"languages": sl,
			"pickups": sp
		});
	}
	
	function getFilter (filterElements, selectedElements, type, disabledElements) {
		return filterElements.map((filter) => (
			<button 
				key={filter}
				className={"uk-button " + (selectedElements.includes(filter)? "filter-item-selected" : "") + (disabledElements.includes(filter)? "filter-item-disabled" : "")}
				onClick={(e) => onFilterSelected(e, type, filter)}
				disabled={disabledElements.includes(filter)}
				>
					{filter.toUpperCase()}
			</button>
		))
	}
	
	return (
		<div className="result-filter">
			<div className="filter-products">
				{languages.length>0 && (
					<>
						<div><span className="filter-products-title">{t('product_list.products_filter.languages_filter')}</span></div>
						{getFilter(languages, selectedLanguages, 'language', disabledLanguages)}
					</>
				)}
			</div>
			<div className="filter-products">
				{pickups.length>0 && (
					<>
						<div><span className="filter-products-title">{t('product_list.products_filter.pickups_filter')}</span></div>
						{getFilter(pickups, selectedPickups, 'pickup', [])}
					</>
				)}
			</div>
		</div>
	);

}


export default function ProductList({
	productList,
	locale,
	selected,
	onSelection
}) {

	const [visibleProductList, setVisibleProductList] = useState(productList);
	const {t} = useTranslation()

	const onClickFilterHandler = (filter) => {
		let prods = [];
		let sl = filter['languages'];
		let sp = filter['pickups'];
		if (sl.length >0) {
			prods = productList.filter((product) => product.languages.some((lang) => sl.includes(lang)));
		} else {
			prods = productList;
		}
		if (sp.length >0) {
			prods = prods.filter((product) => (product.tags.some((tag) => (sp.includes(tag.name)))));
		}
		setVisibleProductList(prods);
	}

	return (
		<div className="result-container">
			{productList.length > 2 && (
				<ProductListFilter
					productList={productList}
					onClick={onClickFilterHandler}
				/>
			)}
			<div className="products-container">
				{visibleProductList.length === 0 && (
					<div id="filter-no-results"><span className="filter-no-results">{t("product_list.products_filter.no_results")}</span></div>
				)}
				{visibleProductList.map((product) => (
					<Product
						key={product.id}
						product={product}
						locale={locale}
						selected={product.id === selected}
						onSelection={onSelection}
					/>
				))}
			</div>
		</div>
	);
}
