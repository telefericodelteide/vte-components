import React from "react";
import VolcanoBookingWidget from "./VolcanoBookingWidget";
import apiConfig from "../../stories/configApi";

export default {
	component: VolcanoBookingWidget,
	title: "Volcano booking widget",
};

const Template = (args) => <VolcanoBookingWidget {...args} />;
export const Default = Template.bind({});
Default.args = {
	apiConfig: apiConfig,
	locale: "es",
	experienceId: 735,
	onConfirm: () => console.log('confirm'), // navigate next page
};

export const Localized = Template.bind({});
Localized.args = {
	...Default.args,
	locale: "en",
	apiConfig: {
		...Default.args.apiConfig,
		locale: "en",
	},
	checkIsActive: false,
};

export const SelectedProduct = Template.bind({});
SelectedProduct.args = {
	...Default.args,
	defaultSelection: {
		productId: "1927",
	},
};

export const AvailabilityBlocked = Template.bind({});
AvailabilityBlocked.args = {
	...Default.args,
	checkIsActive: true,
};

export const trackingCallbacks = Template.bind({});
trackingCallbacks.args = {
	...Default.args,
	checkIsActive: true,
	trackingCallbacks: {
		onBeginCheckoutStep1DLCallback: () => console.log('Contenedor info step1'), //step 1
		onBeginCheckoutStep2DLCallback: (data) => console.log('Contenedor info step2', data), //step 2
		onBeginCheckoutStep3DLCallback: (data) => console.log('Contenedor info step3', data), //step 3
		onBeginCheckoutStep4DLCallback: (data) => console.log('Contenedor info step4', data), //step 4
	},
	onConfirm: () => console.log('confirm'), // navigate next page
};
