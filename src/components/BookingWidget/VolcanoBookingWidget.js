import React, { useState, useEffect } from "react";
import VolcanoApi from "@volcanoteide/volcanoteide-api-client";
import BookingWidget from "./BookingWidget";
import { parseISO } from "date-fns";
import i18nResources from "./config/i18n";
import i18n from "i18next";
import { initReactI18next, I18nextProvider } from "react-i18next";
import Backend from "i18next-http-backend";

const BASE_SELECTION = {
	productId: null,
	date: null,
	session: null,
	rates: [],
};

export default function VolcanoBookingWidget({
	locale,
	apiConfig,
	experienceId,
	defaultSelection,
	checkIsActive,
	onConfirm,
	trackingCallbacks,
}) {
	// init api client
	const client = new VolcanoApi({
		...apiConfig,
		locale: locale,
	});

	if (!i18n.isInitialized) {
		i18n.use(Backend)
			.use(initReactI18next)
			.init({
				interpolation: { escapeValue: false },
				lng: locale,
				resources: i18nResources,
				ns: ["main"],
				defaultNS: "main",
			});
	} else {
		if (i18n.language !== locale) {
			i18n.changeLanguage(locale);
		}
		i18n.addResourceBundle(locale, "main", i18nResources[locale]["main"]);
	}

	const [experience, setExperience] = useState(null);
	const [isLoading, setIsLoading] = useState(true);

	useEffect(() => {
		const fetchData = async () => {
			setIsLoading(true);
			const experience = await client.experience.getExperience(
				experienceId
			);
			setIsLoading(false);
			if (experience) {
				// process products max pax
				experience.products = experience.products.map((product) => {
					if (product.max_pax == 0) {
						product.max_pax = Number.MAX_SAFE_INTEGER;
					}

					return product;
				});
				setExperience(experience);
			}
		};

		fetchData();
	}, []);

	const productsFetcher = () => {
		if (experience) {
			return experience.products;
		}

		return [];
	};

	const productFetcher = (productId, date) => {
		return client.content.product.getProduct(productId, { date: date })
	}

	const availabilityFetcher = (productId, date) => {
		setIsLoading(true);
		return client.experience
			.getProductAvailability(productId, date, true)
			.then((result) => {
				setIsLoading(false);
				return result;
			});
	};

	const availabilityCheckIsActive = (productId, date) => {
		setIsLoading(true);
		return client.experience
			.getProductAvailabilityRange(productId, date, 3)
			.then((result) => {
				setIsLoading(false);
				return result.some((availabilityDay) =>
					availabilityDay.sessions.some(
						(session) => session.available > 0
					)
				);
			});
	};

	const ratesFetcher = (productId, date) => {
		setIsLoading(true);
		return client.experience
			.getProductRates(productId, parseISO(date))
			.then((result) => {
				setIsLoading(false);
				return result;
			});
	};

	const onConfirmHandler = (selection) => {
		setIsLoading(true);
		// add booking to cart and reset
		const booking = {
			type: "booking_date",
			product_id: selection.productId,
			booking_date:
				selection.date +
				" " +
				(selection.session === "day_wide"
					? "00:00:00"
					: selection.session),
			rates: selection.rates
				.filter((rate) => rate.qty > 0)
				.map((rate) => {
					return {
						rate_id: rate.id,
						qty: rate.qty,
					};
				}),
		};
		client.cart.resetLocalCart();
		client.cart.addBooking(booking).then((result) => {
			setIsLoading(false);
			onConfirm(selection);
		});
	};

	return (
		experience ? (
			<I18nextProvider i18n={i18n}>
				<BookingWidget
					experience={experience}
					locale={locale}
					productsFetcher={productsFetcher}
					productFetcher={productFetcher}
					availabilityFetcher={availabilityFetcher}
					ratesFetcher={ratesFetcher}
					defaultSelection={defaultSelection}
					availabilityCheckIsActive={
						checkIsActive && availabilityCheckIsActive
					}
					onConfirm={onConfirmHandler}
					loading={isLoading}
					trackingCallbacks={trackingCallbacks}
				/>
			</I18nextProvider>
		) : null
	);
}
