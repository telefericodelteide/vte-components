import React, { useState, useEffect, useDebugValue, Fragment, useCallback } from "react";
import UIkit from "uikit";
import "uikit/dist/css/uikit.min.css";

import de from "date-fns/locale/de";
import en from "date-fns/locale/en-GB";
import fr from "date-fns/locale/fr";
import es from "date-fns/locale/es";
import { parseISO, format } from "date-fns";

import StepNav from "./StepNav";
import ProductList from "./ProductList/ProductList";
import AvailabilityCalendar from "../AvailabilityCalendar/AvailabilityCalendar";
import SessionList from "../SessionList/SessionList";
import GroupedRateList from "./RateList/GroupedRateList";
import Loading from "../Generic/Loading";
import _isEmpty from "lodash/isEmpty";
import _get from "lodash/get";
import "./booking-widget.css";
import Modal from "../Generic/Modal";
import { isFunction } from "lodash-es";
import { useTranslation } from "react-i18next";

const DEFAULT_LOCALE = "es";

const BASE_SELECTION = {
	productId: null,
	date: null,
	session: null,
	rates: [],
	active: true,
};

/** Available selection steps */
const STEP_PRODUCT = 1;
const STEP_DATE = 2;
const STEP_SESSION = 3;
const STEP_RATES = 4;

const DEFAULT_STEPS = [STEP_PRODUCT, STEP_DATE, STEP_SESSION, STEP_RATES];

const locales = {
	de: de,
	en: en,
	fr: fr,
	es: es,
};

const MASCA_PRODUCTS = [1925, 1927, 1946, 2099, 2123, 1957, 1958, 1959, 2101]
const MASCA_MESSAGE = {
	es: "Las personas residentes en Tenerife acceden gratuitamente. Por ello, en el control de acceso, al verificar la residencia, se tramitará la solicitud de devolución del importe abonado.",
	en: "Residents of Tenerife have free access. Therefore, at the access control point, upon verifying residency, the request for a refund of the amount paid will be processed.",
	de: "Einwohner von Teneriffa haben freien Zugang. Daher wird am Zugangskontrollpunkt, nach Überprüfung des Wohnsitzes, der Antrag auf Rückerstattung des gezahlten Betrags bearbeitet."
}

const fetchData = async (setter, callback, args) => {
	try {
		const result = await callback(...args);
		setter(result);
	} catch (error) { }
};

const getRatesQty = (rates) => {
	let qty = 0;

	rates.forEach((rate) => {
		qty += rate.qty;
	});

	return qty;
};

function BookingTotal({ rates, selectedRates, locale, currency }) {
	const { t } = useTranslation();

	const priceFormatter = new Intl.NumberFormat(locale, {
		style: "currency",
		currency: currency || "eur",
	});

	const priceFormatted = (currency, price) => {
		let formattedPrice = priceFormatter.format(price);
		if (currency && currency === 'PLN') {
			formattedPrice = formattedPrice.replace('PLN', 'zł')
		}
		return formattedPrice;
	}

	const getTotalAmount = () => {
		let total = 0;
		selectedRates.forEach((selRate) => {
			total +=
				rates.find((rate) => rate.id === selRate.id).pvp * selRate.qty;
		});

		return total;
	};

	return (
		<div className="bw-total-amount">
			<div>
				{t("total")} {priceFormatted(currency, getTotalAmount())}
			</div>
		</div>
	);
}

function InfoBox({ product }) {
	const content = _get(
		product,
		"booking_process_information.product_information"
	);
	if (content) {
		return (
			<div className="bw-info-box">
				<i className="bw-info-box-icon far fa-thumbs-up"></i>
				<span className="bw-info-box-content">{content}</span>
			</div>
		);
	} else {
		return null;
	}
}

function AlertBox({ children }) {
	return (
		<div className="bw-alert-box">
			<div className="bw-alert-box-content">{children}</div>
		</div>
	);
}

export default function BookingWidget({
	locale,
	experience,
	availableSteps,
	defaultSelection,
	onConfirm,
	productsFetcher,
	productFetcher,
	availabilityFetcher,
	ratesFetcher,
	availabilityCheckIsActive,
	loading,
	trackingCallbacks: {
		onBeginCheckoutStep1DLCallback,
		onBeginCheckoutStep2DLCallback,
		onBeginCheckoutStep3DLCallback,
		onBeginCheckoutStep4DLCallback,
	} = {}
}) {
	locale = locale || DEFAULT_LOCALE;
	availableSteps = availableSteps || DEFAULT_STEPS;

	const { t } = useTranslation();

	if (typeof window !== `undefined`) {
		UIkit.container = ".uk-scope";
	}

	const initSelection = (selection) => {
		let result = {
			...BASE_SELECTION,
			...selection,
		};

		if (result.productId) {
			result.productId = parseInt(result.productId);
		}

		return result;
	};

	const productList = productsFetcher();
	const [currentStep, setCurrentStep] = useState(null);
	const [sessionList, setSessionList] = useState([]);
	const [rateList, setRateList] = useState([]);
	const [selection, setSelection] = useState(
		initSelection(defaultSelection || {})
	);
	const [bookingReady, setBookingReady] = useState(false);
	const [availabilityCalendar, setAvailabilityCalendar] = useState('');

	const getProduct = (productId) =>
		productList.find((product) => product.id === productId);

	useEffect(() => {
		setCurrentStep(checkStep());
		// check booking is ready
		const qty = getRatesQty(selection.rates);
		const product = getProduct(selection.productId);

		if (product && qty > 0
			&& qty >= product.qty_restrictions.min
			&& (
				qty <= product.qty_restrictions.max ||
				product.qty_restrictions.max === null ||
				product.qty_restrictions.max === -1
			)
		) {
			setBookingReady(true);
		} else {
			setBookingReady(false);
		}
	}, [selection]);

	useEffect(() => {
		if (Number(currentStep) && checkIsFunctionTrackingCallbacks()) {
			processTrackingCallback();
		}

	}, [currentStep]);

	useEffect(() => {
		if (rateList && rateList.length > 0 && isFunction(onBeginCheckoutStep4DLCallback)) {
			onBeginCheckoutStep4DLCallback({
				product: getProduct(selection.productId),
				selection: selection,
				rateList: rateList
			});
		}
	}, [rateList]);

	const checkIsFunctionTrackingCallbacks = () => {
		return !!(isFunction(onBeginCheckoutStep1DLCallback)
			|| isFunction(onBeginCheckoutStep2DLCallback)
			|| isFunction(onBeginCheckoutStep3DLCallback)
			|| isFunction(onBeginCheckoutStep4DLCallback));
	}

	const availabilityFetcherWrapper = (date) => {
		if (!date) {
			date = new Date();
		}
		return availabilityFetcher(selection.productId, date);
	};

	const onSelectStepHandler = (step) => {
		setCurrentStep(step);
	};

	const processTrackingCallback = () => {
		switch (currentStep) {
			case STEP_PRODUCT:
				if (isFunction(onBeginCheckoutStep1DLCallback)) {
					onBeginCheckoutStep1DLCallback();
				}
				break;
			case STEP_SESSION:
				if (isFunction(onBeginCheckoutStep3DLCallback)) {
					onBeginCheckoutStep3DLCallback({
						product: getProduct(selection.productId),
						availability: availabilityCalendar
					});
				}
				break;
		}
	};

	const getAvailabilityText = (availability) => {
		const monthNames = ["enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"];

		if (availability.length < 1) {
			return '';
		}

		const monthStr = monthNames[availability[0].getMonth()];
		const yearStr = availability[0].getFullYear();
		return monthStr + yearStr + '_' + availability.length;
	}
	const availabilityCallBack = (availability) => {
		if (isFunction(onBeginCheckoutStep2DLCallback)) {
			const availabilityText = getAvailabilityText(availability);
			setAvailabilityCalendar(availabilityText);
			const data = {
				product: getProduct(selection.productId),
				availability: availabilityText,
			}
			onBeginCheckoutStep2DLCallback(data);
		}
	}

	const onProductSelectionHandler = (product) => {
		if (availabilityCheckIsActive) {
			availabilityCheckIsActive(product, new Date()).then((isActive) => {
				setSelection({
					...BASE_SELECTION,
					productId: product,
					active: isActive,
				});
			});
		} else {
			setSelection({
				...BASE_SELECTION,
				productId: product,
				active: true,
			});
		}

	};

	const onDateSelectionHandler = (date) => {
		productFetcher(selection.productId, date.date).then((product) => {
			const productIndex = productList.findIndex((pr) => pr.id === product.id)

			productList[productIndex] = product

			if (
				date.sessions.length === 1 &&
				date.sessions[0].session === "day_wide"
			) {
				setSelection({
					...BASE_SELECTION,
					productId: selection.productId,
					active: selection.active,
					date: date.date,
					session: "day_wide",
				});

				setRateList([]);
				fetchData(setRateList, ratesFetcher, [
					selection.productId,
					date.date,
				]);
			} else {
				setSelection({
					...BASE_SELECTION,
					productId: selection.productId,
					active: selection.active,
					date: date.date,
				});
			}
		})

		setSessionList(date.sessions);
	}

	const onSessionSelectionHandler = (session) => {
		setSelection({
			...BASE_SELECTION,
			productId: selection.productId,
			active: selection.active,
			date: selection.date,
			session: session,
		});

		setRateList([]);
		fetchData(setRateList, ratesFetcher, [
			selection.productId,
			selection.date,
		]);
	};

	const onRatesSelectionHandler = (rates) => {
		setSelection({
			...BASE_SELECTION,
			productId: selection.productId,
			active: selection.active,
			date: selection.date,
			session: selection.session,
			rates: rates,
		});

	};

	const onConfirmHandler = () => {
		onConfirm(selection);
	};

	/**
	 * Returns the step required with the current selection.
	 */
	const checkStep = () => {
		// check desired step is available
		if (!selection.productId) {
			return STEP_PRODUCT;
		}

		if (!selection.date) {
			return STEP_DATE;
		}

		if (!selection.session) {
			if (sessionList.length === 0) {
				return STEP_DATE;
			} else if (sessionList[0].session === "day_wide") {
				return STEP_RATES;
			} else {
				return STEP_SESSION;
			}
		}

		return STEP_RATES;
	};

	const isStepDisabled = (step) => {
		if (currentStep > step) {
			return false;
		} else if (currentStep < step) {
			switch (step) {
				case STEP_PRODUCT:
					return !selection.productId;
				case STEP_DATE:
					return !selection.date;
				case STEP_SESSION:
					return !selection.session;
				case STEP_RATES:
					return selection.rates.length === 0;
				default:
					return true;
			}
		}

		return true;
	};

	const getSteps = () => {
		let availableSteps = [1, 2, 4];

		if (selection.date) {
			if (
				sessionList.length > 0 &&
				sessionList[0].session !== "day_wide"
			) {
				availableSteps = [1, 2, 3, 4];
			}
		}

		return availableSteps.map((step, index) => {
			const result = {
				id: step,
				content: {
					number: index + 1,
					icon: null,
					title: null,
					text: null,
					info: null,
				},
			};

			switch (step) {
				case STEP_PRODUCT:
					result.content.componentTitle = t("select.product");
					result.content.title = t("product");
					result.content.icon = (
						<i
							className="step-icon fa fa-play-circle"
							aria-hidden="true"
						></i>
					);
					if (selection.productId) {
						const product = productList.find(
							(product) => product.id === selection.productId
						);

						result.content.text = product ? product.name : "";
					}
					break;
				case STEP_DATE:
					result.content.componentTitle = t("select.date");
					result.content.title = t("date");
					result.content.icon = (
						<i
							className="step-icon far fa-calendar-alt"
							aria-hidden="true"
						></i>
					);
					if (selection.date) {
						result.content.text = format(
							parseISO(selection.date),
							"P",
							{
								locale: locales[locale],
							}
						);
					}
					break;
				case STEP_SESSION:
					result.content.componentTitle = t("select.session");
					result.content.title = t("session");
					result.content.icon = (
						<i
							className="step-icon far fa-clock"
							aria-hidden="true"
						></i>
					);
					if (selection.session) {
						const session = selection.session;
						result.content.text = session.substring(
							0,
							session.length - 3
						);
					}
					break;
				case STEP_RATES:
					result.content.componentTitle = t("select.rate");
					result.content.title = t("rate");
					result.content.icon = (
						<i
							className="step-icon fa fa-ticket-alt fa-rotate-45"
							aria-hidden="true"
						></i>
					);
					if (selection.session) {
						const session = sessionList.find(
							(session) => session.session === selection.session
						);
						result.content.text =
							getRatesQty(selection.rates) + " " + t("tickets");
						if (session.available > 0) {
							result.content.info = (
								<span className="tickets-availables">
									<b>{session.available}</b>
									<i className="fa fa-ticket-alt fa-rotate-45"></i>
									{t("availables")}
								</span>
							);
						}
					}
					break;
				default:
					break;
			}

			return result;
		});
	};

	const notAvailableMessage = () => {
		const notAvailableMsg = getProduct(selection.productId).booking_process_information.not_available_message

		return notAvailableMsg ?
			(<div className="uk-text-default uk-text-justify uk-align">
				{notAvailableMsg}
			</div>)
			:
			(<div className="uk-text-large uk-text-center">
				{t("not_active")}
			</div>)
	}

	const buildStepComponent = () => {
		switch (currentStep) {
			case STEP_PRODUCT:
				return (
					<ProductList
						locale={locale}
						productList={productList}
						selected={selection.productId}
						onSelection={onProductSelectionHandler}
					/>
				);
			case STEP_DATE:
				if (!selection.active) {
					return (
						<AlertBox>
							{notAvailableMessage()}
						</AlertBox>
					);
				} else {
					return (
						<AvailabilityCalendar
							onSelection={onDateSelectionHandler}
							availabilityFetcher={availabilityFetcherWrapper}
							availabilityCallBack={availabilityCallBack}
						/>
					);
				}
			case STEP_SESSION:
				return (
					<SessionList
						sessions={sessionList}
						selected={selection.session}
						onSelection={onSessionSelectionHandler}
					/>
				);
			case STEP_RATES:
				const product = productList.find(
					(product) => product.id === selection.productId
				);
				const session = sessionList.find(
					(session) => session.session === selection.session
				);

				let bookingConfirmation = null;
				if (
					!_isEmpty(product.booking_process_information.confirmation)
				) {
					bookingConfirmation = {
						id: "booking-confirmation",
						header: _get(
							product,
							"booking_process_information.confirmation.title"
						),
						children: (
							<div
								dangerouslySetInnerHTML={{
									__html: _get(
										product,
										"booking_process_information.confirmation.content"
									),
								}}
							/>
						),
						check: _get(
							product,
							"booking_process_information.confirmation.checkboxes"
						),
						onConfirm: onConfirmHandler,
					};
				}

				const showConfirmationModal = () => {
					UIkit.modal("#booking-confirmation", {
						container: "#booking-widget",
					}).show();
				};



				return (
					<div>
						<GroupedRateList
							locale={locale}
							rates={rateList}
							groupId={6}
							groupName={t("resident")}
							visibleGroupValue={19}
							selected={selection.rates}
							max={Math.min(
								session.available,
								(product.max_pax === -1 || !product.max_pax ? Number.MAX_SAFE_INTEGER : product.max_pax)
							)}
							ageRestrictions={experience.age_restriction}
							message={MASCA_PRODUCTS.includes(selection.productId) && MASCA_MESSAGE[locale]}
							onChange={onRatesSelectionHandler}
						/>
						<BookingTotal
							locale={locale}
							currency="eur"
							rates={rateList}
							selectedRates={selection.rates}
						/>
						<div className="bw-confirm">
							<button
								className="uk-button uk-button-default"
								disabled={!bookingReady}
								onClick={
									_isEmpty(bookingConfirmation)
										? onConfirmHandler
										: showConfirmationModal
								}
							>
								{t("buy")}
							</button>
							{!_isEmpty(bookingConfirmation) && (
								<Modal
									locale={locale}
									{...bookingConfirmation}
								/>
							)}
						</div>
					</div>
				);
			default:
				return null;
		}
	};

	return (
		<div id="booking-widget" className="booking-widget uk-scope">
			{loading && <Loading />}
			<div className="bw-container uk-grid-collapse" uk-grid="uk-grid">
				<div className="bw-head-title uk-width-1-1" ukgrid="ukgrid">
					<h3 className="uk-width-1-1@m uk-width-1-4@m">
						{t("buy")}
					</h3>
				</div>
				<div className="bw-sidebar uk-visible@m uk-width-1-3@m">
					<ul key="bw-steps">
						{getSteps().map((step) => (
							<StepNav
								key={step.id}
								step={step.id}
								content={step.content}
								selected={step.id === currentStep}
								disabled={isStepDisabled(step.id)}
								onSelection={onSelectStepHandler}
							/>
						))}
					</ul>
					<InfoBox
						key="booking-info"
						product={getProduct(selection.productId)}
					/>
				</div>
				<div className="bw-main uk-width-2-3@m">
					<ul key="bw-steps">
						{getSteps().map((step) => (
							<StepNav
								key={step.id}
								step={step.id}
								content={step.content}
								selected={step.id === currentStep}
								disabled={isStepDisabled(step.id)}
								responsiveHeader={true}
								onSelection={onSelectStepHandler}
								component={
									step.id === currentStep
										? buildStepComponent()
										: null
								}
							></StepNav>
						))}
					</ul>
				</div>
			</div>
		</div>
	);
}
