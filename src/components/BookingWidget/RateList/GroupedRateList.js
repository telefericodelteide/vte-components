import React, { useState, useEffect, Fragment } from "react";
import { useTranslation, Trans } from "react-i18next";
import UIkit from "uikit";
import RateList from "./RateList";
import Modal from "../../Generic/Modal";
import _has from "lodash/has";
import "./rate-list.css";

export default function GroupedRateList({
	groupId,
	groupName,
	visibleGroupValue,
	rates,
	locale,
	onChange,
	max,
	ageRestrictions,
	message
}) {
	const { t } = useTranslation();

	max = isNaN(max) || max === 0 || max === -1 || max === null ? Number.MAX_SAFE_INTEGER : max;

	const [groupedRatesHiddenChecked, setGroupedRatesHiddenChecked] = useState(
		false
	);
	const [groupedRates, setGroupedRates] = useState({});
	const [selection, setSelection] = useState([]);
	const [maxAvailableQty, setMaxAvailableQty] = useState(0);

	useEffect(() => {
		const processedRates = rates.reduce((acc, rate, index, rates) => {
			const customerType = rate.customer_types.find(
				(ct) => ct.group_id === groupId
			);
			if (!(customerType.id in acc)) {
				acc[customerType.id] = [];
			}
			acc[customerType.id].push(rate);
			return acc;
		}, {});

		if (
			_has(processedRates, 18) &&
			processedRates[18].some((rate) => rate.qty_restrictions.min > 0)
		) {
			setGroupedRatesHiddenChecked(true);
		}

		setGroupedRates(processedRates);

		setSelection(
			rates.map((rate) => {
				const customerType = rate.customer_types.find(
					(ct) => ct.group_id === groupId
				);
				return {
					id: rate.id,
					groupValue: customerType.id,
					qty: rate.qty_restrictions.min,
				};
			})
		);
	}, [rates, groupId]);

	useEffect(() => {
		setMaxAvailableQty(getMaxAvailableQty(selection));
	}, [selection]);

	useEffect(() => {
		if (groupedRatesHiddenChecked) {
			UIkit.modal("#grouped-rate-modal", {
				container: "#show-group-cond",
			}).show();
		}
	}, [groupedRatesHiddenChecked]);

	const getMaxAvailableQty = (selection) =>
		max - selection.reduce((acc, rate) => acc + rate.qty, 0);

	const getGroupQty = (groupValue, selection) =>
		selection
			.filter((rate) => rate.groupValue === groupValue)
			.reduce((acc, rate) => acc + rate.qty, 0);

	const onRateChangeHandler = (rates) => {
		const newSelection = selection.map((selectedRate) => {
			const rate = rates.find((rate) => rate.id === selectedRate.id);
			if (rate) {
				return {
					...selectedRate,
					qty: rate.qty,
				};
			} else {
				return selectedRate;
			}
		});

		setSelection(newSelection);
		onChange(newSelection);
	};

	const onShowRatesHiddenChange = (event) => {
		setGroupedRatesHiddenChecked(true);
	};

	return (
		<div className="grouped-rates-container">
			<div className="grouped-rates-visible">
				{Object.keys(groupedRates).length > 0 && (
					<RateList
						locale={locale}
						rates={groupedRates[visibleGroupValue]}
						rateNameGroupId={5}
						onChange={onRateChangeHandler}
						max={maxAvailableQty + getGroupQty(visibleGroupValue, selection)}
						ageRestrictions={ageRestrictions}
					/>
				)}
			</div>
			{Object.keys(groupedRates).length > 1 && (
				<Fragment>
					<div className="show-group-cond uk-flex uk-flex-center">
						<label
							htmlFor="show-rates-hidden"
							className="show-rates-hidden"
						>
							<input
								id="show-rates-hidden"
								className="uk-checkbox"
								type="checkbox"
								checked={groupedRatesHiddenChecked}
								onChange={onShowRatesHiddenChange}
							/>
							{groupName}
						</label>
						<Modal
							id="grouped-rate-modal"
							className="grouped-rate-modal"
							header={t("grouped_rate_list.header")}
							locale={locale}
						>
							<Trans
								defaults={t("grouped_rate_list.body")} // optional defaultValue
								components={{ p: <p />, b: <b /> }}
							/>
						</Modal>
					</div>
					<div
						id={"rates-hidden-" + groupId}
						className="grouped-rates-hidden grouped-rates-toggle"
						hidden={!groupedRatesHiddenChecked}
					>
						<RateList
							locale={locale}
							rates={groupedRates[18]}
							rateNameGroupId={5}
							onChange={onRateChangeHandler}
							max={maxAvailableQty + getGroupQty(18, selection)}
							ageRestrictions={ageRestrictions}
						/>
						{message &&
							<div className="uk-padding uk-padding-remove-top">
								<b>{message}</b>
							</div>
						}
					</div>
				</Fragment>
			)}
		</div>
	);
}
