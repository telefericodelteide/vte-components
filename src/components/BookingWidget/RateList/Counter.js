import React, { useState, useEffect } from "react";
import "./rate-list.css";

export default function Counter({
  defaultValue,
  min,
  max,
  disabled,
  onChange
}) {

  const [value, setValue] = useState(defaultValue || min);

  const increment = () => {
    if (!disabled && value < max) {
      setValue(value + 1);
    }
  }

  const decrement = () => {
    if (value > min) {
      setValue(value - 1);
    }
  }

  useEffect(() => onChange(value));

  return (
    <div className="counter">
      <span className="counter-decrement"><button className="uk-button uk-button-default" type="button" onClick={decrement}>-</button></span>
      <span className="value">{value}</span>
      <span className="counter-increment"><button className="uk-button uk-button-default" type="button" onClick={increment}>+</button></span>
    </div>
  );
}
