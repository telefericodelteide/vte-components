import React, { useState, useEffect } from "react";
import Counter from "./Counter";
import _get from "lodash/get";
import _template from "lodash/template";
import "./rate-list.css";
import { useTranslation } from "react-i18next";

export function Rate({
	name,
	qtyRestriction,
	ageRestriction,
	isLastRate,
	unitPrice,
	currency,
	defaultValue,
	locale,
	disabled,
	onChange,
}) {
	const [value, setValue] = useState(null);
	useEffect(() => onChange(value), [value]);

	const { t } = useTranslation();

	const priceFormatter = new Intl.NumberFormat(locale, {
		style: "currency",
		currency: currency || "eur",
	});

	const priceFormatted = (currency, price) => {
		let formattedPrice = priceFormatter.format(price);
		if (currency && currency === 'PLN') {
			formattedPrice = formattedPrice.replace('PLN', 'zł');
		}
		return formattedPrice;
	}

	const onRateChangeHandler = (value) => {
		setValue(value);
	};

	return (
		<div className="rate">
			<div className="rate-header">
				<span className="rate-name">{name}</span>
				<span className="rate-price">
					{priceFormatted(currency, unitPrice)}
				</span>
			</div>
			<div className="rate-counter">
				<Counter
					defaultValue={defaultValue}
					min={qtyRestriction.min}
					max={qtyRestriction.max}
					disabled={disabled}
					onChange={onRateChangeHandler}
				/>
			</div>
			<div className="rate-footer">
				{_get(ageRestriction, "min", -1) >= 0 && (
					<span className="rate-footer-age-range">
						{_template(t("rate_list.age_restriction"))({
							from: ageRestriction.min,
							to: ageRestriction.max,
						})}
					</span>
				)}
				{isLastRate && _get(ageRestriction, "min", -1) > 0 && (
					<span className="rate-footer-age-limit">
						{_template(t("rate_list.age_minimum"))({
							age: ageRestriction.min,
						})}
					</span>
				)}
			</div>
		</div>
	);
}

export default function RateSelector({
	rates,
	rateNameGroupId,
	locale,
	onChange,
	max,
	ageRestrictions
}) {
	max = (isNaN(max) || max == null || max === -1) ? Number.MAX_SAFE_INTEGER : max;

	const [selection, setSelection] = useState([]);
	const [maxQtyLimit, setMaxQtyLimit] = useState(false);

	useEffect(() => {
		setSelection(
			rates.map((rate) => {
				return {
					id: rate.id,
					qty: getRateQtyRestriction(rate).min,
				};
			})
		);
	}, [rates]);

	useEffect(() => {
		setMaxQtyLimit(isMaxQtyLimit(selection));
	}, [selection, max]);

	const isMaxQtyLimit = (selection) => {
		let qty = 0;
		selection.forEach((rate) => {
			qty += rate.qty;
		});

		return qty >= max;
	};

	const onRateChangeHandler = (rateId, qty) => {
		const newSelection = selection.map((rate) => {
			if (rate.id === rateId) {
				return {
					id: rate.id,
					qty: qty,
				};
			} else {
				return rate;
			}
		});
		setSelection(newSelection);
		onChange(newSelection);
	};

	const getRateName = (rate) =>
		rate.customer_types.find((ct) => ct.group_id === rateNameGroupId).name;

	const getRateAgeRestriction = (rate) => {
		// const arrAgeRestrictions = Object.values(ageRestrictions)
		// const ct = rate.customer_types.find((ct) => ct.group_id === 5);
		// return ct.age_restriction || {};
		const arrAgeRestrictions = Object.values(ageRestrictions)
		const restrictions = rate.customer_types.
			map((ct) => arrAgeRestrictions.find((r) => r.customer_type_group_value === ct.id)).
			filter((ar) => ar !== undefined)

		return (restrictions && restrictions.length > 0) ? { min: restrictions[0].min, max: restrictions[0].max } : {}
	};

	const getRateQtyRestriction = (rate) => {
		const min = rate.qty_restrictions.min;
		const max = rate.qty_restrictions.max;

		return {
			min: isNaN(min) || min === null ? 0 : min,
			max: isNaN(max) || max === null ? Number.MAX_SAFE_INTEGER : max,
		};
	};

	const isLastRate = (rates, index) => rates.length - 1 == index;

	return (
		<div
			className="rates-container uk-flex uk-flex-center"
			uk-grid="uk-grid"
		>
			{rates.map((rate, index) => (
				<Rate
					className="uk-width-1-2 uk-width-1-4@m"
					locale={locale}
					key={rate.id}
					name={getRateName(rate)}
					qtyRestriction={getRateQtyRestriction(rate)}
					ageRestriction={getRateAgeRestriction(rate)}
					isLastRate={isLastRate(rates, index)}
					unitPrice={rate.pvp}
					disabled={maxQtyLimit}
					onChange={(qty) => onRateChangeHandler(rate.id, qty)}
				/>
			))}
		</div>
	);
}
