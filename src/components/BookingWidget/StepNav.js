import React, { Fragment } from "react";

export default function StepNav({ step, content, selected, disabled, responsiveHeader, component, onSelection }) {

  return (
    <li className={
      "bw-navstep" + 
      (disabled ? " disabled" : "") +
      (selected ? " selected" : "")}
      onClick={() => !disabled && onSelection(step)}>
      <div className={"bw-navstep-header" + (responsiveHeader ? " uk-hidden@m" : "")}>
        <div>
          {content.icon}
          <div className="bw-navstep-number">{content.number}</div>
          <div className="bw-navstep-text">
            <h4>{content.title}</h4>
            {content.info}
            <p>{content.text}</p>
          </div>
        </div>
      </div>
      {component ? 
        <Fragment>
          <div className="bw-navstep-component-title + uk-visible@m">{content.componentTitle}</div>
          <div className="bw-navstep-component">
            {component}
          </div>
        </Fragment> : null
      }
    </li>
  );
};