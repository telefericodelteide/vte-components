import React, { useState, useEffect } from "react";
import "./session.css";

export function Session({ session, selected, onSelection, hiddenAvailable }) {
	const [isSelected, setIsSelected] = useState(selected);

	useEffect(() => {
		setIsSelected(selected);
	}, [selected]);

	const selectionHandler = (session) => {
		setIsSelected(true);
		onSelection(session.session);
	};

	const sessionPlaces = () => {
		return (
			<>
				<span className="separator-ticket"></span>
				<span className="session-places">
					{session.available >= 10 ? "+10" : session.available}{" "}
					<i className="fa fa-ticket-alt" aria-hidden="true"></i>
				</span>
			</>
		)
	}

	return (
		<div
			className={"session" + (isSelected ? " selected" : "")}
			onClick={() => selectionHandler(session)}
		>
			<div>
				<span className="session-time">
					{session.session.substring(0, session.session.length - 3)}{" "}
					<i className="fa fa-clock" aria-hidden="true"></i>
				</span>
				{!hiddenAvailable && sessionPlaces()}
			</div>
		</div>
	);
}

export default function SessionList({ sessions, selected, onSelection, hiddenAvailable }) {
	const [selectedSession, setSelectedSession] = useState(selected);

	const selectionHandler = (session) => {
		setSelectedSession(session);
		onSelection(session);
	};
	return (
		<div className="sessions-container uk-flex uk-flex-wrap">
			{sessions
				.filter((session) => session.available > 0)
				.map((session) => (
					<Session
						key={session.session}
						session={session}
						selected={session.session === selectedSession}
						onSelection={selectionHandler}
						hiddenAvailable={hiddenAvailable}
					/>
				))}
		</div>
	);
}
