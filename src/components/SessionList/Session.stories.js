import React from "react";
import { Session } from "./SessionList";

export default {
	component: Session,
	title: "Booking widget/Sessions/Session",
};

const Template = (args) => <Session {...args} />;

export const Default = Template.bind({});
Default.args = {
	session: JSON.parse(
		'{"session":"09:10:00","consumed":12,"available":8,"max_quantity":20,"highlighted":false}'
	),
	onSelection: (session) => console.log(session),
	hiddenAvailable: false

};

export const Selected = Template.bind({});
Selected.args = {
	...Default.args,
	selected: true,
};

export const SelectedHiddenAvailable = Template.bind({});
SelectedHiddenAvailable.args = {
	...Default.args,
	selected: true,
	hiddenAvailable: true
};
