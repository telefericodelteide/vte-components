import React from "react";
import SessionList from "./SessionList";
import { sessionsFetcher } from "../BookingWidget/stories-mockups";

export default {
	component: SessionList,
	title: "Booking widget/Sessions/Sessions",
};

const Template = (args) => <SessionList {...args} />;

export const Default = Template.bind({});
Default.args = {
	onSelection: (session) => {
		console.log(session);
	},
	sessions: sessionsFetcher(),
	hiddenAvailable: false
};

export const Selected = Template.bind({});
Selected.args = {
	...Default.args,
	selected: "12:30:00",
	hiddenAvailable: false
};

