'use strict'

import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import Context from '../../context'
import { useTranslation } from 'react-i18next'

function TotalRow(props) {  
  const { i18n } = useTranslation('main')
  const { cn } = useContext(Context)

  const priceFormatter = new Intl.NumberFormat(i18n.language, {
    style: "currency",
    currency: "eur"
  })

  const totalAmount = props.totalAmount ? parseFloat(props.totalAmount) : 0
  const roundTotalAmount= priceFormatter.format(parseFloat(totalAmount))
  const icon = cn(`${props.settingType}.icon`)
  return (
    <div className={ cn(`${props.settingType}.wrapper`) }>
      <div className={ cn(`${props.settingType}.row`) }>
        <span className={ cn(`${props.settingType}.label`) }>{ props.label }: </span>
        <span className={ cn(`${props.settingType}.content`) }>{roundTotalAmount} </span>
        { icon ? <span> <i className={ icon }></i></span> : null }
      </div>
    </div>
  )
}

TotalRow.propTypes = {
  totalAmount: PropTypes.number.isRequired,
  settingType: PropTypes.string,
  label: PropTypes.string,
}

export default TotalRow