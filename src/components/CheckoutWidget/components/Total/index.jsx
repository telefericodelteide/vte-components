'use strict'

import React from 'react'
import PropTypes from 'prop-types'
import { useTranslation } from 'react-i18next'

import DiscountRows from './DiscountRows'
import TotalRow from './TotalRow'

function Total(props) { 
  const { t } = useTranslation('main')
  
  if (props.totalAmount >= 0) {
    return [
      <DiscountRows key='discounts' discounts={ props.discounts } />,
      <TotalRow key='total' totalAmount={ props.totalAmount } label={ t('cart.total') } settingType={ 'total' }/>
    ]
  }
  return null
}

Total.propTypes = {
  totalAmount: PropTypes.number,
  discounts: PropTypes.array,
}

export default Total