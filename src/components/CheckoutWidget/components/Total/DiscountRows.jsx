'use strict'

import React from 'react'
import PropTypes from 'prop-types'
import { useTranslation } from 'react-i18next'
import _isArray from 'lodash/isArray'

import TotalRow from './TotalRow'

function DiscountRows(props) { 
  const { t } = useTranslation('main')
  
  function renderDiscounts() {
    const discounts = props.discounts
    
    if (_isArray(discounts)) {
      return discounts.map((discount, index) => {
        const name = discount.name ? discount.name : t('cart.discount')
        return <TotalRow key={`discount-${index}`} label={name} totalAmount={discount.total_amount} settingType={ 'discount' } />
      })
    }  

    return null
  }

  return renderDiscounts() 
}

DiscountRows.propTypes = {
  discounts: PropTypes.array
}

export default DiscountRows