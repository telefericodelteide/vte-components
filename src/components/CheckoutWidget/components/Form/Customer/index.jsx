'use strict'

import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import { useTranslation } from 'react-i18next'

import Context from '../../../context'
import Field from '../Fields/Field'
import PhoneField from '../Fields/PhoneField'
import Textarea from '../Fields/Textarea'
import Checkbox from '../Fields/Checkbox'
import PromoCodeField from '../PromoCodeField'

function Customer(props) { 
  const { t } = useTranslation('main')
  const { cn } = useContext(Context)
  
  if (props.form) {
    const form = props.form

    return [
        <div key='name' className={ cn('form.customer.names.row') } uk-grid="uk-grid">
          <Field 
            id='client.first_name' 
            config={form} 
            values={props.values} 
            className={ cn('form.customer.names.first_name.field') }
            wrapperClassName={ cn('form.customer.names.first_name.fieldWrapper') } />
          <Field 
            id='client.last_name' 
            config={form} 
            values={props.values} 
            className={ cn('form.customer.names.last_name.field') }
            wrapperClassName={ cn('form.customer.names.last_name.fieldWrapper') } />
        </div>,
        <div key='emails' className={ cn('form.customer.emails.row') } uk-grid="uk-grid">
          <Field 
            id='client.email' 
            config={form} 
            values={props.values} 
            className={ cn('form.customer.emails.email.field') }
            wrapperClassName={ cn('form.customer.emails.email.fieldWrapper') } />
          <Field 
            id='client.confirm_email' 
            config={form} 
            values={props.values} 
            className={ cn('form.customer.emails.confirm_email.field') }
            wrapperClassName={ cn('form.customer.emails.confirm_email.fieldWrapper') }/>
        </div>,
        <div key='phones' className={ cn('form.customer.phones.row') } uk-grid="uk-grid">
          <PhoneField 
            id='client.phone' 
            config={form} 
            values={props.values} 
            className={ cn('form.customer.phones.phone.field') }
            wrapperClassName={ cn('form.customer.phones.phone.fieldWrapper') } />
          <PhoneField 
            id='client.phone_other' 
            config={form} 
            values={props.values} 
            className={ cn('form.customer.phones.phone_other.field') }
            wrapperClassName={ cn('form.customer.phones.phone_other.fieldWrapper') }/>
        </div>,
        <div key='promo-observations' className={ cn('form.customer.observations.row') } uk-grid="uk-grid">
          <div className={ cn('form.customer.observations.promo.fieldWrapper') }>
            <PromoCodeField 
              errors={ props.errors }
              promoCode={ props.promoCode } 
              onChange={ props.onChangePromoCode } 
              onApplyPromoCode={ props.onApplyPromoCode } />
          </div>
          <Textarea 
            id='client.observations' 
            config={form} 
            values={props.values}
            className={ cn('form.customer.observations.observations.field') }
            wrapperClassName={ cn('form.customer.observations.observations.fieldWrapper') }/>
        </div>
      ]
  }

  return null
}

Customer.propTypes = {
  form: PropTypes.object.isRequired,
  values: PropTypes.object.isRequired,
  promoCode: PropTypes.string,
  errors: PropTypes.object,
  onApplyPromoCode: PropTypes.func.isRequired,
  onChangePromoCode: PropTypes.func.isRequired,
}

export default Customer