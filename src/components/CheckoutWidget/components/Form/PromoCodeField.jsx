'use strict'

import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import { useTranslation } from 'react-i18next'

import Context from '../../context'

function PromoCodeField(props) { 
  const { t } = useTranslation('main')
  const { cn } = useContext(Context)

  function onChange(e) {
    e.preventDefault()
    props.onChange(e.target.value)
  }
  
  return (
    <div>
      <label>{t('form.promoCodeLabel')}</label>
      <div className={ cn('form.customer.observations.promo.group') }>
        <input name="promo" value={props.promoCode ? props.promoCode : '' } onChange={onChange} type="text" autoComplete="false" className={ cn('form.customer.observations.promo.field') } />
        <button id="promo-check" type="button" onClick={props.onApplyPromoCode} className={ cn('form.customer.observations.promo.button') }>{t('form.promoCodeButton')}</button>
      </div>
      { props.errors['promo-code'] ? <label className={'error'}>{ props.errors['promo-code'] }</label> : null}
    </div>
  )
}

PromoCodeField.propTypes = {
  promoCode: PropTypes.string,
  errors: PropTypes.object,
  onChange: PropTypes.func.isRequired,
  onApplyPromoCode: PropTypes.func.isRequired
}

export default PromoCodeField
