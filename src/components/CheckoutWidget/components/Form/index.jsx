'use strict'

import React, {Fragment, useContext} from 'react'
import PropTypes from 'prop-types'
import {useTranslation} from 'react-i18next'
import {Formik, Form as CustomForm} from 'formik'
import _debounce from 'lodash/debounce'
import _get from 'lodash/get'
import _set from 'lodash/set'
import _merge from 'lodash/merge'
import _isEmpty from 'lodash/isEmpty'

import Context from '../../context'
import Header from './Fields/Header'
import Customer from './Customer'
import Pickup from './Pickup'
import Attendees from './Attendees'
import Invoice from './Invoice'
import Payment from './Payment'
import TermsAndConditions from './TermsAndConditions'
import defaultConfigForm from '../../config/forms'


function Form(props) {
  const {t} = useTranslation('main')
  const {cn} = useContext(Context)

  var updateState = _debounce(function (values) {
    props.onChange(values)
  }, 350);

  function getNeedCheckNif(lineItemsForm, lineItems) {
    let lineItemsFormReturn =  [];
    for(let i = 0; i < lineItemsForm.length; i++) {
      lineItemsFormReturn[i] = {...lineItemsForm[i]};
      lineItemsFormReturn[i].participants = lineItemsForm[i].participants.filter((participant) => {
        if (!participant.rate) {
          return false;
        }

        const rateId = participant.rate.split("-")[0];
        const item = lineItems[0].rates.find((rate) => rate.rate.id === parseInt(rateId));
        const isChild = item.rate.rate_elements.some(
            (element) => element.id === 14
        );
        return isChild ? false : participant;

      }).filter(Boolean);
    }
    return lineItemsFormReturn;
  }

  if (props.client) {
    const defaultForm = _get(defaultConfigForm, 'form')
    const form = {client: defaultForm}
    const pickup = {
      pickup:
        _merge(
          _get(defaultConfigForm, 'pickup'),
          {
            lodgin_id: {options: _get(props, 'pickupData.lodgins')},
            zone_id: {options: _get(props, 'pickupData.zones')},
            pickup_point_id: {options: _get(props, 'pickupData.pickupPoints')}
          }
        )
    }
    const defaultInvoice = _get(defaultConfigForm, 'invoice')
    const invoice = {invoice: _merge(defaultInvoice, _get(props, 'client.invoice'))}
    const participants = _get(props, 'client.participants')
    const lineItems = _get(props, 'lineItems')
    const pickupData = _get(props, 'pickupData')

    const validateForm = (values) => {
      let errors = {};
      const lineItemsForm = _get(values, 'participants', [])

      lineItemsForm.forEach((lineItem, index) => {

        const participants = _get(lineItem, 'participants', [])
        participants
          .map((participant, participantIndex) => {
            return {
              index: index,
              participantIndex: participantIndex,
              value: (participant.first_name + participant.last_name).replace(/[0-9\s"'`´]/g, '').toLowerCase()
            }
          })
          .filter((participant, participantIndex, participants) => {
            const findIndexValue = participants.findIndex(p => p.value.length > 0 && p.value === participant.value)

            return findIndexValue !== participantIndex && findIndexValue > -1
          })
          .forEach((p) => {
            _set(errors,`participants[${p.index}].participants[${p.participantIndex}].first_name`, t("form.validations.repeated"))
          })
      })

      const lineItemsCheckDni = getNeedCheckNif(lineItemsForm, lineItems)
      lineItemsCheckDni.forEach((lineItem, index) => {

        const participants = _get(lineItem, 'participants', [])
        participants
            .map((participant, participantIndex) => {
              return {
                index: index,
                participantIndex: participantIndex,
                value: participant.nif.replace(/ /g, "").toLowerCase()
              }
            })
            .filter((participant, participantIndex, participants) => {
              const findIndexValue = participants.findIndex(p => p.value.length > 0 && p.value === participant.value)
              return findIndexValue !== participantIndex && findIndexValue > -1
            })
            .forEach((p) => {
              _set(errors,`participants[${p.index}].participants[${p.participantIndex}].nif`, t("form.validations.repeatedNif"))
            })
      })

      return errors
    }

    return (
      <Formik
        initialValues={props.values}
        onSubmit={(values) => {
          props.onSubmit(values)
        }}
        enableReinitialize={true}
        validate={validateForm}
      >
        {(formikProps) => {
          const {values, dirty, isValid} = formikProps

          return (
            <CustomForm id='ClientIndexForm' className={cn('form.class')}>
              <div className={cn('form.wrapper')}>
                <Header title={t('form.customerDataTitle')} icon={cn('form.customer.titleIcon')}/>
                <Customer
                  form={form}
                  values={values}
                  errors={props.errors}
                  promoCode={props.promoCode}
                  onChangePromoCode={props.onChangePromoCode}
                  onApplyPromoCode={props.onApplyPromoCode}/>
                {!_isEmpty(pickupData) && (
                  <Fragment>
                    <Header title={t('form.pickup.title')} icon={cn('form.pickup.titleIcon')}/>
                    <Pickup
                      form={pickup}
                      values={values}
                      errors={props.errors}/>
                  </Fragment>
                )}
                {participants && participants.length > 0 && (
                  <Fragment>
                    <Header title={t('form.participants.title')} icon={cn('form.participants.icon')}/>
                    <Attendees
                      participants={participants}
                      values={values}
                      lineItems={lineItems}/>
                  </Fragment>
                )}
                {props.totalAmount > 0 && props.allowInvoice && (
                  <Fragment>
                    <Header>
                      <Invoice
                        wantsInvoice={props.wantsInvoice}
                        form={invoice}
                        values={values}
                        toggleInvoice={props.toggleInvoice}/>
                    </Header>
                  </Fragment>
                )}
                {props.totalAmount > 0 && (
                  <Fragment>
                    <Header title={t('form.customerPayDataTitle')} icon={cn('form.payment.titleIcon')}/>
                    <Payment
                      paymentGateways={props.paymentGateways}
                    />
                    <Header/>
                  </Fragment>
                )}
                <TermsAndConditions
                  form={form}
                  values={values}
                  conditions={props.conditions}
                />
                <div className="cw-form-submit">
                  <button id='btnEnviar' type="submit" className="uk-button cw-form-submit-button"
                          disabled={!dirty || !isValid}>
                    {props.totalAmount > 0 ? t('generic.continue') : t('generic.confirm')}
                  </button>
                  {props.totalAmount > 0 && (
                    <div className="cw-form-submit-info">{t('form.orderWithMandatoryPayment')}</div>)}
                </div>
              </div>
            </CustomForm>
          )
        }}
      </Formik>
    )
  }

  return null
}

Form.propTypes = {
  promoCode: PropTypes.string,
  lineItems: PropTypes.array,
  wantsInvoice: PropTypes.bool,
  paymentGateways: PropTypes.array,
  pickupData: PropTypes.object,
  errors: PropTypes.object,
  toggleInvoice: PropTypes.func.isRequired,
  onApplyPromoCode: PropTypes.func.isRequired,
  onChangePromoCode: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired
}

export default Form
