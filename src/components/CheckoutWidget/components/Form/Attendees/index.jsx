'use strict'

import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import _get from 'lodash/get'
import _isArray from 'lodash/isArray'
import { useTranslation } from 'react-i18next'

import Context from '../../../context'
import AttendeesFormsByProduct from './AttendeesFormsByProduct'

function Attendees(props) { 
  const { t } = useTranslation('main')
  const { cn } = useContext(Context)

  function renderAttendeesFormsByProduct() {
    const participantForms = props.participants
    const lineItems = props.lineItems

    if (participantForms && _isArray(participantForms)) {
      return participantForms.map((participantForm, index) => {
        return <AttendeesFormsByProduct 
                  key={index} 
                  index={index} 
                  lineId={participantForm.line_item_id} 
                  lineItems={lineItems} 
                  participantForm={participantForm} 
                  values={props.values}
                  showHeader={participantForms.length > 1} />
      })
    }

    return null
  }

  if (props.participants) {
    return renderAttendeesFormsByProduct()
  }

  return null
}

Attendees.propTypes = {
  participants: PropTypes.array,
  lineItems: PropTypes.array,
  values: PropTypes.object.isRequired
}

export default Attendees
