"use strict";

import React, { useContext } from "react";
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";
import _get from "lodash/get";
import _isArray from "lodash/isArray";
import { parseISO, format } from "date-fns";

import Context from "../../../context";
import AttendeesForms from "./AttendeesForms";
import { FieldArray } from "formik";

function AttendeesFormsByProduct(props) {
  const { t } = useTranslation("main");
  const { cn } = useContext(Context);

  function getLineItem() {
    const lineId = props.lineId;
    const lineItems = props.lineItems;
    let lineItem = null;

    if (lineItems && _isArray(lineItems)) {
      lineItems.forEach((item) => {
        if (item.id == lineId) {
          lineItem = item;
        }
      });
    }

    return lineItem;
  }

  function renderHeader(lineItem) {
    if (lineItem) {
      let title = _get(lineItem, "product.name");
      let date = parseISO(_get(lineItem, "booking_date"));
      let experienceTitle = _get(lineItem, "product.experience.name");
      let available = _get(lineItem, "available");

      return (
        <div className={cn("form.participants.product.header.sectionContainer")}>
          <div className={cn("form.participants.product.header.section")} uk-grid="uk-grid">
            {!available ? (
              <div className={cn("form.participants.product.no_available")}>
                <span>{t("cart.no_available")}</span>
              </div>
            ) : null}
            <div
              className={
                "uk-vertical-align " +
                cn("form.participants.product.header.titleRow")
              }
            >
              <span>{experienceTitle}</span>
            </div>
            <div className={cn("form.participants.product.header.metadataRow")}>
              <div uk-grid="uk-grid">
                <div className="uk-width-1-1 uk-width-auto@m">
                  <span
                    className={cn(
                      "form.participants.product.header.metadataTitle"
                    )}
                  >
                    {title}{" "}
                  </span>
                  <i className="uk-visible@m fas fa-long-arrow-alt-right"></i>
                </div>
                <div
                  className={
                    "uk-width-expand uk-margin-remove " +
                    cn("form.participants.product.header.metadataDate")
                  }
                >
                  <span
                    className={cn(
                      "form.participants.product.header.metadataDateLabel"
                    )}
                  >
                    {t("generic.date")}:{" "}
                  </span>
                  <span
                    className={cn(
                      "form.participants.product.header.metadataDateContent"
                    )}
                  >
                    {format(date, "dd/MM/yy")}
                  </span>
                  <span
                    className={cn(
                      "form.participants.product.header.metadataDateLabel"
                    )}
                  >
                    {t("generic.hour")}:{" "}
                  </span>
                  <span
                    className={cn(
                      "form.participants.product.header.metadataDateContent"
                    )}
                  >
                    {format(date, "HH:mm")}
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    }
    return null;
  }

  if (props.participantForm) {
    const index = props.index;
    const form = _get(props, "participantForm.form");
    const lineItem = getLineItem();

    return (
      <div className={cn("form.participants.wrapper")} uk-grid="uk-grid">
        {props.showHeader && renderHeader(lineItem)}
        <FieldArray name={`participants[${index}].participants`}>
          <AttendeesForms
            index={index}
            form={form}
            lineItem={lineItem}
            values={props.values}
          />
        </FieldArray>
      </div>
    );
  }
  return null;
}

AttendeesFormsByProduct.propTypes = {
  index: PropTypes.number.isRequired,
  lineId: PropTypes.string.isRequired,
  lineItems: PropTypes.array.isRequired,
  participantForm: PropTypes.object.isRequired,
  values: PropTypes.object.isRequired,
};

export default AttendeesFormsByProduct;
