"use strict";

import React from "react";
import PropTypes from "prop-types";
import _get from "lodash/get";
import _isArray from "lodash/isArray";
import { useTranslation } from "react-i18next";

import AttendeeForm from "./AttendeeForm";

function AttendeesForms(props) {
  const { t } = useTranslation("main");

  function getQuantity() {
    const rates = _get(props, "lineItem.rates");
    let qty = 0;

    if (rates && _isArray(rates)) {
      qty = rates.reduce((result, rate) => result + rate.qty, 0);
    }

    return qty;
  }

  function renderAttendeesForms() {
    const qty = getQuantity();
    let forms = [];

    let subindex = 0;
    props.lineItem.rates.forEach((rate, index) => {
      for (let i = 0; i < rate.qty; i++, subindex++) {
        forms.push(
          <AttendeeForm
            key={`attendee-form-${subindex}`}
            index={props.index}
            subindex={subindex}
            form={props.form}
            values={props.values}
            rateId={rate.rate.id}
            rateIndex={i}
            rateName={rate.rate.name}
            rateElements={rate.rate.rate_elements}
          />
        );
      }
    });

    return forms;
  }

  return renderAttendeesForms();
}

AttendeesForms.propTypes = {
  index: PropTypes.number.isRequired,
  lineItem: PropTypes.object.isRequired,
  form: PropTypes.object.isRequired,
  values: PropTypes.object.isRequired,
};

export default AttendeesForms;
