"use strict";
import React, { useContext } from "react";
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";
import Context from "../../../context";
import Field from "../Fields/Field";

function AttendeeForm(props) {
  const { t } = useTranslation("main");
  const { cn } = useContext(Context);

  const nifChild = (rateElements) => {
    let nifType = "nif";
    if (!rateElements) {
        return false;
    }

    if (rateElements && rateElements.length > 0) {
      const ratesChildIds= [14];
      const rateChild = rateElements.find((element) => ratesChildIds.includes(element.id));
      if (rateChild) {
        nifType = "nif-child";
      }
    }
    return nifType !== "nif";
  }

  if (props.form) {
    const form = Object.fromEntries(
      Object.entries(props.form).map(([key, value]) => [
        key,
        { 
          ...value, 
          name: "form.participants." + value.name 
        },
      ])
    );

    const index = props.index;
    const subindex = props.subindex;
    if (nifChild(props?.rateElements)) {
      form.nif.type = "nif-child";
      form.nif.required = false;
    }

    return (
      <div className={cn("form.participants.row")}>
        <div className={cn("form.participants.wrapper")} uk-grid="uk-grid">
          <div className="col-sm-12">
            <span>
              <span>
                  <i className={cn("form.participants.icon")}></i>{" "}
              </span>
              {`${t("form.participant")} ${subindex + 1} (${props.rateName})`}
            </span>
          </div>
          <Field
            id={`participants[${index}].participants[${subindex}].first_name`}
            configId="first_name"
            config={form}
            values={props.values}
            className={cn("form.participants.first_name.field")}
            wrapperClassName={cn("form.participants.first_name.fieldWrapper")}
          />
          <Field
            id={`participants[${index}].participants[${subindex}].last_name`}
            configId="last_name"
            config={form}
            values={props.values}
            className={cn("form.participants.last_name.field")}
            wrapperClassName={cn("form.participants.last_name.fieldWrapper")}
          />
          <Field
            id={`participants[${index}].participants[${subindex}].nif`}
            configId="nif"
            config={form}
            values={props.values}
            className={cn("form.participants.nif.field")}
            wrapperClassName={cn("form.participants.nif.fieldWrapper")}
          />
        </div>
      </div>
    );
  }

  return null;
}

AttendeeForm.propTypes = {
  index: PropTypes.number.isRequired,
  subindex: PropTypes.number.isRequired,
  form: PropTypes.object.isRequired,
  values: PropTypes.object.isRequired
};

export default AttendeeForm;

