'use strict'

import React from 'react'
import PropTypes from 'prop-types'
import _get from 'lodash/get'
import { useTranslation } from 'react-i18next'
import { Field as FormField, ErrorMessage } from 'formik'
import { validate } from '../Validator'

function Field(props) { 
  const { t } = useTranslation('main')
  const InputTypes = {
    email: 'email',
    string: 'text',
    text: 'text'
  }

  if (props.id) {
    const id = props.id
    const configId = props.configId
    if (props.config) {
      const config = configId ? _get(props.config, configId) : _get(props.config, id)
      if (config) {
        const { type, required, name } = config

        return (
          <div className={props.wrapperClassName} aria-required={required}>
            { props.noLabel ? null :
              <label htmlFor={id}>{t(name)} { required ? <i className="required" aria-required={required}> *</i> : null} </label>
            }
            <FormField 
              className={props.className}
              name={id} 
              type={InputTypes[type]} 
              maxLength={255}
              placeholder={ props.placeholder ? t(name) : null }
              validate={validate.bind(this, config, t, props.values)} />
            
            <ErrorMessage name={ props.id }>
              {(msg) => <label className="cw-form-field-error">{msg}</label>}
            </ErrorMessage> 
          </div>
        )
      }
    }
  }

  return null
}

Field.propTypes = {
  id: PropTypes.string.isRequired,
  config: PropTypes.object,
  values: PropTypes.object,
  wrapperClassName: PropTypes.string,
  fieldClassName: PropTypes.string,
  noLabel: PropTypes.bool,
  placeholder: PropTypes.bool,
}

export default Field