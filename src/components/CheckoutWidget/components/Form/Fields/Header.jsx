'use strict'

import React, { useContext } from 'react'
import PropTypes from 'prop-types'

import Context from '../../../context'

function Header(props) { 
  const { cn } = useContext(Context)

  return (
    <div className={ cn('form.header.row') } uk-grid="uk-grid">
      <div className={ "uk-width-1-1 " + cn('form.header.wrapper') }>
        <span id='clientSectionHeader' className={ cn('form.header.type') }>
          { props.icon ? <span><i className={props.icon}></i> </span> : props.icon } 
          { props.title ? props.title : null}
        </span>
      </div>
    </div>
  )

  return null
}

Header.propTypes = {
  title: PropTypes.string,
  icon: PropTypes.string
}

export default Header