"use strict";

import React from "react";
import PropTypes from "prop-types";
import Select, { Option, ReactSelectProps } from "react-select";
import _get from "lodash/get";
import _isArray from "lodash/isArray";
import { useTranslation } from "react-i18next";
import { Field, ErrorMessage } from "formik";
import { validate } from "../Validator";

function SelectField(props) {
  const { t } = useTranslation("main");

  const customStyles = {
    control: (provided, state) => ({
      ...provided,
      minHeight: "28px",
      height: "28px",
      boxShadow: state.isFocused ? null : null,
    }),

    valueContainer: (provided, state) => ({
      ...provided,
      height: "28px",
      padding: "0 6px",
    }),

    input: (provided, state) => ({
      ...provided,
      margin: "0px",
    }),
    indicatorSeparator: (state) => ({
      display: "none",
    }),
    indicatorsContainer: (provided, state) => ({
      ...provided,
      height: "28px",
    }),
  };

  const prepareOptions = (options, emptyOption) => {
    if (_isArray(options)) {
      const result = options.map((option) => {
        return { value: option.id, label: option.name };
      });

      if (emptyOption == null) {
        return result;
      }

      return [{ value: "-1", label: emptyOption }].concat(result);
    }

    return [];
  };

  const getOption = (options, value) =>
    options && value ? options.find((option) => option.value == value) : null;

  if (props.id) {
    const id = props.id;
    const configId = props.configId;
    if (props.config) {
      const config = configId
        ? _get(props.config, configId)
        : _get(props.config, id);
      if (config) {        
        if (props.loadOptions) {
          config.options = props.loadOptions();
        }
        const {
          type,
          required,
          options,
          name,
          placeholder,
          emptyOption,
        } = config;

        return (
          <div className={props.wrapperClassName} aria-required={required}>
            {props.noLabel ? null : (
              <label htmlFor={id}>
                {t(name)}{" "}
                {required ? (
                  <i className="required" aria-required={required}>
                    {" "}
                    *
                  </i>
                ) : null}{" "}
              </label>
            )}
            <Field
              name={id}
              className={props.className}
              validate={validate.bind(this, config, t, props.values)}
            >
              {({
                field,
                form: {
                  errors,
                  isSubmitting,
                  touched,
                  setFieldTouched,
                  setFieldValue,
                },
              }) => {
                return (
                  <Select
                    isSearchable
                    options={prepareOptions(
                      options,
                      emptyOption ? t(emptyOption) : null
                    )}
                    placeholder={t(placeholder)}
                    value={getOption(options, field.value)}
                    styles={customStyles}
                    onBlur={() => {
                      setFieldTouched(id, true);
                    }}
                    onChange={(selected) => {
                      setFieldValue(id, selected.value);
                      if (props.onSelect) {
                        props.onSelect(selected.value);
                      }
                    }}
                  />
                );
              }}
            </Field>
            <ErrorMessage name={props.id}>
              {(msg) => <label className="cw-form-field-error">{msg}</label>}
            </ErrorMessage>
          </div>
        );
      }
    }
  }

  return null;
}

SelectField.propTypes = {
  id: PropTypes.string.isRequired,
  config: PropTypes.object,
  values: PropTypes.object,
  wrapperClassName: PropTypes.string,
  className: PropTypes.string,
  noLabel: PropTypes.bool,
  loadOptions: PropTypes.func,
  onSelect: PropTypes.func,
};

export default SelectField;
