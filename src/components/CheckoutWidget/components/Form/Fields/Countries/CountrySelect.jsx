import React, { Component } from 'react'

import AllCountries from './AllCountries'

class CountrySelect extends Component {
  constructor(props) {
    super(props)
    
    this.countries = AllCountries.getCountries()
    this.indexedCountries = this.countries.reduce((result, country) => {
      result[country.iso2] = country.name
      return result 
    }, {})
    
    this.state = { 
      country: this.props.defaultCountry,
      showList: false 
    }
  }

  selectCountry = (value) => {
    this.setState({ 
      country: value,
      showList: false
    }, this.props.onChange(value))
  }

  expand = () => {
    this.setState({ showList: true })
  }

  collapse = () => {
    this.setState({ showList: false })
  }

  buildCountryList() {
    return <ul className="country-list"> 
      { 
        this.countries.map((country) => {
          return <li key={country.iso2} className="country" onClick={this.selectCountry.bind(this, country.iso2)}>
            <div className="flag-box">
              <div className={`iti-flag ${country.iso2}`}></div>
            </div>
            <span className="country-name">{ country.name }</span>
          </li>
        })
      }
    </ul>
  }

  render() {
    const { country } = this.state

    return (
      <div className="allow-dropdown intl-tel-input" tabIndex="0" onBlur={ this.collapse } onClick={ this.expand }>
        <input 
          type="text" 
          autoComplete="off" 
          className={this.props.inputClassName} 
          name={this.props.id} 
          id={this.props.id} 
          onChange={()=>{}}
          value={ this.indexedCountries[country] } />
        <div className="flag-container" tabIndex="0" onBlur={ this.collapse }>
          <div className="selected-flag">
            <div className={`iti-flag ${country}`}></div>
            <div className="arrow down">
            </div>
          </div>
          { this.state.showList ? this.buildCountryList() : null }
        </div>
      </div>
    )
  }
}

export default CountrySelect