'use strict'

import React from "react"
import PropTypes from 'prop-types'
import _get from 'lodash/get'
import { useTranslation } from 'react-i18next'
import { Field, ErrorMessage } from 'formik'
import { validate } from '../Validator'

function Checkbox(props) {
  const { t } = useTranslation('main')

  if (props.id) {
    const id = props.id
    const configId = props.configId
    if (props.config) {
      const config = configId ? _get(props.config, configId) : _get(props.config, id)
      if (config) {
        const { type, required, name } = config
        return (
          <div className={props.wrapperClassName}>
            <label htmlFor={ props.id } className="cw-form-field-check-label">
              <Field 
                type="checkbox" 
                id={ props.id }
                name={ props.id } 
                className={props.className}
                validate={validate.bind(this, config, t, props.values)}  />
              { props.noLabel ? null : <span dangerouslySetInnerHTML={{ __html: t(name) }}></span> }
              { required ? <i className="required" aria-required={required}> *</i> : null}
              { props.info ? <div className="cw-form-check-field-info"><p>{ t(props.info) }</p></div> : null }
            </label>
            
            <ErrorMessage name={ props.id }>
              {(msg) => <label className="cw-form-field-error">{msg}</label>}
            </ErrorMessage>             
          </div>
        )
      }
    }
  }

  return null
}

Checkbox.propTypes = {
  id: PropTypes.string.isRequired,
  config: PropTypes.object,
  values: PropTypes.object,
  wrapperClassName: PropTypes.string,
  fieldClassName: PropTypes.string,
  noLabel: PropTypes.bool
}

export default Checkbox