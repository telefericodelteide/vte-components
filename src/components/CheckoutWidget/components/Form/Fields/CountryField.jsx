'use strict'

import React from 'react'
import PropTypes from 'prop-types'
import _get from 'lodash/get'
import { useTranslation } from 'react-i18next'
import { Field, ErrorMessage } from 'formik'

import { validate } from '../Validator'
import CountrySelect from './Countries/CountrySelect'

function CountryField(props) {
  const { t } = useTranslation('main')

  if (props.id) {
    const id = props.id
    if (props.config) {
      const config = _get(props.config, id)
      if (config) {
        const { type, required, name } = config

        return (
          <div className={props.wrapperClassName} aria-required="true">
            { props.noLabel ? null :
              <label htmlFor="ClientName">{name} { required ? <i className="required" aria-required="true"> *</i> : null} </label>
            }
            <Field
              name={id}
              validate={validate.bind(this, config, t, props.values)} >
                {({field, form: {errors, isSubmitting, touched, setFieldTouched, setFieldValue}}) => {
                  return (
                    <CountrySelect
                      defaultCountry='es'
                      preferredCountries={['es', 'gb', "fr", "de", 'it', "ru", "us", "pl", "nl"]}
                      inputClassName={props.className}
                      defaultValue={field.value}
                      id={id}
                      onChange={(value) => {
                        setFieldValue(id, value)
                      }}
                    />
                  )
                }}
            </Field>
            <ErrorMessage name={ props.id }>
              {(msg) => <label className={'error'}>{msg}</label>}
            </ErrorMessage> 
          </div>
        )
      }
    }
  }
  return null
} 

CountryField.propTypes = {
  id: PropTypes.string.isRequired,
  config: PropTypes.object.isRequired
}

export default CountryField