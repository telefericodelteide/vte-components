'use strict'

import React from 'react'
import PropTypes from 'prop-types'
import _get from 'lodash/get'
import { useTranslation } from 'react-i18next'
import { Field, ErrorMessage } from 'formik'
import IntlTelInput from 'react-intl-tel-input'
import 'react-intl-tel-input/dist/main.css'

import { validate } from '../Validator'

function PhoneField(props) {
  const { t } = useTranslation('main')

  function formatPhoneNumberOutput(isValid, newNumber, countryData, fullNumber, isExtension) {
    if (isValid && fullNumber) {
      return fullNumber.replace(/(\s|-)/g, '')
    }

    if (fullNumber) {
      return 'invalid_phone_number'
    }

    return null
  }

  if (props.id) {
    const id = props.id
    if (props.config) {
      const config = _get(props.config, id)
      if (config) {
        const { type, required, name } = config

        return (
          <div className={props.wrapperClassName} aria-required={required}>
            { props.noLabel ? null :
              <label htmlFor={id}>{ t(name) } { required ? <i className="required" aria-required="true"> *</i> : null} </label>
            }
            <Field
              name={id}
              validate={validate.bind(this, config, t, props.values)} >
                {({field, form: {errors, isSubmitting, touched, setFieldTouched, setFieldValue}}) => {
                  return (
                    <IntlTelInput
                      defaultCountry='es'
                      preferredCountries={['es', 'gb', "fr", "de", 'it', "ru", "us", "pl", "nl"]}
                      customPlaceholder={(selectedCountryPlaceholder) => {
                        return selectedCountryPlaceholder.replace(/[0-9]/g,"X")
                      }}
                      containerClassName="intl-tel-input"
                      inputClassName={props.className}
                      defaultValue={field.value}
                      fieldId={id}
                      fieldName={id}
                      onPhoneNumberBlur={() => {
                        setFieldTouched(id, true)
                      }}
                      onPhoneNumberChange={(...args) => {
                        setFieldValue(id, formatPhoneNumberOutput(...args))
                      }}
                    />
                  )
                }}
            </Field>            
            <ErrorMessage name={ props.id }>
              {(msg) => <label className="cw-form-field-error">{msg}</label>}
            </ErrorMessage> 
          </div>
        )
      }
    }
  }
  return null
} 

PhoneField.propTypes = {
  id: PropTypes.string.isRequired,
  config: PropTypes.object.isRequired
}

export default PhoneField