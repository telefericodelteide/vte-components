'use strict'

import React from 'react'
import PropTypes from 'prop-types'
import _get from 'lodash/get'
import { useTranslation } from 'react-i18next'
import { Field as FormField, ErrorMessage } from 'formik'
import { validate } from '../Validator'

function Textarea(props) { 
  const { t } = useTranslation('main')

  if (props.id) {
    const id = props.id
    if (props.config) {
      const config = _get(props.config, id)
      if (config) {
        const { type, required, name } = config
      
        return (
          <div className={props.wrapperClassName} aria-required={required}>
            <label htmlFor={id}>{ t(name) } { required ? <i className="required" aria-required="true"> *</i> : null} </label>
            <FormField className={props.className} name={id} component='textarea' rows={4} validate={validate.bind(this, config, t, props.values)} />
            <ErrorMessage name={ props.id }>
              {(msg) => <label className={'error'}>{msg}</label>}
            </ErrorMessage> 
            {props.description && (
            <span className={props.descriptionClassName}>
              {props.description}
            </span>)}  
          </div>
        )
      }
    }
  }

  return null
}

Textarea.propTypes = {
  id: PropTypes.string.isRequired,
  config: PropTypes.object,
  description: PropTypes.string
}

export default Textarea