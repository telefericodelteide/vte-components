'use strict'

import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import _isArray from 'lodash/isArray'
import { useTranslation } from 'react-i18next'
import { Field, ErrorMessage } from 'formik'

import paymentGatewayImages from '../../../config/paymentGatewayImages'
import Context from '../../../context'
import { validate } from '../Validator'

function Payment(props) { 
  const { t } = useTranslation('main')
  const { cn } = useContext(Context)

  function renderRadioButtons(id, paymentGateways) {
    if (_isArray(paymentGateways)) {
      return paymentGateways.map((paymentGateway) => {
        return <div key={ paymentGateway.payment_method } className={ cn('form.payment.methodWrapper') }>
            <label>
              <div>
                <Field 
                  type="radio" 
                  name={id} 
                  value={paymentGateway.id} 
                  className="uk-radio"
                  validate={validate.bind(this, { required: true }, t, [])} />
                <img 
                  src={ paymentGatewayImages[paymentGateway.payment_method] } 
                  title={ t(paymentGateway.payment_method) } 
                  alt={ t(paymentGateway.payment_method) } />
              </div>
            </label>
          </div>
      })
    }
    return null
  }

  if (props.paymentGateways) {
    const id = 'payment_gateway_id'
    const paymentGateways = props.paymentGateways
    return <div key='payment-gateways' className={ cn('form.payment.row') }>
        <div className={ cn('form.payment.wrapper') }>
          <div className="uk-flex">
            { renderRadioButtons(id, paymentGateways) }
          </div>
        </div>
        <div className={ cn('form.payment.wrapper')}>
          <ErrorMessage name={ id }>
            {(msg) => <label className={'error'}>{msg}</label>}
          </ErrorMessage>
        </div>
      </div>
  }
  return null
}

Payment.propTypes = {
  paymentGateways: PropTypes.array
}

export default Payment