"use strict";

import React, { useContext, useEffect, useState } from "react";
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";

import Context from "../../../context";
import SelectField from "../Fields/SelectField";
import Textarea from "../Fields/Textarea";
import _isEmpty from "lodash/isEmpty";
import _get from "lodash/get";
import { useFormikContext } from "formik";

function PickupInfo(props) {
  const { t } = useTranslation("main");
  const { cn } = useContext(Context);

  const {name, pickup_time} = props;

  return (
    <div key="pickup-info" className="uk-width-1-1">
      <div uk-grid="uk-grid" className={cn("form.pickup.info.wrapper")}>
        <div className={cn("form.pickup.info.title")}>
          {t("form.pickup.pickup_info.title")}:
        </div>
        <div className={cn("form.pickup.info.field")}>
          <span className={cn("form.pickup.info.label")}>{t("form.pickup.pickup_info.venue")}: </span>
          <span className={cn("form.pickup.info.text")}>
            {name}
          </span>
        </div>
        <div className={cn("form.pickup.info.field")}>
          <span className={cn("form.pickup.info.label")}>{t("form.pickup.pickup_info.time")}: </span>
          <span className={cn("form.pickup.info.text")}>
            {pickup_time}
          </span>
        </div>
      </div>
    </div>
  );
}

function Pickup(props) {
  const { t } = useTranslation("main");
  const { cn } = useContext(Context);

  const [lodgin, setLodgin] = useState(null);
  const [zone, setZone] = useState(null);
  const [pickupPoint, setPickupPoint] = useState(null);
  const [selectedPickupPoint, setSelectedPickupPoint] = useState(null);

  const availableLodgins = _get(props, "form.pickup.lodgin_id.options");
  const availableZones = _get(props, "form.pickup.zone_id.options");
  const availablePickupPoints = _get(props, "form.pickup.pickup_point_id.options");
  const formMode = _get(props, 'mode', 'lodgin');

  const formikProps = useFormikContext();

  const onLodginSelectedHandler = (selected) => {
    setLodgin(selected);

    if (selected != null && selected != -1) {
      const lodgin = availableLodgins.find((l) => l.id == selected);
      setSelectedPickupPoint(availablePickupPoints.find((pp) => pp.id == lodgin.pickup_point_id));
    } else {
      setSelectedPickupPoint(null);
    }

    setZone(null);
    setPickupPoint(null);

    formikProps.setFieldValue("pickup.zone_id", null);
    formikProps.setFieldValue("pickup.pickup_point_id", null);
  };

  const onZoneSelectedHandler = (selected) => {
    setZone(selected);
    
    setPickupPoint(null);
    setSelectedPickupPoint(null);
    
    formikProps.setFieldValue("pickup.pickup_point_id", null);
  };

  const onPickupPointSelectHandler = (selected) => {
    setPickupPoint(selected);
    
    if (selected != null && selected != -1) {
      const result = availablePickupPoints.find((pp) => pp.id == selected);
      setSelectedPickupPoint(result);
    } else {
      setSelectedPickupPoint(null);
    }
  };

  const checkStep = (step) => {
    switch (step) {
      case "lodgin":
        return !_isEmpty(availableLodgins);
      case "zone":
        return (
          formMode == 'zone' &&
          ((lodgin == null && !checkStep("lodgin")) || lodgin == -1) &&
          !_isEmpty(availableZones)
        );
      case "pickup_point":
        return (
          formMode == 'zone' &&
          ((lodgin == null && !checkStep("lodgin")) || lodgin == -1) &&
          ((zone == null && !checkStep("zone")) || zone != null) &&
          !_isEmpty(availablePickupPoints)
        );
      case "observations":
        return (
          ((lodgin == null && !checkStep("lodgin")) || lodgin == -1) &&
          ((zone == null && !checkStep("zone")) || zone != null) &&
          ((pickupPoint == null && !checkStep("pickup_point")) ||
            pickupPoint == -1)
        );
    }
  };

  const filterOptionsByZone = (zone, options) => {
    if (zone == null || zone == -1) {
      return options;
    }

    return options.filter((option) => option.zone_id != zone);
  };

  return (
    <div key="pickup" className={cn("form.pickup.row")} uk-grid="uk-grid">
      <div
        key="pickup-data"
        className={cn("form.pickup.wrapper")}
        uk-grid="uk-grid"
      >
        {checkStep("lodgin") && (
          <SelectField
            id="pickup.lodgin_id"
            config={props.form}
            values={props.values}
            className={cn("form.pickup.field.main")}
            wrapperClassName={cn("form.pickup.field.wrapper")}
            onSelect={onLodginSelectedHandler}
          />
        )}
        {checkStep("zone") && (
          <SelectField
            id="pickup.zone_id"
            config={props.form}
            values={props.values}
            className={cn("form.pickup.field.main")}
            wrapperClassName={cn("form.pickup.field.wrapper")}
            onSelect={onZoneSelectedHandler}
          />
        )}
        {checkStep("pickup_point") && (
          <SelectField
            id="pickup.pickup_point_id"
            config={props.form}
            values={props.values}
            className={cn("form.pickup.field.main")}
            wrapperClassName={cn("form.pickup.field.wrapper")}
            loadOptions={() =>
              filterOptionsByZone(
                zone,
                props.form.pickup.pickup_point_id.options
              )
            }
            onSelect={onPickupPointSelectHandler}
          />
        )}
        {checkStep("observations") && [
          <Textarea
            id="pickup.observations"
            config={props.form}
            values={props.values}
            className={cn("form.pickup.observations.main")}
            wrapperClassName={cn("form.pickup.observations.wrapper")}
            description={t("form.pickup.observations.description")}
            descriptionClassName={cn("form.pickup.observations.description")}
          />,
        ]}
      </div>
      {selectedPickupPoint && <PickupInfo {...selectedPickupPoint} />}
    </div>
  );
}

Pickup.propTypes = {
  form: PropTypes.object.isRequired,
  values: PropTypes.object.isRequired,
  errors: PropTypes.object,
};

export default Pickup;
