"use strict";

import React, { Fragment, useContext } from "react";
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";

import Context from "../../../context";
import Field from "../Fields/Field";
import CountryField from "../Fields/CountryField";
import SelectField from "../Fields/SelectField";

function Invoice(props) {
  const { t } = useTranslation("main");
  const { cn } = useContext(Context);
  const vatNumberTypes = () => {
    return props.form.invoice.vat_number_type.options.map((option) => {
      return {
        id: option.id,
        name: t(option.name)
      }
    })    
  } 

  const buildWantsInvoiceCheck = () => (
    <div
      key="name"
      className={cn("form.invoice.wantsInvoice.row")}
      uk-grid="uk-grid"
    >
      <div className={cn("form.invoice.wantsInvoice.fieldWrapper")}>
        <div>
          <label
            id="want-invoice"
            htmlFor={props.id}
            className="cw-form-field-check-label"
          >
            <input
              type="checkbox"
              className={cn("form.invoice.wantsInvoice.field")}
              checked={props.wantsInvoice}
              onChange={props.toggleInvoice}
            />
            {t("form.invoice.wantsInvoice")}
          </label>
        </div>
      </div>
    </div>
  );

  if (props.form) {
    const form = props.form;
    return (
      <Fragment>
        {buildWantsInvoiceCheck()}
        {props.wantsInvoice && (
          <Fragment>
            <div
              key="name"
              className={cn("form.invoice.name.row")}
              uk-grid="uk-grid"
            >
              <Field
                id="invoice.name"
                config={form}
                values={props.values}
                className={cn("form.invoice.name.field")}
                wrapperClassName={cn("form.invoice.name.fieldWrapper")}
              />
            </div>
            <div
              key="vatType"
              className={cn("form.invoice.vat_number_type.row")}
              uk-grid="uk-grid"
            >
              <SelectField
                  id="invoice.vat_number_type"
                  config={form}
                  values={props.values}
                  className={cn("form.invoice.vat_number_type.field")}
                  wrapperClassName={cn("form.invoice.vat_number_type.fieldWrapper")}
                  loadOptions={vatNumberTypes}
              />
            </div>
            <div
              key="vat"
              className={cn("form.invoice.vat_number.row")}
              uk-grid="uk-grid"
            >
              <Field
                id="invoice.vat_number"
                config={form}
                values={props.values}
                className={cn("form.invoice.vat_number.field")}
                wrapperClassName={cn("form.invoice.vat_number.fieldWrapper")}
              />
            </div>
            <div
              key="address"
              className={cn("form.invoice.address.row")}
              uk-grid="uk-grid"
            >
              <div className={cn("form.invoice.address.fieldWrapper")}>
                <label htmlFor={"invoice.address"}>
                  {t("form.address")}
                  <i className="required" aria-required={true}>
                    {" "}
                    *
                  </i>
                </label>
                <div className="uk-grid-row-collapse uk-grid-column-small" uk-grid="uk-grid">
                  <Field
                    id="invoice.address"
                    config={form}
                    values={props.values}
                    noLabel={true}
                    placeholder={true}
                    className={cn("form.invoice.address.address.field")}
                    wrapperClassName={cn(
                      "form.invoice.address.address.fieldWrapper"
                    )}
                  />
                  <Field
                    id="invoice.postal_code"
                    config={form}
                    noLabel={true}
                    placeholder={true}
                    values={props.values}
                    className={cn("form.invoice.address.postal_code.field")}
                    wrapperClassName={cn(
                      "form.invoice.address.postal_code.fieldWrapper"
                    )}
                  />
                  <Field
                    id="invoice.locality"
                    config={form}
                    noLabel={true}
                    placeholder={true}
                    values={props.values}
                    className={cn("form.invoice.address.locality.field")}
                    wrapperClassName={cn(
                      "form.invoice.address.locality.fieldWrapper"
                    )}
                  />
                  <Field
                    id="invoice.state"
                    config={form}
                    noLabel={true}
                    placeholder={true}
                    values={props.values}
                    className={cn("form.invoice.address.state.field")}
                    wrapperClassName={cn(
                      "form.invoice.address.state.fieldWrapper"
                    )}
                  />
                  <CountryField
                    id="invoice.country_id"
                    config={form}
                    noLabel={true}
                    placeholder={true}
                    values={props.values}                    
                    className={cn("form.invoice.address.country_id.field")}
                    wrapperClassName={cn("form.invoice.address.country_id.fieldWrapper")}
                  />
                </div>
              </div>
            </div>
          </Fragment>
        )}
      </Fragment>
    );
  }
  return null;
}

Invoice.propTypes = {
  form: PropTypes.object.isRequired,
  values: PropTypes.object.isRequired,
  wantsInvoice: PropTypes.bool,
  toggleInvoice: PropTypes.func.isRequired,
};

export default Invoice;
