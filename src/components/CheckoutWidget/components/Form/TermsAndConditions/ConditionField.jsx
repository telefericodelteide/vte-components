"use strict";

import React, { useState } from "react";
import PropTypes from "prop-types";
import UIkit from "uikit";
import _get from "lodash/get";
import _isObject from "lodash/isObject";
import { Field, ErrorMessage } from "formik";
import { validate } from "../Validator";
import { useTranslation } from "react-i18next";
import Modal from "../../../../Generic/Modal";

function ConditionField(props) {
  const { t } = useTranslation("main");
  const [checkClicked, setCheckClicked] = useState(false);  

  if (props.id && props.config) {
    const { type, required, name, info, modal } = props.config;

    const modalId = "modal-" + props.id.replace(/\./g, "-");

    const onCheckChange = event => {    
      if (_isObject(modal)) {
        if (!checkClicked) {
          UIkit.modal("#" + modalId, {container: "#checkout-widget"}).show();
          setCheckClicked(true);
        }         
      }
    }

    return (
      <div className={props.wrapperClassName}>
        <label
          htmlFor={props.id}
          className="cw-form-field-check-label"
          onClick={onCheckChange}            
        >
          {_isObject(modal) && (
            <Modal 
              id={modalId}
              className="cw-form-field-check-modal"
              header={modal.title}             
            >
              {modal.content}
            </Modal>          
          )}
          <Field
            type="checkbox"
            id={props.id}
            name={props.id}
            className={props.className}
            validate={validate.bind(this, props.config, t, props.values)}
          />
          {props.noLabel ? null : (
            <span dangerouslySetInnerHTML={{ __html: name }} />
          )}
          {required ? (
            <i className="required" aria-required={required}>
              {" "}
              *
            </i>
          ) : null}
          {info ? (
            <div className="cw-form-check-field-info" dangerouslySetInnerHTML={{ __html: info }} />
          ) : null}
        </label>

        <ErrorMessage name={props.id}>
          {(msg) => <label className="cw-form-field-error">{msg}</label>}
        </ErrorMessage>
      </div>
    );
  }

  return null;
}

ConditionField.propTypes = {
  id: PropTypes.string.isRequired,
  config: PropTypes.object,
  values: PropTypes.object,
  wrapperClassName: PropTypes.string,
  fieldClassName: PropTypes.string,
  noLabel: PropTypes.bool,
};

export default ConditionField;
