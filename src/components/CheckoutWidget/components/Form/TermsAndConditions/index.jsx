'use strict'

import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import { useTranslation } from 'react-i18next'
import _get from 'lodash/get'
import _concat from 'lodash/concat'
import Context from '../../../context'
import ConditionField from './ConditionField'

function TermsAndConditions(props) { 
  const { t } = useTranslation('main')
  const { cn } = useContext(Context)
  
  if (props.conditions) {
    const form = props.form

    const defaultConfig = {
      type: "checkbox",
    }

    const conditions = props.conditions.map(condition => {
      return {
        ...defaultConfig,        
        id: condition.key,
        name: condition.title,
        info: condition.info,
        required: condition.required,
        modal: _get(condition, 'modal')    
      }
    });

    return (
        <div className={ cn('form.terms.row') } uk-grid="uk-grid">
          {conditions.map(condition => (
            <ConditionField 
              key={condition.id}
              id={condition.id}
              config={condition} 
              values={props.values}
              className={ cn('form.terms.field') }
              wrapperClassName={ cn('form.terms.fieldWrapper') } />
            ))}
          <div className={ cn('form.terms.fieldWrapper') }>
            <span><em>{ t('form.mandatoryFieldsMessage') }</em></span>
          </div>          
        </div>
      )
  }

  return null
}

TermsAndConditions.propTypes = {
  form: PropTypes.object.isRequired,
  values: PropTypes.object.isRequired,
  conditions: PropTypes.array.isRequired
}

export default TermsAndConditions