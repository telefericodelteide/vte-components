'use strict'

import _get from 'lodash/get'
import _isEmpty from 'lodash/isEmpty'

function validate(config, t, values, value) {
  const { type, required, equalTo } = config

  if (required) {
    if (!value) {
      return t('form.validations.required')
    }
  }

  if (type === 'email') {
    if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.\w+$/i.test(value)) {
      return t('form.validations.email')
    }

    if (equalTo) {
      let valueOtherField = _get(values, equalTo) 
      if (valueOtherField !== value) {
        return t('form.validations.equalTo')
      }
    }
  }

  if (type === 'phone') {
    if (value === 'invalid_phone_number') {
      return t('form.validations.phone')
    }
  }

  if (type === "nif") {
    const error = haveErrorPersonalNumber(value, t);
    if (error) {
      return error
    }
  }

  if (type === "nif-child") {
    const error = haveErrorPersonalNumberChild(value, t);
    if (error) {
      return error
    }
  }

  return null
}

const haveErrorPersonalNumber = (value, t) => {
  let error = false;

  if (_isEmpty(value)) {
   return t('form.client.personal_identification_required');
  }
  value = value.replace(/ /g, "")
  if (value.length < 6) {
    error = t('form.client.personal_identification_min');
  }

  return error;
}

const haveErrorPersonalNumberChild = (value, t) => {
  let error = false;
  if (_isEmpty(value)) {
    return error;
  }
  value = value.replace(/ /g, "")
  if (value.length < 6) {
    error = t('form.client.personal_identification_min');
  }

  return error;
}

export {
  validate
}
