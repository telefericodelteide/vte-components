'use strict'

import React, { useContext } from 'react'
import _isEmpty from 'lodash/isEmpty'
import _isObject from 'lodash/isObject'
import _reduce from 'lodash/reduce'

import Context from '../../context'

function Alert(props) { 
  const { cn } = useContext(Context)

  function renderErrors() {
    let errors = []
    if (_isObject(props.errors)) {
      errors = _reduce(props.errors, (result, value, key) => {
        if (!_isEmpty(value)) {
          result.push(<p key={key}>{ value }</p>)
        }
        return result
      }, [])
    }
    if (!_isEmpty(errors)) {
      return errors
    }
    return null
  }

  const errors = renderErrors()

  if (errors) {
    return (
      <div className={ cn('error.wrapper') }>
        <div key='alert' className={ cn('error.content') } role="alert">
          { errors }
        </div>
      </div>
    )
  }

  return null
}

export default Alert