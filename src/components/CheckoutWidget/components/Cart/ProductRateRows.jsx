'use strict'

import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import _get from 'lodash/get'
import _isArray from 'lodash/isArray'
import ProductRateRow from './ProductRateRow'
import Context from '../../context'

function ProductRateRows(props) { 
  const { cn } = useContext(Context);
  
  function renderProductRateRows() {
    const rates = _get(props, 'item.rates')
    
    if (rates && _isArray(rates)) {
      return rates.map((rate, index) => {
        return <ProductRateRow key={`rate-${index}`} rate={rate} />
      })
    }

    return null
  }

  return (
    <div className={ cn('cart.product.rate.section') }>
      {renderProductRateRows()}
    </div>
    
  )
}

ProductRateRows.propTypes = {
  rates: PropTypes.array
}

export default ProductRateRows