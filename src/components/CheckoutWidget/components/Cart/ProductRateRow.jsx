'use strict'

import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import _get from 'lodash/get'
import _isNumber from 'lodash/round'
import _round from 'lodash/round'
import { useTranslation } from 'react-i18next'

import Context from '../../context'

function ProductRateRow(props) { 
  const { t, i18n } = useTranslation('main')
  const { cn } = useContext(Context) 
  
  if (props.rate) {
    const priceFormatter = new Intl.NumberFormat(i18n.language, {
      style: "currency",
      currency: "eur"
    })
  
    const name = props.rate.rate.rate_elements.sort((ct1, ct2) => ct1.type - ct2.type).map(ct => ct.name).join(", ")
    const qty = _get(props.rate, 'qty')
    const unitPrice = _get(props.rate, 'rate.unit_price')
    const totalAmount = _get(props.rate, 'total_amount')
    const roundUnitPrice = priceFormatter.format(parseFloat(unitPrice))
    const roundTotalAmount = priceFormatter.format(parseFloat(totalAmount))

    return (
      <div className={ "uk-grid-divider uk-grid-column-small " + cn('cart.product.rate.row') } uk-grid="uk-grid" >
        <div className={ "uk-width-1-2 uk-width-1-4@m " + cn('cart.product.rate.fareType.cell') }>
          <span className={ cn('cart.product.rate.fareType.content') }>{ name }</span>
        </div>
        <div className={ "uk-width-1-2 uk-width-1-6@m " + cn('cart.product.rate.price.cell') }>
          <span className={ cn('cart.product.rate.price.content') }>{ roundUnitPrice }</span>
        </div>
        <div className={ "uk-width-1-1 uk-width-1-4@m " + cn('cart.product.rate.participants.cell') }>
          <span className={ cn('cart.product.rate.participants.label') }>{t('cart.attendees')}: </span>
          <span className={ cn('cart.product.rate.participants.content') }>{ qty }</span>
        </div>
        <div className={ "uk-width-1-1 uk-width-1-6@m " + cn('cart.product.rate.subtotal.cell') }>
          <span className={ cn('cart.product.rate.subtotal.label') }>{t('cart.total')}: </span>
          <span className={ cn('cart.product.rate.subtotal.content') }>{ roundTotalAmount }</span>
        </div>
      </div>
    )
  }
  return null
}

ProductRateRow.propTypes = {
  rate: PropTypes.object.isRequired
}

export default ProductRateRow