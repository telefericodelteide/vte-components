'use strict'

import React, { useContext } from 'react'
import { useTranslation } from 'react-i18next'
import _get from 'lodash/get'

import Context from '../../context'

function ProductTotal(props) { 
  const { t, i18n } = useTranslation('main')
  const { cn } = useContext(Context)

  const priceFormatter = new Intl.NumberFormat(i18n.language, {
    style: "currency",
    currency: "eur"
  })

  const totalAmount = _get(props.item, 'total_amount')
  const roundTotalAmount= priceFormatter.format(totalAmount ? parseFloat(totalAmount) : 0)

  return (
    <div className={ cn('cart.product.subtotal.row') } >
      <div className={ cn('cart.product.subtotal.wrapper') }>        
        <span className={ cn('cart.product.subtotal.label') }>{t('cart.productTotal')}: </span>
        <span className={ cn('cart.product.subtotal.content') }>{ roundTotalAmount }</span>
      </div>
    </div>
  )
}

export default ProductTotal