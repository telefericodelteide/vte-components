'use strict'

import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import { useTranslation } from 'react-i18next'

import Context from '../../context'

function ProductDeleteButton(props) { 
  const { t } = useTranslation('main')
  const { cn } = useContext(Context)

  return (
    <div className={ "uk-float-right " + cn('cart.product.delete.wrapper') }>
      <a href="" className="uk-icon-button" title={t('generic.remove')} onClick={props.onDelete}><i className={ cn('cart.product.delete.icon') }></i></a>
    </div>
  )
}

ProductDeleteButton.propTypes = {
  onDelete: PropTypes.func.isRequired
}

export default ProductDeleteButton