"use strict";

import React, { useContext } from "react";
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";
import _get from "lodash/get";
import { parseISO, format } from "date-fns";

import Context from "../../context";
import ProductRateRows from "./ProductRateRows";
import ProductTotal from "./ProductTotal";
import ProductDeleteButton from "./ProductDeleteButton";

function ProductData(props) {
  const { t } = useTranslation("main");
  const { cn } = useContext(Context);

  function onDelete(e) {
    e.preventDefault();
    let id = _get(props.item, "id");
    props.onDelete(id);
  }

  function renderHeader() {
    let title = _get(props.item, "product.name");
    let date = parseISO(_get(props.item, "booking_date"));
    let experienceTitle = _get(props.item, "product.experience.name");
    let available = _get(props.item, "available");

    return (
      <div className={cn("cart.product.header.section")} uk-grid="uk-grid">
        {!available ? (
          <div className={cn("cart.product.no_available")}>
            <span>{t("cart.no_available")}</span>
          </div>
        ) : null}
        <div className={"uk-vertical-align " + cn("cart.product.header.titleRow")}>
          <ProductDeleteButton onDelete={onDelete} />
          <span>{experienceTitle}</span>
        </div>
        <div className={cn("cart.product.header.metadataRow")}>
          <div uk-grid="uk-grid">
            <div className="uk-width-1-1 uk-width-auto@m">
              <span className={cn("cart.product.header.metadataTitle")}>{title} </span>
              <i className="uk-visible@m fas fa-long-arrow-alt-right"></i>
            </div>
            <div className={"uk-width-expand uk-margin-remove " + cn("cart.product.header.metadataDate")}>
              <span className={cn("cart.product.header.metadataDateLabel")}>{t("generic.date")}: </span>
              <span className={cn("cart.product.header.metadataDateContent")}>{format(date, "dd/MM/yy")}</span>
              {(format(date, "HH:mm") !== "00:00" ) && (
                <>
                  <span className={cn("cart.product.header.metadataDateLabel")}>{t("generic.hour")}: </span>
                  <span className={cn("cart.product.header.metadataDateContent")}>{format(date, "HH:mm")}</span>
                </>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }

  if (props.item) {
    return (
      <div className={"uk-width-1-1 " + cn("cart.product.section")}>
        {renderHeader()}
        <ProductRateRows item={props.item} />
        <ProductTotal item={props.item} discounts={props.discounts} />
      </div>
    );
  }
  return null;
}

ProductData.propTypes = {
  item: PropTypes.object.isRequired,
  onDelete: PropTypes.func.isRequired,
};

export default ProductData;
