'use strict'

import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import _get from 'lodash/get'
import _isArray from 'lodash/isArray'

import Context from '../../context'
import ProductData from './ProductData'

function Cart(props) { 
  const { cn } = useContext(Context)
  
  function renderProducts() {
    const items = _get(props, 'content.line_items')
    const discounts = _get(props, 'content.discounts')

    if (items && _isArray(items)) {
      return items.map((item, index) => {
        return <ProductData key={`key-${index}`} item={item} discounts={discounts} onDelete={props.onDelete}/>
      })
    }
    return <span>No hay productos</span>
  }

  return (
    <div key='cart' className={ cn('cart.wrapper') }>
      <div className={ cn('cart.section') } uk-grid="uk-grid">
        { renderProducts() }
      </div>
    </div>
  )
}

Cart.propTypes = {
  content: PropTypes.object.isRequired
}

export default Cart