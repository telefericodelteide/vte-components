'use strict'

import React, { useContext } from 'react'
import { useTranslation } from 'react-i18next'
import Context from '../../context'

function Loading(props) { 
  const { t } = useTranslation('main')
  const { cn } = useContext(Context)
  return (
    <div key='lading' className={ cn('loading.wrapper') }>
      <div className={ cn('loading.img') }>
        <span>{ t('loading') }</span>
      </div>
    </div>
  )
}

export default Loading