"use strict";

import React, { Component } from "react";
import { withTranslation } from "react-i18next";
import _clone from "lodash/clone";
import _cloneDeep from "lodash/cloneDeep";
import _get from "lodash/get";
import _concat from "lodash/concat";
import _head from "lodash/head";
import _slice from "lodash/slice";
import _isArray from "lodash/isArray";
import _isEmpty from "lodash/isEmpty";
import _uniqBy from "lodash/uniqBy";
import defaultValues from "./config/defaultValues";
import Cart from "./components/Cart";
import Total from "./components/Total";
import Form from "./components/Form";
import Alert from "./components/Alert";
import Loading from "../Generic/Loading";

import UIkit from "uikit";
import "uikit/dist/css/uikit.min.css";
import "./checkout-widget.css";
import { isFunction } from "lodash-es";

/**
 * Main component.
 */
class CheckoutWidget extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loading: {
				cart: false,
				paymentGateways: false,
				route: false,
			},
			errors: {},
			cart: null,
			values: {},
			promoCode: null,
			wantsInvoice: false,
			pickupData: null,
			paymentGateways: [],
		};

		if (typeof window !== `undefined`) {
			UIkit.container = ".uk-scope";
		}
	}

	/**
	 * Loads the cart data from server and initializes form values and payment gateways.
	 */
	componentDidMount() {
		this.loading(["cart", "paymentGateways", "route"]);
		this.loadCart(true);
		this.loadPaymentGateways(true);
	}

	/**
	 * Loads Cart from server.
	 *
	 * @param {boolean} isLoading
	 */
	loadCart(isLoading) {
		if (!isLoading) {
			this.loading("cart");
		}

		this.props.clientApi.cart
			.getCart()
			.then((cart) => {
				this.setState({
					cart,
					errors: this.resetError("cart"),
					values: this.createInitialValues(cart),
					conditions: this.processCartConditions(cart),
					loading: this.loaded("cart"),
				});

				this.loadRoute(cart, true);

				this.checkEmptyCart(cart);
				if (this.props.trackingCallbacks
					&& isFunction(this.props.trackingCallbacks.onLoadCallBack)) {
					this.props.trackingCallbacks.onLoadCallBack(cart);
				}
			})
			.catch((err) => {
				if (err instanceof Error) {
					UIkit.modal.alert("Error: " + err.message);
					this.setState({
						errors: this.setError("cart", err.message),
						loading: this.loaded("cart"),
					});
				} else {
					console.warn(err);
				}
			});
	}

	/**
	 * Check if a cart has no line items. In this case if a handler for this state is defined execute it.
	 *
	 * @param {*} cart
	 */
	checkEmptyCart(cart) {
		if (cart.line_items.length === 0 && this.props.onEmptyCart) {
			this.props.onEmptyCart();
		}
	}

	loadRoute(cart, isLoading) {
		if (!isLoading) {
			this.loading("route");
		}

		// find product and date
		const lineItems = cart.line_items.filter((lineItem) => {
			return (
				lineItem.type === "booking_date" &&
				lineItem.product.with_transport === true
			);
		});

		if (lineItems.length === 0) {
			this.setState({
				errors: this.resetError("route"),
				loading: this.loaded("route"),
			});

			return;
		}

		const productId = lineItems[0].product.id;
		const date = Date.parse(lineItems[0].booking_date);
		this.props.clientApi.route
			.getPickupData(productId, date)
			.then((route) => {
				// process lodgins, zones and pickup points

				// pickup points
				let pickupPoints = _uniqBy(
					route.pickup_configs
						.map((pickupConfig) =>
							pickupConfig.pickup_points.map((pickupPoint) => {
								return {
									...pickupPoint,
									pickup_config_id: pickupConfig.id,
								};
							})
						)
						.flat()
						.sort((a, b) => a.name.localeCompare(b.name)),
					"id"
				);

				// zones
				const zones = _uniqBy(
					pickupPoints
						.filter((pickupPoint) => pickupPoint.zone != null)
						.map((pickupPoint) => pickupPoint.zone)
						.sort((a, b) => a.name.localeCompare(b.name)),
					"id"
				);

				// lodgins
				const lodgins = _uniqBy(
					pickupPoints
						.filter(
							(pickupPoint) =>
								Array.isArray(pickupPoint.lodgins) &&
								pickupPoint.lodgins.length > 0
						)
						.map((pickupPoint) =>
							pickupPoint.lodgins.map((lodgin) => {
								return {
									id: lodgin.id,
									name: lodgin.name,
									pickup_point_id: pickupPoint.id,
								};
							})
						)
						.flat()
						.sort((a, b) => a.name.localeCompare(b.name)),
					"id"
				);

				pickupPoints = pickupPoints.map((pickupPoint) => {
					return {
						id: pickupPoint.id,
						pickup_config_id: pickupPoint.pickup_config_id,
						name: pickupPoint.name,
						coordinates: pickupPoint.coordinates,
						zone_id:
							pickupPoint.zone != null
								? pickupPoint.zone.id
								: null,
						order: pickupPoint.order,
						pickup_time: pickupPoint.pickup_time,
					};
				});

				const pickupData = {
					lodgins: lodgins,
					zones: zones,
					pickupPoints: pickupPoints,
				};

				this.setState({
					pickupData,
					errors: this.resetError("route"),
					loading: this.loaded("route"),
				});
			})
			.catch((err) => {
				if (err instanceof Error) {
					UIkit.modal.alert("Error: " + err.message);
					this.setState({
						errors: this.setError("route", err.message),
						loading: this.loaded("route"),
					});
				} else {
					console.warn(err);
				}
			});
	}

	/**
	 * Loads Payment gateways from server.
	 *
	 * @param {boolean} isLoading
	 */
	loadPaymentGateways(isLoading) {
		if (!isLoading) {
			this.loading("paymentGateways");
		}
		this.props.clientApi.cart
			.getPaymentGateways()
			.then((paymentGateways) => {
				this.setState({
					paymentGateways,
					errors: this.resetError("paymentGateways"),
					loading: this.loaded("paymentGateways"),
				});
			})
			.catch((err) => {
				if (err instanceof Error) {
					UIkit.modal.alert("Error: " + err.message);
					this.setState({
						errors: this.setError("paymentGateways", err.message),
						loading: this.loaded("paymentGateways"),
					});
				} else {
					console.warn(err);
				}
			});
	}

	/**
	 * Updates state with the value of loading to true.
	 *
	 * @param {string|string[]} key
	 */
	loading(key) {
		let loading = _clone(this.state.loading);
		if (_isArray(key)) {
			key.forEach((k) => (loading[k] = true));
		} else {
			loading[key] = true;
		}
		this.setState({ loading });
	}

	/**
	 * Sets the value of loading to false.
	 *
	 * @param {string} key
	 * @returns {object} loading
	 */
	loaded(key) {
		let loading = _clone(this.state.loading);
		loading[key] = false;
		return loading;
	}

	/**
	 * Sets the value of errors.
	 *
	 * @param {string} key
	 * @param {string} msg
	 * @returns {object} errors
	 */
	setError(key, msg) {
		let errors = _clone(this.state.errors);
		errors[key] = msg;
		return errors;
	}

	/**
	 * Resets the value without errors.
	 *
	 * @param {string} key
	 * @returns {object} errors
	 */
	resetError(key) {
		return this.setError(key, null);
	}

	/**
	 * Updates the state with the new form values.
	 *
	 * @param {object} values
	 */
	onChange = (values) => {
		this.setState({
			values,
		});
	};

	/**
	 * Updates the value of promotional code.
	 *
	 * @param {string} promoCode
	 */
	onChangePromoCode = (promoCode) => {
		let errors = !promoCode
			? this.resetError("promo-code")
			: this.state.errors;
		this.setState({
			promoCode,
			errors,
		});
	};

	/**
	 * Deletes a product by id from server and updates cart with the new cart state.
	 *
	 * @param {string} id
	 */
	onDelete = (id) => {
		if (id) {
			this.loading("cart");
			const itemRemove =  this.state.cart.line_items.find((item) => item.id === id);
			this.props.clientApi.cart
				.removeLineItem(id)
				.then((cart) => {
					let values = _clone(this.state.values);
					values["participants"] = this.createParticipantsState(cart);

					if (this.props.trackingCallbacks
						&& isFunction(this.props.trackingCallbacks.onRemoveItemCallBack)) {
						this.props.trackingCallbacks.onRemoveItemCallBack(itemRemove);
					}

					this.setState({
						cart,
						values,
						errors: this.setError("cart", null),
						loading: this.loaded("cart"),
					});
					this.checkEmptyCart(cart);
				})
				.catch((err) => {
					if (err instanceof Error) {
						UIkit.modal.alert("Error: " + err.message);
						this.setState({
							errors: this.setError("cart", err.message),
							loading: this.loaded("cart"),
						});
					} else {
						console.warn(err);
					}
				});
		} else {
			console.warn("This product has not ID");
		}
	};

	/**
	 * Sends promotional code to server to apply discount.
	 */
	onApplyPromoCode = () => {
		if (this.state.promoCode) {
			this.loading("cart");
			this.props.clientApi.cart
				.addDiscount(this.state.promoCode)
				.then((cart) => {
					this.setState({
						cart,
						errors: this.resetError("promo-code"),
						loading: this.loaded("cart"),
					});
				})
				.catch((err) => {
					if (err instanceof Error) {
						UIkit.modal.alert("Error: " + err.message);
						this.setState({
							errors: this.setError("promo-code", err.message),
							loading: this.loaded("cart"),
						});
					} else {
						console.warn(err);
					}
				});
		}
	};

	/**
	 * Sends to server the cart confirmation.
	 *
	 * @param {object} values
	 */
	onSubmit = (values) => {
		const { t } = this.props;
		this.loading("cart");


		if (this.hasNoAvailableItems()) {
			this.setState({
				errors: this.setError("cart", t("error.noAvailableItem")),
			});

			if (this.props.trackingCallbacks
				&& isFunction(this.props.trackingCallbacks.onErrorCallBack)) {
				this.props.trackingCallbacks.onErrorCallBack(this.state.errors["cart"]);
			}

			return null;
		}

		if (this.state.cart.total_amount > 0 && !this.hasPayMethod(values)) {
			this.setState({
				errors: this.setError("cart", t("error.noPayMethod")),
			});

			if (this.props.trackingCallbacks
				&& isFunction(this.props.trackingCallbacks.onErrorCallBack)) {
				this.props.trackingCallbacks.onErrorCallBack(this.state.errors["cart"]);
			}

			return null;
		}

		let data = this.createValue(values);

		this.props.clientApi.cart
			.confirmCart(data)
			.then((cart) => {
				// updated cart
				if (this.props.trackingCallbacks
					&& isFunction(this.props.trackingCallbacks.onSubmitCallBack)) {
					this.props.trackingCallbacks.onSubmitCallBack(cart);
				}
				this.processPaymentRedirection(cart.payment_data);
			})
			.catch((err) => {
				if (err instanceof Error) {
					UIkit.modal.alert("Error: " + err.message);
					this.setState({
						errors: this.setError("cart", err.message),
						loading: this.loaded("cart"),
					});
				} else {
					console.warn(err);
				}
			});
	};

	/**
	 * Process payment data and redirects to the required url.
	 *
	 * @param {object} paymentData
	 */
	processPaymentRedirection(paymentData) {
		if (paymentData.type === "GET") {
			window.location.replace(paymentData.payment_url);
		} else if (paymentData.type === "POST") {
			const form = document.createElement("form");
			form.id = "paymentForm";
			form.name = "paymentForm";
			form.method = "post";
			form.action = paymentData.payment_url;

			// create form fields
			Object.entries(paymentData.data).forEach(
				([fieldName, fieldValue]) => {
					const input = document.createElement("input");
					input.type = "hidden";
					input.name = fieldName;
					input.value = fieldValue;
					form.appendChild(input);
				}
			);

			document.body.appendChild(form);

			form.submit();
		}
	}

	/**
	 * Checks if value has pay method.
	 *
	 * @param {object} values
	 */
	hasPayMethod(values) {
		return !(_isEmpty(values.payment_gateway_id));
	}

	/**
	 * Checks if cart has no available items.
	 */
	hasNoAvailableItems() {
		let lineItems = _get(this.state.cart, "line_items");
		if (lineItems && _isArray(lineItems)) {
			let noAvailable = false;
			lineItems.forEach((lineItem) => {
				if (!lineItem.available) {
					noAvailable = true;
				}
			});
			return noAvailable;
		}
	}

	/**
	 * Removes extra properties to send to server.
	 *
	 * @param {object} v
	 */
	createValue(v) {
		let values = _cloneDeep(v);
		delete values.client.no_diseases;
		delete values.client.accept_terms;

		// process pickup information
		if (this.state.pickupData != null) {
			values.client.pickup = {};
			Object.entries(values.pickup).forEach(([key, value]) => {
				values.client.pickup[key] = value == "-1" ? null : value;
			});
		}
		delete values.pickup;

		// process participants
		if (_isEmpty(values.participants)) {
			delete values.participants;
		}

		// remove invoice data if not required
		if (!this.state.wantsInvoice) {
			delete values.invoice;
		}

		return values;
	}

	/**
	 * Un\enables the invoice form.
	 */
	toggleInvoice = () => {
		this.setState({
			wantsInvoice: !this.state.wantsInvoice,
		});
	};

	processCartConditions(cart) {
		let result = [];
		const conditions = _get(cart.checkout_config, "conditions");

		const termsCondition = _head(conditions);
		if (termsCondition) {
			let condition = {
				key: "client.conditions." + _get(termsCondition, "key"),
				title: _get(termsCondition, "title"),
				info: _get(termsCondition, "content"),
				required: _get(termsCondition, "required"),
			};
			if (_get(cart.checkout_config, "terms").length > 0) {
				condition.modal = {
					title: _get(cart.checkout_config, "terms.0.title"),
					content: (
						<div
							dangerouslySetInnerHTML={{
								__html: _get(
									cart.checkout_config,
									"terms.0.content"
								),
							}}
						/>
					),
				};
			}
			result.push(condition);
		}

		return _concat(
			result,
			_slice(conditions, 1).map((condition, index) => {
				return {
					key: _get(condition, "key")
						? "client.conditions." + _get(condition, "key")
						: "client.conditions.extra_conditions_" + index,
					title: _get(condition, "title"),
					info: _get(condition, "content"),
					required: _get(condition, "required", false),
				};
			})
		);
	}

	createInitialValues(cart) {
		return {
			payment_gateway_id: "",
			client: defaultValues.clientValue,
			pickup: defaultValues.pickupValue,
			invoice: defaultValues.invoiceValue,
			participants: this.createParticipantsState(cart),
		};
	}

	createParticipantsState(cart) {
		const participants = _get(cart, "client.participants");
		const lineItems = _get(cart, "line_items");

		if (_isArray(participants)) {
			return this.initParticipantsByLineitem(lineItems, participants);
		}

		return [];
	}

	initParticipantsByLineitem(lineItems, participants) {
		if (_isArray(lineItems)) {
			return participants.map((participant) => {
				const lineItem = lineItems.find(
					(lineItem) => lineItem.id === participant.line_item_id
				);
				let subindex = 0;
				return {
					line_item_id: lineItem.id,
					participants: lineItem.rates
						.map((rate) => {
							let rateParticipants = [];
							for (let i = 0; i < rate.qty; i++, subindex++) {
								rateParticipants.push({
									...Object.fromEntries(
										Object.keys(
											participant.form
										).map((field) => [field, ""])
									),
									rate: rate.rate.id + "-" + subindex,
								});
							}
							return rateParticipants;
						})
						.flat(),
				};
			});
		}

		return [];
	}

	render() {
		let loading = this.state.loading;
		if (this.state.cart) {
			let cart = this.state.cart;
			let { client, line_items, discounts, total_amount } = cart;

			return (
				<div id="checkout-widget" className="checkout-widget uk-scope">
					{loading.cart ? <Loading /> : null}
					<Alert errors={this.state.errors} />
					<Cart key="cart" content={cart} onDelete={this.onDelete} />
					<Total
						key="total"
						totalAmount={total_amount}
						discounts={discounts}
					/>
					<Form
						key="form"
						client={client}
						lineItems={line_items}
						values={this.state.values}
						promoCode={this.state.promoCode}
						allowInvoice={this.props.allowInvoice}
						wantsInvoice={this.state.wantsInvoice}
						paymentGateways={this.state.paymentGateways}
						pickupData={this.state.pickupData}
						errors={this.state.errors}
						onChange={this.onChange}
						onSubmit={this.onSubmit}
						onApplyPromoCode={this.onApplyPromoCode}
						onChangePromoCode={this.onChangePromoCode}
						toggleInvoice={this.toggleInvoice}
						conditions={this.state.conditions}
						totalAmount={total_amount}
					/>
				</div>
			);
		}

		if (loading.cart) {
			return <Loading />;
		}

		return null;
	}
}

export default withTranslation("main")(CheckoutWidget);
