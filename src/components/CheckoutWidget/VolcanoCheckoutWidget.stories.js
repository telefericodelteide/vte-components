import React from "react";
import VolcanoCheckoutWidget from "./VolcanoCheckoutWidget";
import apiConfig from "../../stories/configApi";

export default {
  component: VolcanoCheckoutWidget,
  title: "Checkout widget",
};

const Template = (args) => <VolcanoCheckoutWidget {...args} />;

export const Default = Template.bind({});
Default.args = {
  apiConfig: apiConfig,
  locale: "es",
  onConfirm: (cart) => console.log(cart),
  onEmptyCart: () => console.log("Empty cart")
};

export const Localized = Template.bind({});
Localized.args = {
  ...Default.args,
  locale: "de",
};

export const WithPromo = Template.bind({});
WithPromo.args = {
  ...Default.args,
  locale: "en",
  apiConfig: {
    ...Default.args.apiConfig,
    locale: "en",
  },
};

export const trackingCallbacks = Template.bind({});
trackingCallbacks.args = {
  ...Default.args,
  trackingCallbacks: {
    onLoadCallBack: (cart) => console.log('onLoadCallBack', cart),
    onSubmitCallBack: (cart) => {console.log('onSubmitCallBack', cart); debugger;},
    onErrorCallBack: (error) => console.log('error', error),
    onRemoveItemCallBack: (itemRemove) => console.log('onRemoveItemCallBack', itemRemove),
  }
};
