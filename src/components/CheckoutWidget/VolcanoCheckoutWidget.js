import React from "react";
import {I18nextProvider, initReactI18next} from "react-i18next";
import VolcanoApi from "@volcanoteide/volcanoteide-api-client";
import CheckoutWidget from "./CheckoutWidget";
import i18nResources from "./config/i18n";
import i18n from "i18next";

export default function VolcanoCheckoutWidget({
	locale,
	apiConfig,
	conditionsConfig,
	onConfirm,
	onEmptyCart,
	allowInvoice,
    trackingCallbacks,
}) {
	// init api client
	const client = new VolcanoApi({
		...apiConfig,
		locale: locale,
	});

	i18n.use(initReactI18next).init({
		interpolation: { escapeValue: false },
		lng: locale,
		resources: i18nResources,
	});

	return (
		<I18nextProvider i18n={i18n}>
			<CheckoutWidget
				clientApi={client}
				conditionsConfig={conditionsConfig}
				onConfirm={onConfirm}
				onEmptyCart={onEmptyCart}
				allowInvoice={allowInvoice}
				trackingCallbacks={trackingCallbacks}
			/>
		</I18nextProvider>
	);
}
