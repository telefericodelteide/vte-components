import React from "react";
import _get from "lodash/get";
import ClassNames from '../config/classNames';
function cn(key) {
  return _get(ClassNames, key);
}

const ClassNamesContext = React.createContext({ cn: cn });

export default ClassNamesContext;