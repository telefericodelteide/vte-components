'use strict'

const clientValue = {
  first_name: "", 
  last_name: "", 
  email: "", 
  confirm_email: "", 
  phone: "", 
  phone_other: "",
  observations: "",
  conditions: {}      
}

const pickupValue = {
  lodgin_id: "",
  zone_id: "",
  pickup_point_id: "",
  observations: ""
}

const invoiceValue = {
  name: "",
  vat_number_type: "",
  vat_number: "",
  address: "",
  postal_code: "",
  locality: "",
  state: "",
  country_id: "es"
}

const participantValue = {
  rate: "",
  first_name: "", 
  last_name: "", 
  nif: "", 
  language: ""
}

export default {
  clientValue,
  pickupValue,
  invoiceValue,
  participantValue
}