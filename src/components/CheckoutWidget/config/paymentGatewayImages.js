'use strict'

const paymentGatewayImages = {
  paypal: "https://www.paypal.com/es_XC/i/logo/PayPal_mark_37x23.gif",
  ingenico: "https://www.volcanoteide.com/img/creditCards.jpg",
  universal_pay: "https://www.volcanoteide.com/img/creditCards.jpg",
  redsys: "https://www.volcanoteide.com/img/creditCards.jpg"
}

export default paymentGatewayImages