'use strict'

const ClassNames = {
  loading: {
    wrapper: 'loading-wrapper',
    img: 'loading'
  },
  cart: {
    wrapper: 'cw-cart',
    section: 'cw-cart-lineitems',
    product: {
      section: 'cw-cart-lineitem',
      no_available: 'cw-cart-lineitem-no-available',
      header: {
        section: 'uk-grid-collapse cw-cart-lineitem-description',
        titleRow: 'uk-width-1-1 cw-cart-lineitem-title',
        title: '',
        metadataRow: 'uk-width-1-1 cw-cart-lineitem-detail',
        metadataTitle: 'cw-cart-lineitem-detail-title',
        metadataDate: 'cw-cart-lineitem-detail-info',
        metadataDateLabel: 'cw-cart-lineitem-detail-info-title cw-cart-text',
        metadataDateContent: 'cw-cart-lineitem-detail-info-content cw-cart-text',
      },
      delete: {
        wrapper: 'cw-cart-lineitem-delete',
        button: '',
        icon: 'far fa-trash-alt'
      },
      rate: {
        section: 'cw-cart-lineitem-rates',
        row: 'cw-cart-lineitem-rate',
        fareType: {
          cell: 'cw-cart-lineitem-rate-type',
          content: 'cw-cart-text-highlight'
        },
        price: {
          cell: 'cw-cart-lineitem-rate-unit-price',
          content: 'cw-cart-text'
        },
        participants: {
          cell: 'cw-cart-lineitem-rate-qty',
          label: 'cw-cart-text-highlight',
          content: 'cw-cart-text'
        },
        subtotal: {
          cell: 'cw-cart-lineitem-rate-total-price',
          label: 'cw-cart-text-highlight',
          content: 'cw-cart-text'
        }
      },
      subtotal: {
        row: 'cw-cart-lineitem-total-price',
        wrapper: '',
        label: 'cw-cart-text',
        content: 'cw-cart-text'
      }
    }
  },
  discount: {
    wrapper: 'col-xs-12 col-sm-10 col-sm-offset-1 cart-total-container',
    row: 'cart-total',
    label: 'cart-txt-highlight',
    content: 'cart-txt-highlight',
    icon: null,
  },
  total: {
    wrapper: 'cw-cart-total-price-container',
    row: 'cw-cart-total-price',
    label: 'cw-cart-text-highlight',
    content: 'cw-cart-text-highlight',
    icon: null,
  },
  form: {
    wrapper: 'cw-form-container',
    class: 'cw-form', 
    header: {
      row: 'cw-form-section-header-container uk-margin-remove-top',
      wrapper: 'cw-form-section-header',
      type: 'cw-form-section-header-title'
    },
    customer: { 
      titleIcon: 'fa fa-pen',
      names: {
        row: 'uk-grid-row-small uk-grid-column-medium uk-margin-remove-top cw-form-field-group',
        first_name: {
          fieldWrapper: 'uk-width-1-1 uk-width-1-2@s cw-form-field',
          field: 'uk-input cw-form-field-control'
        },
        last_name: {
          fieldWrapper: 'uk-width-1-1 uk-width-1-2@s cw-form-field',
          field: 'uk-input cw-form-field-control'
        }
      },
      emails: {
        row: 'uk-grid-row-small uk-grid-column-medium cw-form-field-group',
        email: {
          fieldWrapper: 'uk-width-1-1 uk-width-1-2@s cw-form-field',
          field: 'uk-input cw-form-field-control'
        },
        confirm_email: {
          fieldWrapper: 'uk-width-1-1 uk-width-1-2@s cw-form-field',
          field: 'uk-input cw-form-field-control'
        }
      },
      phones: {
        row: 'uk-grid-row-small uk-grid-column-medium cw-form-field-group',
        phone: {
          fieldWrapper: 'uk-width-1-1 uk-width-1-2@s cw-form-field',
          field: 'uk-input cw-form-field-control'
        },
        phone_other: {
          fieldWrapper: 'uk-width-1-1 uk-width-1-2@s cw-form-field',
          field: 'uk-input cw-form-field-control'
        }
      },
      observations: {
        row: 'uk-grid-row-small uk-grid-column-medium cw-form-field-group',
        promo: {
          fieldWrapper: 'uk-width-1-1 uk-width-1-2@s cw-form-field',
          group: 'cw-form-field-promo',
          field: 'uk-input cw-form-field-control',
          button: 'uk-button cw-form-field-promo-button'
        },
        no_diseases: {
          fieldWrapper: 'cw-form-field-diseases',
          field: 'uk-checkbox'
        },
        observations: {
          fieldWrapper: 'uk-width-1-1 uk-width-1-2@s cw-form-field cw-form-field-observations',
          field: 'uk-textarea cw-form-field-control'
        }
      }
    },
    pickup: {
      titleIcon: 'fa fa-shuttle-van',
      row: 'uk-grid-row-small uk-grid-column-medium uk-margin-remove-top cw-form-field-group',
      wrapper: 'uk-width-1-1 cw-form-pickup',
      field: {
        main: '',
        wrapper: 'uk-width-1-1 uk-width-1-3@s cw-form-field' 
      },
      observations: {
        main: 'uk-textarea cw-form-field-control',
        wrapper: 'uk-width-1-1 uk-width-1-3@s cw-form-field cw-form-field-observations',
        description: ''
      },
      info: {
        wrapper: 'uk-width-1-1 uk-grid-collapse cw-form-pickup-info',
        title: 'uk-width-1-1 cw-form-pickup-info-title',
        field: 'uk-width-1-1 cw-form-pickup-info-field',
        label: 'cw-form-pickup-info-label',
        text: 'cw-form-pickup-info-text'
      }
    },
    participants: {
      wrapper: 'uk-grid-row-small uk-grid-column-medium cw-form-field-group',
      row: 'uk-width-1-1 uk-width-1-2@s uk-width-1-3@m uk-width-1-4@l',
      no_available: 'cw-form-participants-lineitem-no-available',      
      icon: 'fa fa-user',
      product: {
        header: {
          sectionContainer: 'uk-width-1-1',
          section: 'uk-grid-collapse cw-form-participants-lineitem-description',
          titleRow: 'uk-width-1-1 cw-form-participants-lineitem-title',
          title: '',
          metadataRow: 'uk-width-1-1 cw-form-participants-lineitem-detail',
          metadataTitle: 'cw-form-participants-lineitem-detail-title',
          metadataDate: 'cw-form-participants-lineitem-detail-info',
          metadataDateLabel: 'cw-form-participants-lineitem-detail-info-title cw-cart-text',
          metadataDateContent: 'cw-form-participants-lineitem-detail-info-content cw-cart-text',          
        },
      },
      first_name: {
        fieldWrapper: 'uk-width-1-1 cw-form-field',
        field: 'uk-input cw-form-field-control'
      },
      last_name: {
        fieldWrapper: 'uk-width-1-1 cw-form-field',
        field: 'uk-input cw-form-field-control'
      },
      nif: {
        fieldWrapper: 'uk-width-1-1 cw-form-field',
        field: 'uk-input cw-form-field-control'
      },
      language: {
        fieldWrapper: 'uk-width-1-1 cw-form-field',
        field: 'uk-input cw-form-field-control'
      }
    },
    invoice: {
      wantsInvoice: {
        row: 'uk-grid-row-small uk-grid-column-medium uk-margin-remove-top cw-form-field-group',
        fieldWrapper: 'uk-width-1-1 uk-width-1-2@s cw-form-field cw-form-field-invoice',
        field: 'uk-checkbox'           
      },
      name: {
        row: 'uk-grid-row-small cw-form-field-group',
        fieldWrapper: 'uk-width-1-1 uk-width-1-2@s cw-form-field',
        field: 'uk-input cw-form-field-control'
      },
      vat_number_type: {
        row: 'uk-grid-row-small cw-form-field-group',
        fieldWrapper: 'uk-width-1-1 uk-width-1-2@s cw-form-field',
        field: 'uk-input cw-form-field-control'        
      },
      vat_number: {
        row: 'uk-grid-row-small cw-form-field-group',
        fieldWrapper: 'uk-width-1-1 uk-width-1-2@s cw-form-field',
        field: 'uk-input cw-form-field-control'
      },
      address: {
        row: 'uk-grid-row-small cw-form-field-group',
        fieldWrapper: 'uk-width-1-1 uk-width-1-2@s cw-form-field',
        address: {
          fieldWrapper: 'uk-width-1-1 cw-form-field cw-form-field-address',
          field: 'uk-input cw-form-field-control'  
        },
        postal_code: {
          fieldWrapper: 'uk-width-2-5 cw-form-field cw-form-field-address',
          field: 'uk-input cw-form-field-control'  
        },
        locality: {
          fieldWrapper: 'uk-width-3-5 cw-form-field cw-form-field-address',
          field: 'uk-input cw-form-field-control'  
        },
        state: {
          fieldWrapper: 'uk-width-1-2 cw-form-field cw-form-field-address',
          field: 'uk-input cw-form-field-control'  
        },
        country_id: {
          fieldWrapper: 'uk-width-1-2 cw-form-field cw-form-field-address',
          field: 'uk-input cw-form-field-control'  
        },
      }
    },
    payment: {
      titleIcon: 'fa fa-credit-card',
      row: 'uk-grid-row-small uk-grid-column-medium uk-margin-remove-top cw-form-field-group cw-form-payment-methods',
      wrapper: 'uk-width-1-1',
      methodWrapper: 'cw-form-payment-method'
    },
    terms: {
      row: 'uk-grid-row-small uk-grid-column-medium uk-margin-remove-top cw-form-field-group cw-form-field-legal-terms',
      fieldWrapper: 'uk-width-1-1 cw-form-field',
      field: 'uk-checkbox'
    }  
  },
  error: {
    wrapper: 'col-xs-12 col-sm-10 col-sm-offset-1',
    content: 'alert alert-danger'
  }
}

export default ClassNames