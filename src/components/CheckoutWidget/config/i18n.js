'use strict'

import _reduce from 'lodash/reduce';
import es from "./locales/es.json";
import en from "./locales/en.json";
import it from "./locales/it.json";
import nl from "./locales/nl.json";
import pl from "./locales/pl.json";
import de from "./locales/de.json";
import fr from "./locales/fr.json";
import ru from "./locales/ru.json";
import calendarI18nResources from "../../AvailabilityCalendar/config/i18n";

const i18nResources = {
  es: {
    main: es,
    calendar: calendarI18nResources.es.calendar,
  },
  en: {
    main: en,
    calendar: calendarI18nResources.en.calendar,
  },
  it: {
    main: it,
    calendar: calendarI18nResources.it.calendar,
  },
  nl: {
    main: nl,
    calendar: calendarI18nResources.nl.calendar,
  },
  pl: {
    main: pl,
    calendar: calendarI18nResources.pl.calendar,
  },
  de: {
    main: de,
    calendar: calendarI18nResources.de.calendar,
  },
  fr: {
    main: fr,
    calendar: calendarI18nResources.fr.calendar,
  },
  ru: {
    main: ru,
    calendar: calendarI18nResources.ru.calendar,
  }
}

export default i18nResources;