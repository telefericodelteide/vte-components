'use strict'

const defaultConfigForm = {
  "form": {
    "first_name": {
      "type": "string",
      "required": true,
      "name": "form.client.first_name"
    },
    "last_name": {
      "type": "string",
      "required": true,
      "name": "form.client.last_name"
    },
    "email": {
      "type": "email",
      "required": true,
      "name": "form.client.email"
    },
    "confirm_email": {
      "type": "email",
      "required": true,
      "equalTo": "client.email",
      "name": "form.client.confirm_email"
    },
    "phone": {
      "type": "phone",
      "required": true,
      "name": "form.client.phone"
    },
    "phone_other": {
      "type": "phone",
      "required": false,
      "name": "form.client.phone_other"
    },
    "observations": {
      "type": "textarea",
      "required": false,
      "name": "form.client.observations"
    },
    "no_diseases": {
      "type": "checkbox",
      "required": true,
      "name": "form.client.no_diseases"
    },
    "accept_terms": {
      "type": "checkbox",
      "required": true,
      "name": "form.client.accept_terms"
    },
    "allow_promos": {
      "type": "checkbox",
      "required": false,
      "name": "form.client.allow_promos"
    },
  },
  "pickup": {
    "lodgin_id": {
      "type": "select",
      "required": true,
      "name": "form.pickup.lodgin.title",
      "placeholder": "form.pickup.lodgin.placeholder",
      "emptyOption": "form.pickup.lodgin.other"
    },
    "zone_id": {
      "type": "select",
      "required": true,
      "name": "form.pickup.zone.title",
      "placeholder": "form.pickup.zone.placeholder",
      "emptyOption": "form.pickup.zone.other"
    },
    "pickup_point_id": {
      "type": "select",
      "required": true,
      "name": "form.pickup.pickup_point.title",
      "placeholder": "form.pickup.pickup_point.placeholder",
      "emptyOption": "form.pickup.pickup_point.other"
    },
    "observations": {
      "type": "textarea",
      "required": true,
      "name": "form.pickup.observations.title"
    },
  },
  "participants": {
    "form": {
      "rate": {
        "type": "hidden"
      },
      "first_name": {
        "type": "string",
        "required": true,
        "name": "form.participants.first_name"
      },
      "last_name": {
        "type": "string",
        "required": true,
        "name": "form.participants.last_name"
      },
      "nif": {
        "type": "string",
        "required": true,
        "name": "form.participants.nif"
      },
      "language": {
        "type": "select",
        "options": [
            {"id": "", "name": "form.participants.languages.und"},
            {"id": "es", "name": "form.participants.languages.es"},
            {"id": "en", "name": "form.participants.languages.en"},
            {"id": "de", "name": "form.participants.languages.de"},
            {"id": "other", "name": "form.participants.languages.other"}
        ],
        "required": false,
        "name": "form.participants.language"
      }
    }
  },
  "invoice": {
    "name": {
      "type": "string",
      "required": true,
      "name": "form.invoice.name"
    },
    "vat_number_type": {
      "type": "select",
      "required": true,
      "options": [
        {"id": 2, "name": "form.invoice.vat_number_type.nif"},
        {"id": 3, "name": "form.invoice.vat_number_type.passport"},
        {"id": 4, "name": "form.invoice.vat_number_type.document_country"},
        {"id": 5, "name": "form.invoice.vat_number_type.document_residence"},
        {"id": 6, "name": "form.invoice.vat_number_type.document_other"}
      ],
      "name": "form.invoice.vat_number_type.title",
      "placeholder": "form.invoice.vat_number_type.placeholder",
      "emptyOption": "form.invoice.vat_number_type.other"
    },
		"vat_number": {
      "type": "string",
      "required": true,
      "name": "form.invoice.vat_number.title"
    },
		"address": {
      "type": "string",
      "required": true,
      "name": "form.invoice.address.address"
    },
		"postal_code": {
      "type": "string",
      "required": true,
      "name": "form.invoice.address.postal_code"
    },
		"locality": {
      "type": "string",
      "required": true,
      "name": "form.invoice.address.locality"
    },
		"state": {
      "type": "string",
      "required": true,
      "name": "form.invoice.address.state"
    },
    "country_id": {
      "type": "string",
      "required": true,
      "name": "form.invoice.address.country"
    }
  }
}

export default defaultConfigForm