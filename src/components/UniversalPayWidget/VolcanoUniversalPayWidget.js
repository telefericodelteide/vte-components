import React, { useEffect } from "react";
import "./universal-pay-widget.css";

const VolcanoUniversalPayWidget = ({
	scriptUrl,
	baseUrl,
	token,
	merchantId,
	onResult,
}) => {
	const initPayment = () => {
		var cashier = window.com.myriadpayments.api.cashier();

		cashier.init({ baseUrl: baseUrl });

		cashier.show({
			containerId: "ipgCashierDiv",
			merchantId: merchantId,
			token: token,
			successCallback: onResult,
			failureCallback: onResult,
			cancelCallback: onResult,
			styleSheetUrl: "/cashier/css/optional-customisation.css",
		});
	};

	useEffect(() => {
		const scriptElement = document.createElement("script");

		scriptElement.src = scriptUrl + "?ts=" + Date.now();
		scriptElement.async = true;
		scriptElement.onload = () => initPayment();

		document.body.appendChild(scriptElement);

		return () => {
			document.body.removeChild(scriptElement);
		};
	}, []);

	return <div id="ipgCashierDiv"></div>;
};

export default VolcanoUniversalPayWidget;
