import React from "react"
import VolcanoUniversalPayWidget from "./VolcanoUniversalPayWidget"

export default {
  component: VolcanoUniversalPayWidget,
  title: "Universal pay widget",
}

const Template = args => <VolcanoUniversalPayWidget {...args} />

const handleResult = function handleResult(data) {
  console.log(data)
  alert(JSON.stringify(data))
}

export const Default = Template.bind({})
Default.args = {
  scriptUrl: "https://cashierui.test.universalpay.es/js/api.js",
  baseUrl: "https://cashierui.test.universalpay.es/ui/cashier",
  token: "fa702153-2772-435c-a2b4-2486cf7a84d6",
  merchantId: "909952",
  onResult: handleResult,
}
