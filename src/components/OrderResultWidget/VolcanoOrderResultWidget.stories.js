import React from "react";
import VolcanoOrderResultWidget from "./VolcanoOrderResultWidget";
import apiConfig from "../../stories/configApi";

export default {
  component: VolcanoOrderResultWidget,
  title: "Order result widget",
};

const Template = (args) => <VolcanoOrderResultWidget {...args} />;

export const Default = Template.bind({});
Default.args = {
  apiConfig: apiConfig,
  locale: "es",
  params: {
    vte_transaction_id: 433427
  }
};

export const Localized = Template.bind({});
Localized.args = {
  ...Default.args,
  locale: "en",
  apiConfig: {
    ...Default.args.apiConfig,
    locale: "en"
  }
}