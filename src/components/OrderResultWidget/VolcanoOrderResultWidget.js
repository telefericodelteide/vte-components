import React, { useState, useEffect } from "react";
import UIkit from "uikit";
import "uikit/dist/css/uikit.min.css";
import VolcanoApi from "@volcanoteide/volcanoteide-api-client";
import OrderResultWidget from "./OrderResultWidget";
import isError from "lodash/isError";
import { isFunction } from "formik";

const BASE_SELECTION = {
  productId: null,
  date: null,
  session: null,
  rates: []
};

export default function VolcanoOrderResultWidget({
  locale,
  apiConfig,
  params,
  bookingId,
  translations,
  onProcessTransaction
}) {
  
  if (typeof window !== `undefined`) {
    UIkit.container = '.uk-scope';
  }

  // init api client
  const client = new VolcanoApi({
    ...apiConfig,
    locale: locale
  });

  const [bookings, setBookings] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isFailed, setIsFailed] = useState(false);


  useEffect(() => {
    setIsLoading(true);
    const transactionId = params && params.vte_transaction_id;    

    client.paymentTransaction.processTransactionResult( params )
      .then(bookings => {
        setIsLoading(false);
        setBookings(bookings);
        if (isFunction(onProcessTransaction)) {
          onProcessTransaction(bookings);
        }
      })
      .catch(err => {				
        if (err instanceof Error) {					
          setIsLoading(false);
          setIsFailed(true);          
        } else {
          console.warn(err)
        }
      });
  }, []);

    return (
      <OrderResultWidget
        locale={locale}
        bookings={bookings}
        failed={isFailed}
        loading={isLoading}
        translations={translations}
      />
  );
}