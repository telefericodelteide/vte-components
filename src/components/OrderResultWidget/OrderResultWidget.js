import React from "react";
import Loading from "../Generic/Loading";
import Alert from "../Generic/Alert";
import _isEmpty from "lodash/isEmpty";
import _get from "lodash/get";
import _template from 'lodash/template';
import "./order-result-widget.css";
import download from './assets/download.png';

const DEFAULT_LOCALE = "es";


function BookingResult({ locale, booking, translations }) {
  const buildLabel = (className, label, content) => {
    return (
      <div className={className + " ow-booking-label"}>
        <span className="ow-booking-label-title">{label}:</span>
        <span className="ow-booking-label-content">{content}</span>
      </div>
    );
  };

  return (
    <div className="ow-booking uk-scope">
      {buildLabel(
        "ow-booking-order",
        translations[locale].labels.order,
        booking.product.experience.name
      )}
      <div className="ow-booking-pdf"><div dangerouslySetInnerHTML={{__html: _template(translations[locale].labels.pdf)({product: booking.product.name})}} /></div>
      <div className="ow-booking-download">
        <a title={translations[locale].labels.locator} href={booking.links.pdf.href} target="_blank"><img src={download}></img></a>
      </div>
      {buildLabel(
        "ow-booking-activity",
        translations[locale].labels.activity,
        booking.product.name
      )}
      {buildLabel(
        "ow-booking-locator",
        translations[locale].labels.locator,
        booking.locator
      )}
    </div>
  );
}

export default function OrderResultWidget({ locale, bookings, failed, loading, translations }) {
  locale = locale || DEFAULT_LOCALE;

  return (
    <div className="order-result-widget uk-scope">
      {loading && <Loading />}
      {!loading && bookings.length > 0 && (
        <div className="ow-container">
          <div className="ow-title"><h1>{translations[locale].payment_accepted_title}</h1></div>
          {translations[locale].payment_accepted_info_2 &&
            <Alert
              className="ow-alert-payment-info"
              icon="fas fa-info"
              type="red"
              content={translations[locale].payment_accepted_info_2}
            />
          }
          <Alert
            className="ow-alert-payment-accepted"
            icon="fas fa-check"
            content={translations[locale].payment_free}
          />
          {translations[locale].payment_accepted_info && <Alert 
            className="ow-alert-payment-info"
            icon="fas fa-info"
            content={translations[locale].payment_accepted_info}/>
          }
          <Alert
            className="ow-alert-contact-info"
            icon="fas fa-question"
            content={translations[locale].contact_info}
          />
          {bookings.map((booking, index) => (
            <BookingResult
              key={"booking-" + index}
              locale={locale}
              booking={booking}
              translations={translations}
            />
          ))}
        </div>
      )}
      {!loading && failed && (
        <div className="ow-container">
          <div className="ow-title"><h1>{translations[locale].payment_denied_title}</h1></div>
          <Alert
            className="ow-alert-payment-denied"
            icon="fas fa-exclamation-circle"
            content={translations[locale].payment_denied}
          />
        </div>
      )}      
    </div>
  );
}
