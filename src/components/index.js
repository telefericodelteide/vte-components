import VolcanoBookingWidget from "./BookingWidget/VolcanoBookingWidget";
import VolcanoCheckoutWidget from "./CheckoutWidget/VolcanoCheckoutWidget";
import VolcanoOrderResultWidget from "./OrderResultWidget/VolcanoOrderResultWidget";
import VolcanoIntermediarySignupWidget from "./IntermediarySignupWidget/VolcanoIntermediarySignupWidget";
import VolcanoUniversalPayWidget from "./UniversalPayWidget/VolcanoUniversalPayWidget";
import VolcanoBookingManagementWidget from "./BookingManagementWidget/VolcanoBookingManagementWidget";

export {
  VolcanoBookingWidget,
  VolcanoCheckoutWidget,
  VolcanoOrderResultWidget,
  VolcanoIntermediarySignupWidget,
  VolcanoUniversalPayWidget,
  VolcanoBookingManagementWidget
};