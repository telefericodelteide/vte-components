const downloadFile = (data, filename) => {
    const url = window.URL.createObjectURL(new Blob([data], { type: 'application/octet-stream' }));
    var link = document.createElement('a');
    link.href = url;
    link.download = filename;
    link.click();
}

export default {
    downloadFile
}