#!/bin/bash

# Ruta del directorio a buscar
DIR="../src/components/"

# Buscar archivos csv y procesarlos
for csvFile in $(find "./" -type f -name "*.csv"); do
    
    # Obtener la ruta y el nombre del archivo csv original (generic-config-locales-ru.csv)
    jsonFile=$(echo "$(basename "$csvFile" .csv)" | sed -e 's/-/\//g' | tr '[:upper:]' '[:lower:]')
    
    # Ruta de json a modificar (.*/Generic/config/locales/ru.json)
    rutaDestino=$(find $DIR -iwholename .*/$jsonFile.json)
    
    # Implementar avisos de ficheros no encontrados
    # if [$rutaDestino]; then
    #     echo 'Fichero de traduccion no encontrado -> $rutaDestino'
    #     $continue
    # fi

    #cargamos json
    json=$(cat "$rutaDestino" | jq '.')

    #iteramos por el csv linea a linea para cargar el campo valor
    while IFS=';' read -r clave valor; do
        #cargamos en el json los datos nuevos
        json=$(echo $json | jq --arg clave $clave --arg valor "$valor" 'setpath([($clave | split(".") | map(select(length > 0)))[]]; $valor)' --indent 4)
    done < "$csvFile"

    # Guardar el resultado en el archivo JSON
    echo $json > $rutaDestino
    
    # Imprimir mensaje de progreso
    echo "Archivo JSON generado: $rutaDestino"
done