#!/bin/bash

# Ruta del directorio a buscar
DIR="../src/components"

# Buscar archivos es.json y procesarlos
for jsonFile in $(find "$DIR" -type f -name "es.json"); do

    json=$(cat $jsonFile)

    # Convertir el archivo JSON en un objeto plano
    flat=$(echo $json | jq -r '[paths(scalars) as $path | { ($path|join(".")):getpath($path) }] | add | to_entries | map([.key, .value]|join(";"))|join("\\n")')


    # Crear el encabezado del archivo CSV
    header="clave;text\n"

    # Concatenar el encabezado y el objeto plano para crear el archivo CSV
    csv=$header$flat
    
    csvFile=$(echo "$(dirname "$jsonFile" | sed "s|$DIR/||")/$(basename "$jsonFile" .json)" | sed -e 's/\//-/g' | tr '[:upper:]' '[:lower:]')
    
    # Guardar el archivo CSV
    echo $csv > $csvFile.csv

    # Imprimir mensaje de progreso
    echo "Archivo CSV generado: $csvFile"
done


