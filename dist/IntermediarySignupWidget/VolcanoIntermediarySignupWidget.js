"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = VolcanoIntermediarySignupWidget;
var _react = _interopRequireWildcard(require("react"));
var _FormWidget = _interopRequireDefault(require("../Generic/Form/FormWidget"));
var _volcanoteideApiClient = _interopRequireDefault(require("@volcanoteide/volcanoteide-api-client"));
var _get2 = _interopRequireDefault(require("lodash/get"));
var _set2 = _interopRequireDefault(require("lodash/set"));
var _isArray = _interopRequireDefault(require("lodash/isArray"));
var _countries = _interopRequireDefault(require("../assets/countries.json"));
var _streetTypes = _interopRequireDefault(require("../assets/streetTypes.json"));
var _i18n = _interopRequireDefault(require("./config/i18n"));
var _reactI18next = require("react-i18next");
var _i18nextHttpBackend = _interopRequireDefault(require("i18next-http-backend"));
var _i18next = _interopRequireDefault(require("i18next"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function (e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != typeof e && "function" != typeof e) return { default: e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && {}.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n.default = e, t && t.set(e, n), n; }
function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
function _objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
function _defineProperty(e, r, t) { return (r = _toPropertyKey(r)) in e ? Object.defineProperty(e, r, { value: t, enumerable: !0, configurable: !0, writable: !0 }) : e[r] = t, e; }
function _toPropertyKey(t) { var i = _toPrimitive(t, "string"); return "symbol" == typeof i ? i : i + ""; }
function _toPrimitive(t, r) { if ("object" != typeof t || !t) return t; var e = t[Symbol.toPrimitive]; if (void 0 !== e) { var i = e.call(t, r || "default"); if ("object" != typeof i) return i; throw new TypeError("@@toPrimitive must return a primitive value."); } return ("string" === r ? String : Number)(t); }
const DEFAULT_LOCALE = "es";
function VolcanoIntermediarySignupWidget(_ref) {
  let {
    apiConfig,
    locale,
    conditions,
    successConfig,
    defaultValues
  } = _ref;
  locale = locale || DEFAULT_LOCALE;
  _i18next.default.use(_i18nextHttpBackend.default).use(_reactI18next.initReactI18next).init({
    interpolation: {
      escapeValue: false
    },
    lng: locale,
    resources: _i18n.default,
    ns: ["main"],
    defaultNS: "main"
  });
  let data = {};

  // init api client
  const client = new _volcanoteideApiClient.default(_objectSpread(_objectSpread({}, apiConfig), {}, {
    locale: locale
  }));
  const form = {
    fields: [{
      title: "commercial_collaboration_type_fieldset",
      fields: [{
        id: "commercial.collaboration_type.id",
        title: "commercial_collaboration_type",
        required: true,
        type: "select",
        options: ["collaborator_partner", "collaborator_retail", "collaborator_credit", "affiliate", "api"]
      }, {
        id: "commercial.agency_type.id",
        title: "commercial_agency_type",
        required: true,
        type: "select",
        options: ["travel_agency", "tourist_intermediary", "lodgin"]
      }, {
        id: "commercial.business_type.id",
        title: "commercial_establishment_type",
        required: true,
        type: "select",
        options: ["establishment_travel_agency", "establishment_apartment", "establishment_hotel", "establishment_cart_hire", "establishment_tourist_office", "establishment_active_tourism", "establishment_lodgin"]
      }, {
        id: "commercial.registry_number",
        title: "commercial_registry_number",
        required: true,
        type: "text"
      }]
    }, {
      title: "commercial_fieldset",
      fields: [{
        id: "commercial.name",
        title: "commercial_name",
        required: true,
        type: "text"
      }, {
        id: "commercial.contact_details.road_type_id",
        title: "address.street_type",
        required: true,
        type: "select",
        options: _streetTypes.default
      }, {
        id: "commercial.contact_details.address",
        title: "address.address",
        required: true,
        type: "text"
      }, {
        id: "commercial.contact_details.postal_code",
        title: "address.postal_code",
        required: true,
        type: "text"
      }, {
        id: "commercial.contact_details.locality",
        title: "address.city",
        required: true,
        type: "text"
      }, {
        id: "commercial.contact_details.state",
        title: "address.province",
        required: true,
        type: "text"
      }, {
        id: "commercial.contact_details.country_id",
        title: "address.country",
        required: true,
        type: "select",
        options: _countries.default
      }]
    }, {
      title: "commercial_contact_fieldset",
      fields: [{
        id: "commercial.booking_contact.name",
        title: "person.name",
        required: true,
        type: "text"
      }, {
        id: "commercial.booking_contact.phone",
        title: "person.phone",
        required: true,
        type: "text"
      }, {
        id: "commercial.booking_contact.email",
        title: "person.email",
        required: true,
        type: "text"
      }]
    }, {
      title: "fiscal_data_fieldset",
      fields: [{
        id: "billing.name",
        title: "fiscal_company_name",
        required: true,
        type: "text"
      }, {
        id: "billing.vat_number",
        title: "fiscal_vat_number",
        required: true,
        type: "text"
      }, {
        id: "billing.contact_details.road_type_id",
        title: "address.street_type",
        required: true,
        type: "select",
        options: _streetTypes.default
      }, {
        id: "billing.contact_details.address",
        title: "address.address",
        required: true,
        type: "text"
      }, {
        id: "billing.contact_details.postal_code",
        title: "address.postal_code",
        required: true,
        type: "text"
      }, {
        id: "billing.contact_details.locality",
        title: "address.city",
        required: true,
        type: "text"
      }, {
        id: "billing.contact_details.state",
        title: "address.province",
        required: true,
        type: "text"
      }, {
        id: "billing.contact_details.country_id",
        title: "address.country",
        required: true,
        type: "select",
        options: _countries.default
      }, {
        id: "billing.contact_details.email",
        title: "person.email",
        required: true,
        type: "email"
      }, {
        id: "billing.contact_details.phone",
        title: "person.phone",
        required: true,
        type: "text"
      }]
    }],
    conditions: conditions
  };
  var initialValues = defaultValues || {};
  form.fields.forEach((fieldset, i) => fieldset.fields.forEach((field, j) => {
    const defaultValue = (0, _get2.default)(initialValues, field.id);
    if (defaultValue) {
      form.fields[i].fields[j].type = (0, _get2.default)(defaultValue, "hidden", false) ? "hidden" : form.fields[i].fields[j].type;
      (0, _set2.default)(initialValues, field.id, defaultValue.value, "");
    } else {
      (0, _set2.default)(initialValues, field.id, "");
    }
  }));
  const onConfirmHandler = values => new Promise((resolve, reject) => {
    values.language = locale;
    client.intermediary.signup(values).then(result => {
      resolve(values);
    });
  });
  const processForm = (form, i18n) => {
    form.fields = form.fields.map(item => {
      item.title = i18n.t(item.title);
      item.fields = item.fields.map(field => {
        const key = field.title;
        field.title = i18n.t(key);
        if (field.options && (0, _isArray.default)(field.options)) {
          var toptions = {};
          field.options.forEach(option => {
            toptions[option] = i18n.t(key + "_options." + option);
          });
          field.options = toptions;
        }
        return field;
      });
      return item;
    });
    return form;
  };
  if (_i18next.default.isInitialized) {
    data = processForm(form, _i18next.default);
  }
  return /*#__PURE__*/_react.default.createElement("div", {
    id: "intermediary-signup-widget",
    className: "intermediary-signup-widget uk-scope"
  }, /*#__PURE__*/_react.default.createElement(_FormWidget.default, {
    locale: locale,
    initialValues: initialValues,
    form: data,
    successConfig: successConfig,
    confirmButton: {
      title: "continue",
      handler: onConfirmHandler
    }
  }));
}