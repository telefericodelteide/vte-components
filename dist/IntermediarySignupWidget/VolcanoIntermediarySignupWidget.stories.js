"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.Localized = exports.DefaultValues = exports.Default = void 0;
var _react = _interopRequireDefault(require("react"));
var _VolcanoIntermediarySignupWidget = _interopRequireDefault(require("./VolcanoIntermediarySignupWidget"));
var _configApi = _interopRequireDefault(require("../../stories/configApi"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
function _objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
function _defineProperty(e, r, t) { return (r = _toPropertyKey(r)) in e ? Object.defineProperty(e, r, { value: t, enumerable: !0, configurable: !0, writable: !0 }) : e[r] = t, e; }
function _toPropertyKey(t) { var i = _toPrimitive(t, "string"); return "symbol" == typeof i ? i : i + ""; }
function _toPrimitive(t, r) { if ("object" != typeof t || !t) return t; var e = t[Symbol.toPrimitive]; if (void 0 !== e) { var i = e.call(t, r || "default"); if ("object" != typeof i) return i; throw new TypeError("@@toPrimitive must return a primitive value."); } return ("string" === r ? String : Number)(t); }
var _default = exports.default = {
  component: _VolcanoIntermediarySignupWidget.default,
  title: "Intermediary signup widget"
};
const Template = args => /*#__PURE__*/_react.default.createElement(_VolcanoIntermediarySignupWidget.default, args);
const testConfirmMessage = {
  title: "¡Gracias por registrarte!",
  body: "\
    <p>Ya falta poco para completar tu alta. Recibirás un correo en el que te vamos a solicitar que nos remitas:</p>\
    <ul>\
      <li>Comprobante de alta como empresa de actividad turística en el Gobierno de Canarias.</li>\
      <li>Acreditación de tener el seguro en vigor (pago de la prima). Esto se te requerirá anualmente.</li>\
    </ul>\
    <p>Una vez recibidos y verificados por nuestro equipo te enviaremos, al correo que has proporcionado en el formulario de registro, el usuario y contraseña con los que podrás acceder a la extranet para empresas colaboradoras del Camino del Barranco de Masca.</p>\
    <p>¡Recuerda comprobar tu carpeta de spam!</p>\
    "
};
const testFormConditions = [{
  id: "terms",
  title: "Acepto los término y condiciones",
  required: true,
  type: "checkbox",
  modal: {
    title: "Términos y condiciones",
    content: "Contenido para los términos y condiciones"
  }
}, {
  id: "data_consent",
  title: "Expreso mi consentimiento al tratamiento de mis datos personales, por TELEFERICO DEL PICO DE TEIDE, S.A. con el fin de poder colaborar en la provisión de visitas, excursiones y actividades al Teide, a través de la plataforma que se ha creado para ello y a la que tengo acceso privado.",
  required: true,
  type: "checkbox"
}];
const Default = exports.Default = Template.bind({});
Default.args = {
  apiConfig: _configApi.default,
  locale: "es",
  conditions: testFormConditions,
  successConfig: testConfirmMessage
};
const DefaultValues = exports.DefaultValues = Template.bind({});
DefaultValues.args = _objectSpread(_objectSpread({}, Default.args), {}, {
  defaultValues: {
    commercial: {
      collaboration_type: {
        id: {
          value: "collaborator_partner",
          hidden: true
        }
      },
      agency_type: {
        id: {
          value: "tourist_intermediary"
        }
      },
      business_type: {
        id: {
          value: "establishment_active_tourism",
          hidden: true
        }
      },
      contact_details: {
        country_id: {
          value: "ES"
        }
      }
    },
    billing: {
      contact_details: {
        country_id: {
          value: "ES"
        }
      }
    }
  }
});
const Localized = exports.Localized = Template.bind({});
Localized.args = _objectSpread(_objectSpread({}, Default.args), {}, {
  locale: "en"
});