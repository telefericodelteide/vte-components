"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "VolcanoBookingManagementWidget", {
  enumerable: true,
  get: function () {
    return _VolcanoBookingManagementWidget.default;
  }
});
Object.defineProperty(exports, "VolcanoBookingWidget", {
  enumerable: true,
  get: function () {
    return _VolcanoBookingWidget.default;
  }
});
Object.defineProperty(exports, "VolcanoCheckoutWidget", {
  enumerable: true,
  get: function () {
    return _VolcanoCheckoutWidget.default;
  }
});
Object.defineProperty(exports, "VolcanoIntermediarySignupWidget", {
  enumerable: true,
  get: function () {
    return _VolcanoIntermediarySignupWidget.default;
  }
});
Object.defineProperty(exports, "VolcanoOrderResultWidget", {
  enumerable: true,
  get: function () {
    return _VolcanoOrderResultWidget.default;
  }
});
Object.defineProperty(exports, "VolcanoUniversalPayWidget", {
  enumerable: true,
  get: function () {
    return _VolcanoUniversalPayWidget.default;
  }
});
var _VolcanoBookingWidget = _interopRequireDefault(require("./BookingWidget/VolcanoBookingWidget"));
var _VolcanoCheckoutWidget = _interopRequireDefault(require("./CheckoutWidget/VolcanoCheckoutWidget"));
var _VolcanoOrderResultWidget = _interopRequireDefault(require("./OrderResultWidget/VolcanoOrderResultWidget"));
var _VolcanoIntermediarySignupWidget = _interopRequireDefault(require("./IntermediarySignupWidget/VolcanoIntermediarySignupWidget"));
var _VolcanoUniversalPayWidget = _interopRequireDefault(require("./UniversalPayWidget/VolcanoUniversalPayWidget"));
var _VolcanoBookingManagementWidget = _interopRequireDefault(require("./BookingManagementWidget/VolcanoBookingManagementWidget"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }