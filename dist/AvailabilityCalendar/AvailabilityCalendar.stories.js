"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.Default = void 0;
var _react = _interopRequireDefault(require("react"));
var _AvailabilityCalendar = _interopRequireDefault(require("./AvailabilityCalendar"));
var _storiesMockups = require("./stories-mockups");
var _i18n = _interopRequireDefault(require("./config/i18n"));
var _i18next = _interopRequireDefault(require("i18next"));
var _reactI18next = require("react-i18next");
var _i18nextHttpBackend = _interopRequireDefault(require("i18next-http-backend"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
var _default = exports.default = {
  component: _AvailabilityCalendar.default,
  title: "Booking widget/Availability calendar"
};
const Template = args => {
  if (!_i18next.default.isInitialized) {
    _i18next.default.use(_i18nextHttpBackend.default).use(_reactI18next.initReactI18next).init({
      interpolation: {
        escapeValue: false
      },
      lng: "es",
      resources: _i18n.default,
      ns: ["calendar"],
      defaultNS: "calendar"
    });
  } else {
    _i18next.default.addResourceBundle(_i18next.default.language, "calendar", _i18n.default[_i18next.default.language]["calendar"]);
  }
  return /*#__PURE__*/_react.default.createElement(_reactI18next.I18nextProvider, {
    i18n: _i18next.default
  }, /*#__PURE__*/_react.default.createElement(_AvailabilityCalendar.default, args));
};
const Default = exports.Default = Template.bind({});
Default.args = {
  onSelection: date => {
    console.log(date);
  },
  availabilityFetcher: () => (0, _storiesMockups.availabilityFetcher)()
};