"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = AvailabilityDay;
var _react = _interopRequireDefault(require("react"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function AvailabilityDay(_ref) {
  let {
    day,
    date
  } = _ref;
  return /*#__PURE__*/_react.default.createElement("span", null, day);
}