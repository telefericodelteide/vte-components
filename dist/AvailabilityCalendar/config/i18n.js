"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _es = _interopRequireDefault(require("./locales/es.json"));
var _en = _interopRequireDefault(require("./locales/en.json"));
var _it = _interopRequireDefault(require("./locales/it.json"));
var _nl = _interopRequireDefault(require("./locales/nl.json"));
var _pl = _interopRequireDefault(require("./locales/pl.json"));
var _de = _interopRequireDefault(require("./locales/de.json"));
var _fr = _interopRequireDefault(require("./locales/fr.json"));
var _ru = _interopRequireDefault(require("./locales/ru.json"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
const i18nResources = {
  es: {
    calendar: _es.default
  },
  en: {
    calendar: _en.default
  },
  it: {
    calendar: _it.default
  },
  nl: {
    calendar: _nl.default
  },
  pl: {
    calendar: _pl.default
  },
  de: {
    calendar: _de.default
  },
  fr: {
    calendar: _fr.default
  },
  ru: {
    calendar: _ru.default
  }
};
var _default = exports.default = i18nResources;