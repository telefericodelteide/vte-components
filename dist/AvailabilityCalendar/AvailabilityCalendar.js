"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = AvailabilityCalendar;
var _react = _interopRequireWildcard(require("react"));
var _reactDatepicker = _interopRequireWildcard(require("react-datepicker"));
var _reactI18next = require("react-i18next");
var _de = _interopRequireDefault(require("date-fns/locale/de"));
var _enGB = _interopRequireDefault(require("date-fns/locale/en-GB"));
var _fr = _interopRequireDefault(require("date-fns/locale/fr"));
var _es = _interopRequireDefault(require("date-fns/locale/es"));
var _it = _interopRequireDefault(require("date-fns/locale/it"));
var _nl = _interopRequireDefault(require("date-fns/locale/nl"));
var _pl = _interopRequireDefault(require("date-fns/locale/pl"));
var _ru = _interopRequireDefault(require("date-fns/locale/ru"));
var _dateFns = require("date-fns");
require("react-datepicker/dist/react-datepicker.css");
require("./availability-calendar.css");
var _formik = require("formik");
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function (e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != typeof e && "function" != typeof e) return { default: e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && {}.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n.default = e, t && t.set(e, n), n; }
(0, _reactDatepicker.registerLocale)("de", _de.default);
(0, _reactDatepicker.registerLocale)("en", _enGB.default);
(0, _reactDatepicker.registerLocale)("fr", _fr.default);
(0, _reactDatepicker.registerLocale)("es", _es.default);
(0, _reactDatepicker.registerLocale)("it", _it.default);
(0, _reactDatepicker.registerLocale)("nl", _nl.default);
(0, _reactDatepicker.registerLocale)("pl", _pl.default);
(0, _reactDatepicker.registerLocale)("ru", _ru.default);
const fetchData = async (setter, callback, args) => {
  try {
    const result = await callback(...args);
    setter(result);
  } catch (error) {}
};
function AvailabilityCalendar(_ref) {
  let {
    availabilityFetcher,
    onSelection,
    availabilityCallBack
  } = _ref;
  const [availability, setAvailability] = (0, _react.useState)([]);
  const [availableDays, setAvailableDays] = (0, _react.useState)([]);
  const {
    t,
    i18n
  } = (0, _reactI18next.useTranslation)("calendar");
  (0, _react.useEffect)(() => {
    fetchData(setAvailability, availabilityFetcher, []);
  }, []);
  (0, _react.useEffect)(() => {
    setAvailableDays(getAvailableDays(availability));
    if ((0, _formik.isFunction)(availabilityCallBack) && availability.length > 0) {
      availabilityCallBack(getAvailableDays(availability));
    }
  }, [availability]);
  const onMonthChangeHandler = date => {
    fetchData(setAvailability, availabilityFetcher, [(0, _dateFns.startOfMonth)(date)]);
  };
  const onDateSelectionHandler = date => {
    const dateStr = (0, _dateFns.format)(date, "yyyy-MM-dd");
    onSelection(availability.find(dayAvailability => dayAvailability.date === dateStr));
  };

  // process available dates
  const getAvailableDays = availability => {
    return availability.filter(day => {
      return day.sessions.length > 0 && day.sessions.some(session => {
        return session.available > 0 || session.available == -1;
      });
    }).map(day => {
      return (0, _dateFns.parseISO)(day.date);
    });
  };
  const isDayAvailable = date => {
    const today = (0, _dateFns.startOfToday)();
    if (date < today) {
      return false;
    }

    // check if date is available
    return availableDays.some(day => {
      return (0, _dateFns.isEqual)(day, date);
    });
  };

  // const renderDayContents = (day, date) => {
  // 	// TODO: buscar el day availability, si hay coincidencia mostrar comprobar si hay sessiones (sessions.session) en el caso de "day_wide" se carga available si es <> -1
  // 	return (
  // 		<span>
  // 			{date.getDate()}
  // 		</span>)
  // };

  return /*#__PURE__*/_react.default.createElement("div", {
    className: "availability-calendar",
    "uk-grid": "uk-grid"
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: "uk-width-auto@m uk-width-1-1@s"
  }, /*#__PURE__*/_react.default.createElement(_reactDatepicker.default, {
    inline: true,
    disabledKeyboardNavigation: true,
    locale: i18n.language
    // renderDayContents={renderDayContents}
    //selected={selected}
    ,
    filterDate: isDayAvailable,
    dayClassName: date => isDayAvailable(date) ? "available" : undefined,
    minDate: new Date(),
    onMonthChange: onMonthChangeHandler,
    onChange: onDateSelectionHandler
  })), /*#__PURE__*/_react.default.createElement("div", {
    className: "calendar-legend uk-width-expand@m uk-width-1-1@s"
  }, /*#__PURE__*/_react.default.createElement("ul", null, /*#__PURE__*/_react.default.createElement("li", null, /*#__PURE__*/_react.default.createElement("span", {
    className: "legend-color available-day"
  }), t("available")), /*#__PURE__*/_react.default.createElement("li", null, /*#__PURE__*/_react.default.createElement("span", {
    className: "legend-color selected-day"
  }), t("selectedDay")), /*#__PURE__*/_react.default.createElement("li", null, /*#__PURE__*/_react.default.createElement("span", {
    className: "legend-color not-available-day"
  }), t("notAvailable")), /*#__PURE__*/_react.default.createElement("li", null, /*#__PURE__*/_react.default.createElement("span", {
    className: "legend-color special-rate-day"
  }), t("specialRate")), /*#__PURE__*/_react.default.createElement("li", null, /*#__PURE__*/_react.default.createElement("span", {
    className: "legend-color available-places-day"
  }, /*#__PURE__*/_react.default.createElement("i", {
    className: "step-icon fa fa-user",
    "aria-hidden": "true"
  })), t("availablePlaces")))));
}