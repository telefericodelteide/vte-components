"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Session = Session;
exports.default = SessionList;
var _react = _interopRequireWildcard(require("react"));
require("./session.css");
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function (e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != typeof e && "function" != typeof e) return { default: e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && {}.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n.default = e, t && t.set(e, n), n; }
function Session(_ref) {
  let {
    session,
    selected,
    onSelection,
    hiddenAvailable
  } = _ref;
  const [isSelected, setIsSelected] = (0, _react.useState)(selected);
  (0, _react.useEffect)(() => {
    setIsSelected(selected);
  }, [selected]);
  const selectionHandler = session => {
    setIsSelected(true);
    onSelection(session.session);
  };
  const sessionPlaces = () => {
    return /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, /*#__PURE__*/_react.default.createElement("span", {
      className: "separator-ticket"
    }), /*#__PURE__*/_react.default.createElement("span", {
      className: "session-places"
    }, session.available >= 10 ? "+10" : session.available, " ", /*#__PURE__*/_react.default.createElement("i", {
      className: "fa fa-ticket-alt",
      "aria-hidden": "true"
    })));
  };
  return /*#__PURE__*/_react.default.createElement("div", {
    className: "session" + (isSelected ? " selected" : ""),
    onClick: () => selectionHandler(session)
  }, /*#__PURE__*/_react.default.createElement("div", null, /*#__PURE__*/_react.default.createElement("span", {
    className: "session-time"
  }, session.session.substring(0, session.session.length - 3), " ", /*#__PURE__*/_react.default.createElement("i", {
    className: "fa fa-clock",
    "aria-hidden": "true"
  })), !hiddenAvailable && sessionPlaces()));
}
function SessionList(_ref2) {
  let {
    sessions,
    selected,
    onSelection,
    hiddenAvailable
  } = _ref2;
  const [selectedSession, setSelectedSession] = (0, _react.useState)(selected);
  const selectionHandler = session => {
    setSelectedSession(session);
    onSelection(session);
  };
  return /*#__PURE__*/_react.default.createElement("div", {
    className: "sessions-container uk-flex uk-flex-wrap"
  }, sessions.filter(session => session.available > 0).map(session => /*#__PURE__*/_react.default.createElement(Session, {
    key: session.session,
    session: session,
    selected: session.session === selectedSession,
    onSelection: selectionHandler,
    hiddenAvailable: hiddenAvailable
  })));
}