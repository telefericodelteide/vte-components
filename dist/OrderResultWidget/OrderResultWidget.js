"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = OrderResultWidget;
var _react = _interopRequireDefault(require("react"));
var _Loading = _interopRequireDefault(require("../Generic/Loading"));
var _Alert = _interopRequireDefault(require("../Generic/Alert"));
var _isEmpty2 = _interopRequireDefault(require("lodash/isEmpty"));
var _get2 = _interopRequireDefault(require("lodash/get"));
var _template2 = _interopRequireDefault(require("lodash/template"));
require("./order-result-widget.css");
var _download = _interopRequireDefault(require("./assets/download.png"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
const DEFAULT_LOCALE = "es";
function BookingResult(_ref) {
  let {
    locale,
    booking,
    translations
  } = _ref;
  const buildLabel = (className, label, content) => {
    return /*#__PURE__*/_react.default.createElement("div", {
      className: className + " ow-booking-label"
    }, /*#__PURE__*/_react.default.createElement("span", {
      className: "ow-booking-label-title"
    }, label, ":"), /*#__PURE__*/_react.default.createElement("span", {
      className: "ow-booking-label-content"
    }, content));
  };
  return /*#__PURE__*/_react.default.createElement("div", {
    className: "ow-booking uk-scope"
  }, buildLabel("ow-booking-order", translations[locale].labels.order, booking.product.experience.name), /*#__PURE__*/_react.default.createElement("div", {
    className: "ow-booking-pdf"
  }, /*#__PURE__*/_react.default.createElement("div", {
    dangerouslySetInnerHTML: {
      __html: (0, _template2.default)(translations[locale].labels.pdf)({
        product: booking.product.name
      })
    }
  })), /*#__PURE__*/_react.default.createElement("div", {
    className: "ow-booking-download"
  }, /*#__PURE__*/_react.default.createElement("a", {
    title: translations[locale].labels.locator,
    href: booking.links.pdf.href,
    target: "_blank"
  }, /*#__PURE__*/_react.default.createElement("img", {
    src: _download.default
  }))), buildLabel("ow-booking-activity", translations[locale].labels.activity, booking.product.name), buildLabel("ow-booking-locator", translations[locale].labels.locator, booking.locator));
}
function OrderResultWidget(_ref2) {
  let {
    locale,
    bookings,
    failed,
    loading,
    translations
  } = _ref2;
  locale = locale || DEFAULT_LOCALE;
  return /*#__PURE__*/_react.default.createElement("div", {
    className: "order-result-widget uk-scope"
  }, loading && /*#__PURE__*/_react.default.createElement(_Loading.default, null), !loading && bookings.length > 0 && /*#__PURE__*/_react.default.createElement("div", {
    className: "ow-container"
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: "ow-title"
  }, /*#__PURE__*/_react.default.createElement("h1", null, translations[locale].payment_accepted_title)), translations[locale].payment_accepted_info_2 && /*#__PURE__*/_react.default.createElement(_Alert.default, {
    className: "ow-alert-payment-info",
    icon: "fas fa-info",
    type: "red",
    content: translations[locale].payment_accepted_info_2
  }), /*#__PURE__*/_react.default.createElement(_Alert.default, {
    className: "ow-alert-payment-accepted",
    icon: "fas fa-check",
    content: translations[locale].payment_free
  }), translations[locale].payment_accepted_info && /*#__PURE__*/_react.default.createElement(_Alert.default, {
    className: "ow-alert-payment-info",
    icon: "fas fa-info",
    content: translations[locale].payment_accepted_info
  }), /*#__PURE__*/_react.default.createElement(_Alert.default, {
    className: "ow-alert-contact-info",
    icon: "fas fa-question",
    content: translations[locale].contact_info
  }), bookings.map((booking, index) => /*#__PURE__*/_react.default.createElement(BookingResult, {
    key: "booking-" + index,
    locale: locale,
    booking: booking,
    translations: translations
  }))), !loading && failed && /*#__PURE__*/_react.default.createElement("div", {
    className: "ow-container"
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: "ow-title"
  }, /*#__PURE__*/_react.default.createElement("h1", null, translations[locale].payment_denied_title)), /*#__PURE__*/_react.default.createElement(_Alert.default, {
    className: "ow-alert-payment-denied",
    icon: "fas fa-exclamation-circle",
    content: translations[locale].payment_denied
  })));
}