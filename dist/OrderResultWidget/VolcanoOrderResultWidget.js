"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = VolcanoOrderResultWidget;
var _react = _interopRequireWildcard(require("react"));
var _uikit = _interopRequireDefault(require("uikit"));
require("uikit/dist/css/uikit.min.css");
var _volcanoteideApiClient = _interopRequireDefault(require("@volcanoteide/volcanoteide-api-client"));
var _OrderResultWidget = _interopRequireDefault(require("./OrderResultWidget"));
var _isError = _interopRequireDefault(require("lodash/isError"));
var _formik = require("formik");
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function (e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != typeof e && "function" != typeof e) return { default: e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && {}.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n.default = e, t && t.set(e, n), n; }
function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
function _objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
function _defineProperty(e, r, t) { return (r = _toPropertyKey(r)) in e ? Object.defineProperty(e, r, { value: t, enumerable: !0, configurable: !0, writable: !0 }) : e[r] = t, e; }
function _toPropertyKey(t) { var i = _toPrimitive(t, "string"); return "symbol" == typeof i ? i : i + ""; }
function _toPrimitive(t, r) { if ("object" != typeof t || !t) return t; var e = t[Symbol.toPrimitive]; if (void 0 !== e) { var i = e.call(t, r || "default"); if ("object" != typeof i) return i; throw new TypeError("@@toPrimitive must return a primitive value."); } return ("string" === r ? String : Number)(t); }
const BASE_SELECTION = {
  productId: null,
  date: null,
  session: null,
  rates: []
};
function VolcanoOrderResultWidget(_ref) {
  let {
    locale,
    apiConfig,
    params,
    bookingId,
    translations,
    onProcessTransaction
  } = _ref;
  if (typeof window !== "undefined") {
    _uikit.default.container = '.uk-scope';
  }

  // init api client
  const client = new _volcanoteideApiClient.default(_objectSpread(_objectSpread({}, apiConfig), {}, {
    locale: locale
  }));
  const [bookings, setBookings] = (0, _react.useState)([]);
  const [isLoading, setIsLoading] = (0, _react.useState)(true);
  const [isFailed, setIsFailed] = (0, _react.useState)(false);
  (0, _react.useEffect)(() => {
    setIsLoading(true);
    const transactionId = params && params.vte_transaction_id;
    client.paymentTransaction.processTransactionResult(params).then(bookings => {
      setIsLoading(false);
      setBookings(bookings);
      if ((0, _formik.isFunction)(onProcessTransaction)) {
        onProcessTransaction(bookings);
      }
    }).catch(err => {
      if (err instanceof Error) {
        setIsLoading(false);
        setIsFailed(true);
      } else {
        console.warn(err);
      }
    });
  }, []);
  return /*#__PURE__*/_react.default.createElement(_OrderResultWidget.default, {
    locale: locale,
    bookings: bookings,
    failed: isFailed,
    loading: isLoading,
    translations: translations
  });
}