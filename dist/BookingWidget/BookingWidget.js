"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = BookingWidget;
var _react = _interopRequireWildcard(require("react"));
var _uikit = _interopRequireDefault(require("uikit"));
require("uikit/dist/css/uikit.min.css");
var _de = _interopRequireDefault(require("date-fns/locale/de"));
var _enGB = _interopRequireDefault(require("date-fns/locale/en-GB"));
var _fr = _interopRequireDefault(require("date-fns/locale/fr"));
var _es = _interopRequireDefault(require("date-fns/locale/es"));
var _dateFns = require("date-fns");
var _StepNav = _interopRequireDefault(require("./StepNav"));
var _ProductList = _interopRequireDefault(require("./ProductList/ProductList"));
var _AvailabilityCalendar = _interopRequireDefault(require("../AvailabilityCalendar/AvailabilityCalendar"));
var _SessionList = _interopRequireDefault(require("../SessionList/SessionList"));
var _GroupedRateList = _interopRequireDefault(require("./RateList/GroupedRateList"));
var _Loading = _interopRequireDefault(require("../Generic/Loading"));
var _isEmpty2 = _interopRequireDefault(require("lodash/isEmpty"));
var _get2 = _interopRequireDefault(require("lodash/get"));
require("./booking-widget.css");
var _Modal = _interopRequireDefault(require("../Generic/Modal"));
var _lodashEs = require("lodash-es");
var _reactI18next = require("react-i18next");
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function (e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != typeof e && "function" != typeof e) return { default: e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && {}.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n.default = e, t && t.set(e, n), n; }
function _extends() { return _extends = Object.assign ? Object.assign.bind() : function (n) { for (var e = 1; e < arguments.length; e++) { var t = arguments[e]; for (var r in t) ({}).hasOwnProperty.call(t, r) && (n[r] = t[r]); } return n; }, _extends.apply(null, arguments); }
function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
function _objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
function _defineProperty(e, r, t) { return (r = _toPropertyKey(r)) in e ? Object.defineProperty(e, r, { value: t, enumerable: !0, configurable: !0, writable: !0 }) : e[r] = t, e; }
function _toPropertyKey(t) { var i = _toPrimitive(t, "string"); return "symbol" == typeof i ? i : i + ""; }
function _toPrimitive(t, r) { if ("object" != typeof t || !t) return t; var e = t[Symbol.toPrimitive]; if (void 0 !== e) { var i = e.call(t, r || "default"); if ("object" != typeof i) return i; throw new TypeError("@@toPrimitive must return a primitive value."); } return ("string" === r ? String : Number)(t); }
const DEFAULT_LOCALE = "es";
const BASE_SELECTION = {
  productId: null,
  date: null,
  session: null,
  rates: [],
  active: true
};

/** Available selection steps */
const STEP_PRODUCT = 1;
const STEP_DATE = 2;
const STEP_SESSION = 3;
const STEP_RATES = 4;
const DEFAULT_STEPS = [STEP_PRODUCT, STEP_DATE, STEP_SESSION, STEP_RATES];
const locales = {
  de: _de.default,
  en: _enGB.default,
  fr: _fr.default,
  es: _es.default
};
const MASCA_PRODUCTS = [1925, 1927, 1946, 2099, 2123, 1957, 1958, 1959, 2101];
const MASCA_MESSAGE = {
  es: "Las personas residentes en Tenerife acceden gratuitamente. Por ello, en el control de acceso, al verificar la residencia, se tramitará la solicitud de devolución del importe abonado.",
  en: "Residents of Tenerife have free access. Therefore, at the access control point, upon verifying residency, the request for a refund of the amount paid will be processed.",
  de: "Einwohner von Teneriffa haben freien Zugang. Daher wird am Zugangskontrollpunkt, nach Überprüfung des Wohnsitzes, der Antrag auf Rückerstattung des gezahlten Betrags bearbeitet."
};
const fetchData = async (setter, callback, args) => {
  try {
    const result = await callback(...args);
    setter(result);
  } catch (error) {}
};
const getRatesQty = rates => {
  let qty = 0;
  rates.forEach(rate => {
    qty += rate.qty;
  });
  return qty;
};
function BookingTotal(_ref) {
  let {
    rates,
    selectedRates,
    locale,
    currency
  } = _ref;
  const {
    t
  } = (0, _reactI18next.useTranslation)();
  const priceFormatter = new Intl.NumberFormat(locale, {
    style: "currency",
    currency: currency || "eur"
  });
  const priceFormatted = (currency, price) => {
    let formattedPrice = priceFormatter.format(price);
    if (currency && currency === 'PLN') {
      formattedPrice = formattedPrice.replace('PLN', 'zł');
    }
    return formattedPrice;
  };
  const getTotalAmount = () => {
    let total = 0;
    selectedRates.forEach(selRate => {
      total += rates.find(rate => rate.id === selRate.id).pvp * selRate.qty;
    });
    return total;
  };
  return /*#__PURE__*/_react.default.createElement("div", {
    className: "bw-total-amount"
  }, /*#__PURE__*/_react.default.createElement("div", null, t("total"), " ", priceFormatted(currency, getTotalAmount())));
}
function InfoBox(_ref2) {
  let {
    product
  } = _ref2;
  const content = (0, _get2.default)(product, "booking_process_information.product_information");
  if (content) {
    return /*#__PURE__*/_react.default.createElement("div", {
      className: "bw-info-box"
    }, /*#__PURE__*/_react.default.createElement("i", {
      className: "bw-info-box-icon far fa-thumbs-up"
    }), /*#__PURE__*/_react.default.createElement("span", {
      className: "bw-info-box-content"
    }, content));
  } else {
    return null;
  }
}
function AlertBox(_ref3) {
  let {
    children
  } = _ref3;
  return /*#__PURE__*/_react.default.createElement("div", {
    className: "bw-alert-box"
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: "bw-alert-box-content"
  }, children));
}
function BookingWidget(_ref4) {
  let {
    locale,
    experience,
    availableSteps,
    defaultSelection,
    onConfirm,
    productsFetcher,
    productFetcher,
    availabilityFetcher,
    ratesFetcher,
    availabilityCheckIsActive,
    loading,
    trackingCallbacks: {
      onBeginCheckoutStep1DLCallback,
      onBeginCheckoutStep2DLCallback,
      onBeginCheckoutStep3DLCallback,
      onBeginCheckoutStep4DLCallback
    } = {}
  } = _ref4;
  locale = locale || DEFAULT_LOCALE;
  availableSteps = availableSteps || DEFAULT_STEPS;
  const {
    t
  } = (0, _reactI18next.useTranslation)();
  if (typeof window !== "undefined") {
    _uikit.default.container = ".uk-scope";
  }
  const initSelection = selection => {
    let result = _objectSpread(_objectSpread({}, BASE_SELECTION), selection);
    if (result.productId) {
      result.productId = parseInt(result.productId);
    }
    return result;
  };
  const productList = productsFetcher();
  const [currentStep, setCurrentStep] = (0, _react.useState)(null);
  const [sessionList, setSessionList] = (0, _react.useState)([]);
  const [rateList, setRateList] = (0, _react.useState)([]);
  const [selection, setSelection] = (0, _react.useState)(initSelection(defaultSelection || {}));
  const [bookingReady, setBookingReady] = (0, _react.useState)(false);
  const [availabilityCalendar, setAvailabilityCalendar] = (0, _react.useState)('');
  const getProduct = productId => productList.find(product => product.id === productId);
  (0, _react.useEffect)(() => {
    setCurrentStep(checkStep());
    // check booking is ready
    const qty = getRatesQty(selection.rates);
    const product = getProduct(selection.productId);
    if (product && qty > 0 && qty >= product.qty_restrictions.min && (qty <= product.qty_restrictions.max || product.qty_restrictions.max === null || product.qty_restrictions.max === -1)) {
      setBookingReady(true);
    } else {
      setBookingReady(false);
    }
  }, [selection]);
  (0, _react.useEffect)(() => {
    if (Number(currentStep) && checkIsFunctionTrackingCallbacks()) {
      processTrackingCallback();
    }
  }, [currentStep]);
  (0, _react.useEffect)(() => {
    if (rateList && rateList.length > 0 && (0, _lodashEs.isFunction)(onBeginCheckoutStep4DLCallback)) {
      onBeginCheckoutStep4DLCallback({
        product: getProduct(selection.productId),
        selection: selection,
        rateList: rateList
      });
    }
  }, [rateList]);
  const checkIsFunctionTrackingCallbacks = () => {
    return !!((0, _lodashEs.isFunction)(onBeginCheckoutStep1DLCallback) || (0, _lodashEs.isFunction)(onBeginCheckoutStep2DLCallback) || (0, _lodashEs.isFunction)(onBeginCheckoutStep3DLCallback) || (0, _lodashEs.isFunction)(onBeginCheckoutStep4DLCallback));
  };
  const availabilityFetcherWrapper = date => {
    if (!date) {
      date = new Date();
    }
    return availabilityFetcher(selection.productId, date);
  };
  const onSelectStepHandler = step => {
    setCurrentStep(step);
  };
  const processTrackingCallback = () => {
    switch (currentStep) {
      case STEP_PRODUCT:
        if ((0, _lodashEs.isFunction)(onBeginCheckoutStep1DLCallback)) {
          onBeginCheckoutStep1DLCallback();
        }
        break;
      case STEP_SESSION:
        if ((0, _lodashEs.isFunction)(onBeginCheckoutStep3DLCallback)) {
          onBeginCheckoutStep3DLCallback({
            product: getProduct(selection.productId),
            availability: availabilityCalendar
          });
        }
        break;
    }
  };
  const getAvailabilityText = availability => {
    const monthNames = ["enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"];
    if (availability.length < 1) {
      return '';
    }
    const monthStr = monthNames[availability[0].getMonth()];
    const yearStr = availability[0].getFullYear();
    return monthStr + yearStr + '_' + availability.length;
  };
  const availabilityCallBack = availability => {
    if ((0, _lodashEs.isFunction)(onBeginCheckoutStep2DLCallback)) {
      const availabilityText = getAvailabilityText(availability);
      setAvailabilityCalendar(availabilityText);
      const data = {
        product: getProduct(selection.productId),
        availability: availabilityText
      };
      onBeginCheckoutStep2DLCallback(data);
    }
  };
  const onProductSelectionHandler = product => {
    if (availabilityCheckIsActive) {
      availabilityCheckIsActive(product, new Date()).then(isActive => {
        setSelection(_objectSpread(_objectSpread({}, BASE_SELECTION), {}, {
          productId: product,
          active: isActive
        }));
      });
    } else {
      setSelection(_objectSpread(_objectSpread({}, BASE_SELECTION), {}, {
        productId: product,
        active: true
      }));
    }
  };
  const onDateSelectionHandler = date => {
    productFetcher(selection.productId, date.date).then(product => {
      const productIndex = productList.findIndex(pr => pr.id === product.id);
      productList[productIndex] = product;
      if (date.sessions.length === 1 && date.sessions[0].session === "day_wide") {
        setSelection(_objectSpread(_objectSpread({}, BASE_SELECTION), {}, {
          productId: selection.productId,
          active: selection.active,
          date: date.date,
          session: "day_wide"
        }));
        setRateList([]);
        fetchData(setRateList, ratesFetcher, [selection.productId, date.date]);
      } else {
        setSelection(_objectSpread(_objectSpread({}, BASE_SELECTION), {}, {
          productId: selection.productId,
          active: selection.active,
          date: date.date
        }));
      }
    });
    setSessionList(date.sessions);
  };
  const onSessionSelectionHandler = session => {
    setSelection(_objectSpread(_objectSpread({}, BASE_SELECTION), {}, {
      productId: selection.productId,
      active: selection.active,
      date: selection.date,
      session: session
    }));
    setRateList([]);
    fetchData(setRateList, ratesFetcher, [selection.productId, selection.date]);
  };
  const onRatesSelectionHandler = rates => {
    setSelection(_objectSpread(_objectSpread({}, BASE_SELECTION), {}, {
      productId: selection.productId,
      active: selection.active,
      date: selection.date,
      session: selection.session,
      rates: rates
    }));
  };
  const onConfirmHandler = () => {
    onConfirm(selection);
  };

  /**
   * Returns the step required with the current selection.
   */
  const checkStep = () => {
    // check desired step is available
    if (!selection.productId) {
      return STEP_PRODUCT;
    }
    if (!selection.date) {
      return STEP_DATE;
    }
    if (!selection.session) {
      if (sessionList.length === 0) {
        return STEP_DATE;
      } else if (sessionList[0].session === "day_wide") {
        return STEP_RATES;
      } else {
        return STEP_SESSION;
      }
    }
    return STEP_RATES;
  };
  const isStepDisabled = step => {
    if (currentStep > step) {
      return false;
    } else if (currentStep < step) {
      switch (step) {
        case STEP_PRODUCT:
          return !selection.productId;
        case STEP_DATE:
          return !selection.date;
        case STEP_SESSION:
          return !selection.session;
        case STEP_RATES:
          return selection.rates.length === 0;
        default:
          return true;
      }
    }
    return true;
  };
  const getSteps = () => {
    let availableSteps = [1, 2, 4];
    if (selection.date) {
      if (sessionList.length > 0 && sessionList[0].session !== "day_wide") {
        availableSteps = [1, 2, 3, 4];
      }
    }
    return availableSteps.map((step, index) => {
      const result = {
        id: step,
        content: {
          number: index + 1,
          icon: null,
          title: null,
          text: null,
          info: null
        }
      };
      switch (step) {
        case STEP_PRODUCT:
          result.content.componentTitle = t("select.product");
          result.content.title = t("product");
          result.content.icon = /*#__PURE__*/_react.default.createElement("i", {
            className: "step-icon fa fa-play-circle",
            "aria-hidden": "true"
          });
          if (selection.productId) {
            const product = productList.find(product => product.id === selection.productId);
            result.content.text = product ? product.name : "";
          }
          break;
        case STEP_DATE:
          result.content.componentTitle = t("select.date");
          result.content.title = t("date");
          result.content.icon = /*#__PURE__*/_react.default.createElement("i", {
            className: "step-icon far fa-calendar-alt",
            "aria-hidden": "true"
          });
          if (selection.date) {
            result.content.text = (0, _dateFns.format)((0, _dateFns.parseISO)(selection.date), "P", {
              locale: locales[locale]
            });
          }
          break;
        case STEP_SESSION:
          result.content.componentTitle = t("select.session");
          result.content.title = t("session");
          result.content.icon = /*#__PURE__*/_react.default.createElement("i", {
            className: "step-icon far fa-clock",
            "aria-hidden": "true"
          });
          if (selection.session) {
            const session = selection.session;
            result.content.text = session.substring(0, session.length - 3);
          }
          break;
        case STEP_RATES:
          result.content.componentTitle = t("select.rate");
          result.content.title = t("rate");
          result.content.icon = /*#__PURE__*/_react.default.createElement("i", {
            className: "step-icon fa fa-ticket-alt fa-rotate-45",
            "aria-hidden": "true"
          });
          if (selection.session) {
            const session = sessionList.find(session => session.session === selection.session);
            result.content.text = getRatesQty(selection.rates) + " " + t("tickets");
            if (session.available > 0) {
              result.content.info = /*#__PURE__*/_react.default.createElement("span", {
                className: "tickets-availables"
              }, /*#__PURE__*/_react.default.createElement("b", null, session.available), /*#__PURE__*/_react.default.createElement("i", {
                className: "fa fa-ticket-alt fa-rotate-45"
              }), t("availables"));
            }
          }
          break;
        default:
          break;
      }
      return result;
    });
  };
  const notAvailableMessage = () => {
    const notAvailableMsg = getProduct(selection.productId).booking_process_information.not_available_message;
    return notAvailableMsg ? /*#__PURE__*/_react.default.createElement("div", {
      className: "uk-text-default uk-text-justify uk-align"
    }, notAvailableMsg) : /*#__PURE__*/_react.default.createElement("div", {
      className: "uk-text-large uk-text-center"
    }, t("not_active"));
  };
  const buildStepComponent = () => {
    switch (currentStep) {
      case STEP_PRODUCT:
        return /*#__PURE__*/_react.default.createElement(_ProductList.default, {
          locale: locale,
          productList: productList,
          selected: selection.productId,
          onSelection: onProductSelectionHandler
        });
      case STEP_DATE:
        if (!selection.active) {
          return /*#__PURE__*/_react.default.createElement(AlertBox, null, notAvailableMessage());
        } else {
          return /*#__PURE__*/_react.default.createElement(_AvailabilityCalendar.default, {
            onSelection: onDateSelectionHandler,
            availabilityFetcher: availabilityFetcherWrapper,
            availabilityCallBack: availabilityCallBack
          });
        }
      case STEP_SESSION:
        return /*#__PURE__*/_react.default.createElement(_SessionList.default, {
          sessions: sessionList,
          selected: selection.session,
          onSelection: onSessionSelectionHandler
        });
      case STEP_RATES:
        const product = productList.find(product => product.id === selection.productId);
        const session = sessionList.find(session => session.session === selection.session);
        let bookingConfirmation = null;
        if (!(0, _isEmpty2.default)(product.booking_process_information.confirmation)) {
          bookingConfirmation = {
            id: "booking-confirmation",
            header: (0, _get2.default)(product, "booking_process_information.confirmation.title"),
            children: /*#__PURE__*/_react.default.createElement("div", {
              dangerouslySetInnerHTML: {
                __html: (0, _get2.default)(product, "booking_process_information.confirmation.content")
              }
            }),
            check: (0, _get2.default)(product, "booking_process_information.confirmation.checkboxes"),
            onConfirm: onConfirmHandler
          };
        }
        const showConfirmationModal = () => {
          _uikit.default.modal("#booking-confirmation", {
            container: "#booking-widget"
          }).show();
        };
        return /*#__PURE__*/_react.default.createElement("div", null, /*#__PURE__*/_react.default.createElement(_GroupedRateList.default, {
          locale: locale,
          rates: rateList,
          groupId: 6,
          groupName: t("resident"),
          visibleGroupValue: 19,
          selected: selection.rates,
          max: Math.min(session.available, product.max_pax === -1 || !product.max_pax ? Number.MAX_SAFE_INTEGER : product.max_pax),
          ageRestrictions: experience.age_restriction,
          message: MASCA_PRODUCTS.includes(selection.productId) && MASCA_MESSAGE[locale],
          onChange: onRatesSelectionHandler
        }), /*#__PURE__*/_react.default.createElement(BookingTotal, {
          locale: locale,
          currency: "eur",
          rates: rateList,
          selectedRates: selection.rates
        }), /*#__PURE__*/_react.default.createElement("div", {
          className: "bw-confirm"
        }, /*#__PURE__*/_react.default.createElement("button", {
          className: "uk-button uk-button-default",
          disabled: !bookingReady,
          onClick: (0, _isEmpty2.default)(bookingConfirmation) ? onConfirmHandler : showConfirmationModal
        }, t("buy")), !(0, _isEmpty2.default)(bookingConfirmation) && /*#__PURE__*/_react.default.createElement(_Modal.default, _extends({
          locale: locale
        }, bookingConfirmation))));
      default:
        return null;
    }
  };
  return /*#__PURE__*/_react.default.createElement("div", {
    id: "booking-widget",
    className: "booking-widget uk-scope"
  }, loading && /*#__PURE__*/_react.default.createElement(_Loading.default, null), /*#__PURE__*/_react.default.createElement("div", {
    className: "bw-container uk-grid-collapse",
    "uk-grid": "uk-grid"
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: "bw-head-title uk-width-1-1",
    ukgrid: "ukgrid"
  }, /*#__PURE__*/_react.default.createElement("h3", {
    className: "uk-width-1-1@m uk-width-1-4@m"
  }, t("buy"))), /*#__PURE__*/_react.default.createElement("div", {
    className: "bw-sidebar uk-visible@m uk-width-1-3@m"
  }, /*#__PURE__*/_react.default.createElement("ul", {
    key: "bw-steps"
  }, getSteps().map(step => /*#__PURE__*/_react.default.createElement(_StepNav.default, {
    key: step.id,
    step: step.id,
    content: step.content,
    selected: step.id === currentStep,
    disabled: isStepDisabled(step.id),
    onSelection: onSelectStepHandler
  }))), /*#__PURE__*/_react.default.createElement(InfoBox, {
    key: "booking-info",
    product: getProduct(selection.productId)
  })), /*#__PURE__*/_react.default.createElement("div", {
    className: "bw-main uk-width-2-3@m"
  }, /*#__PURE__*/_react.default.createElement("ul", {
    key: "bw-steps"
  }, getSteps().map(step => /*#__PURE__*/_react.default.createElement(_StepNav.default, {
    key: step.id,
    step: step.id,
    content: step.content,
    selected: step.id === currentStep,
    disabled: isStepDisabled(step.id),
    responsiveHeader: true,
    onSelection: onSelectStepHandler,
    component: step.id === currentStep ? buildStepComponent() : null
  }))))));
}