"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.trackingCallbacks = exports.default = exports.SelectedProduct = exports.Localized = exports.Default = exports.AvailabilityBlocked = void 0;
var _react = _interopRequireDefault(require("react"));
var _VolcanoBookingWidget = _interopRequireDefault(require("./VolcanoBookingWidget"));
var _configApi = _interopRequireDefault(require("../../stories/configApi"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
function _objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
function _defineProperty(e, r, t) { return (r = _toPropertyKey(r)) in e ? Object.defineProperty(e, r, { value: t, enumerable: !0, configurable: !0, writable: !0 }) : e[r] = t, e; }
function _toPropertyKey(t) { var i = _toPrimitive(t, "string"); return "symbol" == typeof i ? i : i + ""; }
function _toPrimitive(t, r) { if ("object" != typeof t || !t) return t; var e = t[Symbol.toPrimitive]; if (void 0 !== e) { var i = e.call(t, r || "default"); if ("object" != typeof i) return i; throw new TypeError("@@toPrimitive must return a primitive value."); } return ("string" === r ? String : Number)(t); }
var _default = exports.default = {
  component: _VolcanoBookingWidget.default,
  title: "Volcano booking widget"
};
const Template = args => /*#__PURE__*/_react.default.createElement(_VolcanoBookingWidget.default, args);
const Default = exports.Default = Template.bind({});
Default.args = {
  apiConfig: _configApi.default,
  locale: "es",
  experienceId: 735,
  onConfirm: () => console.log('confirm') // navigate next page
};
const Localized = exports.Localized = Template.bind({});
Localized.args = _objectSpread(_objectSpread({}, Default.args), {}, {
  locale: "en",
  apiConfig: _objectSpread(_objectSpread({}, Default.args.apiConfig), {}, {
    locale: "en"
  }),
  checkIsActive: false
});
const SelectedProduct = exports.SelectedProduct = Template.bind({});
SelectedProduct.args = _objectSpread(_objectSpread({}, Default.args), {}, {
  defaultSelection: {
    productId: "1927"
  }
});
const AvailabilityBlocked = exports.AvailabilityBlocked = Template.bind({});
AvailabilityBlocked.args = _objectSpread(_objectSpread({}, Default.args), {}, {
  checkIsActive: true
});
const trackingCallbacks = exports.trackingCallbacks = Template.bind({});
trackingCallbacks.args = _objectSpread(_objectSpread({}, Default.args), {}, {
  checkIsActive: true,
  trackingCallbacks: {
    onBeginCheckoutStep1DLCallback: () => console.log('Contenedor info step1'),
    //step 1
    onBeginCheckoutStep2DLCallback: data => console.log('Contenedor info step2', data),
    //step 2
    onBeginCheckoutStep3DLCallback: data => console.log('Contenedor info step3', data),
    //step 3
    onBeginCheckoutStep4DLCallback: data => console.log('Contenedor info step4', data) //step 4
  },
  onConfirm: () => console.log('confirm') // navigate next page
});