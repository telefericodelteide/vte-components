"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = ProductList;
var _react = _interopRequireWildcard(require("react"));
require("./product-list.css");
require("@fortawesome/fontawesome-free/css/all.min.css");
var _FlagList = _interopRequireDefault(require("../../Generic/FlagList/FlagList"));
var _calendarIcon = _interopRequireDefault(require("../../assets/calendar-icon.svg"));
var _reactI18next = require("react-i18next");
var _lodash = _interopRequireDefault(require("lodash"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function (e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != typeof e && "function" != typeof e) return { default: e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && {}.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n.default = e, t && t.set(e, n), n; }
function WeekDays(_ref) {
  let {
    weekdays,
    locale
  } = _ref;
  const {
    t
  } = (0, _reactI18next.useTranslation)();
  const processWeekDays = processDays => {
    const availableWeekDays = {
      1: "monday",
      2: "tuesday",
      4: "wednesday",
      8: "thursday",
      16: "friday",
      32: "saturday",
      64: "sunday"
    };
    return Object.keys(availableWeekDays).filter(key => processDays & key).map(key => availableWeekDays[key]);
  };
  const days = processWeekDays(weekdays);
  let textReturn;
  if (days.length === 0) {
    textReturn = /*#__PURE__*/_react.default.createElement("div", {
      className: "day"
    }, t("product_list.noDays"));
  } else if (days.length === 7) {
    textReturn = /*#__PURE__*/_react.default.createElement("div", {
      className: "day"
    }, t("product_list.allDays"));
  } else {
    textReturn = days.map((day, index) => /*#__PURE__*/_react.default.createElement("div", {
      key: index,
      className: "day"
    }, t("product_list.weekdays." + day)));
  }
  return /*#__PURE__*/_react.default.createElement("div", {
    className: "weekdays"
  }, /*#__PURE__*/_react.default.createElement("img", {
    className: "uk-visible@m",
    src: _calendarIcon.default,
    alt: "calendar"
  }), /*#__PURE__*/_react.default.createElement("div", {
    className: "days-container",
    "uk-flex": "true"
  }, textReturn));
}
function ProductContent(_ref2) {
  let {
    htmlString
  } = _ref2;
  const updatedHtmlString = htmlString.replace(/<[a-zA-Z]+(\s+[a-zA-Z]+\s*=\s*("([^"]*)"|'([^']*)'))*\s*\/>/g, '');
  return /*#__PURE__*/_react.default.createElement("div", {
    dangerouslySetInnerHTML: {
      __html: updatedHtmlString
    }
  });
}
function Product(_ref3) {
  let {
    product,
    locale,
    selected,
    onSelection
  } = _ref3;
  const [viewMore, setViewMore] = (0, _react.useState)(false);
  const {
    t
  } = (0, _reactI18next.useTranslation)();
  const id = "product-" + product.id;
  const numberFormat = new Intl.NumberFormat(locale, {
    style: "currency",
    currency: product.lowest_rate.currency
  });
  return /*#__PURE__*/_react.default.createElement("div", {
    className: "product"
  }, /*#__PURE__*/_react.default.createElement("label", null, /*#__PURE__*/_react.default.createElement("input", {
    type: "radio",
    className: "product-radio",
    name: "product",
    value: product.id,
    defaultChecked: selected,
    onClick: () => {
      onSelection(product.id);
    }
  }), /*#__PURE__*/_react.default.createElement("span", {
    className: "product-name"
  }, product.name)), /*#__PURE__*/_react.default.createElement("div", {
    className: "more-info"
  }, /*#__PURE__*/_react.default.createElement("span", {
    className: "product-price"
  }, numberFormat.format(product.lowest_rate.value)), /*#__PURE__*/_react.default.createElement("a", {
    className: "more-info-link",
    "uk-toggle": "animation: uk-animation-fade; target: #more-info-" + id,
    href: "#more-info-" + id,
    onClick: () => setViewMore(!viewMore)
  }, /*#__PURE__*/_react.default.createElement("i", {
    className: "fa fa-plus-circle"
  }), /*#__PURE__*/_react.default.createElement("span", null, viewMore ? t("product_list.viewLess") : t("product_list.viewMore")))), /*#__PURE__*/_react.default.createElement("div", {
    className: "product-flags"
  }, /*#__PURE__*/_react.default.createElement(_FlagList.default, {
    flags: product.languages,
    variant: true
  })), /*#__PURE__*/_react.default.createElement("div", {
    id: "more-info-" + id,
    className: "more-info-container",
    hidden: true
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: "more-info-body"
  }, Object.keys(product.description.summary_block).map(key => /*#__PURE__*/_react.default.createElement(_react.Fragment, {
    key: key
  }, /*#__PURE__*/_react.default.createElement("h4", null, product.description.summary_block[key].title), /*#__PURE__*/_react.default.createElement(ProductContent, {
    htmlString: product.description.summary_block[key].content
  }))))), /*#__PURE__*/_react.default.createElement("div", {
    className: "product-available-weekdays"
  }, /*#__PURE__*/_react.default.createElement(WeekDays, {
    weekdays: parseInt(product.week_days, 2),
    locale: locale
  })));
}
function ProductListFilter(_ref4) {
  let {
    productList,
    onClick
  } = _ref4;
  const [languages, setLanguages] = (0, _react.useState)(initializeLanguages(productList));
  const [disabledLanguages] = (0, _react.useState)(initializeDisabledLanguages(languages, productList));
  const [pickups] = (0, _react.useState)(initializePickups(productList));
  const [selectedLanguages, setSelectedLanguages] = (0, _react.useState)([]);
  const [selectedPickups, setSelectedPickups] = (0, _react.useState)([]);
  const {
    t
  } = (0, _reactI18next.useTranslation)('main');
  if (!(0, _lodash.default)(languages) && disabledLanguages.length === languages.length) {
    setLanguages([]);
  }
  function initializeLanguages(productList) {
    let totalLanguages = [];
    if (productList.length > 2) {
      productList.map(product => {
        product.languages.forEach(language => {
          if (!totalLanguages.includes(language)) {
            totalLanguages.push(language);
          }
        });
      });
      if (totalLanguages.length <= 2) {
        totalLanguages = [];
      }
    }
    return totalLanguages;
  }
  function initializeDisabledLanguages(languages, productList) {
    let totalLanguages = [];
    languages.map(language => {
      totalLanguages[language] = 0;
    });
    if (productList.length > 2) {
      productList.map(product => {
        product.languages.forEach(language => {
          totalLanguages[language] = totalLanguages[language] + 1;
        });
      });
    }
    let disabledLanguages = [];
    languages.map(language => {
      if (totalLanguages[language] === productList.length) {
        disabledLanguages.push(language);
      }
    });
    return disabledLanguages;
  }
  function initializePickups(productList) {
    let totalPickups = [];
    if (productList.length > 2) {
      productList.map(product => {
        product.tags.forEach(tag => {
          if (tag.group && tag.group.key === 'pickups' && !totalPickups.includes(tag.name)) {
            totalPickups.push(tag.name);
          }
        });
      });
      if (totalPickups.length < 2) {
        totalPickups = [];
      }
    }
    return totalPickups;
  }
  const onFilterSelected = (e, type, filter) => {
    let sl = selectedLanguages;
    let sp = selectedPickups;
    if (type === 'language') {
      if (selectedLanguages.includes(filter)) {
        sl = selectedLanguages.filter(language => language !== filter);
      } else {
        sl.push(filter);
      }
      setSelectedLanguages(sl);
    } else if (type === 'pickup') {
      if (selectedPickups.includes(filter)) {
        sp = selectedPickups.filter(pickup => pickup !== filter);
      } else {
        sp.push(filter);
      }
      setSelectedPickups(sp);
    }
    onClick({
      "languages": sl,
      "pickups": sp
    });
  };
  function getFilter(filterElements, selectedElements, type, disabledElements) {
    return filterElements.map(filter => /*#__PURE__*/_react.default.createElement("button", {
      key: filter,
      className: "uk-button " + (selectedElements.includes(filter) ? "filter-item-selected" : "") + (disabledElements.includes(filter) ? "filter-item-disabled" : ""),
      onClick: e => onFilterSelected(e, type, filter),
      disabled: disabledElements.includes(filter)
    }, filter.toUpperCase()));
  }
  return /*#__PURE__*/_react.default.createElement("div", {
    className: "result-filter"
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: "filter-products"
  }, languages.length > 0 && /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, /*#__PURE__*/_react.default.createElement("div", null, /*#__PURE__*/_react.default.createElement("span", {
    className: "filter-products-title"
  }, t('product_list.products_filter.languages_filter'))), getFilter(languages, selectedLanguages, 'language', disabledLanguages))), /*#__PURE__*/_react.default.createElement("div", {
    className: "filter-products"
  }, pickups.length > 0 && /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, /*#__PURE__*/_react.default.createElement("div", null, /*#__PURE__*/_react.default.createElement("span", {
    className: "filter-products-title"
  }, t('product_list.products_filter.pickups_filter'))), getFilter(pickups, selectedPickups, 'pickup', []))));
}
function ProductList(_ref5) {
  let {
    productList,
    locale,
    selected,
    onSelection
  } = _ref5;
  const [visibleProductList, setVisibleProductList] = (0, _react.useState)(productList);
  const {
    t
  } = (0, _reactI18next.useTranslation)();
  const onClickFilterHandler = filter => {
    let prods = [];
    let sl = filter['languages'];
    let sp = filter['pickups'];
    if (sl.length > 0) {
      prods = productList.filter(product => product.languages.some(lang => sl.includes(lang)));
    } else {
      prods = productList;
    }
    if (sp.length > 0) {
      prods = prods.filter(product => product.tags.some(tag => sp.includes(tag.name)));
    }
    setVisibleProductList(prods);
  };
  return /*#__PURE__*/_react.default.createElement("div", {
    className: "result-container"
  }, productList.length > 2 && /*#__PURE__*/_react.default.createElement(ProductListFilter, {
    productList: productList,
    onClick: onClickFilterHandler
  }), /*#__PURE__*/_react.default.createElement("div", {
    className: "products-container"
  }, visibleProductList.length === 0 && /*#__PURE__*/_react.default.createElement("div", {
    id: "filter-no-results"
  }, /*#__PURE__*/_react.default.createElement("span", {
    className: "filter-no-results"
  }, t("product_list.products_filter.no_results"))), visibleProductList.map(product => /*#__PURE__*/_react.default.createElement(Product, {
    key: product.id,
    product: product,
    locale: locale,
    selected: product.id === selected,
    onSelection: onSelection
  }))));
}