"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _es = _interopRequireDefault(require("./locales/es.json"));
var _en = _interopRequireDefault(require("./locales/en.json"));
var _it = _interopRequireDefault(require("./locales/it.json"));
var _nl = _interopRequireDefault(require("./locales/nl.json"));
var _pl = _interopRequireDefault(require("./locales/pl.json"));
var _de = _interopRequireDefault(require("./locales/de.json"));
var _fr = _interopRequireDefault(require("./locales/fr.json"));
var _ru = _interopRequireDefault(require("./locales/ru.json"));
var _i18n = _interopRequireDefault(require("../../AvailabilityCalendar/config/i18n"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
const i18nResources = {
  es: {
    main: _es.default,
    calendar: _i18n.default.es.calendar
  },
  en: {
    main: _en.default,
    calendar: _i18n.default.en.calendar
  },
  it: {
    main: _it.default,
    calendar: _i18n.default.it.calendar
  },
  nl: {
    main: _nl.default,
    calendar: _i18n.default.nl.calendar
  },
  pl: {
    main: _pl.default,
    calendar: _i18n.default.pl.calendar
  },
  de: {
    main: _de.default,
    calendar: _i18n.default.de.calendar
  },
  fr: {
    main: _fr.default,
    calendar: _i18n.default.fr.calendar
  },
  ru: {
    main: _ru.default,
    calendar: _i18n.default.ru.calendar
  }
};
var _default = exports.default = i18nResources;