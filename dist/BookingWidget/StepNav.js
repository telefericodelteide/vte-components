"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = StepNav;
var _react = _interopRequireWildcard(require("react"));
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function (e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != typeof e && "function" != typeof e) return { default: e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && {}.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n.default = e, t && t.set(e, n), n; }
function StepNav(_ref) {
  let {
    step,
    content,
    selected,
    disabled,
    responsiveHeader,
    component,
    onSelection
  } = _ref;
  return /*#__PURE__*/_react.default.createElement("li", {
    className: "bw-navstep" + (disabled ? " disabled" : "") + (selected ? " selected" : ""),
    onClick: () => !disabled && onSelection(step)
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: "bw-navstep-header" + (responsiveHeader ? " uk-hidden@m" : "")
  }, /*#__PURE__*/_react.default.createElement("div", null, content.icon, /*#__PURE__*/_react.default.createElement("div", {
    className: "bw-navstep-number"
  }, content.number), /*#__PURE__*/_react.default.createElement("div", {
    className: "bw-navstep-text"
  }, /*#__PURE__*/_react.default.createElement("h4", null, content.title), content.info, /*#__PURE__*/_react.default.createElement("p", null, content.text)))), component ? /*#__PURE__*/_react.default.createElement(_react.Fragment, null, /*#__PURE__*/_react.default.createElement("div", {
    className: "bw-navstep-component-title + uk-visible@m"
  }, content.componentTitle), /*#__PURE__*/_react.default.createElement("div", {
    className: "bw-navstep-component"
  }, component)) : null);
}
;