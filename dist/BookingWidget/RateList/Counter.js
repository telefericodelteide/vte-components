"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = Counter;
var _react = _interopRequireWildcard(require("react"));
require("./rate-list.css");
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function (e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != typeof e && "function" != typeof e) return { default: e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && {}.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n.default = e, t && t.set(e, n), n; }
function Counter(_ref) {
  let {
    defaultValue,
    min,
    max,
    disabled,
    onChange
  } = _ref;
  const [value, setValue] = (0, _react.useState)(defaultValue || min);
  const increment = () => {
    if (!disabled && value < max) {
      setValue(value + 1);
    }
  };
  const decrement = () => {
    if (value > min) {
      setValue(value - 1);
    }
  };
  (0, _react.useEffect)(() => onChange(value));
  return /*#__PURE__*/_react.default.createElement("div", {
    className: "counter"
  }, /*#__PURE__*/_react.default.createElement("span", {
    className: "counter-decrement"
  }, /*#__PURE__*/_react.default.createElement("button", {
    className: "uk-button uk-button-default",
    type: "button",
    onClick: decrement
  }, "-")), /*#__PURE__*/_react.default.createElement("span", {
    className: "value"
  }, value), /*#__PURE__*/_react.default.createElement("span", {
    className: "counter-increment"
  }, /*#__PURE__*/_react.default.createElement("button", {
    className: "uk-button uk-button-default",
    type: "button",
    onClick: increment
  }, "+")));
}