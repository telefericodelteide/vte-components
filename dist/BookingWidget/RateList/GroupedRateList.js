"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = GroupedRateList;
var _react = _interopRequireWildcard(require("react"));
var _reactI18next = require("react-i18next");
var _uikit = _interopRequireDefault(require("uikit"));
var _RateList = _interopRequireDefault(require("./RateList"));
var _Modal = _interopRequireDefault(require("../../Generic/Modal"));
var _has2 = _interopRequireDefault(require("lodash/has"));
require("./rate-list.css");
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function (e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != typeof e && "function" != typeof e) return { default: e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && {}.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n.default = e, t && t.set(e, n), n; }
function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
function _objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
function _defineProperty(e, r, t) { return (r = _toPropertyKey(r)) in e ? Object.defineProperty(e, r, { value: t, enumerable: !0, configurable: !0, writable: !0 }) : e[r] = t, e; }
function _toPropertyKey(t) { var i = _toPrimitive(t, "string"); return "symbol" == typeof i ? i : i + ""; }
function _toPrimitive(t, r) { if ("object" != typeof t || !t) return t; var e = t[Symbol.toPrimitive]; if (void 0 !== e) { var i = e.call(t, r || "default"); if ("object" != typeof i) return i; throw new TypeError("@@toPrimitive must return a primitive value."); } return ("string" === r ? String : Number)(t); }
function GroupedRateList(_ref) {
  let {
    groupId,
    groupName,
    visibleGroupValue,
    rates,
    locale,
    onChange,
    max,
    ageRestrictions,
    message
  } = _ref;
  const {
    t
  } = (0, _reactI18next.useTranslation)();
  max = isNaN(max) || max === 0 || max === -1 || max === null ? Number.MAX_SAFE_INTEGER : max;
  const [groupedRatesHiddenChecked, setGroupedRatesHiddenChecked] = (0, _react.useState)(false);
  const [groupedRates, setGroupedRates] = (0, _react.useState)({});
  const [selection, setSelection] = (0, _react.useState)([]);
  const [maxAvailableQty, setMaxAvailableQty] = (0, _react.useState)(0);
  (0, _react.useEffect)(() => {
    const processedRates = rates.reduce((acc, rate, index, rates) => {
      const customerType = rate.customer_types.find(ct => ct.group_id === groupId);
      if (!(customerType.id in acc)) {
        acc[customerType.id] = [];
      }
      acc[customerType.id].push(rate);
      return acc;
    }, {});
    if ((0, _has2.default)(processedRates, 18) && processedRates[18].some(rate => rate.qty_restrictions.min > 0)) {
      setGroupedRatesHiddenChecked(true);
    }
    setGroupedRates(processedRates);
    setSelection(rates.map(rate => {
      const customerType = rate.customer_types.find(ct => ct.group_id === groupId);
      return {
        id: rate.id,
        groupValue: customerType.id,
        qty: rate.qty_restrictions.min
      };
    }));
  }, [rates, groupId]);
  (0, _react.useEffect)(() => {
    setMaxAvailableQty(getMaxAvailableQty(selection));
  }, [selection]);
  (0, _react.useEffect)(() => {
    if (groupedRatesHiddenChecked) {
      _uikit.default.modal("#grouped-rate-modal", {
        container: "#show-group-cond"
      }).show();
    }
  }, [groupedRatesHiddenChecked]);
  const getMaxAvailableQty = selection => max - selection.reduce((acc, rate) => acc + rate.qty, 0);
  const getGroupQty = (groupValue, selection) => selection.filter(rate => rate.groupValue === groupValue).reduce((acc, rate) => acc + rate.qty, 0);
  const onRateChangeHandler = rates => {
    const newSelection = selection.map(selectedRate => {
      const rate = rates.find(rate => rate.id === selectedRate.id);
      if (rate) {
        return _objectSpread(_objectSpread({}, selectedRate), {}, {
          qty: rate.qty
        });
      } else {
        return selectedRate;
      }
    });
    setSelection(newSelection);
    onChange(newSelection);
  };
  const onShowRatesHiddenChange = event => {
    setGroupedRatesHiddenChecked(true);
  };
  return /*#__PURE__*/_react.default.createElement("div", {
    className: "grouped-rates-container"
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: "grouped-rates-visible"
  }, Object.keys(groupedRates).length > 0 && /*#__PURE__*/_react.default.createElement(_RateList.default, {
    locale: locale,
    rates: groupedRates[visibleGroupValue],
    rateNameGroupId: 5,
    onChange: onRateChangeHandler,
    max: maxAvailableQty + getGroupQty(visibleGroupValue, selection),
    ageRestrictions: ageRestrictions
  })), Object.keys(groupedRates).length > 1 && /*#__PURE__*/_react.default.createElement(_react.Fragment, null, /*#__PURE__*/_react.default.createElement("div", {
    className: "show-group-cond uk-flex uk-flex-center"
  }, /*#__PURE__*/_react.default.createElement("label", {
    htmlFor: "show-rates-hidden",
    className: "show-rates-hidden"
  }, /*#__PURE__*/_react.default.createElement("input", {
    id: "show-rates-hidden",
    className: "uk-checkbox",
    type: "checkbox",
    checked: groupedRatesHiddenChecked,
    onChange: onShowRatesHiddenChange
  }), groupName), /*#__PURE__*/_react.default.createElement(_Modal.default, {
    id: "grouped-rate-modal",
    className: "grouped-rate-modal",
    header: t("grouped_rate_list.header"),
    locale: locale
  }, /*#__PURE__*/_react.default.createElement(_reactI18next.Trans, {
    defaults: t("grouped_rate_list.body") // optional defaultValue
    ,
    components: {
      p: /*#__PURE__*/_react.default.createElement("p", null),
      b: /*#__PURE__*/_react.default.createElement("b", null)
    }
  }))), /*#__PURE__*/_react.default.createElement("div", {
    id: "rates-hidden-" + groupId,
    className: "grouped-rates-hidden grouped-rates-toggle",
    hidden: !groupedRatesHiddenChecked
  }, /*#__PURE__*/_react.default.createElement(_RateList.default, {
    locale: locale,
    rates: groupedRates[18],
    rateNameGroupId: 5,
    onChange: onRateChangeHandler,
    max: maxAvailableQty + getGroupQty(18, selection),
    ageRestrictions: ageRestrictions
  }), message && /*#__PURE__*/_react.default.createElement("div", {
    className: "uk-padding uk-padding-remove-top"
  }, /*#__PURE__*/_react.default.createElement("b", null, message)))));
}