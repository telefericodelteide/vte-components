"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Rate = Rate;
exports.default = RateSelector;
var _react = _interopRequireWildcard(require("react"));
var _Counter = _interopRequireDefault(require("./Counter"));
var _get2 = _interopRequireDefault(require("lodash/get"));
var _template2 = _interopRequireDefault(require("lodash/template"));
require("./rate-list.css");
var _reactI18next = require("react-i18next");
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function (e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != typeof e && "function" != typeof e) return { default: e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && {}.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n.default = e, t && t.set(e, n), n; }
function Rate(_ref) {
  let {
    name,
    qtyRestriction,
    ageRestriction,
    isLastRate,
    unitPrice,
    currency,
    defaultValue,
    locale,
    disabled,
    onChange
  } = _ref;
  const [value, setValue] = (0, _react.useState)(null);
  (0, _react.useEffect)(() => onChange(value), [value]);
  const {
    t
  } = (0, _reactI18next.useTranslation)();
  const priceFormatter = new Intl.NumberFormat(locale, {
    style: "currency",
    currency: currency || "eur"
  });
  const priceFormatted = (currency, price) => {
    let formattedPrice = priceFormatter.format(price);
    if (currency && currency === 'PLN') {
      formattedPrice = formattedPrice.replace('PLN', 'zł');
    }
    return formattedPrice;
  };
  const onRateChangeHandler = value => {
    setValue(value);
  };
  return /*#__PURE__*/_react.default.createElement("div", {
    className: "rate"
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: "rate-header"
  }, /*#__PURE__*/_react.default.createElement("span", {
    className: "rate-name"
  }, name), /*#__PURE__*/_react.default.createElement("span", {
    className: "rate-price"
  }, priceFormatted(currency, unitPrice))), /*#__PURE__*/_react.default.createElement("div", {
    className: "rate-counter"
  }, /*#__PURE__*/_react.default.createElement(_Counter.default, {
    defaultValue: defaultValue,
    min: qtyRestriction.min,
    max: qtyRestriction.max,
    disabled: disabled,
    onChange: onRateChangeHandler
  })), /*#__PURE__*/_react.default.createElement("div", {
    className: "rate-footer"
  }, (0, _get2.default)(ageRestriction, "min", -1) >= 0 && /*#__PURE__*/_react.default.createElement("span", {
    className: "rate-footer-age-range"
  }, (0, _template2.default)(t("rate_list.age_restriction"))({
    from: ageRestriction.min,
    to: ageRestriction.max
  })), isLastRate && (0, _get2.default)(ageRestriction, "min", -1) > 0 && /*#__PURE__*/_react.default.createElement("span", {
    className: "rate-footer-age-limit"
  }, (0, _template2.default)(t("rate_list.age_minimum"))({
    age: ageRestriction.min
  }))));
}
function RateSelector(_ref2) {
  let {
    rates,
    rateNameGroupId,
    locale,
    onChange,
    max,
    ageRestrictions
  } = _ref2;
  max = isNaN(max) || max == null || max === -1 ? Number.MAX_SAFE_INTEGER : max;
  const [selection, setSelection] = (0, _react.useState)([]);
  const [maxQtyLimit, setMaxQtyLimit] = (0, _react.useState)(false);
  (0, _react.useEffect)(() => {
    setSelection(rates.map(rate => {
      return {
        id: rate.id,
        qty: getRateQtyRestriction(rate).min
      };
    }));
  }, [rates]);
  (0, _react.useEffect)(() => {
    setMaxQtyLimit(isMaxQtyLimit(selection));
  }, [selection, max]);
  const isMaxQtyLimit = selection => {
    let qty = 0;
    selection.forEach(rate => {
      qty += rate.qty;
    });
    return qty >= max;
  };
  const onRateChangeHandler = (rateId, qty) => {
    const newSelection = selection.map(rate => {
      if (rate.id === rateId) {
        return {
          id: rate.id,
          qty: qty
        };
      } else {
        return rate;
      }
    });
    setSelection(newSelection);
    onChange(newSelection);
  };
  const getRateName = rate => rate.customer_types.find(ct => ct.group_id === rateNameGroupId).name;
  const getRateAgeRestriction = rate => {
    // const arrAgeRestrictions = Object.values(ageRestrictions)
    // const ct = rate.customer_types.find((ct) => ct.group_id === 5);
    // return ct.age_restriction || {};
    const arrAgeRestrictions = Object.values(ageRestrictions);
    const restrictions = rate.customer_types.map(ct => arrAgeRestrictions.find(r => r.customer_type_group_value === ct.id)).filter(ar => ar !== undefined);
    return restrictions && restrictions.length > 0 ? {
      min: restrictions[0].min,
      max: restrictions[0].max
    } : {};
  };
  const getRateQtyRestriction = rate => {
    const min = rate.qty_restrictions.min;
    const max = rate.qty_restrictions.max;
    return {
      min: isNaN(min) || min === null ? 0 : min,
      max: isNaN(max) || max === null ? Number.MAX_SAFE_INTEGER : max
    };
  };
  const isLastRate = (rates, index) => rates.length - 1 == index;
  return /*#__PURE__*/_react.default.createElement("div", {
    className: "rates-container uk-flex uk-flex-center",
    "uk-grid": "uk-grid"
  }, rates.map((rate, index) => /*#__PURE__*/_react.default.createElement(Rate, {
    className: "uk-width-1-2 uk-width-1-4@m",
    locale: locale,
    key: rate.id,
    name: getRateName(rate),
    qtyRestriction: getRateQtyRestriction(rate),
    ageRestriction: getRateAgeRestriction(rate),
    isLastRate: isLastRate(rates, index),
    unitPrice: rate.pvp,
    disabled: maxQtyLimit,
    onChange: qty => onRateChangeHandler(rate.id, qty)
  })));
}