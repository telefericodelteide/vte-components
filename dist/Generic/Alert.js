"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _react = _interopRequireDefault(require("react"));
var _lodash = require("lodash");
var _Button = _interopRequireDefault(require("../BookingManagementWidget/Button/Button"));
require("./generic.css");
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function Alert(props) {
  const createMarkup = content => {
    return {
      __html: content
    };
  };
  if ((0, _lodash.isObject)(props.content)) {
    const {
      content: data
    } = props;
    const {
      content,
      button,
      url
    } = data;
    const defaultStyles = {
      wrapper: {
        backgroundColor: "#ededed"
      },
      content: {
        color: "#d84b55"
      },
      icon: {
        borderRadius: "0.35rem",
        backgroundColor: "#d84b55",
        padding: "4px 10px"
      },
      buttonText: {
        fontWeight: "bold",
        marginBottom: "20px"
      }
    };
    const onClick = () => {
      window.location.replace(url);
    };
    return /*#__PURE__*/_react.default.createElement("div", {
      className: "volcano-alert volcano-alert-wrapper " + (props.className ? props.className : ""),
      style: defaultStyles.wrapper
    }, /*#__PURE__*/_react.default.createElement(_Button.default, {
      text: button,
      onClick: onClick,
      style: defaultStyles.buttonText
    }), props.icon && /*#__PURE__*/_react.default.createElement("i", {
      className: "volcano-alert-icon " + props.icon,
      style: defaultStyles.icon
    }), /*#__PURE__*/_react.default.createElement("span", {
      className: "volcano-alert-content",
      dangerouslySetInnerHTML: createMarkup(content),
      style: defaultStyles.content
    }));
  }
  return /*#__PURE__*/_react.default.createElement("div", {
    className: "volcano-alert volcano-alert-wrapper " + (props.className ? props.className : "")
  }, props.icon && /*#__PURE__*/_react.default.createElement("i", {
    className: "volcano-alert-icon " + props.icon
  }), /*#__PURE__*/_react.default.createElement("span", {
    className: "volcano-alert-content",
    dangerouslySetInnerHTML: createMarkup(props.content)
  }));
}
var _default = exports.default = Alert;