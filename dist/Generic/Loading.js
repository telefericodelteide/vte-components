"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _react = _interopRequireDefault(require("react"));
require("./generic.css");
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function Loading(props) {
  return /*#__PURE__*/_react.default.createElement("div", {
    className: "volcano-loading-wrapper"
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: "lds-ripple"
  }, /*#__PURE__*/_react.default.createElement("div", null), /*#__PURE__*/_react.default.createElement("div", null)));
}
var _default = exports.default = Loading;