"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = FormWidget;
var _react = _interopRequireWildcard(require("react"));
var _uikit = _interopRequireDefault(require("uikit"));
require("uikit/dist/css/uikit.min.css");
var _formik = require("formik");
var _FormFieldset = _interopRequireDefault(require("./FormFieldset"));
var _FormConditionField = _interopRequireDefault(require("./FormConditionField"));
var _i18next = _interopRequireDefault(require("i18next"));
var _reactI18next = require("react-i18next");
var _i18n = _interopRequireDefault(require("../config/i18n"));
var _Loading = _interopRequireDefault(require("../Loading"));
var _i18nextHttpBackend = _interopRequireDefault(require("i18next-http-backend"));
require("../generic.css");
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function (e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != typeof e && "function" != typeof e) return { default: e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && {}.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n.default = e, t && t.set(e, n), n; }
function _extends() { return _extends = Object.assign ? Object.assign.bind() : function (n) { for (var e = 1; e < arguments.length; e++) { var t = arguments[e]; for (var r in t) ({}).hasOwnProperty.call(t, r) && (n[r] = t[r]); } return n; }, _extends.apply(null, arguments); }
function SuccessMessage(_ref) {
  let {
    title,
    body
  } = _ref;
  return /*#__PURE__*/_react.default.createElement("div", {
    className: "form-success"
  }, /*#__PURE__*/_react.default.createElement("h2", null, title), /*#__PURE__*/_react.default.createElement("div", {
    dangerouslySetInnerHTML: {
      __html: body
    }
  }));
}
function FormWidget(_ref2) {
  let {
    locale,
    initialValues,
    form,
    successConfig,
    confirmButton,
    cancelButton
  } = _ref2;
  initialValues = initialValues || {};
  if (!_i18next.default.isInitialized) {
    _i18next.default.use(_i18nextHttpBackend.default).use(_reactI18next.initReactI18next).init({
      interpolation: {
        escapeValue: false
      },
      lng: locale,
      resources: _i18n.default,
      ns: ["main"],
      defaultNS: "main"
    });
  } else {
    _i18next.default.addResourceBundle(locale, "main", _i18n.default[locale]["main"]);
  }
  if (typeof window !== "undefined") {
    _uikit.default.container = ".uk-scope";
  }
  const [step, setStep] = (0, _react.useState)(0);
  const handleFormSubmit = (values, actions) => {
    confirmButton.handler(values).then(result => {
      actions.setSubmitting(false);
      setStep(1);
    });
  };
  return /*#__PURE__*/_react.default.createElement(_reactI18next.I18nextProvider, {
    i18n: _i18next.default
  }, /*#__PURE__*/_react.default.createElement("div", {
    id: "form-widget",
    className: "volcano-form"
  }, step === 0 && /*#__PURE__*/_react.default.createElement(_formik.Formik, {
    initialValues: initialValues,
    onSubmit: handleFormSubmit
  }, formikProps => {
    const {
      values,
      dirty,
      isValid,
      isSubmitting
    } = formikProps;
    return /*#__PURE__*/_react.default.createElement(_formik.Form, {
      className: "form-wrapper uk-form-stacked"
    }, isSubmitting && /*#__PURE__*/_react.default.createElement(_Loading.default, null), /*#__PURE__*/_react.default.createElement("div", {
      className: "form-fieldsets-wrapper"
    }, form.fields.map((fieldset, index) => /*#__PURE__*/_react.default.createElement(_FormFieldset.default, _extends({
      key: index
    }, fieldset, {
      values: values
    })))), /*#__PURE__*/_react.default.createElement("div", {
      className: "form-conditions-wrapper uk-margin-top"
    }, form.conditions.map(condition => /*#__PURE__*/_react.default.createElement(_FormConditionField.default, {
      key: condition.id,
      id: condition.id,
      config: condition,
      values: values
    }))), /*#__PURE__*/_react.default.createElement("div", {
      className: "uk-margin"
    }, /*#__PURE__*/_react.default.createElement("button", {
      id: "btnSubmit",
      type: "submit",
      className: "uk-button uk-align-center volcano-button",
      disabled: !dirty || !isValid
    }, confirmButton.title), cancelButton && /*#__PURE__*/_react.default.createElement("button", {
      id: "btnSubmit",
      className: "uk-button uk-align-center volcano-button",
      onClick: () => cancelButton.handler()
    }, cancelButton.title)));
  }), step === 1 && /*#__PURE__*/_react.default.createElement(SuccessMessage, successConfig)));
}