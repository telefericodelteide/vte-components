"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.validate = validate;
var _get2 = _interopRequireDefault(require("lodash/get"));
var _isEmpty2 = _interopRequireDefault(require("lodash/isEmpty"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function validate(config, t, values, value) {
  const {
    type,
    required,
    equalTo
  } = config;
  if (required) {
    let hasError = false;
    switch (type) {
      case "checkbox":
        if (!value) {
          hasError = true;
        }
        break;
      default:
        if (!value || (0, _isEmpty2.default)(value)) {
          hasError = true;
        }
        break;
    }
    if (hasError) {
      return t("validation.required");
    }
  }
  if (type === "email") {
    if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.\w+$/i.test(value)) {
      return t("validation.email");
    }
    if (equalTo) {
      let valueOtherField = (0, _get2.default)(values, equalTo);
      if (valueOtherField !== value) {
        return t("validation.equalTo");
      }
    }
  }
  if (type === "phone") {
    if (value === "invalid_phone_number") {
      return t("validation.phone");
    }
  }
  return null;
}