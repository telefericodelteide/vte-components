"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = FormFieldset;
var _react = _interopRequireDefault(require("react"));
var _reactI18next = require("react-i18next");
var _formik = require("formik");
var _FormValidator = require("./FormValidator");
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function _extends() { return _extends = Object.assign ? Object.assign.bind() : function (n) { for (var e = 1; e < arguments.length; e++) { var t = arguments[e]; for (var r in t) ({}).hasOwnProperty.call(t, r) && (n[r] = t[r]); } return n; }, _extends.apply(null, arguments); }
function FormField(props) {
  const {
    t
  } = (0, _reactI18next.useTranslation)("main");
  const getField = props => {
    switch (props.type) {
      case "select":
        return /*#__PURE__*/_react.default.createElement(_formik.Field, {
          as: "select",
          name: props.id,
          className: "uk-select"
        }, Object.entries(props.options).map(_ref => {
          let [key, value] = _ref;
          return /*#__PURE__*/_react.default.createElement("option", {
            key: key,
            value: key,
            label: value
          });
        }));
      case "email":
        return /*#__PURE__*/_react.default.createElement(_formik.Field, {
          type: "email",
          name: props.id,
          className: "uk-input"
          //validate={validate.bind(this, props, t, props.values)}
        });
      case "hidden":
        return /*#__PURE__*/_react.default.createElement(_formik.Field, {
          type: "hidden",
          name: props.id
        });
      default:
        return /*#__PURE__*/_react.default.createElement(_formik.Field, {
          name: props.id,
          className: "uk-input",
          validate: _FormValidator.validate.bind(this, props, t, props.values)
        });
    }
  };
  if (props.type === "hidden") {
    return getField(props);
  } else {
    return /*#__PURE__*/_react.default.createElement("div", {
      className: "form-field-wrapper uk-width-1-1 uk-width-1-2@m"
    }, /*#__PURE__*/_react.default.createElement("label", {
      htmlFor: props.id,
      className: "form-field-label uk-form-label"
    }, props.title), /*#__PURE__*/_react.default.createElement("div", {
      className: "uk-form-controls"
    }, getField(props), /*#__PURE__*/_react.default.createElement(_formik.ErrorMessage, {
      name: props.id
    }, msg => /*#__PURE__*/_react.default.createElement("div", {
      className: "form-field-error uk-text-danger"
    }, msg))));
  }
}
function FormFieldset(_ref2) {
  let {
    title,
    fields,
    values
  } = _ref2;
  return /*#__PURE__*/_react.default.createElement("div", {
    className: "form-fieldset uk-grid-small",
    "uk-grid": "uk-grid"
  }, title && /*#__PURE__*/_react.default.createElement("div", {
    className: "form-fieldset-title uk-width-1-1 uk-text-large"
  }, title, /*#__PURE__*/_react.default.createElement("hr", null)), /*#__PURE__*/_react.default.createElement("div", {
    className: "uk-width-1-1"
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: "uk-grid-small",
    "uk-grid": "uk-grid"
  }, fields.map(field => /*#__PURE__*/_react.default.createElement(FormField, _extends({
    key: field.id,
    values: values
  }, field))))));
}