"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.Localized = exports.Default = void 0;
var _react = _interopRequireDefault(require("react"));
var _FormWidget = _interopRequireDefault(require("./FormWidget"));
var _set2 = _interopRequireDefault(require("lodash/set"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
function _objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
function _defineProperty(e, r, t) { return (r = _toPropertyKey(r)) in e ? Object.defineProperty(e, r, { value: t, enumerable: !0, configurable: !0, writable: !0 }) : e[r] = t, e; }
function _toPropertyKey(t) { var i = _toPrimitive(t, "string"); return "symbol" == typeof i ? i : i + ""; }
function _toPrimitive(t, r) { if ("object" != typeof t || !t) return t; var e = t[Symbol.toPrimitive]; if (void 0 !== e) { var i = e.call(t, r || "default"); if ("object" != typeof i) return i; throw new TypeError("@@toPrimitive must return a primitive value."); } return ("string" === r ? String : Number)(t); }
var _default = exports.default = {
  component: _FormWidget.default,
  title: "Form widget"
};
const Template = args => /*#__PURE__*/_react.default.createElement(_FormWidget.default, args);
const testForm = {
  fields: [{
    title: "Modalidad de colaboración",
    fields: [{
      id: "commercial.colaboration_type.id",
      title: "Tipo de colaboración",
      required: true,
      type: "select",
      options: {
        collaborator_wholesaler: "Colaborador",
        collaborator_retail: "Retail",
        collaborator_credit: "Crédito",
        affiliate: "Afiliado",
        api: "API"
      }
    }, {
      id: "commercial.agency_type.id",
      title: "Tipo de agencia",
      required: true,
      type: "select",
      options: {
        travel_agency: "Agencia de viajes",
        tourist_intermediary: "Intermediador turístico"
      }
    }, {
      id: "commercial.business_type.id",
      title: "Tipo de establecimiento",
      required: true,
      type: "select",
      options: {
        travel_agency: "Agencia de viajes",
        apartment: "Apartamentos",
        hotel: "Hotel",
        rent_a_car: "Alquiler de vehículos",
        tour_office: "Oficina de turismo"
      }
    }, {
      id: "commercial.registration_number",
      title: "Nº de registro",
      required: true,
      type: "text"
    }]
  }, {
    title: "Datsos comerciales",
    fields: [{
      id: "commercial.name",
      title: "Nombre comercial",
      required: true,
      type: "text"
    }, {
      id: "commercial.contact_details.road_type_id",
      title: "Tipo de vía",
      required: true,
      type: "select",
      options: []
    }, {
      id: "commercial.contact_details.address",
      title: "Dirección",
      required: true,
      type: "text"
    }, {
      id: "commercial.contact_details.postal_code",
      title: "Código postal",
      required: true,
      type: "text"
    }, {
      id: "commercial.contact_details.locality",
      title: "Localidad",
      required: true,
      type: "text"
    }, {
      id: "commercial.contact_details.state",
      title: "Provincia",
      required: true,
      type: "text"
    }, {
      id: "commercial.contact_details.country_id",
      title: "País",
      required: true,
      type: "select",
      options: []
    }]
  }, {
    title: "Persona de contacto - Excursiones",
    fields: [{
      id: "commercial.booking_contact.name",
      title: "Nombre",
      required: true,
      type: "text"
    }, {
      id: "commercial.booking_contact.phone",
      title: "Teléfono",
      required: true,
      type: "text"
    }, {
      id: "commercial.booking_contact.email",
      title: "Correo electrónico",
      required: true,
      type: "text"
    }]
  }, {
    title: "Datos fiscales",
    fields: [{
      id: "billing.name",
      title: "Razón social",
      required: true,
      type: "text"
    }, {
      id: "billing.vat_number",
      title: "CIF",
      required: true,
      type: "text"
    }, {
      id: "billing.contact_details.road_type_id",
      title: "Tipo de vía",
      required: true,
      type: "select",
      options: []
    }, {
      id: "billing.contact_details.address",
      title: "Dirección",
      required: true,
      type: "text"
    }, {
      id: "billing.contact_details.postal_code",
      title: "Código postal",
      required: true,
      type: "text"
    }, {
      id: "billing.contact_details.locality",
      title: "Localidad",
      required: true,
      type: "text"
    }, {
      id: "billing.contact_details.state",
      title: "Provincia",
      required: true,
      type: "text"
    }, {
      id: "billing.contact_details.country_id",
      title: "País",
      required: true,
      type: "select",
      options: []
    }, {
      id: "billing.contact_details.email",
      title: "Correo electrónico",
      required: true,
      type: "email"
    }, {
      id: "billing.contact_details.phone",
      title: "Teléfono",
      required: true,
      type: "text"
    }]
  }],
  conditions: [{
    id: "terms",
    title: "Acepto los término y condiciones",
    required: true,
    type: "checkbox",
    modal: {
      title: "Términos y condiciones",
      content: "Contenido para los términos y condiciones"
    }
  }, {
    id: "data_consent",
    title: "Expreso mi consentimiento al tratamiento de mis datos personales, por TELEFERICO DEL PICO DE TEIDE, S.A. con el fin de poder colaborar en la provisión de visitas, excursiones y actividades al Teide, a través de la plataforma que se ha creado para ello y a la que tengo acceso privado.",
    required: true,
    type: "checkbox"
  }]
};
const testConfirmMessage = {
  title: "Solicitud de alta realizada",
  body: 'Se ha efectuado la solicitud de alta. Para completar el proceso es necesario que envía al correo electrónico "reservas@caminobarrancodemasca.com" la siguiente información:'
};
var testInitialValues = {};
testForm.fields.forEach(fieldset => fieldset.fields.forEach(field => (0, _set2.default)(testInitialValues, field.id, "")));
const Default = exports.Default = Template.bind({});
Default.args = {
  locale: "es",
  initialValues: testInitialValues,
  form: testForm,
  successConfig: testConfirmMessage,
  confirmButton: {
    title: "Aceptar",
    handler: values => new Promise((resolve, reject) => {
      console.log(values);
      resolve(values);
    })
  }
};
const Localized = exports.Localized = Template.bind({});
Localized.args = _objectSpread(_objectSpread({}, Default.args), {}, {
  locale: "en"
});