"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _react = _interopRequireDefault(require("react"));
require("./flag-list.css");
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function FlagList(_ref) {
  let {
    flags,
    variant = false
  } = _ref;
  flags = flags || [];
  return /*#__PURE__*/_react.default.createElement("div", {
    className: "flag-list"
  }, flags.map((flag, index) => variant ? /*#__PURE__*/_react.default.createElement("span", {
    key: index,
    className: "flag-iso"
  }, flag) : /*#__PURE__*/_react.default.createElement("i", {
    key: index,
    className: "flag flag-" + flag
  })));
}
var _default = exports.default = FlagList;