"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _react = _interopRequireWildcard(require("react"));
var _reactI18next = require("react-i18next");
var _uikit = _interopRequireDefault(require("uikit"));
require("./generic.css");
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function (e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != typeof e && "function" != typeof e) return { default: e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && {}.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n.default = e, t && t.set(e, n), n; }
function Modal(props) {
  const [checked, setChecked] = (0, _react.useState)(false);
  const className = props.className ? props.className : "";
  const {
    t
  } = (0, _reactI18next.useTranslation)("main");
  const onAcceptHandler = () => {
    _uikit.default.modal("#" + props.id).hide();
    if (props.onConfirm) {
      setTimeout(() => props.onConfirm(), 500);
    }
  };
  (0, _react.useEffect)(() => {
    if (!props.check) {
      setChecked(true);
    }
  }, []);
  return /*#__PURE__*/_react.default.createElement("div", {
    id: props.id,
    className: "volcano-modal " + className,
    "uk-modal": "esc-close:false; bg-close:false"
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: "uk-modal-dialog"
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: "uk-modal-header"
  }, /*#__PURE__*/_react.default.createElement("h2", {
    className: "uk-modal-title"
  }, props.header)), /*#__PURE__*/_react.default.createElement("div", {
    className: "uk-modal-body",
    "uk-overflow-auto": "uk-overflow-auto"
  }, props.children, props.check && /*#__PURE__*/_react.default.createElement("label", {
    htmlFor: "volcano-modal-check",
    className: "volcano-modal-check"
  }, /*#__PURE__*/_react.default.createElement("input", {
    id: "volcano-modal-check",
    className: "uk-checkbox",
    type: "checkbox",
    onChange: () => {
      setChecked(!checked);
    }
  }), props.check[0])), /*#__PURE__*/_react.default.createElement("div", {
    className: "uk-modal-footer uk-text-right"
  }, /*#__PURE__*/_react.default.createElement("button", {
    className: "uk-button volcano-button",
    disabled: !checked,
    type: "button",
    onClick: onAcceptHandler
  }, t("accept")))));
}
var _default = exports.default = Modal;