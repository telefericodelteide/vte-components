"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _i18next = _interopRequireDefault(require("i18next"));
var _i18nextHttpBackend = _interopRequireDefault(require("i18next-http-backend"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
//import LanguageDetector from "i18next-browser-languagedetector";

_i18next.default.use(_i18nextHttpBackend.default) // lazy loads translations from /public/locales
//	.use(LanguageDetector) // detect user language
.init({
  fallbackLng: "es",
  debug: true,
  lng: "es",
  interpolation: {
    escapeValue: false
  },
  react: {
    wait: true,
    useSuspense: false
  }
});
var _default = exports.default = _i18next.default;