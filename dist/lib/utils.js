"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
const downloadFile = (data, filename) => {
  const url = window.URL.createObjectURL(new Blob([data], {
    type: 'application/octet-stream'
  }));
  var link = document.createElement('a');
  link.href = url;
  link.download = filename;
  link.click();
};
var _default = exports.default = {
  downloadFile
};