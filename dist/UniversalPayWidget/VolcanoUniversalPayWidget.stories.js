"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.Default = void 0;
var _react = _interopRequireDefault(require("react"));
var _VolcanoUniversalPayWidget = _interopRequireDefault(require("./VolcanoUniversalPayWidget"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
var _default = exports.default = {
  component: _VolcanoUniversalPayWidget.default,
  title: "Universal pay widget"
};
const Template = args => /*#__PURE__*/_react.default.createElement(_VolcanoUniversalPayWidget.default, args);
const handleResult = function handleResult(data) {
  console.log(data);
  alert(JSON.stringify(data));
};
const Default = exports.Default = Template.bind({});
Default.args = {
  scriptUrl: "https://cashierui.test.universalpay.es/js/api.js",
  baseUrl: "https://cashierui.test.universalpay.es/ui/cashier",
  token: "fa702153-2772-435c-a2b4-2486cf7a84d6",
  merchantId: "909952",
  onResult: handleResult
};