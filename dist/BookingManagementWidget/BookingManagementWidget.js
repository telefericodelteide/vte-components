"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _react = _interopRequireDefault(require("react"));
var _Login = _interopRequireDefault(require("./Login/Login"));
require("./booking-management-widget.css");
require("uikit/dist/css/uikit.min.css");
var _auth = require("./context/auth.context");
var _reactI18next = require("react-i18next");
var _BookingManagement = _interopRequireDefault(require("./BookingManagement/BookingManagement"));
var _uikit = _interopRequireDefault(require("uikit"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
const BookingManagementWidget = _ref => {
  let {
    availabilityFetcher,
    loading
  } = _ref;
  const authContext = (0, _auth.useAuth)();
  const {
    t
  } = (0, _reactI18next.useTranslation)('bm');
  if (typeof window !== "undefined") {
    _uikit.default.container = ".uk-scope";
  }
  const handleLogout = () => {
    authContext.logout();
  };
  return /*#__PURE__*/_react.default.createElement("div", {
    id: "booking-management-widget",
    className: "booking-management-widget uk-scope ".concat(authContext.isAuthenticated ? "logged" : "not-logged")
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: "title"
  }, /*#__PURE__*/_react.default.createElement("i", {
    className: "fa fa-ticket-alt fa-rotate-45"
  }), /*#__PURE__*/_react.default.createElement("h1", null, t('manage_booking.title'))), authContext.isAuthenticated ? /*#__PURE__*/_react.default.createElement(_BookingManagement.default, {
    onLogout: handleLogout,
    availabilityFetcher: availabilityFetcher,
    loading: loading
  }) : /*#__PURE__*/_react.default.createElement(_Login.default, null));
};
var _default = exports.default = BookingManagementWidget;