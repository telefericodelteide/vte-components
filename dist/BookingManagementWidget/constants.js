"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
// Available operations over Booking.
//
const BOOKING_ACTION_CANCEL_REQUEST = 'cancellation_request';
const BOOKING_ACTION_CHANGE_DATE = 'booking_date_change';
const BOOKING_ACTION_CANCEL_REFUND = 'cancel_refund';
const BOOKING_ACTION_VOUCHER = 'voucher';
const BOOKING_ACTION_INVOICES = 'invoices';
const BOOKING_ACTION_INVOICE_REQUEST = 'invoice_request';
const BOOKING_ACTION_LOGOUT = 'logout';
//
var _default = exports.default = {
  BOOKING_ACTION_CANCEL_REQUEST,
  BOOKING_ACTION_CHANGE_DATE,
  BOOKING_ACTION_CANCEL_REFUND,
  BOOKING_ACTION_VOUCHER,
  BOOKING_ACTION_INVOICES,
  BOOKING_ACTION_INVOICE_REQUEST,
  BOOKING_ACTION_LOGOUT
};