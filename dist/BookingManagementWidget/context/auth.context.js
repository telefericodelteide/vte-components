"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useAuth = exports.default = void 0;
var _get2 = _interopRequireDefault(require("lodash/get"));
var _react = _interopRequireWildcard(require("react"));
var _auth = require("./auth.reducer");
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function (e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != typeof e && "function" != typeof e) return { default: e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && {}.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n.default = e, t && t.set(e, n), n; }
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
function _objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
function _defineProperty(e, r, t) { return (r = _toPropertyKey(r)) in e ? Object.defineProperty(e, r, { value: t, enumerable: !0, configurable: !0, writable: !0 }) : e[r] = t, e; }
function _toPropertyKey(t) { var i = _toPrimitive(t, "string"); return "symbol" == typeof i ? i : i + ""; }
function _toPrimitive(t, r) { if ("object" != typeof t || !t) return t; var e = t[Symbol.toPrimitive]; if (void 0 !== e) { var i = e.call(t, r || "default"); if ("object" != typeof i) return i; throw new TypeError("@@toPrimitive must return a primitive value."); } return ("string" === r ? String : Number)(t); }
const stub = () => {
  throw new Error('You forgot to wrap your component in <AuthProvider></AuthProvider>.');
};
const initialContext = {
  isAuthenticated: false,
  error: null,
  token: null,
  authenticate: stub,
  logout: stub
};
const Context = /*#__PURE__*/(0, _react.createContext)(initialContext);
const AuthProvider = _ref => {
  let {
    apiClient,
    children
  } = _ref;
  const [state, dispatch] = (0, _react.useReducer)(_auth.reducer, initialContext);
  const [ready, setReady] = (0, _react.useState)(false);
  const authenticate = (0, _react.useCallback)((bookingLocator, customerEmail) => {
    return apiClient.authenticateCustomer(bookingLocator, customerEmail).then(result => {
      localStorage.setItem("token", result.token);
      localStorage.setItem("bookingLocator", bookingLocator);
      dispatch({
        type: 'INITIALISE',
        token: localStorage.token
      });
    }).catch(function (error) {
      localStorage.removeItem("token");
      localStorage.removeItem("bookingLocator");
      localStorage.removeItem("bookingId");
      dispatch({
        type: 'ERROR',
        error: error.message
      });
    });
  }, [apiClient]);
  const logout = (0, _react.useCallback)(() => {
    localStorage.removeItem("token");
    localStorage.removeItem("bookingLocator");
    localStorage.removeItem("bookingId");
    apiClient.logout();
    dispatch({
      type: 'LOGOUT'
    });
  }, []);
  (0, _react.useEffect)(() => {
    if (localStorage.token) {
      apiClient.setToken(localStorage.token);
      if ((0, _get2.default)(apiClient.getPayload(), 'exp') * 1000 < Date.now()) {
        logout();
      } else {
        dispatch({
          type: 'INITIALISE',
          token: localStorage.token
        });
      }
    }
    setReady(true);
  }, [apiClient]);
  const contextValue = (0, _react.useMemo)(() => {
    return _objectSpread(_objectSpread({}, state), {}, {
      authenticate,
      logout
    });
  }, [state, authenticate, logout]);
  return ready && /*#__PURE__*/_react.default.createElement(Context.Provider, {
    value: contextValue
  }, children);
};
var _default = exports.default = AuthProvider;
const useAuth = () => (0, _react.useContext)(Context);
exports.useAuth = useAuth;