"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useBooking = exports.default = void 0;
var _react = _interopRequireWildcard(require("react"));
var _booking = require("./booking.reducer");
var _get2 = _interopRequireDefault(require("lodash/get"));
var _set2 = _interopRequireDefault(require("lodash/set"));
var _auth = require("./auth.context");
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function (e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != typeof e && "function" != typeof e) return { default: e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && {}.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n.default = e, t && t.set(e, n), n; }
function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
function _objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
function _defineProperty(e, r, t) { return (r = _toPropertyKey(r)) in e ? Object.defineProperty(e, r, { value: t, enumerable: !0, configurable: !0, writable: !0 }) : e[r] = t, e; }
function _toPropertyKey(t) { var i = _toPrimitive(t, "string"); return "symbol" == typeof i ? i : i + ""; }
function _toPrimitive(t, r) { if ("object" != typeof t || !t) return t; var e = t[Symbol.toPrimitive]; if (void 0 !== e) { var i = e.call(t, r || "default"); if ("object" != typeof i) return i; throw new TypeError("@@toPrimitive must return a primitive value."); } return ("string" === r ? String : Number)(t); }
const stub = () => {
  throw new Error('You forgot to wrap your component in <BookingProvider></BookingProvider>.');
};
const getBookingStateMessage = booking => {
  if ((0, _get2.default)(booking, 'state') === 'cancelled') {
    return 'cancelled';
  }
  if ((0, _get2.default)(booking, 'validation_date')) {
    return 'validated';
  }
  if ((0, _get2.default)(booking, 'order.billing_type') === 1 && (0, _get2.default)(booking, 'order.collaborator')) {
    return 'collaborator';
  }
  return 'default';
};
const initialContext = {
  booking: null,
  availableActions: null,
  error: null,
  loading: false,
  clearContext: stub,
  getBooking: stub,
  cancellationRequest: stub,
  cancelRefund: stub,
  dateChangeRequest: stub,
  getVoucherPdf: stub,
  getInvoicePdf: stub,
  requestInvoice: stub
};
const Context = /*#__PURE__*/(0, _react.createContext)(initialContext);
const BookingProvider = _ref => {
  let {
    apiClient,
    children
  } = _ref;
  const authContext = (0, _auth.useAuth)();
  const [state, dispatch] = (0, _react.useReducer)(_booking.reducer, initialContext);
  const [ready, setReady] = (0, _react.useState)(false);
  const [loading, setLoading] = (0, _react.useState)(false);
  const clearContext = (0, _react.useCallback)(() => {
    dispatch({
      type: 'INITIALISE',
      booking: null
    });
  });
  const loadExtendedData = (0, _react.useCallback)(async booking => {
    return Promise.all([booking.getRelatedEntities()]).then(_ref2 => {
      let [extBooking] = _ref2;
      return Promise.all([booking.getAvailableActions()]).then(_ref3 => {
        let [actions] = _ref3;
        dispatch({
          type: 'LOAD_EXTENDED_DATA',
          booking: extBooking,
          availableActions: actions
        });
        setLoading(false);
        return true;
      }).catch(function (error) {
        dispatch({
          type: 'ERROR',
          error: error.message
        });
        setLoading(false);
        return false;
      });
    }).catch(function (error) {
      dispatch({
        type: 'ERROR',
        error: error.message
      });
      return false;
    });
  }, []);
  const getBookingLocator = (0, _react.useCallback)(() => {
    return apiClient.booking.getBookings({
      locator: localStorage.bookingLocator
    }).then(collection => {
      if (collection.getCount() === 1) {
        let booking = collection.getItems()[0];
        localStorage.setItem('bookingId', (0, _get2.default)(booking, 'id'));
      } else {
        setLoading(false);
        dispatch({
          type: 'ERROR',
          error: 'Booking not found'
        });
      }
    }).catch(function (error) {
      setLoading(false);
      dispatch({
        type: 'ERROR',
        error: error.message
      });
    });
  }, [apiClient.booking]);
  const getBooking = (0, _react.useCallback)(async () => {
    setLoading(true);
    if (!localStorage.bookingId) {
      await getBookingLocator();
    }
    apiClient.booking.getBooking(localStorage.bookingId).then(booking => {
      booking = (0, _set2.default)(booking, 'product.contact_msg', getBookingStateMessage(booking));
      loadExtendedData(booking);
    }).catch(error => {
      setLoading(false);
      dispatch({
        type: 'ERROR',
        error: error.message
      });
    });
  }, [apiClient.booking, loadExtendedData]);
  const cancellationRequest = (0, _react.useCallback)(async () => {
    setLoading(true);
    return apiClient.booking.cancelBooking((0, _get2.default)(state.booking, 'id')).then(async booking => {
      dispatch({
        type: 'CANCEL_REQUEST',
        booking: booking
      });
      return Promise.all([loadExtendedData(booking)]).then(success => {
        setLoading(false);
        return {
          success: success
        };
      }).catch(function (error) {
        dispatch({
          type: 'ERROR',
          error: error.message
        });
        setLoading(false);
        return {
          success: false,
          error: error.message
        };
      });
    }).catch(function (error) {
      setLoading(false);
      dispatch({
        type: 'ERROR',
        error: error.message
      });
      return {
        success: false,
        error: error.message
      };
    });
  }, [apiClient.booking, state.booking, loadExtendedData]);
  const dateChangeRequest = (0, _react.useCallback)(async date => {
    setLoading(true);
    return apiClient.booking.changeBookingDate((0, _get2.default)(state.booking, 'id'), date).then(async booking => {
      dispatch({
        type: 'DATE_CHANGE',
        booking: booking
      });
      return Promise.all([loadExtendedData(booking)]).then(success => {
        setLoading(false);
        return {
          success: success
        };
      }).catch(function (error) {
        dispatch({
          type: 'ERROR',
          error: error.message
        });
        setLoading(false);
        return {
          success: false,
          error: error.message
        };
      });
    }).catch(error => {
      dispatch({
        type: 'ERROR',
        error: error.message
      });
      return {
        success: false,
        error: error.message
      };
    });
  }, [apiClient.booking, state.booking, loadExtendedData]);
  const getVoucherPdf = (0, _react.useCallback)(async () => {
    setLoading(true);
    return apiClient.booking.getBookingPdf((0, _get2.default)(state.booking, 'id'), (0, _get2.default)(state.booking, 'order.customer.sid')).then(result => {
      setLoading(false);
      return {
        success: true,
        voucher: {
          fileName: (0, _get2.default)(state.booking, 'locator') + ".pdf",
          blob: result
        }
      };
    }).catch(error => {
      setLoading(false);
      dispatch({
        type: 'ERROR',
        error: error.message
      });
      return {
        success: false,
        error: error.message
      };
    });
  }, [apiClient.booking, state.booking]);
  const getInvoicePdf = (0, _react.useCallback)(async (invoiceId, number) => {
    setLoading(true);
    return apiClient.invoice.getInvoicePdf(invoiceId, {
      order_id: (0, _get2.default)(state.booking, 'order.id')
    }).then(result => {
      setLoading(false);
      return {
        success: true,
        invoice: {
          fileName: number + '_' + (0, _get2.default)(state.booking, 'locator') + ".pdf",
          blob: result
        }
      };
    }).catch(error => {
      setLoading(false);
      dispatch({
        type: 'ERROR',
        error: error.message
      });
      return {
        success: false,
        error: error.message
      };
    });
  }, [apiClient.invoice, state.booking]);
  const cancelRefund = (0, _react.useCallback)(async () => {
    setLoading(true);
    return state.booking.getPendingRefund().then(refund => {
      return apiClient.refund.cancelRefund((0, _get2.default)(refund, 'id'), {
        order_id: (0, _get2.default)(state.booking, 'order.id')
      }).then(() => {
        getBooking();
        setLoading(false);
        return {
          success: true
        };
      }).catch(function (error) {
        setLoading(false);
        dispatch({
          type: 'ERROR',
          error: error.message
        });
        return {
          success: false,
          error: error.message
        };
      });
    }).catch(function (error) {
      setLoading(false);
      dispatch({
        type: 'ERROR',
        error: error.message
      });
      return {
        success: false,
        error: error.message
      };
    });
  }, [apiClient.refund, state.booking]);
  const requestInvoice = (0, _react.useCallback)(async data => {
    setLoading(true);
    return apiClient.invoice.createInvoice((0, _get2.default)(state.booking, 'order.id'), data).then(invoice => {
      getBooking();
      setLoading(false);
      return {
        success: true,
        invoice: invoice
      };
    }).catch(function (error) {
      setLoading(false);
      dispatch({
        type: 'ERROR',
        error: error.message
      });
      return {
        success: false,
        error: error.message
      };
    });
  }, [apiClient.booking, state.booking]);
  (0, _react.useEffect)(() => {
    if (authContext.isAuthenticated) {
      setReady(false);
      getBooking();
    }
    setReady(true);
  }, [authContext.isAuthenticated]);
  const contextValue = (0, _react.useMemo)(() => {
    return _objectSpread(_objectSpread({}, state), {}, {
      loading: loading,
      clearContext,
      getBooking,
      cancellationRequest,
      dateChangeRequest,
      getVoucherPdf,
      cancelRefund,
      getInvoicePdf,
      requestInvoice
    });
  }, [state, loading, clearContext, getBooking, cancellationRequest, dateChangeRequest, getVoucherPdf, cancelRefund, getInvoicePdf, requestInvoice]);
  return ready && /*#__PURE__*/_react.default.createElement(Context.Provider, {
    value: contextValue
  }, children);
};
var _default = exports.default = BookingProvider;
const useBooking = () => (0, _react.useContext)(Context);
exports.useBooking = useBooking;