"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.invoices = exports.booking = void 0;
var _get2 = _interopRequireDefault(require("lodash/get"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
const bookings = {
  "total": 1,
  "page_count": 1,
  "count": 1,
  "page": 1,
  "execution_time": 6,
  "_links": {
    "self": {
      "href": "https://api.qa.stg.volcanoteide.com/admin/bookings?locator=29LKV-2449&page=1&limit=20"
    },
    "first": {
      "href": "https://api.qa.stg.volcanoteide.com/admin/bookings?locator=29LKV-2449"
    },
    "last": {
      "href": "https://api.qa.stg.volcanoteide.com/admin/bookings?locator=29LKV-2449&page=1&limit=20"
    },
    "send_email": {
      "bookings": {
        "href": "https://api.qa.stg.volcanoteide.com/admin/tasks",
        "method": "POST",
        "scope": "entity"
      }
    },
    "cancellation_request": {
      "bookings": {
        "href": "https://api.qa.stg.volcanoteide.com/admin/tasks",
        "method": "POST",
        "scope": "entity"
      }
    },
    "booking_date_change": {
      "bookings": {
        "href": "https://api.qa.stg.volcanoteide.com/admin/tasks",
        "method": "POST",
        "scope": "entity"
      }
    },
    "product_change_request": {
      "bookings": {
        "href": "https://api.qa.stg.volcanoteide.com/admin/tasks",
        "method": "POST",
        "scope": "entity"
      }
    },
    "set_no_show": {
      "bookings": {
        "href": "https://api.qa.stg.volcanoteide.com/admin/tasks",
        "method": "POST",
        "scope": "entity"
      }
    },
    "set_exchange_date": {
      "bookings": {
        "href": "https://api.qa.stg.volcanoteide.com/admin/tasks",
        "method": "POST",
        "scope": "entity"
      }
    }
  },
  "_aggregate": {
    "bookings": []
  },
  "_embedded": {
    "bookings": [{
      "id": 2414239,
      "product": {
        "id": 46,
        "name": "Ticket Subida y Bajada",
        "name_spa": "Ticket Subida y Bajada",
        "_links": {
          "self": {
            "href": "https://api.qa.stg.volcanoteide.com/products/46"
          }
        },
        "state": "active",
        "sku": "",
        "validation_type": "single",
        "with_participants": false,
        "with_tickets": true,
        "with_pickup": false,
        "with_confirmation": false,
        "with_sms_notifications": true,
        "requires_confirmation": false,
        "transport_id": 0,
        "active": true,
        "qty_restrictions": {
          "min": 1,
          "max": 0
        },
        "experience": {
          "id": 48,
          "name": "Entradas Teleférico del Teide",
          "name_spa": "Entradas Teleférico del Teide",
          "_links": {
            "self": {
              "href": "https://api.qa.stg.volcanoteide.com/experiences/48"
            }
          },
          "duration": "01:00:00",
          "duration_days": 0,
          "duration_flexible": false,
          "difficulty": {
            "id": 1
          },
          "languages": "en,de,fr,es,ru,it,pl,nl"
        },
        "supplier": {
          "id": 6,
          "name": "Teleférico del Pico de Teide"
        },
        "remote_product_provider": {
          "id": 1,
          "alias": "Teide",
          "type": "IACPOS"
        }
      },
      "experience": {
        "id": 48,
        "name": "Entradas Teleférico del Teide",
        "_links": {
          "self": {
            "href": "https://api.qa.stg.volcanoteide.com/experiences/48"
          }
        },
        "duration": "01:00:00",
        "duration_days": 0,
        "duration_flexible": false,
        "difficulty": {
          "id": 1
        }
      },
      "locator": "29LKV-2449",
      "qr_code": "VTE/1/1/29LKV-2449/-/20241020/1210/46/-/1/990",
      "requires_confirmation": 0,
      "with_participants": false,
      "with_tickets": true,
      "with_confirmation": false,
      "confirmed": true,
      "cancel_state": null,
      "custom_fields": null,
      "booking_date": "2024-10-20 12:10:00",
      "state": "valid",
      "payment_method": "credit_card",
      "no_show_cost": 186.89,
      "last_history_state": {
        "id": "93c79827-7742-4b4a-8940-1ed873605430",
        "order_id": 100002068425,
        "line_item_model": null,
        "line_item_id": null,
        "order_log_entry_type": {
          "id": "payment_completed",
          "name": "Pago completado"
        },
        "description": null,
        "data": {
          "payment_method_id": 3,
          "payment_gateway_id": "811d800a-6c7e-11ec-a04c-0cc47ac3a6b8",
          "transaction_id": 1339674
        },
        "created": "2024-10-11 17:10:48",
        "user_id": "b377b976-28e0-11eb-80ba-0cc47ac3a6b8",
        "user": "null"
      },
      "remote_sync_pending": true,
      "product_rates": [{
        "id": 2858221,
        "rate_id": 19924,
        "rate_name": "Adult, Non-resident",
        "rate_name_abbr": "AN",
        "rate_name_spa": "Adulto, No residente",
        "customer_type_ids": "16,19",
        "qty": 1,
        "pvp": 186.891,
        "neto": 186.891,
        "collaborator_pvd": null,
        "collaborator_igic": null,
        "unit_retail_price": 186.891,
        "unit_net_price": 186.891,
        "total_amount_pvp": 186.891,
        "total_amount_net": 186.891,
        "total_base": 174.66,
        "total_amount": 186.89,
        "total_tax": 12.23,
        "main_currency": {
          "total_base": 40.35,
          "total_amount": 43.18,
          "total_tax": 2.83,
          "unit_retail_price": 43.1798,
          "unit_net_price": 43.1798
        },
        "unit_price": 186.891,
        "qty_restrictions": {
          "min": 0,
          "max": null
        },
        "tickets": [{
          "id": 1549143,
          "locator": "29LKV-2449-71680",
          "qr_code": "VTE/1/1/29LKV-2449-71680/1549143/20241020/1210/46/16,19/1/129",
          "number": 1,
          "rate_name": "Adult, Non-resident",
          "pvp": 186.891,
          "rate_name_abbr": "AN"
        }]
      }],
      "amount": {
        "total_amount_pvp": 186.89,
        "total_amount_net": 186.89,
        "total_amount": 186.89,
        "total_tax": 12.23,
        "total_base": 174.66,
        "main_currency": {
          "total_amount": {
            "value": 43.18,
            "formatted": "43,18",
            "formatted_currency": "43,18 €"
          },
          "total_tax": {
            "value": 2.83,
            "formatted": "2,83",
            "formatted_currency": "2,83 €"
          },
          "total_base": {
            "value": 40.35,
            "formatted": "40,35",
            "formatted_currency": "40,35 €"
          }
        }
      },
      "total": 186.89,
      "ordered_booking_tickets": [{
        "id": 1549143,
        "locator": "29LKV-2449-71680",
        "qr_code": "VTE/1/1/29LKV-2449-71680/1549143/20241020/1210/46/16,19/1/129",
        "number": 1,
        "rate_name": "Adult, Non-resident",
        "pvp": 186.891,
        "rate_name_abbr": "AN"
      }],
      "order": {
        "id": 100002068425,
        "language": "eng",
        "voucher_id": null,
        "site_id": 1,
        "tpvid": 350206842521,
        "pp_transaction_id": null,
        "billing_type": 1,
        "new_total": 186.89,
        "currency": "PLN",
        "main_currency": "EUR",
        "currency_exchange_rate": 4.3282,
        "created": "2024-10-11 17:10:20",
        "transaction_date": "2024-10-11 18:10:00",
        "customer": {
          "id": 2137896,
          "id_card": null,
          "first_name": "prueba soporte",
          "last_name": "ss",
          "full_name": "prueba soporte ss",
          "email": "test2@volcanoteide.com",
          "phone": null,
          "phone_other": null,
          "location": null,
          "hotel": null,
          "room": null,
          "comments": null,
          "sid": "e31d31ef-88aa-4fcb-b8ba-88c250aa648e",
          "_links": {
            "self": {
              "href": "https://api.qa.stg.volcanoteide.com/admin/orders/100002068425/customers/2137896"
            }
          }
        },
        "enterprise": {
          "id": 1,
          "name": "Teleférico del Pico de Teide, S.A."
        },
        "site": {
          "id": 1,
          "name": "telefericoteide"
        },
        "main_site": {
          "id": 1,
          "name": "telefericoteide"
        },
        "_links": {
          "self": {
            "href": "https://api.qa.stg.volcanoteide.com/admin/orders/100002068425"
          }
        }
      },
      "managed": false,
      "locked_by": null,
      "_links": {
        "self": {
          "href": "https://api.qa.stg.volcanoteide.com/admin/bookings/2414239"
        }
      }
    }]
  }
};
const booking = exports.booking = (0, _get2.default)(bookings, "_embedded.bookings[0]");
const invoices = exports.invoices = [{
  "id": 826130,
  "title": "UO22078720",
  "code": "UO",
  "year": 2022,
  "number": 78720,
  "type": "normal",
  "state": "paid",
  "created": "2022-07-28 16:00:18",
  "data": {
    "payment_method_id": 3,
    "provider": {
      "id": 1,
      "cif": "A-38002549",
      "name": "Teleférico del Pico de Teide, S.A.",
      "address": "Calle San Francisco nº 5 - 4ª Planta",
      "cp": "38002",
      "city": "Santa Cruz de Tenerife",
      "phone": "922 010 440",
      "fax": "922 287 837",
      "invoice_footer": "<span style=\"font-size: 7pt; font-family: arial, helvetica, sans-serif; line-height:9pt\"><strong>Telef&eacute;rico del Pico del Teide, S.A.</strong>- Inscrita en Registro Mercantil de Santa Cruz de Tenerife el 12 de Agosto de 1960, libro 1&ordm; de la secci&oacute;n 3&ordf;, folio 174, hoja 15. </span><br><span style=\"font-size: 7pt; font-family: arial, helvetica, sans-serif;\">C/ San Francisco, 5 - 4&ordm;. 38002. Santa Cruz de Tenerife - <strong>CIF A-38002549</strong>- teleferico@telefericoteide.com - Tel&eacute;fono 922 010 440 - Fax 922 287 837</span>",
      "logo_url": "logo_teleferico.png",
      "seal": "sello.png"
    },
    "orders": [{
      "id": 100001364125,
      "payment_method_id": 3,
      "bookings": [{
        "id": 1786363,
        "locator": "1MGFR-EA3F",
        "experience": {
          "id": 48,
          "name": "Entradas Teleférico del Teide"
        },
        "product": {
          "id": 46,
          "name": "Ticket Subida y Bajada"
        },
        "booking_rates": [{
          "id": 2102920,
          "qty": 4,
          "customer_type": {
            "16": "Adulto",
            "19": "No residente"
          },
          "description": "Ticket Subida y Bajada (Adulto, No residente)"
        }, {
          "id": 2102921,
          "qty": 1,
          "customer_type": {
            "16": "Adulto",
            "19": "No residente"
          },
          "product_option": {
            "id": 22,
            "name": "Libro Guía de ascensión al Teide"
          },
          "description": "Libro Guía de ascensión al Teide (Adulto, No residente)"
        }]
      }]
    }]
  },
  "enterprise": {
    "id": 1,
    "name": "Teleférico del Pico de Teide, S.A.",
    "_links": {
      "self": {
        "href": "https://api.staging.volcanoteide.com/admin/enterprises/1"
      }
    }
  },
  "line_items": [{
    "id": 947138,
    "simplified_invoice_id": 826130,
    "type": 1,
    "foreign_key_id": 1786363,
    "booking_date": "2022-12-30 09:50:00",
    "description": "Entradas Teleférico del Teide [1MGFR-EA3F]",
    "total_price": 166,
    "total_tax": 5.34,
    "simplified_invoice_line_item_entries": [{
      "id": 1196959,
      "simplified_invoice_line_item_id": 947138,
      "type": 1,
      "foreign_key_id": 2102920,
      "description": "Ticket Subida y Bajada (Adulto, No residente)",
      "quantity": 4,
      "unit_price": 38,
      "tax_percentage": 3,
      "total_base": 147.5728,
      "total_price": 152,
      "total_tax": 4.4272
    }, {
      "id": 1196960,
      "simplified_invoice_line_item_id": 947138,
      "type": 1,
      "foreign_key_id": 2102921,
      "description": "Libro Guía de ascensión al Teide (Adulto, No residente)",
      "quantity": 1,
      "unit_price": 14,
      "tax_percentage": 7,
      "total_base": 13.0841,
      "total_price": 14,
      "total_tax": 0.9159
    }]
  }],
  "amount": {
    "total_amount": 166,
    "total_tax": 5.34,
    "components": [{
      "total_amount": 152,
      "total_base": 147.57,
      "total_tax": 4.43,
      "tax_percentage": 3
    }, {
      "total_amount": 14,
      "total_base": 13.08,
      "total_tax": 0.92,
      "tax_percentage": 7
    }]
  },
  "summary": {
    "48": {
      "name": "Entradas Teleférico del Teide",
      "products": {
        "46": {
          "name": "Ticket Subida y Bajada",
          "rates": {
            "16-19": {
              "name": "Adulto, No residente",
              "quantity": 5
            }
          }
        }
      }
    }
  },
  "_links": {
    "self": {
      "href": "https://api.staging.volcanoteide.com/simplified-invoices/826130"
    }
  },
  "actions": []
}, {
  "id": 930712,
  "title": "RUO2252954",
  "code": "RUO",
  "year": 2022,
  "number": 52954,
  "type": "full_refund",
  "state": "issued",
  "created": "2022-12-30 08:26:03",
  "data": {
    "payment_method_id": 3,
    "provider": {
      "id": 1,
      "cif": "A-38002549",
      "name": "Teleférico del Pico de Teide, S.A.",
      "address": "Calle San Francisco nº 5 - 4ª Planta",
      "cp": "38002",
      "city": "Santa Cruz de Tenerife",
      "phone": "922 010 440",
      "fax": "922 287 837",
      "invoice_footer": "<span style=\"font-size: 7pt; font-family: arial, helvetica, sans-serif; line-height:9pt\"><strong>Telef&eacute;rico del Pico del Teide, S.A.</strong>- Inscrita en Registro Mercantil de Santa Cruz de Tenerife el 12 de Agosto de 1960, libro 1&ordm; de la secci&oacute;n 3&ordf;, folio 174, hoja 15. </span><br><span style=\"font-size: 7pt; font-family: arial, helvetica, sans-serif;\">C/ San Francisco, 5 - 4&ordm;. 38002. Santa Cruz de Tenerife - <strong>CIF A-38002549</strong>- teleferico@telefericoteide.com - Tel&eacute;fono 922 010 440 - Fax 922 287 837</span>",
      "logo_url": "logo_teleferico.png",
      "seal": "sello.png"
    },
    "orders": [{
      "id": 100001364125,
      "payment_method_id": 3,
      "bookings": [{
        "id": 1786363,
        "locator": "1MGFR-EA3F",
        "experience": {
          "id": 48,
          "name": "Entradas Teleférico del Teide"
        },
        "product": {
          "id": 46,
          "name": "Ticket Subida y Bajada"
        },
        "booking_rates": [{
          "id": 2102920,
          "qty": 4,
          "customer_type": {
            "16": "Adulto",
            "19": "No residente"
          },
          "description": "Ticket Subida y Bajada (Adulto, No residente)"
        }, {
          "id": 2102921,
          "qty": 1,
          "customer_type": {
            "16": "Adulto",
            "19": "No residente"
          },
          "product_option": {
            "id": 22,
            "name": "Libro Guía de ascensión al Teide"
          },
          "description": "Libro Guía de ascensión al Teide (Adulto, No residente)"
        }]
      }]
    }]
  },
  "enterprise": {
    "id": 1,
    "name": "Teleférico del Pico de Teide, S.A.",
    "_links": {
      "self": {
        "href": "https://api.staging.volcanoteide.com/admin/enterprises/1"
      }
    }
  },
  "line_items": [{
    "id": 1166886,
    "simplified_invoice_id": 930712,
    "type": 1,
    "foreign_key_id": 1786363,
    "booking_date": "2022-12-30 09:50:00",
    "description": "Entradas Teleférico del Teide [1MGFR-EA3F]",
    "total_price": -166,
    "total_tax": -5.34,
    "simplified_invoice_line_item_entries": [{
      "id": 1458449,
      "simplified_invoice_line_item_id": 1166886,
      "type": 1,
      "foreign_key_id": 2102920,
      "description": "Ticket Subida y Bajada (Adulto, No residente)",
      "quantity": 4,
      "unit_price": -38,
      "tax_percentage": 3,
      "total_base": 147.5728,
      "total_price": -152,
      "total_tax": -4.4272
    }, {
      "id": 1458450,
      "simplified_invoice_line_item_id": 1166886,
      "type": 1,
      "foreign_key_id": 2102921,
      "description": "Libro Guía de ascensión al Teide (Adulto, No residente)",
      "quantity": 1,
      "unit_price": -14,
      "tax_percentage": 7,
      "total_base": 13.0841,
      "total_price": -14,
      "total_tax": -0.9159
    }]
  }],
  "amount": {
    "total_amount": -166,
    "total_tax": -5.34,
    "components": [{
      "total_amount": -152,
      "total_base": -147.57,
      "total_tax": -4.43,
      "tax_percentage": 3
    }, {
      "total_amount": -14,
      "total_base": -13.08,
      "total_tax": -0.92,
      "tax_percentage": 7
    }]
  },
  "summary": {
    "48": {
      "name": "Entradas Teleférico del Teide",
      "products": {
        "46": {
          "name": "Ticket Subida y Bajada",
          "rates": {
            "16-19": {
              "name": "Adulto, No residente",
              "quantity": 5
            }
          }
        }
      }
    }
  },
  "_links": {
    "self": {
      "href": "https://api.staging.volcanoteide.com/simplified-invoices/930712"
    }
  },
  "actions": []
}];