"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.Default = void 0;
var _react = _interopRequireDefault(require("react"));
var _configApi = _interopRequireDefault(require("../../stories/configApi"));
var _VolcanoBookingManagementWidget = _interopRequireDefault(require("./VolcanoBookingManagementWidget"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
var _default = exports.default = {
  component: _VolcanoBookingManagementWidget.default,
  title: "Booking Management Widget/Volcano booking management widget"
};
const Template = args => /*#__PURE__*/_react.default.createElement(_VolcanoBookingManagementWidget.default, args);
const Default = exports.Default = Template.bind({});
Default.args = {
  locale: 'es',
  apiConfig: _configApi.default
};