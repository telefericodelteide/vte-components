"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
require("./login.css");
var _react = _interopRequireWildcard(require("react"));
var _reactI18next = require("react-i18next");
var _isEmpty2 = _interopRequireDefault(require("lodash/isEmpty"));
var _Button = _interopRequireDefault(require("../Button/Button"));
var _auth = require("../context/auth.context");
var _formik = require("formik");
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function (e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != typeof e && "function" != typeof e) return { default: e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && {}.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n.default = e, t && t.set(e, n), n; }
const Login = () => {
  const authContext = (0, _auth.useAuth)();
  const [error, setError] = (0, _react.useState)({
    type: undefined,
    msg: ''
  });
  const {
    t
  } = (0, _reactI18next.useTranslation)('bm');
  const initialValues = {
    bookingLocator: '',
    customerEmail: ''
  };
  const handleFormSubmit = (values, actions) => {
    if (values.bookingLocator.trim() === '') {
      setError({
        type: 'locator',
        msg: t('manage_booking.login.submit_locator_error')
      });
    } else if (values.customerEmail.trim() === '') {
      setError({
        type: 'email',
        msg: t('manage_booking.login.submit_email_empty_error')
      });
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.\w+$/i.test(values.customerEmail)) {
      setError({
        type: 'email',
        msg: t('manage_booking.login.submit_email_format_error')
      });
    } else {
      setError({
        type: undefined,
        msg: ''
      });
      authContext.authenticate(values.bookingLocator.trim(), values.customerEmail.trim());
    }
  };
  (0, _react.useEffect)(() => {
    if (!(0, _isEmpty2.default)(authContext.error)) {
      setError({
        type: 'login',
        msg: t('manage_booking.login.login_request_error')
      });
    }
  }, [authContext, t]);
  return /*#__PURE__*/_react.default.createElement("div", {
    id: "login",
    className: "login"
  }, /*#__PURE__*/_react.default.createElement(_formik.Formik, {
    initialValues: initialValues,
    onSubmit: handleFormSubmit
  }, /*#__PURE__*/_react.default.createElement(_formik.Form, {
    className: "form-wrapper uk-form-stacked"
  }, /*#__PURE__*/_react.default.createElement("p", null, t('manage_booking.login.body')), /*#__PURE__*/_react.default.createElement("div", {
    className: "form-field-wrapper"
  }, /*#__PURE__*/_react.default.createElement("label", {
    htmlFor: "bookingLocator",
    className: "form-field-label uk-form-label"
  }, t('manage_booking.login.locator_title')), /*#__PURE__*/_react.default.createElement("div", {
    className: "uk-form-controls"
  }, /*#__PURE__*/_react.default.createElement(_formik.Field, {
    name: "bookingLocator",
    className: "uk-input",
    placeholder: "NKN6-48CAF"
  }), error.type === 'locator' ? /*#__PURE__*/_react.default.createElement("div", {
    className: "form-field-error uk-text-danger"
  }, error.msg) : '')), /*#__PURE__*/_react.default.createElement("div", {
    className: "form-field-wrapper"
  }, /*#__PURE__*/_react.default.createElement("label", {
    htmlFor: "customerEmail",
    className: "form-field-label uk-form-label"
  }, t('manage_booking.login.email_title')), /*#__PURE__*/_react.default.createElement("div", {
    className: "uk-form-controls"
  }, /*#__PURE__*/_react.default.createElement(_formik.Field, {
    name: "customerEmail",
    className: "uk-input"
  }), error.type === 'email' ? /*#__PURE__*/_react.default.createElement("div", {
    className: "form-field-error uk-text-danger"
  }, error.msg) : '')), /*#__PURE__*/_react.default.createElement(_Button.default, {
    text: t('manage_booking.login.button')
  }))), /*#__PURE__*/_react.default.createElement("div", {
    className: "uk-text-danger"
  }, error.type === 'login' ? error.msg : authContext.error));
};
var _default = exports.default = Login;