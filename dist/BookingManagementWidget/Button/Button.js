"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _react = _interopRequireWildcard(require("react"));
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function (e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != typeof e && "function" != typeof e) return { default: e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && {}.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n.default = e, t && t.set(e, n), n; }
const Button = _ref => {
  let {
    text,
    title,
    disabled,
    className,
    style,
    hideOnClick,
    onClick
  } = _ref;
  const type = onClick ? "" : "submit";
  const [visible, setVisible] = (0, _react.useState)(true);
  const onButtonClick = event => {
    if (hideOnClick) {
      setVisible(false);
    }
    if (onClick !== undefined) {
      onClick();
      event.preventDefault();
    }
  };
  return visible && /*#__PURE__*/_react.default.createElement("button", {
    disabled: disabled,
    type: type,
    className: "uk-button uk-align-center volcano-button " + className,
    style: style,
    title: title,
    onClick: e => onButtonClick(e)
  }, text);
};
var _default = exports.default = Button;