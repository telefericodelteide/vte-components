"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _react = _interopRequireWildcard(require("react"));
var _volcanoteideApiClient = _interopRequireDefault(require("@volcanoteide/volcanoteide-api-client"));
var _BookingManagementWidget = _interopRequireDefault(require("./BookingManagementWidget"));
var _i18n = _interopRequireDefault(require("./config/i18n"));
var _i18next = _interopRequireDefault(require("i18next"));
var _reactI18next = require("react-i18next");
var _i18nextHttpBackend = _interopRequireDefault(require("i18next-http-backend"));
var _auth = _interopRequireDefault(require("./context/auth.context"));
var _booking = _interopRequireDefault(require("./context/booking.context"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function (e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != typeof e && "function" != typeof e) return { default: e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && {}.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n.default = e, t && t.set(e, n), n; }
function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
function _objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
function _defineProperty(e, r, t) { return (r = _toPropertyKey(r)) in e ? Object.defineProperty(e, r, { value: t, enumerable: !0, configurable: !0, writable: !0 }) : e[r] = t, e; }
function _toPropertyKey(t) { var i = _toPrimitive(t, "string"); return "symbol" == typeof i ? i : i + ""; }
function _toPrimitive(t, r) { if ("object" != typeof t || !t) return t; var e = t[Symbol.toPrimitive]; if (void 0 !== e) { var i = e.call(t, r || "default"); if ("object" != typeof i) return i; throw new TypeError("@@toPrimitive must return a primitive value."); } return ("string" === r ? String : Number)(t); }
const VolcanoBookingManagementWidget = _ref => {
  let {
    locale,
    apiConfig
  } = _ref;
  // init api client
  const client = new _volcanoteideApiClient.default(_objectSpread(_objectSpread({}, apiConfig), {}, {
    locale: locale
  }));
  const [isLoading, setLoading] = (0, _react.useState)(false);
  if (!_i18next.default.isInitialized) {
    _i18next.default.use(_i18nextHttpBackend.default).use(_reactI18next.initReactI18next).init({
      interpolation: {
        escapeValue: false
      },
      lng: locale,
      resources: _i18n.default,
      ns: ["bm"],
      defaultNS: "bm"
    });
  } else {
    if (_i18next.default.language !== locale) {
      _i18next.default.changeLanguage(locale);
    }
    _i18next.default.addResourceBundle(locale, "bm", _i18n.default[locale]["bm"]);
  }
  const availabilityFetcher = (bookingId, productId, quantity, date) => {
    setLoading(true);
    const params = {
      booking_id: bookingId,
      min_qty: quantity
    };
    return client.experience.getProductAvailability(productId, date, true, params).then(result => {
      setLoading(false);
      return result;
    }).catch(function (error) {
      setLoading(false);
      return [];
    });
  };
  return /*#__PURE__*/_react.default.createElement(_reactI18next.I18nextProvider, {
    i18n: _i18next.default
  }, /*#__PURE__*/_react.default.createElement(_auth.default, {
    apiClient: client
  }, /*#__PURE__*/_react.default.createElement(_booking.default, {
    apiClient: client
  }, /*#__PURE__*/_react.default.createElement(_BookingManagementWidget.default, {
    availabilityFetcher: availabilityFetcher,
    loading: isLoading
  }))));
};
var _default = exports.default = VolcanoBookingManagementWidget;