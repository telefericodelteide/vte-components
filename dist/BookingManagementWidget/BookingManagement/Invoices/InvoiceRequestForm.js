"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _react = _interopRequireDefault(require("react"));
var _reactI18next = require("react-i18next");
var _countries = _interopRequireDefault(require("../../../assets/countries.json"));
var _streetTypes = _interopRequireDefault(require("../../../assets/streetTypes.json"));
var _FormWidget = _interopRequireDefault(require("../../../Generic/Form/FormWidget"));
var _get2 = _interopRequireDefault(require("lodash/get"));
var _set2 = _interopRequireDefault(require("lodash/set"));
var _isArray = _interopRequireDefault(require("lodash/isArray"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
function _objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
function _defineProperty(e, r, t) { return (r = _toPropertyKey(r)) in e ? Object.defineProperty(e, r, { value: t, enumerable: !0, configurable: !0, writable: !0 }) : e[r] = t, e; }
function _toPropertyKey(t) { var i = _toPrimitive(t, "string"); return "symbol" == typeof i ? i : i + ""; }
function _toPrimitive(t, r) { if ("object" != typeof t || !t) return t; var e = t[Symbol.toPrimitive]; if (void 0 !== e) { var i = e.call(t, r || "default"); if ("object" != typeof i) return i; throw new TypeError("@@toPrimitive must return a primitive value."); } return ("string" === r ? String : Number)(t); }
const InvoiceRequestForm = _ref => {
  let {
    onSubmitForm,
    submitParams,
    onCancelClick,
    initVals
  } = _ref;
  const {
    t,
    i18n
  } = (0, _reactI18next.useTranslation)('bm');
  let initialValues = initVals ? initVals : {
    customer: {
      type: {
        value: 'person'
      },
      vat_number_type: {
        value: 'nif'
      },
      country: {
        id: {
          value: 'ES'
        }
      }
    }
  };
  let data = {};
  const handleFormSubmit = async values => {
    return new Promise((resolve, reject) => {
      return onSubmitForm(_objectSpread(_objectSpread({}, submitParams), {}, {
        data: values
      })).then(() => {
        resolve(values);
      });
    });
  };
  const form = {
    fields: [{
      title: null,
      fields: [{
        id: 'customer.type',
        title: 'manage_booking.booking_actions.invoices.request_form.customer_type',
        required: true,
        type: 'select',
        options: ['person', 'enterprise']
      }, {
        id: 'customer.name',
        title: 'manage_booking.booking_actions.invoices.request_form.customer_name',
        required: true,
        type: 'text'
      }, {
        id: 'customer.vat_number_type',
        title: 'manage_booking.booking_actions.invoices.request_form.vat_number_type',
        required: true,
        type: 'select',
        options: ['nif', 'passport', 'document_country', 'document_residence', 'document_other']
      }, {
        id: 'customer.vat_number',
        title: 'manage_booking.booking_actions.invoices.request_form.vat_number',
        required: true,
        type: 'text'
      }, {
        id: "customer.road_type.id",
        title: 'manage_booking.booking_actions.invoices.request_form.road_type_id',
        required: true,
        type: "select",
        options: _streetTypes.default
      }, {
        id: 'customer.address',
        title: 'manage_booking.booking_actions.invoices.request_form.address',
        required: true,
        type: 'text'
      }, {
        id: 'customer.locality',
        title: 'manage_booking.booking_actions.invoices.request_form.locality',
        required: true,
        type: 'text'
      }, {
        id: 'customer.postal_code',
        title: 'manage_booking.booking_actions.invoices.request_form.postal_code',
        required: true,
        type: 'text'
      }, {
        id: 'customer.state',
        title: 'manage_booking.booking_actions.invoices.request_form.state',
        required: true,
        type: 'text'
      }, {
        id: "customer.country.id",
        title: 'manage_booking.booking_actions.invoices.request_form.country_id',
        required: true,
        type: "select",
        options: _countries.default
      }]
    }],
    conditions: []
  };
  form.fields.forEach((fieldset, i) => fieldset.fields.forEach((field, j) => {
    const defaultValue = (0, _get2.default)(initialValues, field.id);
    if (defaultValue) {
      form.fields[i].fields[j].type = (0, _get2.default)(defaultValue, 'hidden', false) ? 'hidden' : form.fields[i].fields[j].type;
      (0, _set2.default)(initialValues, field.id, defaultValue.value, '');
    } else {
      (0, _set2.default)(initialValues, field.id, '');
    }
  }));
  const processForm = form => {
    form.fields = form.fields.map(item => {
      item.title = t(item.title);
      item.fields = item.fields.map(field => {
        const key = field.title;
        field.title = t(key);
        if (field.options && (0, _isArray.default)(field.options)) {
          var toptions = {};
          field.options.forEach(option => {
            toptions[option] = t(key + '_options.' + option);
          });
          field.options = toptions;
        }
        return field;
      });
      return item;
    });
    return form;
  };
  if (i18n.isInitialized) {
    data = processForm(form);
  }
  return /*#__PURE__*/_react.default.createElement("div", {
    className: "invoice-request-form uk-width-1-1 uk-width-2-3@m"
  }, /*#__PURE__*/_react.default.createElement("h2", {
    className: "uk-text-large"
  }, t('manage_booking.booking_actions.invoices.request_form.title')), /*#__PURE__*/_react.default.createElement("div", {
    className: "uk-text-small uk-margin-bottom"
  }, t('manage_booking.booking_actions.invoices.request_form.body')), /*#__PURE__*/_react.default.createElement(_FormWidget.default, {
    locale: i18n.language,
    initialValues: initialValues,
    form: data,
    confirmButton: {
      title: t('manage_booking.booking_actions.invoices.request_form.submit_button'),
      handler: handleFormSubmit
    },
    cancelButton: {
      title: t('manage_booking.booking_actions.cancel_request.cancel_button'),
      handler: onCancelClick
    }
  }));
};
var _default = exports.default = InvoiceRequestForm;