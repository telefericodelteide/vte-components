"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _react = _interopRequireDefault(require("react"));
var _reactI18next = require("react-i18next");
var _Button = _interopRequireDefault(require("../../Button/Button"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
const InvoicesListItem = _ref => {
  let {
    number,
    type,
    date,
    amount,
    onDownloadClick
  } = _ref;
  const {
    t,
    i18n
  } = (0, _reactI18next.useTranslation)('bm');
  const priceFormatter = new Intl.NumberFormat(i18n.language, {
    style: "currency",
    currency: "eur"
  });
  return /*#__PURE__*/_react.default.createElement("tr", null, /*#__PURE__*/_react.default.createElement("td", null, number), /*#__PURE__*/_react.default.createElement("td", null, date), /*#__PURE__*/_react.default.createElement("td", null, priceFormatter.format(amount)), /*#__PURE__*/_react.default.createElement("td", null, /*#__PURE__*/_react.default.createElement(_Button.default, {
    className: "fa fa-download",
    title: t('manage_booking.booking_actions.invoices.download_button'),
    onClick: onDownloadClick
  })));
};
var _default = exports.default = InvoicesListItem;