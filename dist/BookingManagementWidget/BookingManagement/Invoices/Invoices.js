"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _react = _interopRequireWildcard(require("react"));
var _reactI18next = require("react-i18next");
var _InvoicesListItem = _interopRequireDefault(require("./InvoicesListItem"));
var _get2 = _interopRequireDefault(require("lodash/get"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function (e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != typeof e && "function" != typeof e) return { default: e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && {}.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n.default = e, t && t.set(e, n), n; }
function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
function _objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
function _defineProperty(e, r, t) { return (r = _toPropertyKey(r)) in e ? Object.defineProperty(e, r, { value: t, enumerable: !0, configurable: !0, writable: !0 }) : e[r] = t, e; }
function _toPropertyKey(t) { var i = _toPrimitive(t, "string"); return "symbol" == typeof i ? i : i + ""; }
function _toPrimitive(t, r) { if ("object" != typeof t || !t) return t; var e = t[Symbol.toPrimitive]; if (void 0 !== e) { var i = e.call(t, r || "default"); if ("object" != typeof i) return i; throw new TypeError("@@toPrimitive must return a primitive value."); } return ("string" === r ? String : Number)(t); }
const Invoices = _ref => {
  let {
    invoices,
    onDownloadClick,
    downloadParams
  } = _ref;
  const {
    t
  } = (0, _reactI18next.useTranslation)('bm');
  const handleDownloadClick = (0, _react.useCallback)(index => {
    onDownloadClick(_objectSpread(_objectSpread({}, downloadParams), {}, {
      invoiceId: (0, _get2.default)(invoices[index], 'id'),
      number: (0, _get2.default)(invoices[index], 'title')
    }));
  }, [invoices, onDownloadClick, downloadParams]);
  return /*#__PURE__*/_react.default.createElement("div", {
    className: "invoices uk-width-1-1 uk-width-2-3@m"
  }, /*#__PURE__*/_react.default.createElement("h2", {
    className: "uk-text-large"
  }, t('manage_booking.booking_actions.invoices.title')), /*#__PURE__*/_react.default.createElement("table", {
    className: "uk-table uk-table-striped"
  }, /*#__PURE__*/_react.default.createElement("thead", null, /*#__PURE__*/_react.default.createElement("tr", null, /*#__PURE__*/_react.default.createElement("th", null, t('manage_booking.booking_actions.invoices.table_header_number')), /*#__PURE__*/_react.default.createElement("th", null, t('manage_booking.booking_actions.invoices.table_header_date')), /*#__PURE__*/_react.default.createElement("th", null, t('manage_booking.booking_actions.invoices.table_header_amount')), /*#__PURE__*/_react.default.createElement("th", null))), /*#__PURE__*/_react.default.createElement("tbody", null, invoices.map((invoice, index) => /*#__PURE__*/_react.default.createElement(_InvoicesListItem.default, {
    key: (0, _get2.default)(invoice, 'id'),
    number: (0, _get2.default)(invoice, 'title'),
    type: (0, _get2.default)(invoice, 'type'),
    date: (0, _get2.default)(invoice, 'created'),
    amount: (0, _get2.default)(invoice, 'amount.total_amount'),
    onDownloadClick: () => handleDownloadClick(index)
  })))));
};
var _default = exports.default = Invoices;