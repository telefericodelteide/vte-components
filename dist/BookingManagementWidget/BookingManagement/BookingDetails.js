"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _react = _interopRequireDefault(require("react"));
var _reactI18next = require("react-i18next");
var _get2 = _interopRequireDefault(require("lodash/get"));
var _Rate = _interopRequireDefault(require("./Rate"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
const BookingDetails = _ref => {
  let {
    booking
  } = _ref;
  const {
    t
  } = (0, _reactI18next.useTranslation)('bm');
  return /*#__PURE__*/_react.default.createElement("div", {
    className: "booking-details"
  }, /*#__PURE__*/_react.default.createElement("h2", {
    className: "uk-text-large"
  }, t('manage_booking.booking_details.title')), /*#__PURE__*/_react.default.createElement("p", null, /*#__PURE__*/_react.default.createElement("span", {
    className: "strong"
  }, t('manage_booking.booking_details.locator'), ":"), ' '), /*#__PURE__*/_react.default.createElement("p", null, (0, _get2.default)(booking, 'locator')), /*#__PURE__*/_react.default.createElement("p", null, /*#__PURE__*/_react.default.createElement("span", {
    className: "strong"
  }, t('manage_booking.booking_details.experience'), ":"), ' '), /*#__PURE__*/_react.default.createElement("p", null, (0, _get2.default)(booking, 'product.experience.name')), /*#__PURE__*/_react.default.createElement("p", null, /*#__PURE__*/_react.default.createElement("span", {
    className: "strong"
  }, t('manage_booking.booking_details.product'), ":"), ' '), /*#__PURE__*/_react.default.createElement("p", null, (0, _get2.default)(booking, 'product.name')), /*#__PURE__*/_react.default.createElement("p", null, /*#__PURE__*/_react.default.createElement("span", {
    className: "strong"
  }, t('manage_booking.booking_details.date'), ":"), ' '), /*#__PURE__*/_react.default.createElement("p", null, (0, _get2.default)(booking, 'booking_date')), /*#__PURE__*/_react.default.createElement("p", null, /*#__PURE__*/_react.default.createElement("span", {
    className: "strong"
  }, t('manage_booking.booking_details.rates'))), /*#__PURE__*/_react.default.createElement("div", {
    className: "ratesbm"
  }, (0, _get2.default)(booking, 'product_rates', []).map(rate => /*#__PURE__*/_react.default.createElement(_Rate.default, {
    key: rate.id,
    name: rate.rate_name,
    unitPrice: rate.pvp,
    quantity: rate.qty,
    total: rate.total_amount_pvp,
    currency: (0, _get2.default)(booking.order, 'currency')
  }))));
};
var _default = exports.default = BookingDetails;