"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _replace2 = _interopRequireDefault(require("lodash/replace"));
var _react = _interopRequireDefault(require("react"));
var _reactI18next = require("react-i18next");
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
const Rate = _ref => {
  let {
    name,
    unitPrice,
    quantity,
    total,
    currency
  } = _ref;
  const {
    t,
    i18n
  } = (0, _reactI18next.useTranslation)("bm");
  const priceFormatter = new Intl.NumberFormat(i18n.language, {
    style: "currency",
    currency: currency || "eur"
  });
  const priceFormatted = (currency, price) => {
    let formattedPrice = priceFormatter.format(price);
    if (currency && currency === 'PLN') {
      formattedPrice = formattedPrice.replace('PLN', 'zł');
    }
    return formattedPrice;
  };
  return /*#__PURE__*/_react.default.createElement("div", {
    className: "ratebm"
  }, /*#__PURE__*/_react.default.createElement("ul", {
    className: "uk-list"
  }, /*#__PURE__*/_react.default.createElement("li", null, /*#__PURE__*/_react.default.createElement("strong", null, " ", (0, _replace2.default)(t("manage_booking.booking_details.rate"), ",", ", "), ": "), " ", name), /*#__PURE__*/_react.default.createElement("li", null, /*#__PURE__*/_react.default.createElement("strong", null, t("manage_booking.booking_details.price"), ":"), " ", priceFormatted(currency, unitPrice), " - ", /*#__PURE__*/_react.default.createElement("strong", null, t("manage_booking.booking_details.quantity"), ":"), " ", quantity), /*#__PURE__*/_react.default.createElement("li", null, /*#__PURE__*/_react.default.createElement("strong", null, " ", t("manage_booking.booking_details.total"), ": "), " ", priceFormatted(currency, total))));
};
var _default = exports.default = Rate;