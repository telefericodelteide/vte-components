"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _react = _interopRequireWildcard(require("react"));
var _reactI18next = require("react-i18next");
var _AvailabilityCalendar = _interopRequireDefault(require("../../AvailabilityCalendar/AvailabilityCalendar"));
var _SessionList = _interopRequireDefault(require("../../SessionList/SessionList"));
var _Button = _interopRequireDefault(require("../Button/Button"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function (e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != typeof e && "function" != typeof e) return { default: e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && {}.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n.default = e, t && t.set(e, n), n; }
function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
function _objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
function _defineProperty(e, r, t) { return (r = _toPropertyKey(r)) in e ? Object.defineProperty(e, r, { value: t, enumerable: !0, configurable: !0, writable: !0 }) : e[r] = t, e; }
function _toPropertyKey(t) { var i = _toPrimitive(t, "string"); return "symbol" == typeof i ? i : i + ""; }
function _toPrimitive(t, r) { if ("object" != typeof t || !t) return t; var e = t[Symbol.toPrimitive]; if (void 0 !== e) { var i = e.call(t, r || "default"); if ("object" != typeof i) return i; throw new TypeError("@@toPrimitive must return a primitive value."); } return ("string" === r ? String : Number)(t); }
const DateChange = _ref => {
  let {
    bookingId,
    productId,
    quantity,
    availabilityFetcher,
    onConfirm
  } = _ref;
  const {
    t
  } = (0, _reactI18next.useTranslation)('bm');
  const BASE_SELECTION = {
    productId: productId,
    date: null,
    session: null
  };
  const initSelection = selection => {
    let result = _objectSpread(_objectSpread({}, BASE_SELECTION), selection);
    if (result.productId) {
      result.productId = parseInt(result.productId);
    }
    return result;
  };
  const [selection, setSelection] = (0, _react.useState)(initSelection());
  const [sessionList, setSessionList] = (0, _react.useState)([]);
  const onSessionSelectionHandler = session => {
    setSelection(_objectSpread(_objectSpread({}, BASE_SELECTION), {}, {
      date: selection.date,
      session: session
    }));
  };
  const onDateSelectionHandler = date => {
    if (date.sessions.length === 1 && date.sessions[0].session === 'day_wide') {
      setSelection(_objectSpread(_objectSpread({}, BASE_SELECTION), {}, {
        date: date.date,
        session: 'day_wide'
      }));
    } else {
      setSelection(_objectSpread(_objectSpread({}, BASE_SELECTION), {}, {
        date: date.date
      }));
    }
    setSessionList(date.sessions);
  };
  const availabilityFetcherWrapper = date => {
    if (!date) {
      date = new Date();
    }
    return availabilityFetcher(bookingId, productId, quantity, date);
  };
  const onFormSubmit = event => {
    if (selection.date && selection.session) {
      onConfirm(selection);
    }
    event.preventDefault();
  };
  return /*#__PURE__*/_react.default.createElement("div", {
    id: "date-change",
    className: "date-change uk-width-1-1 uk-width-2-3@m"
  }, /*#__PURE__*/_react.default.createElement("h2", {
    className: "uk-text-large"
  }, t('manage_booking.booking_actions.date_change.title')), /*#__PURE__*/_react.default.createElement("p", null, t('manage_booking.booking_actions.date_change.body')), /*#__PURE__*/_react.default.createElement(_AvailabilityCalendar.default, {
    onSelection: onDateSelectionHandler,
    availabilityFetcher: availabilityFetcherWrapper
  }), sessionList && sessionList.length > 0 && /*#__PURE__*/_react.default.createElement("div", null, /*#__PURE__*/_react.default.createElement("p", null, t('manage_booking.booking_actions.date_change.body_sessions')), /*#__PURE__*/_react.default.createElement(_SessionList.default, {
    sessions: sessionList,
    selected: selection.session,
    onSelection: onSessionSelectionHandler,
    hiddenAvailable: true
  })), /*#__PURE__*/_react.default.createElement("form", {
    onSubmit: e => onFormSubmit(e)
  }, selection && selection.session && /*#__PURE__*/_react.default.createElement(_Button.default, {
    text: t('manage_booking.booking_actions.date_change.confirm_button')
  })));
};
var _default = exports.default = DateChange;