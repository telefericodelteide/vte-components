"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _react = _interopRequireWildcard(require("react"));
var _reactI18next = require("react-i18next");
var _Button = _interopRequireDefault(require("../Button/Button"));
var _formik = require("formik");
var _isEmpty2 = _interopRequireDefault(require("lodash/isEmpty"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function (e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != typeof e && "function" != typeof e) return { default: e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && {}.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n.default = e, t && t.set(e, n), n; }
const CancelRequestForm = _ref => {
  let {
    confirmationConditionsMessage,
    onConfirm,
    onCancelClick
  } = _ref;
  const {
    t
  } = (0, _reactI18next.useTranslation)("bm");
  const [error, setError] = (0, _react.useState)("");
  const initialValues = {
    termsAccepted: false
  };
  const handleFormSubmit = (values, actions) => {
    if (values.termsAccepted) {
      onConfirm();
    } else {
      setError(t("manage_booking.booking_actions.cancel_request.confirm_terms_required"));
    }
  };
  return /*#__PURE__*/_react.default.createElement("div", {
    id: "cancel-request-form",
    className: "cancel-request-form uk-width-1-1 uk-width-2-3@m"
  }, /*#__PURE__*/_react.default.createElement("h2", {
    className: "uk-text-large"
  }, t("manage_booking.booking_actions.cancel_request.title")), /*#__PURE__*/_react.default.createElement("p", null, confirmationConditionsMessage), /*#__PURE__*/_react.default.createElement(_formik.Formik, {
    initialValues: initialValues,
    onSubmit: handleFormSubmit
  }, /*#__PURE__*/_react.default.createElement(_formik.Form, {
    className: "form-wrapper uk-form-stacked"
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: "form-field-wrapper"
  }, /*#__PURE__*/_react.default.createElement(_formik.Field, {
    type: "checkbox",
    id: "termsAccepted",
    name: "termsAccepted",
    className: "uk-checkbox"
  }), /*#__PURE__*/_react.default.createElement("label", {
    htmlFor: "termsAccepted",
    className: "form-field-condition-label"
  }, t("manage_booking.booking_actions.cancel_request.confirm_terms")), !(0, _isEmpty2.default)(error) ? /*#__PURE__*/_react.default.createElement("div", {
    className: "form-field-error uk-text-danger"
  }, error) : ""), /*#__PURE__*/_react.default.createElement("div", {
    className: "booking-actions"
  }, /*#__PURE__*/_react.default.createElement(_Button.default, {
    text: t("manage_booking.booking_actions.cancel_request.confirm_button")
  }), /*#__PURE__*/_react.default.createElement(_Button.default, {
    text: t('manage_booking.booking_actions.cancel_request.cancel_button'),
    onClick: () => onCancelClick()
  })))));
};
var _default = exports.default = CancelRequestForm;