"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _react = _interopRequireWildcard(require("react"));
require("./booking-management.css");
var _reactI18next = require("react-i18next");
var _get2 = _interopRequireDefault(require("lodash/get"));
var _isEmpty2 = _interopRequireDefault(require("lodash/isEmpty"));
var _BookingDetails = _interopRequireDefault(require("./BookingDetails"));
var _BookingActions = _interopRequireDefault(require("./BookingActions"));
var _booking = require("../context/booking.context");
var _constants = _interopRequireDefault(require("../constants"));
var _utils = _interopRequireDefault(require("../../lib/utils"));
var _CancelRequestForm = _interopRequireDefault(require("./CancelRequestForm"));
var _DateChange = _interopRequireDefault(require("./DateChange"));
var _Invoices = _interopRequireDefault(require("./Invoices/Invoices"));
var _InvoiceRequestForm = _interopRequireDefault(require("./Invoices/InvoiceRequestForm"));
var _Loading = _interopRequireDefault(require("../../Generic/Loading"));
var _dateFns = require("date-fns");
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function (e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != typeof e && "function" != typeof e) return { default: e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && {}.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n.default = e, t && t.set(e, n), n; }
// View steps
const STEPS = {
  STEP_INITIAL: 0,
  STEP_CANCEL_REQUEST_CLICKED: 1,
  STEP_CANCEL_REQUEST_CONFIRMED: 2,
  STEP_DATE_CHANGE_REQUEST_CLICKED: 3,
  STEP_INVOICES_CLICKED: 4,
  STEP_INVOICE_REQUEST_CLICKED: 5
};
//

const allowSomeMainAction = availableActions => {
  if ((0, _isEmpty2.default)(availableActions)) {
    return false;
  }
  return availableActions.change_date || availableActions.cancel || availableActions.cancel_refund;
};
const BookingManagement = _ref => {
  let {
    onLogout,
    availabilityFetcher,
    loading
  } = _ref;
  const bookingContext = (0, _booking.useBooking)();
  const {
    t
  } = (0, _reactI18next.useTranslation)('bm');
  const [errorMsg, setErrorMsg] = (0, _react.useState)('');
  const [successMsg, setSuccessMsg] = (0, _react.useState)('');
  const [actions, setActions] = (0, _react.useState)({});
  const [currentStep, setCurrentStep] = (0, _react.useState)(STEPS.STEP_INITIAL);
  const handleActionClick = (0, _react.useCallback)(async action => {
    switch (action.type) {
      case _constants.default.BOOKING_ACTION_CANCEL_REQUEST:
        if (action.action === "confirm") {
          bookingContext.cancellationRequest().then(result => {
            if (result.success) {
              if ((0, _get2.default)(bookingContext.booking, 'state') === 'cancelled') {
                setSuccessMsg(t('manage_booking.booking_actions.cancel_request.cancelled'));
              } else {
                setSuccessMsg(t('manage_booking.booking_actions.cancel_request.refund_requested'));
              }
              setCurrentStep(STEPS.STEP_CANCEL_REQUEST_CONFIRMED);
            }
          });
        } else if (action.action === "cancel") {
          setCurrentStep(STEPS.STEP_INITIAL);
        } else {
          setCurrentStep(STEPS.STEP_CANCEL_REQUEST_CLICKED);
          setSuccessMsg('');
        }
        break;
      case _constants.default.BOOKING_ACTION_CANCEL_REFUND:
        bookingContext.cancelRefund().then(result => {
          if (result.success) {
            setSuccessMsg(t('manage_booking.booking_actions.cancel_request.refund_cancelled'));
            setCurrentStep(STEPS.STEP_INITIAL);
          }
        });
        break;
      case _constants.default.BOOKING_ACTION_CHANGE_DATE:
        if (currentStep === STEPS.STEP_DATE_CHANGE_REQUEST_CLICKED) {
          const date = action.selection.date + 'T' + (action.selection.session === 'day_wide' ? '00:00:00' : action.selection.session);
          bookingContext.dateChangeRequest((0, _dateFns.parseISO)(date)).then(result => {
            if (result.success) {
              setSuccessMsg(t('manage_booking.booking_actions.date_change.success'));
              setCurrentStep(STEPS.STEP_INITIAL);
            } else {
              setSuccessMsg(t('manage_booking.booking_actions.date_change.defeat'));
            }
          });
        } else {
          setCurrentStep(STEPS.STEP_DATE_CHANGE_REQUEST_CLICKED);
          setSuccessMsg('');
        }
        break;
      case _constants.default.BOOKING_ACTION_VOUCHER:
        bookingContext.getVoucherPdf().then(result => {
          if (result.success) {
            _utils.default.downloadFile(result.voucher.blob, result.voucher.fileName);
          } else {
            setSuccessMsg(t('manage_booking.booking_actions.voucher_download.defeat'));
          }
        });
        break;
      case _constants.default.BOOKING_ACTION_INVOICES:
        if (action.action === "download") {
          bookingContext.getInvoicePdf(action.invoiceId, action.number).then(result => {
            if (result.success) {
              _utils.default.downloadFile(result.invoice.blob, result.invoice.fileName);
            } else {
              setSuccessMsg(t('manage_booking.booking_actions.voucher_download.defeat'));
            }
          });
          if (currentStep === STEPS.STEP_INVOICE_REQUEST_CLICKED) {
            setCurrentStep(STEPS.STEP_INITIAL);
          }
        } else {
          setCurrentStep(STEPS.STEP_INVOICES_CLICKED);
          setSuccessMsg('');
        }
        break;
      case _constants.default.BOOKING_ACTION_INVOICE_REQUEST:
        if (action.action === "submit") {
          return bookingContext.requestInvoice((0, _get2.default)(action.data, 'customer')).then(result => {
            if (result.success) {
              setSuccessMsg(t('manage_booking.booking_actions.invoices.request_form.success'));
              handleActionClick({
                type: _constants.default.BOOKING_ACTION_INVOICES,
                action: 'download',
                invoiceId: (0, _get2.default)(result.invoice, 'id'),
                number: (0, _get2.default)(result.invoice, 'title')
              });
            } else {
              //action.data
              //setCurrentStep(STEPS.STEP_INVOICE_REQUEST_CLICKED)
              setCurrentStep(STEPS.STEP_INITIAL);
            }
            return result.success;
          });
        } else if (action.action === "cancel") {
          setCurrentStep(STEPS.STEP_INITIAL);
        } else {
          setCurrentStep(STEPS.STEP_INVOICE_REQUEST_CLICKED);
          setSuccessMsg('');
        }
        break;
      case _constants.default.BOOKING_ACTION_LOGOUT:
        onLogout();
        bookingContext.clearContext();
        break;
      default:
        alert('onActionClick: ' + action.type);
        break;
    }
  }, [bookingContext, currentStep, t, onLogout]);
  const processActions = (0, _react.useCallback)((availableActions, hasRefund, invoices) => {
    const actions = {};
    if (!(0, _isEmpty2.default)(availableActions)) {
      if (allowSomeMainAction(availableActions) || hasRefund) {
        actions[_constants.default.BOOKING_ACTION_CHANGE_DATE] = {
          type: _constants.default.BOOKING_ACTION_CHANGE_DATE,
          visible: currentStep !== STEPS.STEP_DATE_CHANGE_REQUEST_CLICKED,
          active: availableActions.change_date,
          text: t('manage_booking.booking_actions.date_change.button')
        };
        if (availableActions.cancel_refund) {
          actions[_constants.default.BOOKING_ACTION_CANCEL_REFUND] = {
            type: _constants.default.BOOKING_ACTION_CANCEL_REFUND,
            visible: true,
            active: true,
            hideOnClick: true,
            text: t('manage_booking.booking_actions.cancel_request.refund_cancel_button')
          };
        } else {
          actions[_constants.default.BOOKING_ACTION_CANCEL_REQUEST] = {
            type: _constants.default.BOOKING_ACTION_CANCEL_REQUEST,
            visible: (allowSomeMainAction(availableActions) || hasRefund) && currentStep !== STEPS.STEP_CANCEL_REQUEST_CLICKED,
            active: availableActions.cancel,
            text: availableActions.cancel ? t('manage_booking.booking_actions.cancel_request.button') : t('manage_booking.booking_actions.cancel_request.in_process_button')
          };
        }
        actions[_constants.default.BOOKING_ACTION_VOUCHER] = {
          type: _constants.default.BOOKING_ACTION_VOUCHER,
          visible: true,
          active: true,
          text: t('manage_booking.booking_actions.voucher_download.button')
        };
      }
      if ((0, _isEmpty2.default)(invoices) && availableActions.request_invoice) {
        actions[_constants.default.BOOKING_ACTION_INVOICE_REQUEST] = {
          type: _constants.default.BOOKING_ACTION_INVOICE_REQUEST,
          visible: currentStep !== STEPS.STEP_INVOICE_REQUEST_CLICKED,
          active: true,
          text: t('manage_booking.booking_actions.invoices.request_button')
        };
      }
    }
    if (!(0, _isEmpty2.default)(invoices)) {
      actions[_constants.default.BOOKING_ACTION_INVOICES] = {
        type: _constants.default.BOOKING_ACTION_INVOICES,
        visible: currentStep !== STEPS.STEP_INVOICES_CLICKED,
        active: true,
        text: t('manage_booking.booking_actions.invoices.button')
      };
    }
    actions[_constants.default.BOOKING_ACTION_LOGOUT] = {
      type: _constants.default.BOOKING_ACTION_LOGOUT,
      visible: true,
      active: true,
      text: t('manage_booking.logout.button')
    };
    return actions;
  }, [t, currentStep]);
  const renderView = (0, _react.useCallback)(booking => {
    const loadedBooking = booking && booking !== undefined;
    switch (currentStep) {
      case STEPS.STEP_CANCEL_REQUEST_CLICKED:
        const cancellationRequestAction = (0, _get2.default)(booking._links, _constants.default.BOOKING_ACTION_CANCEL_REQUEST);
        return /*#__PURE__*/_react.default.createElement(_CancelRequestForm.default, {
          confirmationConditionsMessage: (0, _get2.default)(cancellationRequestAction, 'data.confirmation_conditions_message'),
          onConfirm: () => handleActionClick({
            type: _constants.default.BOOKING_ACTION_CANCEL_REQUEST,
            action: "confirm"
          }),
          onCancelClick: () => handleActionClick({
            type: _constants.default.BOOKING_ACTION_CANCEL_REQUEST,
            action: "cancel"
          })
        });
      case STEPS.STEP_DATE_CHANGE_REQUEST_CLICKED:
        return /*#__PURE__*/_react.default.createElement(_DateChange.default, {
          bookingId: (0, _get2.default)(booking, 'id'),
          quantity: loadedBooking ? booking.getTotalQuantity() : 0,
          productId: (0, _get2.default)(booking, 'product.id'),
          availabilityFetcher: availabilityFetcher,
          onConfirm: selection => handleActionClick({
            type: _constants.default.BOOKING_ACTION_CHANGE_DATE,
            selection: selection
          })
        });
      case STEPS.STEP_INVOICES_CLICKED:
        return /*#__PURE__*/_react.default.createElement(_Invoices.default, {
          invoices: (0, _get2.default)(bookingContext.booking, 'invoices'),
          onDownloadClick: handleActionClick,
          downloadParams: {
            type: _constants.default.BOOKING_ACTION_INVOICES,
            action: "download"
          }
        });
      case STEPS.STEP_INVOICE_REQUEST_CLICKED:
        return /*#__PURE__*/_react.default.createElement(_InvoiceRequestForm.default, {
          onSubmitForm: handleActionClick,
          submitParams: {
            type: _constants.default.BOOKING_ACTION_INVOICE_REQUEST,
            action: "submit"
          },
          onCancelClick: () => handleActionClick({
            type: _constants.default.BOOKING_ACTION_INVOICE_REQUEST,
            action: "cancel"
          })
        });
      default:
        return;
    }
  }, [currentStep, handleActionClick, bookingContext.booking]);
  const processRenderMessages = (successMsg, errorMsg) => {
    if (successMsg) {
      return /*#__PURE__*/_react.default.createElement("div", {
        className: "success uk-text-success"
      }, successMsg);
    }
    if (errorMsg) {
      return /*#__PURE__*/_react.default.createElement("div", {
        className: "error uk-text-danger"
      }, errorMsg);
    }
  };
  (0, _react.useEffect)(() => {
    if (bookingContext.booking && bookingContext.availableActions) {
      const hasRefund = (0, _get2.default)(bookingContext.booking, 'state') === 'refund_requested';
      setActions(processActions(bookingContext.availableActions, hasRefund, (0, _get2.default)(bookingContext.booking, 'invoices')));
      if (currentStep !== STEPS.STEP_CANCEL_REQUEST_CONFIRMED && !(allowSomeMainAction(bookingContext.availableActions) || hasRefund)) {
        setErrorMsg(t('manage_booking.booking_exceptional_states.' + (0, _get2.default)(bookingContext.booking, 'product.contact_msg')));
      } else {
        setErrorMsg('');
      }
    }
  }, [bookingContext.booking, bookingContext.availableActions, currentStep, processActions]);
  const isLoading = loading || bookingContext.loading;
  return /*#__PURE__*/_react.default.createElement("div", {
    id: "booking-management",
    className: "booking-management uk-container-center"
  }, isLoading && /*#__PURE__*/_react.default.createElement(_Loading.default, null), /*#__PURE__*/_react.default.createElement("div", {
    className: "uk-grid"
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: [STEPS.STEP_INITIAL, STEPS.STEP_CANCEL_REQUEST_CONFIRMED].includes(currentStep) ? 'uk-width-1-1' : 'uk-width-1-1 uk-width-1-3@m uk-first-column'
  }, /*#__PURE__*/_react.default.createElement(_BookingDetails.default, {
    booking: bookingContext.booking
  }), processRenderMessages(successMsg, (0, _isEmpty2.default)(bookingContext.error) ? errorMsg : bookingContext.error), /*#__PURE__*/_react.default.createElement(_BookingActions.default, {
    actions: actions,
    onActionClick: handleActionClick
  })), renderView(bookingContext.booking)));
};
var _default = exports.default = BookingManagement;