"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _react = _interopRequireDefault(require("react"));
var _Button = _interopRequireDefault(require("../Button/Button"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
const BookingActions = _ref => {
  let {
    actions,
    onActionClick
  } = _ref;
  return /*#__PURE__*/_react.default.createElement("div", {
    className: "booking-actions"
  }, Object.keys(actions).map(key => actions[key].visible && /*#__PURE__*/_react.default.createElement(_Button.default, {
    key: actions[key].type,
    disabled: !actions[key].active,
    hideOnClick: actions[key].hideOnClick,
    text: actions[key].text,
    onClick: () => onActionClick(actions[key])
  })));
};
var _default = exports.default = BookingActions;