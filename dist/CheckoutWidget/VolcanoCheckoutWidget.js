"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = VolcanoCheckoutWidget;
var _react = _interopRequireDefault(require("react"));
var _reactI18next = require("react-i18next");
var _volcanoteideApiClient = _interopRequireDefault(require("@volcanoteide/volcanoteide-api-client"));
var _CheckoutWidget = _interopRequireDefault(require("./CheckoutWidget"));
var _i18n = _interopRequireDefault(require("./config/i18n"));
var _i18next = _interopRequireDefault(require("i18next"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
function _objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
function _defineProperty(e, r, t) { return (r = _toPropertyKey(r)) in e ? Object.defineProperty(e, r, { value: t, enumerable: !0, configurable: !0, writable: !0 }) : e[r] = t, e; }
function _toPropertyKey(t) { var i = _toPrimitive(t, "string"); return "symbol" == typeof i ? i : i + ""; }
function _toPrimitive(t, r) { if ("object" != typeof t || !t) return t; var e = t[Symbol.toPrimitive]; if (void 0 !== e) { var i = e.call(t, r || "default"); if ("object" != typeof i) return i; throw new TypeError("@@toPrimitive must return a primitive value."); } return ("string" === r ? String : Number)(t); }
function VolcanoCheckoutWidget(_ref) {
  let {
    locale,
    apiConfig,
    conditionsConfig,
    onConfirm,
    onEmptyCart,
    allowInvoice,
    trackingCallbacks
  } = _ref;
  // init api client
  const client = new _volcanoteideApiClient.default(_objectSpread(_objectSpread({}, apiConfig), {}, {
    locale: locale
  }));
  _i18next.default.use(_reactI18next.initReactI18next).init({
    interpolation: {
      escapeValue: false
    },
    lng: locale,
    resources: _i18n.default
  });
  return /*#__PURE__*/_react.default.createElement(_reactI18next.I18nextProvider, {
    i18n: _i18next.default
  }, /*#__PURE__*/_react.default.createElement(_CheckoutWidget.default, {
    clientApi: client,
    conditionsConfig: conditionsConfig,
    onConfirm: onConfirm,
    onEmptyCart: onEmptyCart,
    allowInvoice: allowInvoice,
    trackingCallbacks: trackingCallbacks
  }));
}