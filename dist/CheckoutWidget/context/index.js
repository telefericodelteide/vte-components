"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _react = _interopRequireDefault(require("react"));
var _get2 = _interopRequireDefault(require("lodash/get"));
var _classNames = _interopRequireDefault(require("../config/classNames"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function cn(key) {
  return (0, _get2.default)(_classNames.default, key);
}
const ClassNamesContext = /*#__PURE__*/_react.default.createContext({
  cn: cn
});
var _default = exports.default = ClassNamesContext;