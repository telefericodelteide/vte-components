'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _react = _interopRequireWildcard(require("react"));
var _isEmpty2 = _interopRequireDefault(require("lodash/isEmpty"));
var _isObject2 = _interopRequireDefault(require("lodash/isObject"));
var _reduce2 = _interopRequireDefault(require("lodash/reduce"));
var _context = _interopRequireDefault(require("../../context"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function (e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != typeof e && "function" != typeof e) return { default: e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && {}.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n.default = e, t && t.set(e, n), n; }
function Alert(props) {
  const {
    cn
  } = (0, _react.useContext)(_context.default);
  function renderErrors() {
    let errors = [];
    if ((0, _isObject2.default)(props.errors)) {
      errors = (0, _reduce2.default)(props.errors, (result, value, key) => {
        if (!(0, _isEmpty2.default)(value)) {
          result.push(/*#__PURE__*/_react.default.createElement("p", {
            key: key
          }, value));
        }
        return result;
      }, []);
    }
    if (!(0, _isEmpty2.default)(errors)) {
      return errors;
    }
    return null;
  }
  const errors = renderErrors();
  if (errors) {
    return /*#__PURE__*/_react.default.createElement("div", {
      className: cn('error.wrapper')
    }, /*#__PURE__*/_react.default.createElement("div", {
      key: "alert",
      className: cn('error.content'),
      role: "alert"
    }, errors));
  }
  return null;
}
var _default = exports.default = Alert;