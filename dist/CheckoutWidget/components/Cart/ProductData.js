"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _react = _interopRequireWildcard(require("react"));
var _propTypes = _interopRequireDefault(require("prop-types"));
var _reactI18next = require("react-i18next");
var _get2 = _interopRequireDefault(require("lodash/get"));
var _dateFns = require("date-fns");
var _context = _interopRequireDefault(require("../../context"));
var _ProductRateRows = _interopRequireDefault(require("./ProductRateRows"));
var _ProductTotal = _interopRequireDefault(require("./ProductTotal"));
var _ProductDeleteButton = _interopRequireDefault(require("./ProductDeleteButton"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function (e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != typeof e && "function" != typeof e) return { default: e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && {}.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n.default = e, t && t.set(e, n), n; }
function ProductData(props) {
  const {
    t
  } = (0, _reactI18next.useTranslation)("main");
  const {
    cn
  } = (0, _react.useContext)(_context.default);
  function onDelete(e) {
    e.preventDefault();
    let id = (0, _get2.default)(props.item, "id");
    props.onDelete(id);
  }
  function renderHeader() {
    let title = (0, _get2.default)(props.item, "product.name");
    let date = (0, _dateFns.parseISO)((0, _get2.default)(props.item, "booking_date"));
    let experienceTitle = (0, _get2.default)(props.item, "product.experience.name");
    let available = (0, _get2.default)(props.item, "available");
    return /*#__PURE__*/_react.default.createElement("div", {
      className: cn("cart.product.header.section"),
      "uk-grid": "uk-grid"
    }, !available ? /*#__PURE__*/_react.default.createElement("div", {
      className: cn("cart.product.no_available")
    }, /*#__PURE__*/_react.default.createElement("span", null, t("cart.no_available"))) : null, /*#__PURE__*/_react.default.createElement("div", {
      className: "uk-vertical-align " + cn("cart.product.header.titleRow")
    }, /*#__PURE__*/_react.default.createElement(_ProductDeleteButton.default, {
      onDelete: onDelete
    }), /*#__PURE__*/_react.default.createElement("span", null, experienceTitle)), /*#__PURE__*/_react.default.createElement("div", {
      className: cn("cart.product.header.metadataRow")
    }, /*#__PURE__*/_react.default.createElement("div", {
      "uk-grid": "uk-grid"
    }, /*#__PURE__*/_react.default.createElement("div", {
      className: "uk-width-1-1 uk-width-auto@m"
    }, /*#__PURE__*/_react.default.createElement("span", {
      className: cn("cart.product.header.metadataTitle")
    }, title, " "), /*#__PURE__*/_react.default.createElement("i", {
      className: "uk-visible@m fas fa-long-arrow-alt-right"
    })), /*#__PURE__*/_react.default.createElement("div", {
      className: "uk-width-expand uk-margin-remove " + cn("cart.product.header.metadataDate")
    }, /*#__PURE__*/_react.default.createElement("span", {
      className: cn("cart.product.header.metadataDateLabel")
    }, t("generic.date"), ": "), /*#__PURE__*/_react.default.createElement("span", {
      className: cn("cart.product.header.metadataDateContent")
    }, (0, _dateFns.format)(date, "dd/MM/yy")), (0, _dateFns.format)(date, "HH:mm") !== "00:00" && /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, /*#__PURE__*/_react.default.createElement("span", {
      className: cn("cart.product.header.metadataDateLabel")
    }, t("generic.hour"), ": "), /*#__PURE__*/_react.default.createElement("span", {
      className: cn("cart.product.header.metadataDateContent")
    }, (0, _dateFns.format)(date, "HH:mm")))))));
  }
  if (props.item) {
    return /*#__PURE__*/_react.default.createElement("div", {
      className: "uk-width-1-1 " + cn("cart.product.section")
    }, renderHeader(), /*#__PURE__*/_react.default.createElement(_ProductRateRows.default, {
      item: props.item
    }), /*#__PURE__*/_react.default.createElement(_ProductTotal.default, {
      item: props.item,
      discounts: props.discounts
    }));
  }
  return null;
}
ProductData.propTypes = {
  item: _propTypes.default.object.isRequired,
  onDelete: _propTypes.default.func.isRequired
};
var _default = exports.default = ProductData;