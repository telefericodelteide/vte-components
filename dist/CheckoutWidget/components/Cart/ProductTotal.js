'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _react = _interopRequireWildcard(require("react"));
var _reactI18next = require("react-i18next");
var _get2 = _interopRequireDefault(require("lodash/get"));
var _context = _interopRequireDefault(require("../../context"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function (e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != typeof e && "function" != typeof e) return { default: e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && {}.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n.default = e, t && t.set(e, n), n; }
function ProductTotal(props) {
  const {
    t,
    i18n
  } = (0, _reactI18next.useTranslation)('main');
  const {
    cn
  } = (0, _react.useContext)(_context.default);
  const priceFormatter = new Intl.NumberFormat(i18n.language, {
    style: "currency",
    currency: "eur"
  });
  const totalAmount = (0, _get2.default)(props.item, 'total_amount');
  const roundTotalAmount = priceFormatter.format(totalAmount ? parseFloat(totalAmount) : 0);
  return /*#__PURE__*/_react.default.createElement("div", {
    className: cn('cart.product.subtotal.row')
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: cn('cart.product.subtotal.wrapper')
  }, /*#__PURE__*/_react.default.createElement("span", {
    className: cn('cart.product.subtotal.label')
  }, t('cart.productTotal'), ": "), /*#__PURE__*/_react.default.createElement("span", {
    className: cn('cart.product.subtotal.content')
  }, roundTotalAmount)));
}
var _default = exports.default = ProductTotal;