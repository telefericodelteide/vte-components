'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _react = _interopRequireWildcard(require("react"));
var _propTypes = _interopRequireDefault(require("prop-types"));
var _get2 = _interopRequireDefault(require("lodash/get"));
var _round2 = _interopRequireDefault(require("lodash/round"));
var _reactI18next = require("react-i18next");
var _context = _interopRequireDefault(require("../../context"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function (e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != typeof e && "function" != typeof e) return { default: e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && {}.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n.default = e, t && t.set(e, n), n; }
function ProductRateRow(props) {
  const {
    t,
    i18n
  } = (0, _reactI18next.useTranslation)('main');
  const {
    cn
  } = (0, _react.useContext)(_context.default);
  if (props.rate) {
    const priceFormatter = new Intl.NumberFormat(i18n.language, {
      style: "currency",
      currency: "eur"
    });
    const name = props.rate.rate.rate_elements.sort((ct1, ct2) => ct1.type - ct2.type).map(ct => ct.name).join(", ");
    const qty = (0, _get2.default)(props.rate, 'qty');
    const unitPrice = (0, _get2.default)(props.rate, 'rate.unit_price');
    const totalAmount = (0, _get2.default)(props.rate, 'total_amount');
    const roundUnitPrice = priceFormatter.format(parseFloat(unitPrice));
    const roundTotalAmount = priceFormatter.format(parseFloat(totalAmount));
    return /*#__PURE__*/_react.default.createElement("div", {
      className: "uk-grid-divider uk-grid-column-small " + cn('cart.product.rate.row'),
      "uk-grid": "uk-grid"
    }, /*#__PURE__*/_react.default.createElement("div", {
      className: "uk-width-1-2 uk-width-1-4@m " + cn('cart.product.rate.fareType.cell')
    }, /*#__PURE__*/_react.default.createElement("span", {
      className: cn('cart.product.rate.fareType.content')
    }, name)), /*#__PURE__*/_react.default.createElement("div", {
      className: "uk-width-1-2 uk-width-1-6@m " + cn('cart.product.rate.price.cell')
    }, /*#__PURE__*/_react.default.createElement("span", {
      className: cn('cart.product.rate.price.content')
    }, roundUnitPrice)), /*#__PURE__*/_react.default.createElement("div", {
      className: "uk-width-1-1 uk-width-1-4@m " + cn('cart.product.rate.participants.cell')
    }, /*#__PURE__*/_react.default.createElement("span", {
      className: cn('cart.product.rate.participants.label')
    }, t('cart.attendees'), ": "), /*#__PURE__*/_react.default.createElement("span", {
      className: cn('cart.product.rate.participants.content')
    }, qty)), /*#__PURE__*/_react.default.createElement("div", {
      className: "uk-width-1-1 uk-width-1-6@m " + cn('cart.product.rate.subtotal.cell')
    }, /*#__PURE__*/_react.default.createElement("span", {
      className: cn('cart.product.rate.subtotal.label')
    }, t('cart.total'), ": "), /*#__PURE__*/_react.default.createElement("span", {
      className: cn('cart.product.rate.subtotal.content')
    }, roundTotalAmount)));
  }
  return null;
}
ProductRateRow.propTypes = {
  rate: _propTypes.default.object.isRequired
};
var _default = exports.default = ProductRateRow;