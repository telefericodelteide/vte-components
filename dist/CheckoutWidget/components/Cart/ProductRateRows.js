'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _react = _interopRequireWildcard(require("react"));
var _propTypes = _interopRequireDefault(require("prop-types"));
var _get2 = _interopRequireDefault(require("lodash/get"));
var _isArray2 = _interopRequireDefault(require("lodash/isArray"));
var _ProductRateRow = _interopRequireDefault(require("./ProductRateRow"));
var _context = _interopRequireDefault(require("../../context"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function (e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != typeof e && "function" != typeof e) return { default: e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && {}.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n.default = e, t && t.set(e, n), n; }
function ProductRateRows(props) {
  const {
    cn
  } = (0, _react.useContext)(_context.default);
  function renderProductRateRows() {
    const rates = (0, _get2.default)(props, 'item.rates');
    if (rates && (0, _isArray2.default)(rates)) {
      return rates.map((rate, index) => {
        return /*#__PURE__*/_react.default.createElement(_ProductRateRow.default, {
          key: "rate-".concat(index),
          rate: rate
        });
      });
    }
    return null;
  }
  return /*#__PURE__*/_react.default.createElement("div", {
    className: cn('cart.product.rate.section')
  }, renderProductRateRows());
}
ProductRateRows.propTypes = {
  rates: _propTypes.default.array
};
var _default = exports.default = ProductRateRows;