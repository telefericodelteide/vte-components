'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _react = _interopRequireWildcard(require("react"));
var _propTypes = _interopRequireDefault(require("prop-types"));
var _get2 = _interopRequireDefault(require("lodash/get"));
var _isArray2 = _interopRequireDefault(require("lodash/isArray"));
var _context = _interopRequireDefault(require("../../context"));
var _ProductData = _interopRequireDefault(require("./ProductData"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function (e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != typeof e && "function" != typeof e) return { default: e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && {}.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n.default = e, t && t.set(e, n), n; }
function Cart(props) {
  const {
    cn
  } = (0, _react.useContext)(_context.default);
  function renderProducts() {
    const items = (0, _get2.default)(props, 'content.line_items');
    const discounts = (0, _get2.default)(props, 'content.discounts');
    if (items && (0, _isArray2.default)(items)) {
      return items.map((item, index) => {
        return /*#__PURE__*/_react.default.createElement(_ProductData.default, {
          key: "key-".concat(index),
          item: item,
          discounts: discounts,
          onDelete: props.onDelete
        });
      });
    }
    return /*#__PURE__*/_react.default.createElement("span", null, "No hay productos");
  }
  return /*#__PURE__*/_react.default.createElement("div", {
    key: "cart",
    className: cn('cart.wrapper')
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: cn('cart.section'),
    "uk-grid": "uk-grid"
  }, renderProducts()));
}
Cart.propTypes = {
  content: _propTypes.default.object.isRequired
};
var _default = exports.default = Cart;