"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _react = _interopRequireWildcard(require("react"));
var _propTypes = _interopRequireDefault(require("prop-types"));
var _uikit = _interopRequireDefault(require("uikit"));
var _get2 = _interopRequireDefault(require("lodash/get"));
var _isObject2 = _interopRequireDefault(require("lodash/isObject"));
var _formik = require("formik");
var _Validator = require("../Validator");
var _reactI18next = require("react-i18next");
var _Modal = _interopRequireDefault(require("../../../../Generic/Modal"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function (e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != typeof e && "function" != typeof e) return { default: e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && {}.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n.default = e, t && t.set(e, n), n; }
function ConditionField(props) {
  const {
    t
  } = (0, _reactI18next.useTranslation)("main");
  const [checkClicked, setCheckClicked] = (0, _react.useState)(false);
  if (props.id && props.config) {
    const {
      type,
      required,
      name,
      info,
      modal
    } = props.config;
    const modalId = "modal-" + props.id.replace(/\./g, "-");
    const onCheckChange = event => {
      if ((0, _isObject2.default)(modal)) {
        if (!checkClicked) {
          _uikit.default.modal("#" + modalId, {
            container: "#checkout-widget"
          }).show();
          setCheckClicked(true);
        }
      }
    };
    return /*#__PURE__*/_react.default.createElement("div", {
      className: props.wrapperClassName
    }, /*#__PURE__*/_react.default.createElement("label", {
      htmlFor: props.id,
      className: "cw-form-field-check-label",
      onClick: onCheckChange
    }, (0, _isObject2.default)(modal) && /*#__PURE__*/_react.default.createElement(_Modal.default, {
      id: modalId,
      className: "cw-form-field-check-modal",
      header: modal.title
    }, modal.content), /*#__PURE__*/_react.default.createElement(_formik.Field, {
      type: "checkbox",
      id: props.id,
      name: props.id,
      className: props.className,
      validate: _Validator.validate.bind(this, props.config, t, props.values)
    }), props.noLabel ? null : /*#__PURE__*/_react.default.createElement("span", {
      dangerouslySetInnerHTML: {
        __html: name
      }
    }), required ? /*#__PURE__*/_react.default.createElement("i", {
      className: "required",
      "aria-required": required
    }, " ", "*") : null, info ? /*#__PURE__*/_react.default.createElement("div", {
      className: "cw-form-check-field-info",
      dangerouslySetInnerHTML: {
        __html: info
      }
    }) : null), /*#__PURE__*/_react.default.createElement(_formik.ErrorMessage, {
      name: props.id
    }, msg => /*#__PURE__*/_react.default.createElement("label", {
      className: "cw-form-field-error"
    }, msg)));
  }
  return null;
}
ConditionField.propTypes = {
  id: _propTypes.default.string.isRequired,
  config: _propTypes.default.object,
  values: _propTypes.default.object,
  wrapperClassName: _propTypes.default.string,
  fieldClassName: _propTypes.default.string,
  noLabel: _propTypes.default.bool
};
var _default = exports.default = ConditionField;