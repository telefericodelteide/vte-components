'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _react = _interopRequireWildcard(require("react"));
var _propTypes = _interopRequireDefault(require("prop-types"));
var _reactI18next = require("react-i18next");
var _context = _interopRequireDefault(require("../../../context"));
var _Field = _interopRequireDefault(require("../Fields/Field"));
var _PhoneField = _interopRequireDefault(require("../Fields/PhoneField"));
var _Textarea = _interopRequireDefault(require("../Fields/Textarea"));
var _Checkbox = _interopRequireDefault(require("../Fields/Checkbox"));
var _PromoCodeField = _interopRequireDefault(require("../PromoCodeField"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function (e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != typeof e && "function" != typeof e) return { default: e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && {}.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n.default = e, t && t.set(e, n), n; }
function Customer(props) {
  const {
    t
  } = (0, _reactI18next.useTranslation)('main');
  const {
    cn
  } = (0, _react.useContext)(_context.default);
  if (props.form) {
    const form = props.form;
    return [/*#__PURE__*/_react.default.createElement("div", {
      key: "name",
      className: cn('form.customer.names.row'),
      "uk-grid": "uk-grid"
    }, /*#__PURE__*/_react.default.createElement(_Field.default, {
      id: "client.first_name",
      config: form,
      values: props.values,
      className: cn('form.customer.names.first_name.field'),
      wrapperClassName: cn('form.customer.names.first_name.fieldWrapper')
    }), /*#__PURE__*/_react.default.createElement(_Field.default, {
      id: "client.last_name",
      config: form,
      values: props.values,
      className: cn('form.customer.names.last_name.field'),
      wrapperClassName: cn('form.customer.names.last_name.fieldWrapper')
    })), /*#__PURE__*/_react.default.createElement("div", {
      key: "emails",
      className: cn('form.customer.emails.row'),
      "uk-grid": "uk-grid"
    }, /*#__PURE__*/_react.default.createElement(_Field.default, {
      id: "client.email",
      config: form,
      values: props.values,
      className: cn('form.customer.emails.email.field'),
      wrapperClassName: cn('form.customer.emails.email.fieldWrapper')
    }), /*#__PURE__*/_react.default.createElement(_Field.default, {
      id: "client.confirm_email",
      config: form,
      values: props.values,
      className: cn('form.customer.emails.confirm_email.field'),
      wrapperClassName: cn('form.customer.emails.confirm_email.fieldWrapper')
    })), /*#__PURE__*/_react.default.createElement("div", {
      key: "phones",
      className: cn('form.customer.phones.row'),
      "uk-grid": "uk-grid"
    }, /*#__PURE__*/_react.default.createElement(_PhoneField.default, {
      id: "client.phone",
      config: form,
      values: props.values,
      className: cn('form.customer.phones.phone.field'),
      wrapperClassName: cn('form.customer.phones.phone.fieldWrapper')
    }), /*#__PURE__*/_react.default.createElement(_PhoneField.default, {
      id: "client.phone_other",
      config: form,
      values: props.values,
      className: cn('form.customer.phones.phone_other.field'),
      wrapperClassName: cn('form.customer.phones.phone_other.fieldWrapper')
    })), /*#__PURE__*/_react.default.createElement("div", {
      key: "promo-observations",
      className: cn('form.customer.observations.row'),
      "uk-grid": "uk-grid"
    }, /*#__PURE__*/_react.default.createElement("div", {
      className: cn('form.customer.observations.promo.fieldWrapper')
    }, /*#__PURE__*/_react.default.createElement(_PromoCodeField.default, {
      errors: props.errors,
      promoCode: props.promoCode,
      onChange: props.onChangePromoCode,
      onApplyPromoCode: props.onApplyPromoCode
    })), /*#__PURE__*/_react.default.createElement(_Textarea.default, {
      id: "client.observations",
      config: form,
      values: props.values,
      className: cn('form.customer.observations.observations.field'),
      wrapperClassName: cn('form.customer.observations.observations.fieldWrapper')
    }))];
  }
  return null;
}
Customer.propTypes = {
  form: _propTypes.default.object.isRequired,
  values: _propTypes.default.object.isRequired,
  promoCode: _propTypes.default.string,
  errors: _propTypes.default.object,
  onApplyPromoCode: _propTypes.default.func.isRequired,
  onChangePromoCode: _propTypes.default.func.isRequired
};
var _default = exports.default = Customer;