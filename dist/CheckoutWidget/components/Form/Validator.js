'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.validate = validate;
var _get2 = _interopRequireDefault(require("lodash/get"));
var _isEmpty2 = _interopRequireDefault(require("lodash/isEmpty"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function validate(config, t, values, value) {
  const {
    type,
    required,
    equalTo
  } = config;
  if (required) {
    if (!value) {
      return t('form.validations.required');
    }
  }
  if (type === 'email') {
    if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.\w+$/i.test(value)) {
      return t('form.validations.email');
    }
    if (equalTo) {
      let valueOtherField = (0, _get2.default)(values, equalTo);
      if (valueOtherField !== value) {
        return t('form.validations.equalTo');
      }
    }
  }
  if (type === 'phone') {
    if (value === 'invalid_phone_number') {
      return t('form.validations.phone');
    }
  }
  if (type === "nif") {
    const error = haveErrorPersonalNumber(value, t);
    if (error) {
      return error;
    }
  }
  if (type === "nif-child") {
    const error = haveErrorPersonalNumberChild(value, t);
    if (error) {
      return error;
    }
  }
  return null;
}
const haveErrorPersonalNumber = (value, t) => {
  let error = false;
  if ((0, _isEmpty2.default)(value)) {
    return t('form.client.personal_identification_required');
  }
  value = value.replace(/ /g, "");
  if (value.length < 6) {
    error = t('form.client.personal_identification_min');
  }
  return error;
};
const haveErrorPersonalNumberChild = (value, t) => {
  let error = false;
  if ((0, _isEmpty2.default)(value)) {
    return error;
  }
  value = value.replace(/ /g, "");
  if (value.length < 6) {
    error = t('form.client.personal_identification_min');
  }
  return error;
};