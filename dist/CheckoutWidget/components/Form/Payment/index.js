'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _react = _interopRequireWildcard(require("react"));
var _propTypes = _interopRequireDefault(require("prop-types"));
var _isArray2 = _interopRequireDefault(require("lodash/isArray"));
var _reactI18next = require("react-i18next");
var _formik = require("formik");
var _paymentGatewayImages = _interopRequireDefault(require("../../../config/paymentGatewayImages"));
var _context = _interopRequireDefault(require("../../../context"));
var _Validator = require("../Validator");
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function (e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != typeof e && "function" != typeof e) return { default: e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && {}.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n.default = e, t && t.set(e, n), n; }
function Payment(props) {
  const {
    t
  } = (0, _reactI18next.useTranslation)('main');
  const {
    cn
  } = (0, _react.useContext)(_context.default);
  function renderRadioButtons(id, paymentGateways) {
    if ((0, _isArray2.default)(paymentGateways)) {
      return paymentGateways.map(paymentGateway => {
        return /*#__PURE__*/_react.default.createElement("div", {
          key: paymentGateway.payment_method,
          className: cn('form.payment.methodWrapper')
        }, /*#__PURE__*/_react.default.createElement("label", null, /*#__PURE__*/_react.default.createElement("div", null, /*#__PURE__*/_react.default.createElement(_formik.Field, {
          type: "radio",
          name: id,
          value: paymentGateway.id,
          className: "uk-radio",
          validate: _Validator.validate.bind(this, {
            required: true
          }, t, [])
        }), /*#__PURE__*/_react.default.createElement("img", {
          src: _paymentGatewayImages.default[paymentGateway.payment_method],
          title: t(paymentGateway.payment_method),
          alt: t(paymentGateway.payment_method)
        }))));
      });
    }
    return null;
  }
  if (props.paymentGateways) {
    const id = 'payment_gateway_id';
    const paymentGateways = props.paymentGateways;
    return /*#__PURE__*/_react.default.createElement("div", {
      key: "payment-gateways",
      className: cn('form.payment.row')
    }, /*#__PURE__*/_react.default.createElement("div", {
      className: cn('form.payment.wrapper')
    }, /*#__PURE__*/_react.default.createElement("div", {
      className: "uk-flex"
    }, renderRadioButtons(id, paymentGateways))), /*#__PURE__*/_react.default.createElement("div", {
      className: cn('form.payment.wrapper')
    }, /*#__PURE__*/_react.default.createElement(_formik.ErrorMessage, {
      name: id
    }, msg => /*#__PURE__*/_react.default.createElement("label", {
      className: 'error'
    }, msg))));
  }
  return null;
}
Payment.propTypes = {
  paymentGateways: _propTypes.default.array
};
var _default = exports.default = Payment;