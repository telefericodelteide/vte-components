'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _react = _interopRequireDefault(require("react"));
var _propTypes = _interopRequireDefault(require("prop-types"));
var _get2 = _interopRequireDefault(require("lodash/get"));
var _reactI18next = require("react-i18next");
var _formik = require("formik");
var _Validator = require("../Validator");
var _CountrySelect = _interopRequireDefault(require("./Countries/CountrySelect"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function CountryField(props) {
  const {
    t
  } = (0, _reactI18next.useTranslation)('main');
  if (props.id) {
    const id = props.id;
    if (props.config) {
      const config = (0, _get2.default)(props.config, id);
      if (config) {
        const {
          type,
          required,
          name
        } = config;
        return /*#__PURE__*/_react.default.createElement("div", {
          className: props.wrapperClassName,
          "aria-required": "true"
        }, props.noLabel ? null : /*#__PURE__*/_react.default.createElement("label", {
          htmlFor: "ClientName"
        }, name, " ", required ? /*#__PURE__*/_react.default.createElement("i", {
          className: "required",
          "aria-required": "true"
        }, " *") : null, " "), /*#__PURE__*/_react.default.createElement(_formik.Field, {
          name: id,
          validate: _Validator.validate.bind(this, config, t, props.values)
        }, _ref => {
          let {
            field,
            form: {
              errors,
              isSubmitting,
              touched,
              setFieldTouched,
              setFieldValue
            }
          } = _ref;
          return /*#__PURE__*/_react.default.createElement(_CountrySelect.default, {
            defaultCountry: "es",
            preferredCountries: ['es', 'gb', "fr", "de", 'it', "ru", "us", "pl", "nl"],
            inputClassName: props.className,
            defaultValue: field.value,
            id: id,
            onChange: value => {
              setFieldValue(id, value);
            }
          });
        }), /*#__PURE__*/_react.default.createElement(_formik.ErrorMessage, {
          name: props.id
        }, msg => /*#__PURE__*/_react.default.createElement("label", {
          className: 'error'
        }, msg)));
      }
    }
  }
  return null;
}
CountryField.propTypes = {
  id: _propTypes.default.string.isRequired,
  config: _propTypes.default.object.isRequired
};
var _default = exports.default = CountryField;