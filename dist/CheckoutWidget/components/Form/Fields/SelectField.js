"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _react = _interopRequireDefault(require("react"));
var _propTypes = _interopRequireDefault(require("prop-types"));
var _reactSelect = _interopRequireWildcard(require("react-select"));
var _get2 = _interopRequireDefault(require("lodash/get"));
var _isArray2 = _interopRequireDefault(require("lodash/isArray"));
var _reactI18next = require("react-i18next");
var _formik = require("formik");
var _Validator = require("../Validator");
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function (e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != typeof e && "function" != typeof e) return { default: e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && {}.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n.default = e, t && t.set(e, n), n; }
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
function _objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
function _defineProperty(e, r, t) { return (r = _toPropertyKey(r)) in e ? Object.defineProperty(e, r, { value: t, enumerable: !0, configurable: !0, writable: !0 }) : e[r] = t, e; }
function _toPropertyKey(t) { var i = _toPrimitive(t, "string"); return "symbol" == typeof i ? i : i + ""; }
function _toPrimitive(t, r) { if ("object" != typeof t || !t) return t; var e = t[Symbol.toPrimitive]; if (void 0 !== e) { var i = e.call(t, r || "default"); if ("object" != typeof i) return i; throw new TypeError("@@toPrimitive must return a primitive value."); } return ("string" === r ? String : Number)(t); }
function SelectField(props) {
  const {
    t
  } = (0, _reactI18next.useTranslation)("main");
  const customStyles = {
    control: (provided, state) => _objectSpread(_objectSpread({}, provided), {}, {
      minHeight: "28px",
      height: "28px",
      boxShadow: state.isFocused ? null : null
    }),
    valueContainer: (provided, state) => _objectSpread(_objectSpread({}, provided), {}, {
      height: "28px",
      padding: "0 6px"
    }),
    input: (provided, state) => _objectSpread(_objectSpread({}, provided), {}, {
      margin: "0px"
    }),
    indicatorSeparator: state => ({
      display: "none"
    }),
    indicatorsContainer: (provided, state) => _objectSpread(_objectSpread({}, provided), {}, {
      height: "28px"
    })
  };
  const prepareOptions = (options, emptyOption) => {
    if ((0, _isArray2.default)(options)) {
      const result = options.map(option => {
        return {
          value: option.id,
          label: option.name
        };
      });
      if (emptyOption == null) {
        return result;
      }
      return [{
        value: "-1",
        label: emptyOption
      }].concat(result);
    }
    return [];
  };
  const getOption = (options, value) => options && value ? options.find(option => option.value == value) : null;
  if (props.id) {
    const id = props.id;
    const configId = props.configId;
    if (props.config) {
      const config = configId ? (0, _get2.default)(props.config, configId) : (0, _get2.default)(props.config, id);
      if (config) {
        if (props.loadOptions) {
          config.options = props.loadOptions();
        }
        const {
          type,
          required,
          options,
          name,
          placeholder,
          emptyOption
        } = config;
        return /*#__PURE__*/_react.default.createElement("div", {
          className: props.wrapperClassName,
          "aria-required": required
        }, props.noLabel ? null : /*#__PURE__*/_react.default.createElement("label", {
          htmlFor: id
        }, t(name), " ", required ? /*#__PURE__*/_react.default.createElement("i", {
          className: "required",
          "aria-required": required
        }, " ", "*") : null, " "), /*#__PURE__*/_react.default.createElement(_formik.Field, {
          name: id,
          className: props.className,
          validate: _Validator.validate.bind(this, config, t, props.values)
        }, _ref => {
          let {
            field,
            form: {
              errors,
              isSubmitting,
              touched,
              setFieldTouched,
              setFieldValue
            }
          } = _ref;
          return /*#__PURE__*/_react.default.createElement(_reactSelect.default, {
            isSearchable: true,
            options: prepareOptions(options, emptyOption ? t(emptyOption) : null),
            placeholder: t(placeholder),
            value: getOption(options, field.value),
            styles: customStyles,
            onBlur: () => {
              setFieldTouched(id, true);
            },
            onChange: selected => {
              setFieldValue(id, selected.value);
              if (props.onSelect) {
                props.onSelect(selected.value);
              }
            }
          });
        }), /*#__PURE__*/_react.default.createElement(_formik.ErrorMessage, {
          name: props.id
        }, msg => /*#__PURE__*/_react.default.createElement("label", {
          className: "cw-form-field-error"
        }, msg)));
      }
    }
  }
  return null;
}
SelectField.propTypes = {
  id: _propTypes.default.string.isRequired,
  config: _propTypes.default.object,
  values: _propTypes.default.object,
  wrapperClassName: _propTypes.default.string,
  className: _propTypes.default.string,
  noLabel: _propTypes.default.bool,
  loadOptions: _propTypes.default.func,
  onSelect: _propTypes.default.func
};
var _default = exports.default = SelectField;