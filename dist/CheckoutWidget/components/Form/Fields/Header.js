'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _react = _interopRequireWildcard(require("react"));
var _propTypes = _interopRequireDefault(require("prop-types"));
var _context = _interopRequireDefault(require("../../../context"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function (e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != typeof e && "function" != typeof e) return { default: e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && {}.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n.default = e, t && t.set(e, n), n; }
function Header(props) {
  const {
    cn
  } = (0, _react.useContext)(_context.default);
  return /*#__PURE__*/_react.default.createElement("div", {
    className: cn('form.header.row'),
    "uk-grid": "uk-grid"
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: "uk-width-1-1 " + cn('form.header.wrapper')
  }, /*#__PURE__*/_react.default.createElement("span", {
    id: "clientSectionHeader",
    className: cn('form.header.type')
  }, props.icon ? /*#__PURE__*/_react.default.createElement("span", null, /*#__PURE__*/_react.default.createElement("i", {
    className: props.icon
  }), " ") : props.icon, props.title ? props.title : null)));
  return null;
}
Header.propTypes = {
  title: _propTypes.default.string,
  icon: _propTypes.default.string
};
var _default = exports.default = Header;