'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _react = _interopRequireDefault(require("react"));
var _propTypes = _interopRequireDefault(require("prop-types"));
var _get2 = _interopRequireDefault(require("lodash/get"));
var _reactI18next = require("react-i18next");
var _formik = require("formik");
var _reactIntlTelInput = _interopRequireDefault(require("react-intl-tel-input"));
require("react-intl-tel-input/dist/main.css");
var _Validator = require("../Validator");
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function PhoneField(props) {
  const {
    t
  } = (0, _reactI18next.useTranslation)('main');
  function formatPhoneNumberOutput(isValid, newNumber, countryData, fullNumber, isExtension) {
    if (isValid && fullNumber) {
      return fullNumber.replace(/(\s|-)/g, '');
    }
    if (fullNumber) {
      return 'invalid_phone_number';
    }
    return null;
  }
  if (props.id) {
    const id = props.id;
    if (props.config) {
      const config = (0, _get2.default)(props.config, id);
      if (config) {
        const {
          type,
          required,
          name
        } = config;
        return /*#__PURE__*/_react.default.createElement("div", {
          className: props.wrapperClassName,
          "aria-required": required
        }, props.noLabel ? null : /*#__PURE__*/_react.default.createElement("label", {
          htmlFor: id
        }, t(name), " ", required ? /*#__PURE__*/_react.default.createElement("i", {
          className: "required",
          "aria-required": "true"
        }, " *") : null, " "), /*#__PURE__*/_react.default.createElement(_formik.Field, {
          name: id,
          validate: _Validator.validate.bind(this, config, t, props.values)
        }, _ref => {
          let {
            field,
            form: {
              errors,
              isSubmitting,
              touched,
              setFieldTouched,
              setFieldValue
            }
          } = _ref;
          return /*#__PURE__*/_react.default.createElement(_reactIntlTelInput.default, {
            defaultCountry: "es",
            preferredCountries: ['es', 'gb', "fr", "de", 'it', "ru", "us", "pl", "nl"],
            customPlaceholder: selectedCountryPlaceholder => {
              return selectedCountryPlaceholder.replace(/[0-9]/g, "X");
            },
            containerClassName: "intl-tel-input",
            inputClassName: props.className,
            defaultValue: field.value,
            fieldId: id,
            fieldName: id,
            onPhoneNumberBlur: () => {
              setFieldTouched(id, true);
            },
            onPhoneNumberChange: function () {
              setFieldValue(id, formatPhoneNumberOutput(...arguments));
            }
          });
        }), /*#__PURE__*/_react.default.createElement(_formik.ErrorMessage, {
          name: props.id
        }, msg => /*#__PURE__*/_react.default.createElement("label", {
          className: "cw-form-field-error"
        }, msg)));
      }
    }
  }
  return null;
}
PhoneField.propTypes = {
  id: _propTypes.default.string.isRequired,
  config: _propTypes.default.object.isRequired
};
var _default = exports.default = PhoneField;