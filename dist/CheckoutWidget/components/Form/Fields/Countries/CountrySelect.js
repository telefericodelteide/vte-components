"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _react = _interopRequireWildcard(require("react"));
var _AllCountries = _interopRequireDefault(require("./AllCountries"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function (e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != typeof e && "function" != typeof e) return { default: e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && {}.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n.default = e, t && t.set(e, n), n; }
function _defineProperty(e, r, t) { return (r = _toPropertyKey(r)) in e ? Object.defineProperty(e, r, { value: t, enumerable: !0, configurable: !0, writable: !0 }) : e[r] = t, e; }
function _toPropertyKey(t) { var i = _toPrimitive(t, "string"); return "symbol" == typeof i ? i : i + ""; }
function _toPrimitive(t, r) { if ("object" != typeof t || !t) return t; var e = t[Symbol.toPrimitive]; if (void 0 !== e) { var i = e.call(t, r || "default"); if ("object" != typeof i) return i; throw new TypeError("@@toPrimitive must return a primitive value."); } return ("string" === r ? String : Number)(t); }
class CountrySelect extends _react.Component {
  constructor(props) {
    super(props);
    _defineProperty(this, "selectCountry", value => {
      this.setState({
        country: value,
        showList: false
      }, this.props.onChange(value));
    });
    _defineProperty(this, "expand", () => {
      this.setState({
        showList: true
      });
    });
    _defineProperty(this, "collapse", () => {
      this.setState({
        showList: false
      });
    });
    this.countries = _AllCountries.default.getCountries();
    this.indexedCountries = this.countries.reduce((result, country) => {
      result[country.iso2] = country.name;
      return result;
    }, {});
    this.state = {
      country: this.props.defaultCountry,
      showList: false
    };
  }
  buildCountryList() {
    return /*#__PURE__*/_react.default.createElement("ul", {
      className: "country-list"
    }, this.countries.map(country => {
      return /*#__PURE__*/_react.default.createElement("li", {
        key: country.iso2,
        className: "country",
        onClick: this.selectCountry.bind(this, country.iso2)
      }, /*#__PURE__*/_react.default.createElement("div", {
        className: "flag-box"
      }, /*#__PURE__*/_react.default.createElement("div", {
        className: "iti-flag ".concat(country.iso2)
      })), /*#__PURE__*/_react.default.createElement("span", {
        className: "country-name"
      }, country.name));
    }));
  }
  render() {
    const {
      country
    } = this.state;
    return /*#__PURE__*/_react.default.createElement("div", {
      className: "allow-dropdown intl-tel-input",
      tabIndex: "0",
      onBlur: this.collapse,
      onClick: this.expand
    }, /*#__PURE__*/_react.default.createElement("input", {
      type: "text",
      autoComplete: "off",
      className: this.props.inputClassName,
      name: this.props.id,
      id: this.props.id,
      onChange: () => {},
      value: this.indexedCountries[country]
    }), /*#__PURE__*/_react.default.createElement("div", {
      className: "flag-container",
      tabIndex: "0",
      onBlur: this.collapse
    }, /*#__PURE__*/_react.default.createElement("div", {
      className: "selected-flag"
    }, /*#__PURE__*/_react.default.createElement("div", {
      className: "iti-flag ".concat(country)
    }), /*#__PURE__*/_react.default.createElement("div", {
      className: "arrow down"
    })), this.state.showList ? this.buildCountryList() : null));
  }
}
var _default = exports.default = CountrySelect;