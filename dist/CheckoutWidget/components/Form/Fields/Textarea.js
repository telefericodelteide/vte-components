'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _react = _interopRequireDefault(require("react"));
var _propTypes = _interopRequireDefault(require("prop-types"));
var _get2 = _interopRequireDefault(require("lodash/get"));
var _reactI18next = require("react-i18next");
var _formik = require("formik");
var _Validator = require("../Validator");
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function Textarea(props) {
  const {
    t
  } = (0, _reactI18next.useTranslation)('main');
  if (props.id) {
    const id = props.id;
    if (props.config) {
      const config = (0, _get2.default)(props.config, id);
      if (config) {
        const {
          type,
          required,
          name
        } = config;
        return /*#__PURE__*/_react.default.createElement("div", {
          className: props.wrapperClassName,
          "aria-required": required
        }, /*#__PURE__*/_react.default.createElement("label", {
          htmlFor: id
        }, t(name), " ", required ? /*#__PURE__*/_react.default.createElement("i", {
          className: "required",
          "aria-required": "true"
        }, " *") : null, " "), /*#__PURE__*/_react.default.createElement(_formik.Field, {
          className: props.className,
          name: id,
          component: "textarea",
          rows: 4,
          validate: _Validator.validate.bind(this, config, t, props.values)
        }), /*#__PURE__*/_react.default.createElement(_formik.ErrorMessage, {
          name: props.id
        }, msg => /*#__PURE__*/_react.default.createElement("label", {
          className: 'error'
        }, msg)), props.description && /*#__PURE__*/_react.default.createElement("span", {
          className: props.descriptionClassName
        }, props.description));
      }
    }
  }
  return null;
}
Textarea.propTypes = {
  id: _propTypes.default.string.isRequired,
  config: _propTypes.default.object,
  description: _propTypes.default.string
};
var _default = exports.default = Textarea;