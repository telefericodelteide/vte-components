'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _react = _interopRequireDefault(require("react"));
var _propTypes = _interopRequireDefault(require("prop-types"));
var _get2 = _interopRequireDefault(require("lodash/get"));
var _reactI18next = require("react-i18next");
var _formik = require("formik");
var _Validator = require("../Validator");
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function Field(props) {
  const {
    t
  } = (0, _reactI18next.useTranslation)('main');
  const InputTypes = {
    email: 'email',
    string: 'text',
    text: 'text'
  };
  if (props.id) {
    const id = props.id;
    const configId = props.configId;
    if (props.config) {
      const config = configId ? (0, _get2.default)(props.config, configId) : (0, _get2.default)(props.config, id);
      if (config) {
        const {
          type,
          required,
          name
        } = config;
        return /*#__PURE__*/_react.default.createElement("div", {
          className: props.wrapperClassName,
          "aria-required": required
        }, props.noLabel ? null : /*#__PURE__*/_react.default.createElement("label", {
          htmlFor: id
        }, t(name), " ", required ? /*#__PURE__*/_react.default.createElement("i", {
          className: "required",
          "aria-required": required
        }, " *") : null, " "), /*#__PURE__*/_react.default.createElement(_formik.Field, {
          className: props.className,
          name: id,
          type: InputTypes[type],
          maxLength: 255,
          placeholder: props.placeholder ? t(name) : null,
          validate: _Validator.validate.bind(this, config, t, props.values)
        }), /*#__PURE__*/_react.default.createElement(_formik.ErrorMessage, {
          name: props.id
        }, msg => /*#__PURE__*/_react.default.createElement("label", {
          className: "cw-form-field-error"
        }, msg)));
      }
    }
  }
  return null;
}
Field.propTypes = {
  id: _propTypes.default.string.isRequired,
  config: _propTypes.default.object,
  values: _propTypes.default.object,
  wrapperClassName: _propTypes.default.string,
  fieldClassName: _propTypes.default.string,
  noLabel: _propTypes.default.bool,
  placeholder: _propTypes.default.bool
};
var _default = exports.default = Field;