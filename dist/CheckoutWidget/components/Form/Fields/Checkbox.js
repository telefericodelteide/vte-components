'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _react = _interopRequireDefault(require("react"));
var _propTypes = _interopRequireDefault(require("prop-types"));
var _get2 = _interopRequireDefault(require("lodash/get"));
var _reactI18next = require("react-i18next");
var _formik = require("formik");
var _Validator = require("../Validator");
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function Checkbox(props) {
  const {
    t
  } = (0, _reactI18next.useTranslation)('main');
  if (props.id) {
    const id = props.id;
    const configId = props.configId;
    if (props.config) {
      const config = configId ? (0, _get2.default)(props.config, configId) : (0, _get2.default)(props.config, id);
      if (config) {
        const {
          type,
          required,
          name
        } = config;
        return /*#__PURE__*/_react.default.createElement("div", {
          className: props.wrapperClassName
        }, /*#__PURE__*/_react.default.createElement("label", {
          htmlFor: props.id,
          className: "cw-form-field-check-label"
        }, /*#__PURE__*/_react.default.createElement(_formik.Field, {
          type: "checkbox",
          id: props.id,
          name: props.id,
          className: props.className,
          validate: _Validator.validate.bind(this, config, t, props.values)
        }), props.noLabel ? null : /*#__PURE__*/_react.default.createElement("span", {
          dangerouslySetInnerHTML: {
            __html: t(name)
          }
        }), required ? /*#__PURE__*/_react.default.createElement("i", {
          className: "required",
          "aria-required": required
        }, " *") : null, props.info ? /*#__PURE__*/_react.default.createElement("div", {
          className: "cw-form-check-field-info"
        }, /*#__PURE__*/_react.default.createElement("p", null, t(props.info))) : null), /*#__PURE__*/_react.default.createElement(_formik.ErrorMessage, {
          name: props.id
        }, msg => /*#__PURE__*/_react.default.createElement("label", {
          className: "cw-form-field-error"
        }, msg)));
      }
    }
  }
  return null;
}
Checkbox.propTypes = {
  id: _propTypes.default.string.isRequired,
  config: _propTypes.default.object,
  values: _propTypes.default.object,
  wrapperClassName: _propTypes.default.string,
  fieldClassName: _propTypes.default.string,
  noLabel: _propTypes.default.bool
};
var _default = exports.default = Checkbox;