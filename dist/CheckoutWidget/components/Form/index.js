'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _react = _interopRequireWildcard(require("react"));
var _propTypes = _interopRequireDefault(require("prop-types"));
var _reactI18next = require("react-i18next");
var _formik = require("formik");
var _debounce2 = _interopRequireDefault(require("lodash/debounce"));
var _get2 = _interopRequireDefault(require("lodash/get"));
var _set2 = _interopRequireDefault(require("lodash/set"));
var _merge2 = _interopRequireDefault(require("lodash/merge"));
var _isEmpty2 = _interopRequireDefault(require("lodash/isEmpty"));
var _context = _interopRequireDefault(require("../../context"));
var _Header = _interopRequireDefault(require("./Fields/Header"));
var _Customer = _interopRequireDefault(require("./Customer"));
var _Pickup = _interopRequireDefault(require("./Pickup"));
var _Attendees = _interopRequireDefault(require("./Attendees"));
var _Invoice = _interopRequireDefault(require("./Invoice"));
var _Payment = _interopRequireDefault(require("./Payment"));
var _TermsAndConditions = _interopRequireDefault(require("./TermsAndConditions"));
var _forms = _interopRequireDefault(require("../../config/forms"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function (e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != typeof e && "function" != typeof e) return { default: e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && {}.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n.default = e, t && t.set(e, n), n; }
function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
function _objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
function _defineProperty(e, r, t) { return (r = _toPropertyKey(r)) in e ? Object.defineProperty(e, r, { value: t, enumerable: !0, configurable: !0, writable: !0 }) : e[r] = t, e; }
function _toPropertyKey(t) { var i = _toPrimitive(t, "string"); return "symbol" == typeof i ? i : i + ""; }
function _toPrimitive(t, r) { if ("object" != typeof t || !t) return t; var e = t[Symbol.toPrimitive]; if (void 0 !== e) { var i = e.call(t, r || "default"); if ("object" != typeof i) return i; throw new TypeError("@@toPrimitive must return a primitive value."); } return ("string" === r ? String : Number)(t); }
function Form(props) {
  const {
    t
  } = (0, _reactI18next.useTranslation)('main');
  const {
    cn
  } = (0, _react.useContext)(_context.default);
  var updateState = (0, _debounce2.default)(function (values) {
    props.onChange(values);
  }, 350);
  function getNeedCheckNif(lineItemsForm, lineItems) {
    let lineItemsFormReturn = [];
    for (let i = 0; i < lineItemsForm.length; i++) {
      lineItemsFormReturn[i] = _objectSpread({}, lineItemsForm[i]);
      lineItemsFormReturn[i].participants = lineItemsForm[i].participants.filter(participant => {
        if (!participant.rate) {
          return false;
        }
        const rateId = participant.rate.split("-")[0];
        const item = lineItems[0].rates.find(rate => rate.rate.id === parseInt(rateId));
        const isChild = item.rate.rate_elements.some(element => element.id === 14);
        return isChild ? false : participant;
      }).filter(Boolean);
    }
    return lineItemsFormReturn;
  }
  if (props.client) {
    const defaultForm = (0, _get2.default)(_forms.default, 'form');
    const form = {
      client: defaultForm
    };
    const pickup = {
      pickup: (0, _merge2.default)((0, _get2.default)(_forms.default, 'pickup'), {
        lodgin_id: {
          options: (0, _get2.default)(props, 'pickupData.lodgins')
        },
        zone_id: {
          options: (0, _get2.default)(props, 'pickupData.zones')
        },
        pickup_point_id: {
          options: (0, _get2.default)(props, 'pickupData.pickupPoints')
        }
      })
    };
    const defaultInvoice = (0, _get2.default)(_forms.default, 'invoice');
    const invoice = {
      invoice: (0, _merge2.default)(defaultInvoice, (0, _get2.default)(props, 'client.invoice'))
    };
    const participants = (0, _get2.default)(props, 'client.participants');
    const lineItems = (0, _get2.default)(props, 'lineItems');
    const pickupData = (0, _get2.default)(props, 'pickupData');
    const validateForm = values => {
      let errors = {};
      const lineItemsForm = (0, _get2.default)(values, 'participants', []);
      lineItemsForm.forEach((lineItem, index) => {
        const participants = (0, _get2.default)(lineItem, 'participants', []);
        participants.map((participant, participantIndex) => {
          return {
            index: index,
            participantIndex: participantIndex,
            value: (participant.first_name + participant.last_name).replace(/[0-9\s"'`´]/g, '').toLowerCase()
          };
        }).filter((participant, participantIndex, participants) => {
          const findIndexValue = participants.findIndex(p => p.value.length > 0 && p.value === participant.value);
          return findIndexValue !== participantIndex && findIndexValue > -1;
        }).forEach(p => {
          (0, _set2.default)(errors, "participants[".concat(p.index, "].participants[").concat(p.participantIndex, "].first_name"), t("form.validations.repeated"));
        });
      });
      const lineItemsCheckDni = getNeedCheckNif(lineItemsForm, lineItems);
      lineItemsCheckDni.forEach((lineItem, index) => {
        const participants = (0, _get2.default)(lineItem, 'participants', []);
        participants.map((participant, participantIndex) => {
          return {
            index: index,
            participantIndex: participantIndex,
            value: participant.nif.replace(/ /g, "").toLowerCase()
          };
        }).filter((participant, participantIndex, participants) => {
          const findIndexValue = participants.findIndex(p => p.value.length > 0 && p.value === participant.value);
          return findIndexValue !== participantIndex && findIndexValue > -1;
        }).forEach(p => {
          (0, _set2.default)(errors, "participants[".concat(p.index, "].participants[").concat(p.participantIndex, "].nif"), t("form.validations.repeatedNif"));
        });
      });
      return errors;
    };
    return /*#__PURE__*/_react.default.createElement(_formik.Formik, {
      initialValues: props.values,
      onSubmit: values => {
        props.onSubmit(values);
      },
      enableReinitialize: true,
      validate: validateForm
    }, formikProps => {
      const {
        values,
        dirty,
        isValid
      } = formikProps;
      return /*#__PURE__*/_react.default.createElement(_formik.Form, {
        id: "ClientIndexForm",
        className: cn('form.class')
      }, /*#__PURE__*/_react.default.createElement("div", {
        className: cn('form.wrapper')
      }, /*#__PURE__*/_react.default.createElement(_Header.default, {
        title: t('form.customerDataTitle'),
        icon: cn('form.customer.titleIcon')
      }), /*#__PURE__*/_react.default.createElement(_Customer.default, {
        form: form,
        values: values,
        errors: props.errors,
        promoCode: props.promoCode,
        onChangePromoCode: props.onChangePromoCode,
        onApplyPromoCode: props.onApplyPromoCode
      }), !(0, _isEmpty2.default)(pickupData) && /*#__PURE__*/_react.default.createElement(_react.Fragment, null, /*#__PURE__*/_react.default.createElement(_Header.default, {
        title: t('form.pickup.title'),
        icon: cn('form.pickup.titleIcon')
      }), /*#__PURE__*/_react.default.createElement(_Pickup.default, {
        form: pickup,
        values: values,
        errors: props.errors
      })), participants && participants.length > 0 && /*#__PURE__*/_react.default.createElement(_react.Fragment, null, /*#__PURE__*/_react.default.createElement(_Header.default, {
        title: t('form.participants.title'),
        icon: cn('form.participants.icon')
      }), /*#__PURE__*/_react.default.createElement(_Attendees.default, {
        participants: participants,
        values: values,
        lineItems: lineItems
      })), props.totalAmount > 0 && props.allowInvoice && /*#__PURE__*/_react.default.createElement(_react.Fragment, null, /*#__PURE__*/_react.default.createElement(_Header.default, null, /*#__PURE__*/_react.default.createElement(_Invoice.default, {
        wantsInvoice: props.wantsInvoice,
        form: invoice,
        values: values,
        toggleInvoice: props.toggleInvoice
      }))), props.totalAmount > 0 && /*#__PURE__*/_react.default.createElement(_react.Fragment, null, /*#__PURE__*/_react.default.createElement(_Header.default, {
        title: t('form.customerPayDataTitle'),
        icon: cn('form.payment.titleIcon')
      }), /*#__PURE__*/_react.default.createElement(_Payment.default, {
        paymentGateways: props.paymentGateways
      }), /*#__PURE__*/_react.default.createElement(_Header.default, null)), /*#__PURE__*/_react.default.createElement(_TermsAndConditions.default, {
        form: form,
        values: values,
        conditions: props.conditions
      }), /*#__PURE__*/_react.default.createElement("div", {
        className: "cw-form-submit"
      }, /*#__PURE__*/_react.default.createElement("button", {
        id: "btnEnviar",
        type: "submit",
        className: "uk-button cw-form-submit-button",
        disabled: !dirty || !isValid
      }, props.totalAmount > 0 ? t('generic.continue') : t('generic.confirm')), props.totalAmount > 0 && /*#__PURE__*/_react.default.createElement("div", {
        className: "cw-form-submit-info"
      }, t('form.orderWithMandatoryPayment')))));
    });
  }
  return null;
}
Form.propTypes = {
  promoCode: _propTypes.default.string,
  lineItems: _propTypes.default.array,
  wantsInvoice: _propTypes.default.bool,
  paymentGateways: _propTypes.default.array,
  pickupData: _propTypes.default.object,
  errors: _propTypes.default.object,
  toggleInvoice: _propTypes.default.func.isRequired,
  onApplyPromoCode: _propTypes.default.func.isRequired,
  onChangePromoCode: _propTypes.default.func.isRequired,
  onSubmit: _propTypes.default.func.isRequired
};
var _default = exports.default = Form;