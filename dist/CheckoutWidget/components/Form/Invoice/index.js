"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _react = _interopRequireWildcard(require("react"));
var _propTypes = _interopRequireDefault(require("prop-types"));
var _reactI18next = require("react-i18next");
var _context = _interopRequireDefault(require("../../../context"));
var _Field = _interopRequireDefault(require("../Fields/Field"));
var _CountryField = _interopRequireDefault(require("../Fields/CountryField"));
var _SelectField = _interopRequireDefault(require("../Fields/SelectField"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function (e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != typeof e && "function" != typeof e) return { default: e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && {}.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n.default = e, t && t.set(e, n), n; }
function Invoice(props) {
  const {
    t
  } = (0, _reactI18next.useTranslation)("main");
  const {
    cn
  } = (0, _react.useContext)(_context.default);
  const vatNumberTypes = () => {
    return props.form.invoice.vat_number_type.options.map(option => {
      return {
        id: option.id,
        name: t(option.name)
      };
    });
  };
  const buildWantsInvoiceCheck = () => /*#__PURE__*/_react.default.createElement("div", {
    key: "name",
    className: cn("form.invoice.wantsInvoice.row"),
    "uk-grid": "uk-grid"
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: cn("form.invoice.wantsInvoice.fieldWrapper")
  }, /*#__PURE__*/_react.default.createElement("div", null, /*#__PURE__*/_react.default.createElement("label", {
    id: "want-invoice",
    htmlFor: props.id,
    className: "cw-form-field-check-label"
  }, /*#__PURE__*/_react.default.createElement("input", {
    type: "checkbox",
    className: cn("form.invoice.wantsInvoice.field"),
    checked: props.wantsInvoice,
    onChange: props.toggleInvoice
  }), t("form.invoice.wantsInvoice")))));
  if (props.form) {
    const form = props.form;
    return /*#__PURE__*/_react.default.createElement(_react.Fragment, null, buildWantsInvoiceCheck(), props.wantsInvoice && /*#__PURE__*/_react.default.createElement(_react.Fragment, null, /*#__PURE__*/_react.default.createElement("div", {
      key: "name",
      className: cn("form.invoice.name.row"),
      "uk-grid": "uk-grid"
    }, /*#__PURE__*/_react.default.createElement(_Field.default, {
      id: "invoice.name",
      config: form,
      values: props.values,
      className: cn("form.invoice.name.field"),
      wrapperClassName: cn("form.invoice.name.fieldWrapper")
    })), /*#__PURE__*/_react.default.createElement("div", {
      key: "vatType",
      className: cn("form.invoice.vat_number_type.row"),
      "uk-grid": "uk-grid"
    }, /*#__PURE__*/_react.default.createElement(_SelectField.default, {
      id: "invoice.vat_number_type",
      config: form,
      values: props.values,
      className: cn("form.invoice.vat_number_type.field"),
      wrapperClassName: cn("form.invoice.vat_number_type.fieldWrapper"),
      loadOptions: vatNumberTypes
    })), /*#__PURE__*/_react.default.createElement("div", {
      key: "vat",
      className: cn("form.invoice.vat_number.row"),
      "uk-grid": "uk-grid"
    }, /*#__PURE__*/_react.default.createElement(_Field.default, {
      id: "invoice.vat_number",
      config: form,
      values: props.values,
      className: cn("form.invoice.vat_number.field"),
      wrapperClassName: cn("form.invoice.vat_number.fieldWrapper")
    })), /*#__PURE__*/_react.default.createElement("div", {
      key: "address",
      className: cn("form.invoice.address.row"),
      "uk-grid": "uk-grid"
    }, /*#__PURE__*/_react.default.createElement("div", {
      className: cn("form.invoice.address.fieldWrapper")
    }, /*#__PURE__*/_react.default.createElement("label", {
      htmlFor: "invoice.address"
    }, t("form.address"), /*#__PURE__*/_react.default.createElement("i", {
      className: "required",
      "aria-required": true
    }, " ", "*")), /*#__PURE__*/_react.default.createElement("div", {
      className: "uk-grid-row-collapse uk-grid-column-small",
      "uk-grid": "uk-grid"
    }, /*#__PURE__*/_react.default.createElement(_Field.default, {
      id: "invoice.address",
      config: form,
      values: props.values,
      noLabel: true,
      placeholder: true,
      className: cn("form.invoice.address.address.field"),
      wrapperClassName: cn("form.invoice.address.address.fieldWrapper")
    }), /*#__PURE__*/_react.default.createElement(_Field.default, {
      id: "invoice.postal_code",
      config: form,
      noLabel: true,
      placeholder: true,
      values: props.values,
      className: cn("form.invoice.address.postal_code.field"),
      wrapperClassName: cn("form.invoice.address.postal_code.fieldWrapper")
    }), /*#__PURE__*/_react.default.createElement(_Field.default, {
      id: "invoice.locality",
      config: form,
      noLabel: true,
      placeholder: true,
      values: props.values,
      className: cn("form.invoice.address.locality.field"),
      wrapperClassName: cn("form.invoice.address.locality.fieldWrapper")
    }), /*#__PURE__*/_react.default.createElement(_Field.default, {
      id: "invoice.state",
      config: form,
      noLabel: true,
      placeholder: true,
      values: props.values,
      className: cn("form.invoice.address.state.field"),
      wrapperClassName: cn("form.invoice.address.state.fieldWrapper")
    }), /*#__PURE__*/_react.default.createElement(_CountryField.default, {
      id: "invoice.country_id",
      config: form,
      noLabel: true,
      placeholder: true,
      values: props.values,
      className: cn("form.invoice.address.country_id.field"),
      wrapperClassName: cn("form.invoice.address.country_id.fieldWrapper")
    }))))));
  }
  return null;
}
Invoice.propTypes = {
  form: _propTypes.default.object.isRequired,
  values: _propTypes.default.object.isRequired,
  wantsInvoice: _propTypes.default.bool,
  toggleInvoice: _propTypes.default.func.isRequired
};
var _default = exports.default = Invoice;