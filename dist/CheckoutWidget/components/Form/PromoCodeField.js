'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _react = _interopRequireWildcard(require("react"));
var _propTypes = _interopRequireDefault(require("prop-types"));
var _reactI18next = require("react-i18next");
var _context = _interopRequireDefault(require("../../context"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function (e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != typeof e && "function" != typeof e) return { default: e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && {}.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n.default = e, t && t.set(e, n), n; }
function PromoCodeField(props) {
  const {
    t
  } = (0, _reactI18next.useTranslation)('main');
  const {
    cn
  } = (0, _react.useContext)(_context.default);
  function onChange(e) {
    e.preventDefault();
    props.onChange(e.target.value);
  }
  return /*#__PURE__*/_react.default.createElement("div", null, /*#__PURE__*/_react.default.createElement("label", null, t('form.promoCodeLabel')), /*#__PURE__*/_react.default.createElement("div", {
    className: cn('form.customer.observations.promo.group')
  }, /*#__PURE__*/_react.default.createElement("input", {
    name: "promo",
    value: props.promoCode ? props.promoCode : '',
    onChange: onChange,
    type: "text",
    autoComplete: "false",
    className: cn('form.customer.observations.promo.field')
  }), /*#__PURE__*/_react.default.createElement("button", {
    id: "promo-check",
    type: "button",
    onClick: props.onApplyPromoCode,
    className: cn('form.customer.observations.promo.button')
  }, t('form.promoCodeButton'))), props.errors['promo-code'] ? /*#__PURE__*/_react.default.createElement("label", {
    className: 'error'
  }, props.errors['promo-code']) : null);
}
PromoCodeField.propTypes = {
  promoCode: _propTypes.default.string,
  errors: _propTypes.default.object,
  onChange: _propTypes.default.func.isRequired,
  onApplyPromoCode: _propTypes.default.func.isRequired
};
var _default = exports.default = PromoCodeField;