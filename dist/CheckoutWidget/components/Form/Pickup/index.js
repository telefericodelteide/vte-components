"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _react = _interopRequireWildcard(require("react"));
var _propTypes = _interopRequireDefault(require("prop-types"));
var _reactI18next = require("react-i18next");
var _context = _interopRequireDefault(require("../../../context"));
var _SelectField = _interopRequireDefault(require("../Fields/SelectField"));
var _Textarea = _interopRequireDefault(require("../Fields/Textarea"));
var _isEmpty2 = _interopRequireDefault(require("lodash/isEmpty"));
var _get2 = _interopRequireDefault(require("lodash/get"));
var _formik = require("formik");
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function (e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != typeof e && "function" != typeof e) return { default: e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && {}.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n.default = e, t && t.set(e, n), n; }
function PickupInfo(props) {
  const {
    t
  } = (0, _reactI18next.useTranslation)("main");
  const {
    cn
  } = (0, _react.useContext)(_context.default);
  const {
    name,
    pickup_time
  } = props;
  return /*#__PURE__*/_react.default.createElement("div", {
    key: "pickup-info",
    className: "uk-width-1-1"
  }, /*#__PURE__*/_react.default.createElement("div", {
    "uk-grid": "uk-grid",
    className: cn("form.pickup.info.wrapper")
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: cn("form.pickup.info.title")
  }, t("form.pickup.pickup_info.title"), ":"), /*#__PURE__*/_react.default.createElement("div", {
    className: cn("form.pickup.info.field")
  }, /*#__PURE__*/_react.default.createElement("span", {
    className: cn("form.pickup.info.label")
  }, t("form.pickup.pickup_info.venue"), ": "), /*#__PURE__*/_react.default.createElement("span", {
    className: cn("form.pickup.info.text")
  }, name)), /*#__PURE__*/_react.default.createElement("div", {
    className: cn("form.pickup.info.field")
  }, /*#__PURE__*/_react.default.createElement("span", {
    className: cn("form.pickup.info.label")
  }, t("form.pickup.pickup_info.time"), ": "), /*#__PURE__*/_react.default.createElement("span", {
    className: cn("form.pickup.info.text")
  }, pickup_time))));
}
function Pickup(props) {
  const {
    t
  } = (0, _reactI18next.useTranslation)("main");
  const {
    cn
  } = (0, _react.useContext)(_context.default);
  const [lodgin, setLodgin] = (0, _react.useState)(null);
  const [zone, setZone] = (0, _react.useState)(null);
  const [pickupPoint, setPickupPoint] = (0, _react.useState)(null);
  const [selectedPickupPoint, setSelectedPickupPoint] = (0, _react.useState)(null);
  const availableLodgins = (0, _get2.default)(props, "form.pickup.lodgin_id.options");
  const availableZones = (0, _get2.default)(props, "form.pickup.zone_id.options");
  const availablePickupPoints = (0, _get2.default)(props, "form.pickup.pickup_point_id.options");
  const formMode = (0, _get2.default)(props, 'mode', 'lodgin');
  const formikProps = (0, _formik.useFormikContext)();
  const onLodginSelectedHandler = selected => {
    setLodgin(selected);
    if (selected != null && selected != -1) {
      const lodgin = availableLodgins.find(l => l.id == selected);
      setSelectedPickupPoint(availablePickupPoints.find(pp => pp.id == lodgin.pickup_point_id));
    } else {
      setSelectedPickupPoint(null);
    }
    setZone(null);
    setPickupPoint(null);
    formikProps.setFieldValue("pickup.zone_id", null);
    formikProps.setFieldValue("pickup.pickup_point_id", null);
  };
  const onZoneSelectedHandler = selected => {
    setZone(selected);
    setPickupPoint(null);
    setSelectedPickupPoint(null);
    formikProps.setFieldValue("pickup.pickup_point_id", null);
  };
  const onPickupPointSelectHandler = selected => {
    setPickupPoint(selected);
    if (selected != null && selected != -1) {
      const result = availablePickupPoints.find(pp => pp.id == selected);
      setSelectedPickupPoint(result);
    } else {
      setSelectedPickupPoint(null);
    }
  };
  const checkStep = step => {
    switch (step) {
      case "lodgin":
        return !(0, _isEmpty2.default)(availableLodgins);
      case "zone":
        return formMode == 'zone' && (lodgin == null && !checkStep("lodgin") || lodgin == -1) && !(0, _isEmpty2.default)(availableZones);
      case "pickup_point":
        return formMode == 'zone' && (lodgin == null && !checkStep("lodgin") || lodgin == -1) && (zone == null && !checkStep("zone") || zone != null) && !(0, _isEmpty2.default)(availablePickupPoints);
      case "observations":
        return (lodgin == null && !checkStep("lodgin") || lodgin == -1) && (zone == null && !checkStep("zone") || zone != null) && (pickupPoint == null && !checkStep("pickup_point") || pickupPoint == -1);
    }
  };
  const filterOptionsByZone = (zone, options) => {
    if (zone == null || zone == -1) {
      return options;
    }
    return options.filter(option => option.zone_id != zone);
  };
  return /*#__PURE__*/_react.default.createElement("div", {
    key: "pickup",
    className: cn("form.pickup.row"),
    "uk-grid": "uk-grid"
  }, /*#__PURE__*/_react.default.createElement("div", {
    key: "pickup-data",
    className: cn("form.pickup.wrapper"),
    "uk-grid": "uk-grid"
  }, checkStep("lodgin") && /*#__PURE__*/_react.default.createElement(_SelectField.default, {
    id: "pickup.lodgin_id",
    config: props.form,
    values: props.values,
    className: cn("form.pickup.field.main"),
    wrapperClassName: cn("form.pickup.field.wrapper"),
    onSelect: onLodginSelectedHandler
  }), checkStep("zone") && /*#__PURE__*/_react.default.createElement(_SelectField.default, {
    id: "pickup.zone_id",
    config: props.form,
    values: props.values,
    className: cn("form.pickup.field.main"),
    wrapperClassName: cn("form.pickup.field.wrapper"),
    onSelect: onZoneSelectedHandler
  }), checkStep("pickup_point") && /*#__PURE__*/_react.default.createElement(_SelectField.default, {
    id: "pickup.pickup_point_id",
    config: props.form,
    values: props.values,
    className: cn("form.pickup.field.main"),
    wrapperClassName: cn("form.pickup.field.wrapper"),
    loadOptions: () => filterOptionsByZone(zone, props.form.pickup.pickup_point_id.options),
    onSelect: onPickupPointSelectHandler
  }), checkStep("observations") && [/*#__PURE__*/_react.default.createElement(_Textarea.default, {
    id: "pickup.observations",
    config: props.form,
    values: props.values,
    className: cn("form.pickup.observations.main"),
    wrapperClassName: cn("form.pickup.observations.wrapper"),
    description: t("form.pickup.observations.description"),
    descriptionClassName: cn("form.pickup.observations.description")
  })]), selectedPickupPoint && /*#__PURE__*/_react.default.createElement(PickupInfo, selectedPickupPoint));
}
Pickup.propTypes = {
  form: _propTypes.default.object.isRequired,
  values: _propTypes.default.object.isRequired,
  errors: _propTypes.default.object
};
var _default = exports.default = Pickup;