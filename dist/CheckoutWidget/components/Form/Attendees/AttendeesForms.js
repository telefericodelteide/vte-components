"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _react = _interopRequireDefault(require("react"));
var _propTypes = _interopRequireDefault(require("prop-types"));
var _get2 = _interopRequireDefault(require("lodash/get"));
var _isArray2 = _interopRequireDefault(require("lodash/isArray"));
var _reactI18next = require("react-i18next");
var _AttendeeForm = _interopRequireDefault(require("./AttendeeForm"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function AttendeesForms(props) {
  const {
    t
  } = (0, _reactI18next.useTranslation)("main");
  function getQuantity() {
    const rates = (0, _get2.default)(props, "lineItem.rates");
    let qty = 0;
    if (rates && (0, _isArray2.default)(rates)) {
      qty = rates.reduce((result, rate) => result + rate.qty, 0);
    }
    return qty;
  }
  function renderAttendeesForms() {
    const qty = getQuantity();
    let forms = [];
    let subindex = 0;
    props.lineItem.rates.forEach((rate, index) => {
      for (let i = 0; i < rate.qty; i++, subindex++) {
        forms.push(/*#__PURE__*/_react.default.createElement(_AttendeeForm.default, {
          key: "attendee-form-".concat(subindex),
          index: props.index,
          subindex: subindex,
          form: props.form,
          values: props.values,
          rateId: rate.rate.id,
          rateIndex: i,
          rateName: rate.rate.name,
          rateElements: rate.rate.rate_elements
        }));
      }
    });
    return forms;
  }
  return renderAttendeesForms();
}
AttendeesForms.propTypes = {
  index: _propTypes.default.number.isRequired,
  lineItem: _propTypes.default.object.isRequired,
  form: _propTypes.default.object.isRequired,
  values: _propTypes.default.object.isRequired
};
var _default = exports.default = AttendeesForms;