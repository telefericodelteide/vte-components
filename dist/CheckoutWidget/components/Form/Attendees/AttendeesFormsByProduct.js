"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _react = _interopRequireWildcard(require("react"));
var _propTypes = _interopRequireDefault(require("prop-types"));
var _reactI18next = require("react-i18next");
var _get2 = _interopRequireDefault(require("lodash/get"));
var _isArray2 = _interopRequireDefault(require("lodash/isArray"));
var _dateFns = require("date-fns");
var _context = _interopRequireDefault(require("../../../context"));
var _AttendeesForms = _interopRequireDefault(require("./AttendeesForms"));
var _formik = require("formik");
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function (e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != typeof e && "function" != typeof e) return { default: e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && {}.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n.default = e, t && t.set(e, n), n; }
function AttendeesFormsByProduct(props) {
  const {
    t
  } = (0, _reactI18next.useTranslation)("main");
  const {
    cn
  } = (0, _react.useContext)(_context.default);
  function getLineItem() {
    const lineId = props.lineId;
    const lineItems = props.lineItems;
    let lineItem = null;
    if (lineItems && (0, _isArray2.default)(lineItems)) {
      lineItems.forEach(item => {
        if (item.id == lineId) {
          lineItem = item;
        }
      });
    }
    return lineItem;
  }
  function renderHeader(lineItem) {
    if (lineItem) {
      let title = (0, _get2.default)(lineItem, "product.name");
      let date = (0, _dateFns.parseISO)((0, _get2.default)(lineItem, "booking_date"));
      let experienceTitle = (0, _get2.default)(lineItem, "product.experience.name");
      let available = (0, _get2.default)(lineItem, "available");
      return /*#__PURE__*/_react.default.createElement("div", {
        className: cn("form.participants.product.header.sectionContainer")
      }, /*#__PURE__*/_react.default.createElement("div", {
        className: cn("form.participants.product.header.section"),
        "uk-grid": "uk-grid"
      }, !available ? /*#__PURE__*/_react.default.createElement("div", {
        className: cn("form.participants.product.no_available")
      }, /*#__PURE__*/_react.default.createElement("span", null, t("cart.no_available"))) : null, /*#__PURE__*/_react.default.createElement("div", {
        className: "uk-vertical-align " + cn("form.participants.product.header.titleRow")
      }, /*#__PURE__*/_react.default.createElement("span", null, experienceTitle)), /*#__PURE__*/_react.default.createElement("div", {
        className: cn("form.participants.product.header.metadataRow")
      }, /*#__PURE__*/_react.default.createElement("div", {
        "uk-grid": "uk-grid"
      }, /*#__PURE__*/_react.default.createElement("div", {
        className: "uk-width-1-1 uk-width-auto@m"
      }, /*#__PURE__*/_react.default.createElement("span", {
        className: cn("form.participants.product.header.metadataTitle")
      }, title, " "), /*#__PURE__*/_react.default.createElement("i", {
        className: "uk-visible@m fas fa-long-arrow-alt-right"
      })), /*#__PURE__*/_react.default.createElement("div", {
        className: "uk-width-expand uk-margin-remove " + cn("form.participants.product.header.metadataDate")
      }, /*#__PURE__*/_react.default.createElement("span", {
        className: cn("form.participants.product.header.metadataDateLabel")
      }, t("generic.date"), ":", " "), /*#__PURE__*/_react.default.createElement("span", {
        className: cn("form.participants.product.header.metadataDateContent")
      }, (0, _dateFns.format)(date, "dd/MM/yy")), /*#__PURE__*/_react.default.createElement("span", {
        className: cn("form.participants.product.header.metadataDateLabel")
      }, t("generic.hour"), ":", " "), /*#__PURE__*/_react.default.createElement("span", {
        className: cn("form.participants.product.header.metadataDateContent")
      }, (0, _dateFns.format)(date, "HH:mm")))))));
    }
    return null;
  }
  if (props.participantForm) {
    const index = props.index;
    const form = (0, _get2.default)(props, "participantForm.form");
    const lineItem = getLineItem();
    return /*#__PURE__*/_react.default.createElement("div", {
      className: cn("form.participants.wrapper"),
      "uk-grid": "uk-grid"
    }, props.showHeader && renderHeader(lineItem), /*#__PURE__*/_react.default.createElement(_formik.FieldArray, {
      name: "participants[".concat(index, "].participants")
    }, /*#__PURE__*/_react.default.createElement(_AttendeesForms.default, {
      index: index,
      form: form,
      lineItem: lineItem,
      values: props.values
    })));
  }
  return null;
}
AttendeesFormsByProduct.propTypes = {
  index: _propTypes.default.number.isRequired,
  lineId: _propTypes.default.string.isRequired,
  lineItems: _propTypes.default.array.isRequired,
  participantForm: _propTypes.default.object.isRequired,
  values: _propTypes.default.object.isRequired
};
var _default = exports.default = AttendeesFormsByProduct;