'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _react = _interopRequireWildcard(require("react"));
var _propTypes = _interopRequireDefault(require("prop-types"));
var _get2 = _interopRequireDefault(require("lodash/get"));
var _isArray2 = _interopRequireDefault(require("lodash/isArray"));
var _reactI18next = require("react-i18next");
var _context = _interopRequireDefault(require("../../../context"));
var _AttendeesFormsByProduct = _interopRequireDefault(require("./AttendeesFormsByProduct"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function (e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != typeof e && "function" != typeof e) return { default: e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && {}.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n.default = e, t && t.set(e, n), n; }
function Attendees(props) {
  const {
    t
  } = (0, _reactI18next.useTranslation)('main');
  const {
    cn
  } = (0, _react.useContext)(_context.default);
  function renderAttendeesFormsByProduct() {
    const participantForms = props.participants;
    const lineItems = props.lineItems;
    if (participantForms && (0, _isArray2.default)(participantForms)) {
      return participantForms.map((participantForm, index) => {
        return /*#__PURE__*/_react.default.createElement(_AttendeesFormsByProduct.default, {
          key: index,
          index: index,
          lineId: participantForm.line_item_id,
          lineItems: lineItems,
          participantForm: participantForm,
          values: props.values,
          showHeader: participantForms.length > 1
        });
      });
    }
    return null;
  }
  if (props.participants) {
    return renderAttendeesFormsByProduct();
  }
  return null;
}
Attendees.propTypes = {
  participants: _propTypes.default.array,
  lineItems: _propTypes.default.array,
  values: _propTypes.default.object.isRequired
};
var _default = exports.default = Attendees;