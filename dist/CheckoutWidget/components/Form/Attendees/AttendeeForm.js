"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _react = _interopRequireWildcard(require("react"));
var _propTypes = _interopRequireDefault(require("prop-types"));
var _reactI18next = require("react-i18next");
var _context = _interopRequireDefault(require("../../../context"));
var _Field = _interopRequireDefault(require("../Fields/Field"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function (e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != typeof e && "function" != typeof e) return { default: e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && {}.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n.default = e, t && t.set(e, n), n; }
function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
function _objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
function _defineProperty(e, r, t) { return (r = _toPropertyKey(r)) in e ? Object.defineProperty(e, r, { value: t, enumerable: !0, configurable: !0, writable: !0 }) : e[r] = t, e; }
function _toPropertyKey(t) { var i = _toPrimitive(t, "string"); return "symbol" == typeof i ? i : i + ""; }
function _toPrimitive(t, r) { if ("object" != typeof t || !t) return t; var e = t[Symbol.toPrimitive]; if (void 0 !== e) { var i = e.call(t, r || "default"); if ("object" != typeof i) return i; throw new TypeError("@@toPrimitive must return a primitive value."); } return ("string" === r ? String : Number)(t); }
function AttendeeForm(props) {
  const {
    t
  } = (0, _reactI18next.useTranslation)("main");
  const {
    cn
  } = (0, _react.useContext)(_context.default);
  const nifChild = rateElements => {
    let nifType = "nif";
    if (!rateElements) {
      return false;
    }
    if (rateElements && rateElements.length > 0) {
      const ratesChildIds = [14];
      const rateChild = rateElements.find(element => ratesChildIds.includes(element.id));
      if (rateChild) {
        nifType = "nif-child";
      }
    }
    return nifType !== "nif";
  };
  if (props.form) {
    const form = Object.fromEntries(Object.entries(props.form).map(_ref => {
      let [key, value] = _ref;
      return [key, _objectSpread(_objectSpread({}, value), {}, {
        name: "form.participants." + value.name
      })];
    }));
    const index = props.index;
    const subindex = props.subindex;
    if (nifChild(props === null || props === void 0 ? void 0 : props.rateElements)) {
      form.nif.type = "nif-child";
      form.nif.required = false;
    }
    return /*#__PURE__*/_react.default.createElement("div", {
      className: cn("form.participants.row")
    }, /*#__PURE__*/_react.default.createElement("div", {
      className: cn("form.participants.wrapper"),
      "uk-grid": "uk-grid"
    }, /*#__PURE__*/_react.default.createElement("div", {
      className: "col-sm-12"
    }, /*#__PURE__*/_react.default.createElement("span", null, /*#__PURE__*/_react.default.createElement("span", null, /*#__PURE__*/_react.default.createElement("i", {
      className: cn("form.participants.icon")
    }), " "), "".concat(t("form.participant"), " ").concat(subindex + 1, " (").concat(props.rateName, ")"))), /*#__PURE__*/_react.default.createElement(_Field.default, {
      id: "participants[".concat(index, "].participants[").concat(subindex, "].first_name"),
      configId: "first_name",
      config: form,
      values: props.values,
      className: cn("form.participants.first_name.field"),
      wrapperClassName: cn("form.participants.first_name.fieldWrapper")
    }), /*#__PURE__*/_react.default.createElement(_Field.default, {
      id: "participants[".concat(index, "].participants[").concat(subindex, "].last_name"),
      configId: "last_name",
      config: form,
      values: props.values,
      className: cn("form.participants.last_name.field"),
      wrapperClassName: cn("form.participants.last_name.fieldWrapper")
    }), /*#__PURE__*/_react.default.createElement(_Field.default, {
      id: "participants[".concat(index, "].participants[").concat(subindex, "].nif"),
      configId: "nif",
      config: form,
      values: props.values,
      className: cn("form.participants.nif.field"),
      wrapperClassName: cn("form.participants.nif.fieldWrapper")
    })));
  }
  return null;
}
AttendeeForm.propTypes = {
  index: _propTypes.default.number.isRequired,
  subindex: _propTypes.default.number.isRequired,
  form: _propTypes.default.object.isRequired,
  values: _propTypes.default.object.isRequired
};
var _default = exports.default = AttendeeForm;