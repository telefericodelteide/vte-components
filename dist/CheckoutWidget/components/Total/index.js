'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _react = _interopRequireDefault(require("react"));
var _propTypes = _interopRequireDefault(require("prop-types"));
var _reactI18next = require("react-i18next");
var _DiscountRows = _interopRequireDefault(require("./DiscountRows"));
var _TotalRow = _interopRequireDefault(require("./TotalRow"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function Total(props) {
  const {
    t
  } = (0, _reactI18next.useTranslation)('main');
  if (props.totalAmount >= 0) {
    return [/*#__PURE__*/_react.default.createElement(_DiscountRows.default, {
      key: "discounts",
      discounts: props.discounts
    }), /*#__PURE__*/_react.default.createElement(_TotalRow.default, {
      key: "total",
      totalAmount: props.totalAmount,
      label: t('cart.total'),
      settingType: 'total'
    })];
  }
  return null;
}
Total.propTypes = {
  totalAmount: _propTypes.default.number,
  discounts: _propTypes.default.array
};
var _default = exports.default = Total;