'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _react = _interopRequireDefault(require("react"));
var _propTypes = _interopRequireDefault(require("prop-types"));
var _reactI18next = require("react-i18next");
var _isArray2 = _interopRequireDefault(require("lodash/isArray"));
var _TotalRow = _interopRequireDefault(require("./TotalRow"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function DiscountRows(props) {
  const {
    t
  } = (0, _reactI18next.useTranslation)('main');
  function renderDiscounts() {
    const discounts = props.discounts;
    if ((0, _isArray2.default)(discounts)) {
      return discounts.map((discount, index) => {
        const name = discount.name ? discount.name : t('cart.discount');
        return /*#__PURE__*/_react.default.createElement(_TotalRow.default, {
          key: "discount-".concat(index),
          label: name,
          totalAmount: discount.total_amount,
          settingType: 'discount'
        });
      });
    }
    return null;
  }
  return renderDiscounts();
}
DiscountRows.propTypes = {
  discounts: _propTypes.default.array
};
var _default = exports.default = DiscountRows;