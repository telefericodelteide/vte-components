'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _react = _interopRequireWildcard(require("react"));
var _propTypes = _interopRequireDefault(require("prop-types"));
var _context = _interopRequireDefault(require("../../context"));
var _reactI18next = require("react-i18next");
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function (e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != typeof e && "function" != typeof e) return { default: e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && {}.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n.default = e, t && t.set(e, n), n; }
function TotalRow(props) {
  const {
    i18n
  } = (0, _reactI18next.useTranslation)('main');
  const {
    cn
  } = (0, _react.useContext)(_context.default);
  const priceFormatter = new Intl.NumberFormat(i18n.language, {
    style: "currency",
    currency: "eur"
  });
  const totalAmount = props.totalAmount ? parseFloat(props.totalAmount) : 0;
  const roundTotalAmount = priceFormatter.format(parseFloat(totalAmount));
  const icon = cn("".concat(props.settingType, ".icon"));
  return /*#__PURE__*/_react.default.createElement("div", {
    className: cn("".concat(props.settingType, ".wrapper"))
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: cn("".concat(props.settingType, ".row"))
  }, /*#__PURE__*/_react.default.createElement("span", {
    className: cn("".concat(props.settingType, ".label"))
  }, props.label, ": "), /*#__PURE__*/_react.default.createElement("span", {
    className: cn("".concat(props.settingType, ".content"))
  }, roundTotalAmount, " "), icon ? /*#__PURE__*/_react.default.createElement("span", null, " ", /*#__PURE__*/_react.default.createElement("i", {
    className: icon
  })) : null));
}
TotalRow.propTypes = {
  totalAmount: _propTypes.default.number.isRequired,
  settingType: _propTypes.default.string,
  label: _propTypes.default.string
};
var _default = exports.default = TotalRow;