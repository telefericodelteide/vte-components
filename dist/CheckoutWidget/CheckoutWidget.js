"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _react = _interopRequireWildcard(require("react"));
var _reactI18next = require("react-i18next");
var _clone2 = _interopRequireDefault(require("lodash/clone"));
var _cloneDeep2 = _interopRequireDefault(require("lodash/cloneDeep"));
var _get2 = _interopRequireDefault(require("lodash/get"));
var _concat2 = _interopRequireDefault(require("lodash/concat"));
var _head2 = _interopRequireDefault(require("lodash/head"));
var _slice2 = _interopRequireDefault(require("lodash/slice"));
var _isArray2 = _interopRequireDefault(require("lodash/isArray"));
var _isEmpty2 = _interopRequireDefault(require("lodash/isEmpty"));
var _uniqBy2 = _interopRequireDefault(require("lodash/uniqBy"));
var _defaultValues = _interopRequireDefault(require("./config/defaultValues"));
var _Cart = _interopRequireDefault(require("./components/Cart"));
var _Total = _interopRequireDefault(require("./components/Total"));
var _Form = _interopRequireDefault(require("./components/Form"));
var _Alert = _interopRequireDefault(require("./components/Alert"));
var _Loading = _interopRequireDefault(require("../Generic/Loading"));
var _uikit = _interopRequireDefault(require("uikit"));
require("uikit/dist/css/uikit.min.css");
require("./checkout-widget.css");
var _lodashEs = require("lodash-es");
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function (e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != typeof e && "function" != typeof e) return { default: e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && {}.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n.default = e, t && t.set(e, n), n; }
function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
function _objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
function _defineProperty(e, r, t) { return (r = _toPropertyKey(r)) in e ? Object.defineProperty(e, r, { value: t, enumerable: !0, configurable: !0, writable: !0 }) : e[r] = t, e; }
function _toPropertyKey(t) { var i = _toPrimitive(t, "string"); return "symbol" == typeof i ? i : i + ""; }
function _toPrimitive(t, r) { if ("object" != typeof t || !t) return t; var e = t[Symbol.toPrimitive]; if (void 0 !== e) { var i = e.call(t, r || "default"); if ("object" != typeof i) return i; throw new TypeError("@@toPrimitive must return a primitive value."); } return ("string" === r ? String : Number)(t); }
/**
 * Main component.
 */
class CheckoutWidget extends _react.Component {
  constructor(props) {
    super(props);
    /**
     * Updates the state with the new form values.
     *
     * @param {object} values
     */
    _defineProperty(this, "onChange", values => {
      this.setState({
        values
      });
    });
    /**
     * Updates the value of promotional code.
     *
     * @param {string} promoCode
     */
    _defineProperty(this, "onChangePromoCode", promoCode => {
      let errors = !promoCode ? this.resetError("promo-code") : this.state.errors;
      this.setState({
        promoCode,
        errors
      });
    });
    /**
     * Deletes a product by id from server and updates cart with the new cart state.
     *
     * @param {string} id
     */
    _defineProperty(this, "onDelete", id => {
      if (id) {
        this.loading("cart");
        const itemRemove = this.state.cart.line_items.find(item => item.id === id);
        this.props.clientApi.cart.removeLineItem(id).then(cart => {
          let values = (0, _clone2.default)(this.state.values);
          values["participants"] = this.createParticipantsState(cart);
          if (this.props.trackingCallbacks && (0, _lodashEs.isFunction)(this.props.trackingCallbacks.onRemoveItemCallBack)) {
            this.props.trackingCallbacks.onRemoveItemCallBack(itemRemove);
          }
          this.setState({
            cart,
            values,
            errors: this.setError("cart", null),
            loading: this.loaded("cart")
          });
          this.checkEmptyCart(cart);
        }).catch(err => {
          if (err instanceof Error) {
            _uikit.default.modal.alert("Error: " + err.message);
            this.setState({
              errors: this.setError("cart", err.message),
              loading: this.loaded("cart")
            });
          } else {
            console.warn(err);
          }
        });
      } else {
        console.warn("This product has not ID");
      }
    });
    /**
     * Sends promotional code to server to apply discount.
     */
    _defineProperty(this, "onApplyPromoCode", () => {
      if (this.state.promoCode) {
        this.loading("cart");
        this.props.clientApi.cart.addDiscount(this.state.promoCode).then(cart => {
          this.setState({
            cart,
            errors: this.resetError("promo-code"),
            loading: this.loaded("cart")
          });
        }).catch(err => {
          if (err instanceof Error) {
            _uikit.default.modal.alert("Error: " + err.message);
            this.setState({
              errors: this.setError("promo-code", err.message),
              loading: this.loaded("cart")
            });
          } else {
            console.warn(err);
          }
        });
      }
    });
    /**
     * Sends to server the cart confirmation.
     *
     * @param {object} values
     */
    _defineProperty(this, "onSubmit", values => {
      const {
        t
      } = this.props;
      this.loading("cart");
      if (this.hasNoAvailableItems()) {
        this.setState({
          errors: this.setError("cart", t("error.noAvailableItem"))
        });
        if (this.props.trackingCallbacks && (0, _lodashEs.isFunction)(this.props.trackingCallbacks.onErrorCallBack)) {
          this.props.trackingCallbacks.onErrorCallBack(this.state.errors["cart"]);
        }
        return null;
      }
      if (this.state.cart.total_amount > 0 && !this.hasPayMethod(values)) {
        this.setState({
          errors: this.setError("cart", t("error.noPayMethod"))
        });
        if (this.props.trackingCallbacks && (0, _lodashEs.isFunction)(this.props.trackingCallbacks.onErrorCallBack)) {
          this.props.trackingCallbacks.onErrorCallBack(this.state.errors["cart"]);
        }
        return null;
      }
      let data = this.createValue(values);
      this.props.clientApi.cart.confirmCart(data).then(cart => {
        // updated cart
        if (this.props.trackingCallbacks && (0, _lodashEs.isFunction)(this.props.trackingCallbacks.onSubmitCallBack)) {
          this.props.trackingCallbacks.onSubmitCallBack(cart);
        }
        this.processPaymentRedirection(cart.payment_data);
      }).catch(err => {
        if (err instanceof Error) {
          _uikit.default.modal.alert("Error: " + err.message);
          this.setState({
            errors: this.setError("cart", err.message),
            loading: this.loaded("cart")
          });
        } else {
          console.warn(err);
        }
      });
    });
    /**
     * Un\enables the invoice form.
     */
    _defineProperty(this, "toggleInvoice", () => {
      this.setState({
        wantsInvoice: !this.state.wantsInvoice
      });
    });
    this.state = {
      loading: {
        cart: false,
        paymentGateways: false,
        route: false
      },
      errors: {},
      cart: null,
      values: {},
      promoCode: null,
      wantsInvoice: false,
      pickupData: null,
      paymentGateways: []
    };
    if (typeof window !== "undefined") {
      _uikit.default.container = ".uk-scope";
    }
  }

  /**
   * Loads the cart data from server and initializes form values and payment gateways.
   */
  componentDidMount() {
    this.loading(["cart", "paymentGateways", "route"]);
    this.loadCart(true);
    this.loadPaymentGateways(true);
  }

  /**
   * Loads Cart from server.
   *
   * @param {boolean} isLoading
   */
  loadCart(isLoading) {
    if (!isLoading) {
      this.loading("cart");
    }
    this.props.clientApi.cart.getCart().then(cart => {
      this.setState({
        cart,
        errors: this.resetError("cart"),
        values: this.createInitialValues(cart),
        conditions: this.processCartConditions(cart),
        loading: this.loaded("cart")
      });
      this.loadRoute(cart, true);
      this.checkEmptyCart(cart);
      if (this.props.trackingCallbacks && (0, _lodashEs.isFunction)(this.props.trackingCallbacks.onLoadCallBack)) {
        this.props.trackingCallbacks.onLoadCallBack(cart);
      }
    }).catch(err => {
      if (err instanceof Error) {
        _uikit.default.modal.alert("Error: " + err.message);
        this.setState({
          errors: this.setError("cart", err.message),
          loading: this.loaded("cart")
        });
      } else {
        console.warn(err);
      }
    });
  }

  /**
   * Check if a cart has no line items. In this case if a handler for this state is defined execute it.
   *
   * @param {*} cart
   */
  checkEmptyCart(cart) {
    if (cart.line_items.length === 0 && this.props.onEmptyCart) {
      this.props.onEmptyCart();
    }
  }
  loadRoute(cart, isLoading) {
    if (!isLoading) {
      this.loading("route");
    }

    // find product and date
    const lineItems = cart.line_items.filter(lineItem => {
      return lineItem.type === "booking_date" && lineItem.product.with_transport === true;
    });
    if (lineItems.length === 0) {
      this.setState({
        errors: this.resetError("route"),
        loading: this.loaded("route")
      });
      return;
    }
    const productId = lineItems[0].product.id;
    const date = Date.parse(lineItems[0].booking_date);
    this.props.clientApi.route.getPickupData(productId, date).then(route => {
      // process lodgins, zones and pickup points

      // pickup points
      let pickupPoints = (0, _uniqBy2.default)(route.pickup_configs.map(pickupConfig => pickupConfig.pickup_points.map(pickupPoint => {
        return _objectSpread(_objectSpread({}, pickupPoint), {}, {
          pickup_config_id: pickupConfig.id
        });
      })).flat().sort((a, b) => a.name.localeCompare(b.name)), "id");

      // zones
      const zones = (0, _uniqBy2.default)(pickupPoints.filter(pickupPoint => pickupPoint.zone != null).map(pickupPoint => pickupPoint.zone).sort((a, b) => a.name.localeCompare(b.name)), "id");

      // lodgins
      const lodgins = (0, _uniqBy2.default)(pickupPoints.filter(pickupPoint => Array.isArray(pickupPoint.lodgins) && pickupPoint.lodgins.length > 0).map(pickupPoint => pickupPoint.lodgins.map(lodgin => {
        return {
          id: lodgin.id,
          name: lodgin.name,
          pickup_point_id: pickupPoint.id
        };
      })).flat().sort((a, b) => a.name.localeCompare(b.name)), "id");
      pickupPoints = pickupPoints.map(pickupPoint => {
        return {
          id: pickupPoint.id,
          pickup_config_id: pickupPoint.pickup_config_id,
          name: pickupPoint.name,
          coordinates: pickupPoint.coordinates,
          zone_id: pickupPoint.zone != null ? pickupPoint.zone.id : null,
          order: pickupPoint.order,
          pickup_time: pickupPoint.pickup_time
        };
      });
      const pickupData = {
        lodgins: lodgins,
        zones: zones,
        pickupPoints: pickupPoints
      };
      this.setState({
        pickupData,
        errors: this.resetError("route"),
        loading: this.loaded("route")
      });
    }).catch(err => {
      if (err instanceof Error) {
        _uikit.default.modal.alert("Error: " + err.message);
        this.setState({
          errors: this.setError("route", err.message),
          loading: this.loaded("route")
        });
      } else {
        console.warn(err);
      }
    });
  }

  /**
   * Loads Payment gateways from server.
   *
   * @param {boolean} isLoading
   */
  loadPaymentGateways(isLoading) {
    if (!isLoading) {
      this.loading("paymentGateways");
    }
    this.props.clientApi.cart.getPaymentGateways().then(paymentGateways => {
      this.setState({
        paymentGateways,
        errors: this.resetError("paymentGateways"),
        loading: this.loaded("paymentGateways")
      });
    }).catch(err => {
      if (err instanceof Error) {
        _uikit.default.modal.alert("Error: " + err.message);
        this.setState({
          errors: this.setError("paymentGateways", err.message),
          loading: this.loaded("paymentGateways")
        });
      } else {
        console.warn(err);
      }
    });
  }

  /**
   * Updates state with the value of loading to true.
   *
   * @param {string|string[]} key
   */
  loading(key) {
    let loading = (0, _clone2.default)(this.state.loading);
    if ((0, _isArray2.default)(key)) {
      key.forEach(k => loading[k] = true);
    } else {
      loading[key] = true;
    }
    this.setState({
      loading
    });
  }

  /**
   * Sets the value of loading to false.
   *
   * @param {string} key
   * @returns {object} loading
   */
  loaded(key) {
    let loading = (0, _clone2.default)(this.state.loading);
    loading[key] = false;
    return loading;
  }

  /**
   * Sets the value of errors.
   *
   * @param {string} key
   * @param {string} msg
   * @returns {object} errors
   */
  setError(key, msg) {
    let errors = (0, _clone2.default)(this.state.errors);
    errors[key] = msg;
    return errors;
  }

  /**
   * Resets the value without errors.
   *
   * @param {string} key
   * @returns {object} errors
   */
  resetError(key) {
    return this.setError(key, null);
  }
  /**
   * Process payment data and redirects to the required url.
   *
   * @param {object} paymentData
   */
  processPaymentRedirection(paymentData) {
    if (paymentData.type === "GET") {
      window.location.replace(paymentData.payment_url);
    } else if (paymentData.type === "POST") {
      const form = document.createElement("form");
      form.id = "paymentForm";
      form.name = "paymentForm";
      form.method = "post";
      form.action = paymentData.payment_url;

      // create form fields
      Object.entries(paymentData.data).forEach(_ref => {
        let [fieldName, fieldValue] = _ref;
        const input = document.createElement("input");
        input.type = "hidden";
        input.name = fieldName;
        input.value = fieldValue;
        form.appendChild(input);
      });
      document.body.appendChild(form);
      form.submit();
    }
  }

  /**
   * Checks if value has pay method.
   *
   * @param {object} values
   */
  hasPayMethod(values) {
    return !(0, _isEmpty2.default)(values.payment_gateway_id);
  }

  /**
   * Checks if cart has no available items.
   */
  hasNoAvailableItems() {
    let lineItems = (0, _get2.default)(this.state.cart, "line_items");
    if (lineItems && (0, _isArray2.default)(lineItems)) {
      let noAvailable = false;
      lineItems.forEach(lineItem => {
        if (!lineItem.available) {
          noAvailable = true;
        }
      });
      return noAvailable;
    }
  }

  /**
   * Removes extra properties to send to server.
   *
   * @param {object} v
   */
  createValue(v) {
    let values = (0, _cloneDeep2.default)(v);
    delete values.client.no_diseases;
    delete values.client.accept_terms;

    // process pickup information
    if (this.state.pickupData != null) {
      values.client.pickup = {};
      Object.entries(values.pickup).forEach(_ref2 => {
        let [key, value] = _ref2;
        values.client.pickup[key] = value == "-1" ? null : value;
      });
    }
    delete values.pickup;

    // process participants
    if ((0, _isEmpty2.default)(values.participants)) {
      delete values.participants;
    }

    // remove invoice data if not required
    if (!this.state.wantsInvoice) {
      delete values.invoice;
    }
    return values;
  }
  processCartConditions(cart) {
    let result = [];
    const conditions = (0, _get2.default)(cart.checkout_config, "conditions");
    const termsCondition = (0, _head2.default)(conditions);
    if (termsCondition) {
      let condition = {
        key: "client.conditions." + (0, _get2.default)(termsCondition, "key"),
        title: (0, _get2.default)(termsCondition, "title"),
        info: (0, _get2.default)(termsCondition, "content"),
        required: (0, _get2.default)(termsCondition, "required")
      };
      if ((0, _get2.default)(cart.checkout_config, "terms").length > 0) {
        condition.modal = {
          title: (0, _get2.default)(cart.checkout_config, "terms.0.title"),
          content: /*#__PURE__*/_react.default.createElement("div", {
            dangerouslySetInnerHTML: {
              __html: (0, _get2.default)(cart.checkout_config, "terms.0.content")
            }
          })
        };
      }
      result.push(condition);
    }
    return (0, _concat2.default)(result, (0, _slice2.default)(conditions, 1).map((condition, index) => {
      return {
        key: (0, _get2.default)(condition, "key") ? "client.conditions." + (0, _get2.default)(condition, "key") : "client.conditions.extra_conditions_" + index,
        title: (0, _get2.default)(condition, "title"),
        info: (0, _get2.default)(condition, "content"),
        required: (0, _get2.default)(condition, "required", false)
      };
    }));
  }
  createInitialValues(cart) {
    return {
      payment_gateway_id: "",
      client: _defaultValues.default.clientValue,
      pickup: _defaultValues.default.pickupValue,
      invoice: _defaultValues.default.invoiceValue,
      participants: this.createParticipantsState(cart)
    };
  }
  createParticipantsState(cart) {
    const participants = (0, _get2.default)(cart, "client.participants");
    const lineItems = (0, _get2.default)(cart, "line_items");
    if ((0, _isArray2.default)(participants)) {
      return this.initParticipantsByLineitem(lineItems, participants);
    }
    return [];
  }
  initParticipantsByLineitem(lineItems, participants) {
    if ((0, _isArray2.default)(lineItems)) {
      return participants.map(participant => {
        const lineItem = lineItems.find(lineItem => lineItem.id === participant.line_item_id);
        let subindex = 0;
        return {
          line_item_id: lineItem.id,
          participants: lineItem.rates.map(rate => {
            let rateParticipants = [];
            for (let i = 0; i < rate.qty; i++, subindex++) {
              rateParticipants.push(_objectSpread(_objectSpread({}, Object.fromEntries(Object.keys(participant.form).map(field => [field, ""]))), {}, {
                rate: rate.rate.id + "-" + subindex
              }));
            }
            return rateParticipants;
          }).flat()
        };
      });
    }
    return [];
  }
  render() {
    let loading = this.state.loading;
    if (this.state.cart) {
      let cart = this.state.cart;
      let {
        client,
        line_items,
        discounts,
        total_amount
      } = cart;
      return /*#__PURE__*/_react.default.createElement("div", {
        id: "checkout-widget",
        className: "checkout-widget uk-scope"
      }, loading.cart ? /*#__PURE__*/_react.default.createElement(_Loading.default, null) : null, /*#__PURE__*/_react.default.createElement(_Alert.default, {
        errors: this.state.errors
      }), /*#__PURE__*/_react.default.createElement(_Cart.default, {
        key: "cart",
        content: cart,
        onDelete: this.onDelete
      }), /*#__PURE__*/_react.default.createElement(_Total.default, {
        key: "total",
        totalAmount: total_amount,
        discounts: discounts
      }), /*#__PURE__*/_react.default.createElement(_Form.default, {
        key: "form",
        client: client,
        lineItems: line_items,
        values: this.state.values,
        promoCode: this.state.promoCode,
        allowInvoice: this.props.allowInvoice,
        wantsInvoice: this.state.wantsInvoice,
        paymentGateways: this.state.paymentGateways,
        pickupData: this.state.pickupData,
        errors: this.state.errors,
        onChange: this.onChange,
        onSubmit: this.onSubmit,
        onApplyPromoCode: this.onApplyPromoCode,
        onChangePromoCode: this.onChangePromoCode,
        toggleInvoice: this.toggleInvoice,
        conditions: this.state.conditions,
        totalAmount: total_amount
      }));
    }
    if (loading.cart) {
      return /*#__PURE__*/_react.default.createElement(_Loading.default, null);
    }
    return null;
  }
}
var _default = exports.default = (0, _reactI18next.withTranslation)("main")(CheckoutWidget);