Web components for Volcano Teide booking engine

## Install

~~~~
yarn add @volcanoteide/vte-components
~~~~

In the project directory, you can run:

## Usage

Import the required component 

~~~~
import VolcanoBookingWidget from "@volcanoteide/VolcanoBookingWidget";
import VolcanoCheckoutWidget from "@volcanoteide/VolcanoCheckoutWidget";
import VolcanoOrderResultWidget from "@volcanoteide/VolcanoOrderResultWidget";
~~~~

Add the component with the rquired configuration properties

### Booking widget

~~~~
  <VolcanoBookingWidget 
    apiConfig={{
      protocol: 'https',
      host: [API_URL]',
      port: 443,
      strictSSL: true,
      timeout: 10000,
      site_key: [API_KEY]
    }}
    locale=[LOCALE]
    experienceId={[EXPERIENCE_ID]}
    defaultSelection={[DEFAULT_SELECTION]}
    onConfirm={[BOOKING_CONFIRM_HANDLER]}
  />
~~~~

| Option | Description | Required |
| --- | --- | --- |
| API_URL | Volcano API url | Yes |
| API_KEY | Provided Volcano API site KEY | Yes |
| LOCALE | Date and pricing will be formatted with this locale | Yes |
| EXPERIENCE | Experience identifier | Yes |
| DEFAULT_SELECTION | Default widget values: productId, date, session, rates | No |
| BOOKING_CONFIRM_HANDLER | This callback will be used after the booking is confirmed and added to the commerce cart | Yes |

### Checkout widget

~~~~
  <VolcanoCheckoutWidget 
    apiConfig={{
      protocol: 'https',
      host: [API_URL]',
      port: 443,
      strictSSL: true,
      timeout: 10000,
      site_key: [API_KEY]
    }}
    locale=[LOCALE]
    conditionsConfig={[CONDITIONS_CONFIG]}
    sohwPromoCheck={[SHOW_PROMO_CHECK]}
  />
~~~~

| Option | Description | Required |
| --- | --- | --- |
| API_URL | Volcano API url | Yes |
| API_KEY | Provided Volcano API site KEY | Yes |
| LOCALE | Date and pricing will be formatted with this locale | Yes |
| CONDITIONS_CONFIG | Legal terms checkboxes configuration | Yes |
| SHOW_PROMO_CHECK | Show or hide promotions check | No |

**Conditions config**

This configuration describes the information that will be sohwn in the legal terms acceptance block.

~~~~
const conditionsConfig = {
  title: "Legal terms checkbox label"
  info: "Information text for legal terms checkbox"
  modal: {
    title: "Title for the popup shown with the first click in legal terms checkbox"
    content: JSX with the popup content
  },
  extraConditions: [
    {
      id: "client.conditions.extra1_conditions",
      title: "Extra required condition checkbox title"
    },
    ...
    {
      id: "client.conditions.extraN_conditions",
      title: "Extra required condition checkbox title"
    }
  ]
}
~~~~

### Order result widget

~~~~
  <VolcanoOrderResultWidget 
    apiConfig={{
      protocol: 'https',
      host: [API_URL]',
      port: 443,
      strictSSL: true,
      timeout: 10000,
      site_key: [API_KEY]
    }}
    locale=[LOCALE]
  />
~~~~

| Option | Description | Required |
| --- | --- | --- |
| API_URL | Volcano API url | Yes |
| API_KEY | Provided Volcano API site KEY | Yes |
| LOCALE | Locale | Yes |

